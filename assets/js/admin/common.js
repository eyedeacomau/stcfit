head.ready(function () {
	(function ($) {
		if($('.user-search').length){
			$('#clients ul.nav li a').click(function(){
				var tab = $(this).data('tab-name');
				$('.user-search input#tab').val(tab);
			});
		}

		if($('.change_trainer').length){
			$('.change_trainer').on('change', function(){
				var user = $(this).data('user-id');
				var new_trainer = $(this).val();
				var tr = $(this).parent().parent();
				$.ajax({
					type: "POST",
					url: "/admin/users/change-trainer",
					data: "user_id="+user+"&trainer="+new_trainer,
					dataType: 'json',
					success: function(response){
						if(response.changed == true){
							if(tr.hasClass('red-row')){
								var count = parseInt($('#user_section a span.counter').html());
								var ele = $('#user_section a span.counter, #clients ul.nav li a span.counter');
								if(count > 1){
									var new_count = (count-1);
									ele.html(new_count);
								} else {
									ele.remove();
								}

								tr.removeClass('red-row');
							}
							alert('This user\'s trainer has successfully been changed to '+response.trainer);
						}
					}
				})
			});
		}


		if($('#ex_type').length){
			$('#ex_type').on('change', function(){
				$('#edit-area').html('');
				if($(this).val() == 2){ // Circuits
					$('#exercises').hide();
					$('#circuits').show();
					$('#exercise_type').val(2);
				} else { // Exercises
					$('#circuits').hide();
					$('#exercises').show();
					$('#exercise_type').val(1);
				}
			});
		}
		if($('a.exercise-option').length){
			$('a.exercise-option').on('click', function(e){
				e.preventDefault();
				var option = $(this).data('option');
				var type = $('#exercise_type').val();
				var type_name = '';

				if(type == 1){
					type_name = 'exercise';
					var ex = $('#exercises select');
				} else {
					type_name = 'circuit';
					var ex = $('#circuits select');
				}

				if(ex.val() == '' && option != 'add_new'){
					alert('You must select an exercise to edit or delete');
					return false;
				}

				if(option == 'delete') {
					var check = confirm('Are you sure you would like to delete this exercise?');
					if(check == true){
						$.ajax({
							type: "GET",
							url: '/admin/exercise-library/delete-exercise/'+ex.val(),
							dataType: 'json',
							success: function(response){
								$('#edit-area').html('');
								$("option[value='"+ex.val()+"']",ex).remove();
								ex.trigger("chosen:updated");
								alert('You have successfully deleted this exercise.');
							}
						});
					}
				} else {
					var id = ex.val();
					if(option == 'add_new'){
						id = '';
					}
					$.ajax({
						type: "GET",
						url: '/admin/exercise-library/add-edit-'+type_name+'/'+id,
						dataType: 'html',
						success: function(response){
							$('#edit-area').html('');
							$('#edit-area').html(response);
							$('.nice-file input[type=file]').on('change', function(e){
							var ext = $(this).val().split('.').pop().toLowerCase();
							if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
								e.preventDefault();
								alert('Invalid file type!');
							} else {
								var filename = $(this).val().replace(/.*(\/|\\)/, '');
								if(filename != ''){
									if(filename.length > 20){
										filename = filename.substr((filename.length-20),filename.length) + '...';
									}
									$(this).parent().attr('data-filename', filename);
								}
								if($(this).parent().attr('data-show-preview') == 'yes'){
									var reader = new FileReader();
									var parent = $(this).parent();
									var img_no = $(this).parent().attr('data-imageno');

									reader.onload = function (e) {
										$('#image_'+img_no).attr('src', e.target.result);
									}

									input = $(this);
									reader.readAsDataURL(input[0].files[0]);
								}
							}
						}).click(function(){
					        var img_no = $(this).parent().attr('data-imageno');
					        $('#image_'+img_no).attr('src', '/assets/images/no-image.jpg');
							$(this).parent().attr('data-filename', 'No file chosen.');
						});
						$("form.validate").each(function(){
							$(this).unbind().validate();
						});
						}
					});
				}
			});
		}

		if($('#management_type').length){
			$('#management_type').on('change', function(){
				$('.program').css('display', 'none');
				$('#'+$(this).val()).show();
			});
		}

		if($('#training_type').length){
			$('#training_type').on('change', function(){
				reload_phases($(this).val());
			});
		}

		if($('form.custom_validation').length){
			$('form.custom_validation').unbind().on('submit', function(e){
				$('input[type=text].table-input').each(function(){
					$(this).val($(this).val().replace(/\s/g, ''));
					var valid = $(this).data();
					var errors = '';
					if(typeof valid.required !== 'undefined' && $(this).val() == ''){
						errors += 'This field is required';
					} else if(typeof valid.intOnly !== 'undefined'){
						if(isNaN($(this).val())){
							errors += 'This field can only contain numbers' +"\n";
						}
					}
					if(errors != ''){
						e.preventDefault();
						$(this).tooltip('destroy').tooltip({
							title: errors
						});
						$(this).tooltip('show');
					}
				}).focus(function(){
					$(this).tooltip('destroy');
				});					
			});
		}

		$('#add_edit').on('hidden.bs.modal', function(){
			$('.modal-content',this).html('');
		});
	if($('#report_container').length){
		$('#report_container').highcharts({
			title: {
				text: null
			},
			chart:{
				height: 450
			},
			subtitle: {
				text: null
			},
			xAxis: {
				categories: cats,
				labels: {
					enabled: show_labels
				},
				lineColor: '#000000',
				lineWidth: 1,
				title : {
					text: graph_title,
					offset: 40,
					style: {
						'font-weight' : '700',
						'color' : '#000000'
					}
				}
			},
			exporting: {
				enabled: false
			},
			credits : {
				enabled: false   
			},
			yAxis: {
				title: {
					text: null
				},
				lineColor: '#000000',
				labels: {
					formatter: function() {
						return data_prefix + Highcharts.numberFormat(this.value, 0);
					}
				},
				lineWidth: 1,
				tickColor: '#000000',
				tickWidth: 1
			},
			legend: {
				enabled: false
			},
			series: [{
				name: graph_title,
				color: line_colour,
				data: points
			}],
			tooltip: {
				crosshairs: true,
				shadow: false,
				borderWidth: 0,
				shared:true,
				valuePrefix: data_prefix,
				valueDecimals: decimal_places
			},
			plotOptions: {
				line: {
					marker: {
						symbol: 'square'
					}
				}
			}
		});
	}
	})(jQuery);
});

function get_data(options){
	var return_data = [];
	$.ajax({
		type: "POST",
		url: '/admin/program-management/get-data/'+options.data_type,
		async: false,
		data: options.post_data,
		dataType: 'json',
		success: function(response){
			return_data = response;
		}
	});
	return return_data;
}

function bind_options(){
	$('.option').unbind().on('click', function(e){
		e.preventDefault();
		var option = $(this).data('option');
		var action = $(this).data('action');
		option = option.replace(/_/g, '-');
		var address = '/admin/program-management/'+option;
		var return_type = 'html';
		var session_identifier = '';
		var phase_number = '';
		var training_type = $('#training_type').val();
		var ask = true;
		var post_data = '';

		if($('#session').length){
			session_identifier = $('#session').val();
		}

		if($('#phase').length){
			phase_number = $('#phase').val();
		}

		switch(option) {
			case 'delete-phase':
				return_type = 'json';
				post_data = 'phase_number='+phase_number+'&training_type='+training_type+'&action='+action;
				ask = confirm('Are you sure you would like to delete this phase?');
				break;
			case 'delete-session':
				return_type = 'json';
				post_data = 'session_identifier='+session_identifier+'&action='+action;
				ask = confirm('Are you sure you would like to delete this session?');
				break;
			case 'add-edit-phase':
				if(action == 'edit' && (phase_number == '' || training_type == '')){
					ask = false;
					alert('Please select the phase you would like to edit.');
				} else {
					post_data = 'phase_number='+phase_number+'&training_type='+training_type+'&action='+action;
				}
				break;
			case 'add-edit-session':
				if(action == 'edit' && (phase_number == '' || training_type == '' || session_identifier == '')){
					ask = false;
					alert('Please select the session you would like to edit.');
				} else {
					post_data = 'session_identifier='+session_identifier+'&phase_number='+phase_number+'&training_type='+training_type+'&action='+action;
				}
				break;
		}

		if(ask == true){
			$.ajax({
				url: address,
				type: "POST",
				data: post_data,
				dataType: return_type,
				success: function(response){
					if(return_type != 'json'){
						$('#add_edit .modal-content').html(response);
						$('#add_edit').modal('show');
					} else {
						if(response.deleted == true){
							var type = response.change;
							var parent = $("#"+type+" option[value='"+response.remove_id+"']").parent();
							$("#"+type+" option[value='"+response.remove_id+"']").remove();
							parent.trigger('chosen:updated');
							$('.session_exercises').html('');
							if(type == 'phase'){
								$('#session_select_group').remove();
							}
						}
					}		
				}
			});
		}
	});
}

function reload_phases(training_type){
	$('#phase').parent().parent().remove(); // Remove chosen.
	$("#phase").unbind('change').remove(); // Unbind all events
	$('#session').parent().parent().remove(); // Remove chosen.
	$("#session").unbind('change').remove(); // Unbind all events
	$('.session_exercises').html('');

	var phase_options = {
		'data_type':'phases',
		'post_data': 'training_type='+training_type
	};
	var phases = get_data(phase_options);
	var phase_select = '\
		<div class="select-group" id="phase_select_group">\
			<div class="select-container">\
				<select name="training_phase" id="phase" class="chzn" data-placeholder="Select phase">\
					<option value=""></option>\
	';

	if(phases.length > 0){
		for(x=0;x<phases.length;x++){
			phase_select += '<option value="'+phases[x]['phase_number']+'">'+phases[x]['name']+'</option>'+"\n";
		}
	}
	phase_select += '\
				</select>\
			</div>\
			<div class="group-options">\
				<a href="#EDIT" class="option" data-option="add_edit_phase" data-action="edit"><span>E</span>Edit</a>\
				<a href="#DELETE" class="option" data-option="delete_phase" data-action="delete"><span><strong>-</strong></span>Delete</a>\
				<a href="#ADD-NEW" class="option" data-option="add_edit_phase" data-action="add"><span><strong>+</strong></span>Add New</a>\
			</div>\
		</div>\
	';
	$('.selection').append(phase_select);
	$('#phase').chosen({
		width: '100%'
	});
	bind_options();


	$('#phase').unbind('change').on('change', function(){
		reload_sessions($(this).val(), training_type);
	});
}

function reload_sessions(phase_number, training_type){
	$('#session').parent().parent().remove(); // Remove chosen.
	$("#session").unbind('change').remove(); // Unbind all events
	$('.session_exercises').html('');

	var session_options = {
		'data_type':'sessions',
		'post_data':'training_type='+training_type+'&phase_number='+phase_number
	};
	var sessions = get_data(session_options);
	var session_select = '\
		<div class="select-group" id="session_select_group">\
			<div class="select-container">\
				<select name="training_session" id="session" class="chzn" data-placeholder="Select session">\
					<option value=""></option>\
	';

	if(sessions.length > 0){
		for(x=0;x<sessions.length;x++){
			session_select += '<option value="'+sessions[x]['session_identifier']+'">'+sessions[x]['name']+'</option>'+"\n";
		}
	}
	session_select += '\
				</select>\
			</div>\
			<div class="group-options">\
				<a href="#EDIT" class="option" data-option="add_edit_session" data-action="edit"><span>E</span>Edit</a>\
				<a href="#DELETE" class="option" data-option="delete_session" data-action="delete"><span><strong>-</strong></span>Delete</a>\
				<a href="#ADD-NEW" class="option" data-option="add_edit_session" data-action="add"><span><strong>+</strong></span>Add New</a>\
			</div>\
		</div>\
	';
	$('.selection').append(session_select);
	$('#session').chosen({
		width: '100%'
	});
	bind_options();

	$('#session').unbind('change').on('change', function(){
		$('.session_exercises').html('');
		var session_identifier = $(this).val();
		$.ajax({
			type: "POST",
			url: '/admin/program-management/get-data/exercises',
			data: 'session_identifier='+session_identifier,
			dataType: 'html',
			success: function(response){
				$('.session_exercises').html(response);
			}
		});
	});
}