head.ready(function () {
	(function ($) {
		if($('#revenue_graph').length){
			$('#revenue_graph').highcharts({
				title: {
					text: null
				},
				chart:{
					height: 450
				},
				subtitle: {
					text: null
				},
				xAxis: {
					categories: cats,
					lineColor: '#000000',
					lineWidth: 1,
					title : {
						text: 'Total Revenue for the last 12 months',
						offset: 40,
						style: {
							'font-weight' : '700',
							'color' : '#000000'
						}
					}
				},
				exporting: {
					enabled: false
				},
				credits : {
					enabled: false   
				},
				yAxis: {
					title: {
						text: null
					},
					lineColor: '#000000',
					labels: {
						formatter: function() {
							return '$' + Highcharts.numberFormat(this.value, 0);
						}
					},
					lineWidth: 1,
					tickColor: '#000000',
					tickWidth: 1
				},
				legend: {
					enabled: false
				},
				series: [{
					name: 'Revenue',
					data: points
				}],
				tooltip: {
					crosshairs: true,
					shadow: false,
					borderWidth: 0,
					shared:true,
					valuePrefix: '$',
					valueDecimals: 2
				},
				plotOptions: {
					line: {
						marker: {
							symbol: 'square'
						}
					}
				}
			});
		}
		if(trainer_graphs.length){
			for(x=0;x<trainer_graphs.length;x++){
				generate_graph(trainer_graphs[x]);
			}
		}
	})(jQuery);
});

function generate_graph(options){
	if($('#'+options.graph_id).length){
		$('#'+options.graph_id).highcharts({
			chart: {
				type: 'column'
			},
			title: {
				text: null
			},
			xAxis: {
				categories: options.cats,
				title : {
						text: options.trainer_name+' '+options.trainer_age+' Month(s) old',
						offset: 40,
						style: {
							'color' : '#000000'
						}
					}
			},
			legend:{
				enabled: false
			},
			colors: [
				'#000000',
				'#ee4533',
				'#20a4f7'
			],
			credits: {
				enabled: false   
			},
			yAxis: {
				min: 0,
				title: {
					text: null
				}
			},
			tooltip: {
				headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
				pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
					'<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
				footerFormat: '</table>',
				shared: true,
				useHTML: true
			},
			plotOptions: {
				column: {
					pointPadding: 0,
					borderWidth: 0,
					pointWidth: 15
				}
			},
			series: options.plots
		});
	}
}