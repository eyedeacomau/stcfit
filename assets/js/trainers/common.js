head.ready(function () {
	(function ($) {
		if($('#programs .pagination').length){
			$('#programs .pagination li a').not('#programs .pagination li.active a').on('click', function(e){
				e.preventDefault();
				var page = $(this).data('pagenumber');
				var type = $(this).parent().parent().data('training-type');
				var phase_div = $(this).parent().parent().parent().parent().find('.phase_container');
				$('#programs .pagination li').each(function(){
					$(this).removeClass('active');
				});
				$(this).parent().addClass('active');
				$.ajax({
					type: "GET",
					url: "/trainers/programs/phase-page/"+page+"/"+type,
					success: function(response){
						phase_div.html(response);
					}
				});
			});
		}
		if($('#phase_popup').length){
			$('.view-phase').on('click', function(e){
				e.preventDefault();
				var phase_id = $(this).data('phaseid');
				var address = '/trainers/programs/view-phase/'+phase_id;
				$('#phase_popup').modal('show');
				$.ajax({
					type: "GET",
					url: address,
					beforeSend: function(){
						$('#phase_popup .loading-animation').show();
					},
					success: function(response){
						$('#phase_popup .loading-animation').hide();
						$('#phase_popup .modal-content .phase-content').show();
						$('#phase_popup .phase-content').html(response);
					}
				})
			});

			$('#phase_popup').on('hidden.bs.modal', function () {
				$('#phase_popup .modal-content .phase-content').html('').hide();
				$('#phase_popup .modal-content .loading-animation').show();
			});
		}
		if($(".panel").length){
			$(".panel").on('shown.bs.collapse', function(){			
				var trainer = $(this).data('trainer-id');
				var user = $(this).data('user-id');
				var address = '/trainers/messages/mark_as_read';
				var unread = parseInt($(this).data('unread'));
				var current_count = 0;
				var conversation_messages = $('a span.counter', this);
				$('.messages-section', this).scrollTop($('.messages-section', this).prop("scrollHeight"));
				if($('#message_count a span').length){
					current_count = parseInt($('#message_count a span.counter').html());
				}

				if(unread > 0){
					$.ajax({
					 	type: "POST",
					 	url: address,
					 	data: 'trainer_id='+trainer+'&user_id='+user,
					 	dataType: 'json',
					 	success: function(response){
					 		if(response.completed == true){
					 			conversation_messages.hide();
					 			if((current_count-unread) < 1){
					 				$('#message_count a span.counter').hide()
					 			}
					 			$('#message_count a span.counter').html(''+(current_count-unread));
					 		}
						}
					})
				}
			});
		}
	})(jQuery);
});