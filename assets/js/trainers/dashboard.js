head.ready(function () {
    (function ($) {
    	if($('#client_graph').length){
				$('#client_graph').highcharts({
					title: {
						text: null
					},
					chart:{
						height: 450
					},
					subtitle: {
						text: null
					},
					xAxis: {
						categories: cats,
						lineColor: '#000000',
            			lineWidth: 1,
            			title : {
            				text: 'Total Clients Lost Clients over the last 6 months',
            				offset: 40,
            				style: {
            					'font-weight' : '700',
            					'color' : '#000000'
            				}
            			}
					},
					exporting: {
						enabled: false
					},
					credits : {
						enabled: false   
					},
					yAxis: {
						title: {
							text: 'Total Number of Clients',
							offset: 60,
							style: {
								'font-weight' : '700'
							}
						},
						lineColor: '#000000',
            			lineWidth: 1,
						tickColor: '#000000',
						tickWidth: 1
					},
					legend: {
						enabled: false
					},
					series: points,
					tooltip: {
						crosshairs: true,
						shadow: false,
						borderWidth: 0,
						shared:true
					},
					plotOptions: {
						line: {
							marker: {
								symbol: 'square'
							}
						}
					}
				});
			}
			$('.resubscribe').on('click', function(e){
				e.preventDefault();
				$('#change_subscription input#user_id').val($(this).data('userid'));
				$('#change_subscription input#deletion_id').val($(this).data('deletionid'));
				$('#change_subscription span#user_name').html($(this).data('name'));
				$('#change_subscription').modal('show');
			});

			$('#change_subscription').on('hidden.bs.modal', function(){
				$('#change_subscription input#user_id').val('');
				$('#change_subscription input#deletion_id').val('');
				$('#change_subscription span#user_name').html('');
			});
			if($('#programs .pagination').length){
				alert('made it here');
				$('#programs .pagination li a').on('click', function(e){
					e.preventDefault();
					var page = $(this).data('pagenumber');
					var type = $(this).parent().parent().data('training-type');
				})
			}
    })(jQuery);
});