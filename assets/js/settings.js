head.ready(function () {
    (function ($) {
		if($('form#delete_card').length){
			$('form#delete_card').on('submit', function(e){
				var ask = confirm('Are you sure that you would like to delete this card?');
				if(ask == true){
					return true;
				} else {
					e.preventDefault();
            		return false;
				}
			});
		}
    })(jQuery);
});