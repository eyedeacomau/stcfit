head.ready(function () {
    (function ($) {
    	if($('#dash_graph').length){
			$('#dash_graph').highcharts({
				title: {
					text: null
				},
				subtitle: {
					text: null
				},
				xAxis: {
					categories: cats,
					lineColor: '#000000',
        			lineWidth: 1
				},
				exporting: {
					enabled: false
				},
				credits : {
					enabled: false
				},
				yAxis: {
					title: {
						text: null
					},
					lineColor: '#000000',
        			lineWidth: 1,
					tickColor: '#000000',
					tickWidth: 1
				},
				legend: {
					enabled: false
				},
				series: [{
					name: null,
					color: '#000000',
					data: points
				}],
				tooltip: {
					crosshairs: true,
					useHTML: true,
					backgroundColor: '#ffffff',
					shadow: false,
					borderWidth: 0,
					style: {
						padding: 0
					},
					headerFormat:'\
						<div class="data-wrapper">\
					',
					pointFormat: '\
						<div class="body-fat">Body Fat<span>{point.body_fat}%</span></div>\
					',
					footerFormat: '</div>',
				},
				plotOptions: {
					line: {
						marker: {
							symbol: 'square',
							states: {
								hover: {
									fillColor: '#ee4234',
									radius: 8
								}
							}
						}
					}
				}
			});
		}
		if($('#training_progress').length){
			$('#training_progress .graph').highcharts({
				chart: {
					plotBackgroundColor: null,
					plotBorderWidth: null,
					plotShadow: false
				},
				credits: false,
				exporting: {
					enabled:false
				},
				title: {
					text: null
				},
				tooltip: {
					enabled: false
				},
				plotOptions: {
					pie: {
						size: '100%',
						borderWidth: 0,
						animation: false,
						allowPointSelect: false,
						cursor: 'default',
							states: {
								hover: {
								enabled:false
							}
						},
						dataLabels: {
							enabled: false,
						}
					}
				},
				series: [{
					type: 'pie',
					data: training // On dashboard/index.php
				}]
			});
		}

		// if($('#food_progress').length){
		// 	$('#food_progress .graph').highcharts({
		// 		chart: {
		// 			plotBackgroundColor: null,
		// 			plotBorderWidth: null,
		// 			plotShadow: false
		// 		},
		// 		credits: false,
		// 		exporting: {
		// 			enabled:false
		// 		},
		// 		title: {
		// 			text: null
		// 		},
		// 		tooltip: {
		// 			enabled: false
		// 		},
		// 		plotOptions: {
		// 			pie: {
		// 				borderWidth: 0,
		// 				animation: false,
		// 				allowPointSelect: false,
		// 				cursor: 'default',
		// 					states: {
		// 						hover: {
		// 						enabled:false
		// 					}
		// 				},
		// 				dataLabels: {
		// 					enabled: false,
		// 				}
		// 			}
		// 		},
		// 		series: [{
		// 			type: 'pie',
		// 			data: food // On dashboard/index.php
		// 		}]
		// 	});
		// }
		/* Welcome Modal */
		if($('#welcome_message').length != 0) {
			$('#welcome_message').on('show.bs.modal', function (e) {
				$('#welcome_message input.dont_show').change(function(e) {
					var dont_show = $(this).is(':checked') === true ? 1 : 0;
					$.ajax({
						type: "POST",
						url: '/dashboard/update_message/',
						data: {
							'dont_show': dont_show
						},
						success: function(response){
							console.debug(response);
						}
					});
				});
			}).modal('show');
		}
    })(jQuery);
});
