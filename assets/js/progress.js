head.ready(function () {
    (function ($) {
    	if($('#progress_graph').length){
				$('#progress_graph').highcharts({
					title: {
						text: null
					},
					subtitle: {
						text: null
					},
					xAxis: {
						categories: cats,
						lineColor: '#000000',
            			lineWidth: 1
					},
					exporting: {
						enabled: false
					},
					credits : {
						enabled: false   
					},
					yAxis: {
						title: {
							text: 'Sum of Girth Measurements',
							offset: 80,
							style: {"font-weight": "700"}
						},
						lineColor: '#000000',
						lineWidth: 1,
						tickColor: '#000000',
						tickWidth: 1
					},
					legend: {
						enabled: false
					},
					series: [{
						name: null,
						color: '#000000',
						data: points
					}],
					tooltip: {
						crosshairs: true,
						useHTML: true,
						shared: true,
						followPointer: true,
						shape: 'square',
						backgroundColor: '#ffffff',
						shadow: false,
						borderWidth: 0,
						style: {
							padding: 0
						},
						headerFormat:'\
							<div class="data-wrapper">\
						',
						pointFormat: '\
							<table border="0" class="graph-table">\
								<tr>\
									<th>Area</th>\
									<th>Original</th>\
									<th>At this Date</th>\
								</tr>\
								<tr>\
									<td class="type"><span>1.</span> Shoulders</td>\
									<td>{point.original_shoulders}cm</td>\
									<td>{point.shoulders}cm</td>\
								</tr>\
								<tr>\
									<td class="type"><span>2.</span> Neck</td>\
									<td>{point.original_neck}cm</td>\
									<td>{point.neck}cm</td>\
								</tr>\
								<tr>\
									<td class="type"><span>3.</span> Chest</td>\
									<td>{point.original_chest}cm</td>\
									<td>{point.chest}cm</td>\
								</tr>\
								<tr>\
									<td class="type"><span>4.</span> Waist</td>\
									<td>{point.original_waist}cm</td>\
									<td>{point.waist}cm</td>\
								</tr>\
								<tr>\
									<td class="type"><span>5.</span> Bicep</td>\
									<td>{point.original_bicep}cm</td>\
									<td>{point.bicep}cm</td>\
								</tr>\
								<tr>\
									<td class="type"><span>6.</span> Hips</td>\
									<td>{point.original_hips}cm</td>\
									<td>{point.hips}cm</td>\
								</tr>\
								<tr>\
									<td class="type"><span>7.</span> Quads</td>\
									<td>{point.original_quads}cm</td>\
									<td>{point.quads}cm</td>\
								</tr>\
								<tr>\
									<td class="type"><span>8.</span> Calf</td>\
									<td>{point.original_calf}cm</td>\
									<td>{point.calf}cm</td>\
								</tr>\
								<tr>\
									<td class="type"><span>9.</span> Wrist</td>\
									<td>{point.original_wrist}cm</td>\
									<td>{point.wrist}cm</td>\
								</tr>\
								<tr>\
									<td class="type"><span>10.</span> Forearm</td>\
									<td>{point.original_forearm}cm</td>\
									<td>{point.forearm}cm</td>\
								</tr>\
							</table>\
							<div class="body-fat">Body Fat<span>{point.body_fat}%</span></div>\
						',
						footerFormat: '</div>',
					},
					plotOptions: {
						line: {
							marker: {
								symbol: 'square',
								states: {
									hover: {
										fillColor: '#ee4234',
										radius: 8
									}
								}
							}
						}
					}
				});
			}

		if($('#main_image').length && $('ul#thumbnails').length){
			$('ul#thumbnails li a.change-image').on('click', function(e){
				e.preventDefault();
				var new_image = $(this).data('image');
				var image_id = $(this).data('imageid');
				var date_taken = $(this).data('date-taken');
				var main_images = $('ul#main_images');
				$('li', main_images).each(function(){
					$(this).hide();
				});

				if(!$('li#image_'+image_id,main_images).length){
					main_images.append('\
						<li id="image_'+image_id+'" style="display:none;">\
							<div class="date-taken">'+date_taken+'</div>\
							<img src="'+new_image+'" />\
						</li>\
					');
					$('li#image_'+image_id+' img', main_images).load(function(){
						if($('li#image_'+image_id,main_images).length){
							$('li#image_'+image_id,main_images).fadeIn(400);
						}
					});
				} else {
					$('li#image_'+image_id,main_images).fadeIn(400);
				}
			});
		}
    })(jQuery);
});