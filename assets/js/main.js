head.ready(function () {
	(function ($) {
		// Validation
		if($('.chzn').length){
			$('.chzn').chosen({
				width:"100%",
				height:"34px"
			});
		}
		$.validator.setDefaults({
			debug: false,
			ignore: ':hidden:not(select)',
			highlight: function(element) {
				$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
			},
			unhighlight: function(element) {
				$(element).tooltip('destroy').closest('.form-group').removeClass('has-error');
			},
			success: function(label, element) {
				$(element).closest('.form-group').addClass('has-success');
			},
			errorElement: 'span',
			errorClass: 'help-block',
			errorPlacement: function(error, element) {
				var el = element;
			   	el.tooltip('destroy').tooltip({
					title: error.text()
				});
				el.tooltip('show');
				var input = el;
			}
		});
		
		jQuery.validator.addMethod('expiry', function(value, element) {
			return this.optional(element) || /^(0[1-9]|1[0-2])\/[1-9]\d{1}$/.test(value);
		}, 'A valid expiry date is in the format MM/YY');
		
		$("form.validate").each(function(){
			$(this).validate();
		});
		$("label.placeholder").inFieldLabels();

		if($('#popup_message').length){
			$('#popup_message').modal('show');
		}

		if($('#payment_method').length){
			change_price();
			$('#payment_method').on('change', function(){
				change_price();
			});
		}

		if($('#exercise_popup').length){
			$('.get_exercise').on('click', function(e){
				e.preventDefault();
				var exercise_id = $(this).data('exerciseid');
				var address = '/training/exercise/'+exercise_id;
				$('#exercise_popup').modal('show');

				$('#exercise_popup .modal-content .exercise-content').load(address, function(){
					$('a.shift').bind('click', function(){
						$('ul.large-images li').css('display', 'none');
						$('ul.large-images li#loading-animation').show();
						var type = $(this).data('type');
						var actual = $(this).data('actual');
						var number = $(this).data('number');

						if(!$('ul.large-images li#'+type+'_'+number).length){
							if(type == 'youtube'){
								var width = $(this).data('width');
								var height = $(this).data('height');
								$('ul.large-images').append('\
									<li style="display:none;" id="'+type+'_'+number+'">\
										<iframe width="'+width+'" height="'+height+'" src="'+actual+'" frameborder="0" allowfullscreen></iframe>\
									</li>\
								');

								$('ul.large-images li#loading-animation').hide();
								$('ul.large-images li#'+type+'_'+number).fadeIn(400);
							} else if(type == 'image'){
								$('ul.large-images').append('\
									<li style="display:none;" id="'+type+'_'+number+'">\
										<img src="'+actual+'" />\
									</li>\
								');
								$('ul.large-images li#'+type+'_'+number+' img').load(function(){
									if($('ul.large-images li#'+type+'_'+number).length){
										$('ul.large-images li#loading-animation').hide();
										$('ul.large-images li#'+type+'_'+number).fadeIn(400);
									}
								});
							}
						} else {
							$('ul.large-images li#loading-animation').hide();
							$('ul.large-images li#'+type+'_'+number).fadeIn(400);
						}
					});
					$('#exercise_popup .modal-content .loading-animation').hide();
					$('#exercise_popup .modal-content .exercise-content').show();
				});
			});

			$('#exercise_popup').on('hidden.bs.modal', function () {
				$('#exercise_popup .modal-content .exercise-content').html('').hide();
				$('#exercise_popup .modal-content .loading-animation').show();
			});
		}

		$('.nice-file input[type=file]').on('change', function(e){
			var ext = $(this).val().split('.').pop().toLowerCase();
			if($.inArray(ext, ['gif','png','jpg','jpeg', 'pdf']) == -1) {
				e.preventDefault();
				alert('Invalid file type!');
			} else {
				var filename = $(this).val().replace(/.*(\/|\\)/, '');
				if(filename != ''){
					if(filename.length > 20){
						filename = filename.substr((filename.length-20),filename.length) + '...';
					}
					$(this).parent().attr('data-filename', filename);
				}
				if($(this).parent().attr('data-show-preview') == 'yes'){
					var reader = new FileReader();
					var parent = $(this).parent();
					var img_no = $(this).parent().attr('data-imageno');

					reader.onload = function (e) {
						$('#image_'+img_no).attr('src', e.target.result);
					}

					input = $(this);
					reader.readAsDataURL(input[0].files[0]);
				}
			}
		}).click(function(){
            var img_no = $(this).parent().attr('data-imageno');
            $('#image_'+img_no).attr('src', '/assets/images/no-image.jpg');
			$(this).parent().attr('data-filename', 'No file chosen.');
		});
		if($('.datepicker').length){
			$('.datepicker').each(function(){
				var picker = $(this);
				var nowTemp = new Date();
				var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
				picker.attr('readonly', 'readonly').datepicker({
					startDate: "dateToday",
					format: 'dd/mm/yyyy',
					onRender: function(date) {
						if(picker.attr('data-min-today')){
							return date.valueOf() < now.valueOf() ? 'disabled' : '';
						}
					}
				}).on('changeDate', function(){
					$(this).parent().parent().find('label.placeholder').hide();
				}); // initialising the bootstrap datepicker plugin
			});

			$('.date .input-group-addon').on('click', function(){
				$(this).parent().find('input.datepicker').focus();
			});
		}

		if($('select#state').length && $('select#country').length){
			$('select#country').on('change', function(){
				if($(this).val() == 2){ // International
					$('select#state').append('<option value="N/A"> --- Not Applicable --- </option>');
					$('select#state').val('N/A');
				} else { // Australia
					$('select#state option[value="N/A"]').remove();
					$('select#state').val('');
				}
			});
		}

		if($('.ask-first').length){
			$('.ask-first').on('click', function(e){
				var ask = confirm($(this).data('message'));
				if(ask == false){
					e.preventDefault();
				} else {
					return true;
				}
			});
		}

		if($('.print-button').length && $('#print_content').length){
			console.log('Made it here!');
			$('.print-button').on('click', function(e){
				e.preventDefault();
				var w = window.open();
				var content = $('#print_content').html();
				var html = '\
					<!DOCTYPE HTML>\
					<html class="print-version">\
					<head>\
						<link rel="stylesheet" href="/assets/css/plugins/bootstrap.min.css">\
						<link rel="stylesheet" href="/assets/css/main.css">\
						<link rel="stylesheet" href="/assets/css/training.css">\
						<link rel="stylesheet" href="/assets/css/dashboard.css">\
						<style>\
							input, textarea {\
								display:none;\
							}\
							td {\
								width:51px;\
								height:32px;\
							}\
							.btn.btn-primary {\
								display:none;\
							}\
							.sprite-questionmark {\
								display:none;\
							}\
						</style>\
					</head>\
					<body>\
						'+content+'\
					</body>\
					</html>\
				';
				w.document.write(html);
			});
		}
		if($('#payment_form').length){
			$('#payment_form').on('submit', function(){
				$('#payment_submit', this).val('Working...');
				if($(this).valid() == false){
					$('#payment_submit', this).val('Complete signup now!');
				}
			});
		}
	})(jQuery);
});

function change_price(){
	var to = $('#payment_method').val();
	$('h1.change-price').html('$'+methods[to]['amount'].toFixed(2)+' <span class="aud">AUD</span>');
	$('p.per-what').html(methods[to]['title']);
}