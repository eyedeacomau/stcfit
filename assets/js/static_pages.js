head.ready(function () {
    (function ($) {
		$('a.image-area').on('click', function(e){
			e.preventDefault();
			var img = $(this).data('large-img');
			$('.loading-animation').show();
			$('#picture_view').modal('show');
			$('#picture_view .img-content img').attr('src', img).load(function(){
				$('.loading-animation').hide();
				$('.img-content', this).html('<img />');
				$('.img-content').show();
			});
		});
		$('#picture_view').on('hidden.bs.modal', function(){
			$('.img-content', this).html('<img />');
			$('.img-content').hide();
		});
        var pos = $("#quotes").position();
        $(window).scroll(function() {
            var windowpos = $(window).scrollTop();
            if (windowpos >= pos.top) {
                $("#quotes").addClass("stick");
            } else {
                $("#quotes").removeClass("stick");
            }
        });
    })(jQuery);
});
