head.ready(function () {
    (function ($) {
		if($("#food_goal_container").length && $("input[name='selected_plan']").length){
			$("input[name='selected_plan']").on('change', function(){
				var plan = $(this).val();
				$("#food_goal_container").html('');
				$.ajax({
					url: '/food/get-goals/'+plan,
					method: 'GET',
					data: '',
					dataType: 'html',
					success: function(response){
						$("#food_goal_container").html(response);
						$('html, body').animate({
							scrollTop: $("#food_goal_container").offset().top
						}, 1000);
					}
				});
			});
		}

		if($('#goal_number').length){
			var goal_number = $("#goal_number").val();
			var plan_id = $('#plan_id').val();
			previous_entries(goal_number, plan_id);
		}

		if($("select#goal_number").length && $('.goal_group_container').length){
			$('#previous_food_entries').html('');
			$("select#goal_number").on('change', function(){
				$('#previous_food_entries').html('');
				var show = $(this).val();
				$('.goal_group_container').each(function(){
					$(this).hide();
				}).promise().done(function(){
					$('#goals_'+show).show();
				});
				previous_entries(show, plan_id);
			});
		}
    })(jQuery);
});

function previous_entries(goal_number, plan_id){
	var return_data = '';
	$.ajax({
		url: '/food/previous-entries/'+plan_id+'/'+goal_number,
		method: 'GET',
		async: true,
		data: '',
		dataType: 'html',
		success: function(response){
			$('#previous_food_entries').html(response);
		}
	});
}