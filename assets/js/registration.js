head.ready(function () {
    (function ($) {
		if($('#trainer_code').length){
			$('#trainer_code').on('blur', function(){
				var code = $(this).val();
				$.ajax({
					url: '/registration/check_trainer',
					method: "POST",
					data: 'trainer_code='+code,
					dataType: 'json',
					success: function(response){
						var html_content = '';
						if(response.found == true){
							html_content = '\
								<span class="sprite sprite-tick"></span>'+response.data.first_name+' '+response.data.last_name+'\
							';
						} else{
							html_content = '\
								<span class="sprite sprite-cross"></span>\
							';
						}
						$('#trainer-name').html(html_content);
					}
				});
			});
		}

        $("#submit_pre_questions").on('click',function(){
            if($("#preex input[value=2]:checked").length > 0){
                $("#medical_text").text('It looks like you have a risk factor that may impact your ability to train, STCfit recommends you seek medical advice before continuing with this program.');
            } else {
                $("#medical_text").text('You\'re good to go, however STCfit always recommends a visit to a medical professional before you undergo any nutrition or training program.');
            }
    		$("#medical_modal").modal();
    		$("#medical_ok").on('click',function(){
    			if($("#medical_check").is(":checked")){
    				$("#preex").submit();
                    $("#medical_modal").modal('hide');
    			};
    		});
    	})

    })(jQuery);
});
