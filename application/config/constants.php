<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');

define('SITE_NAME', 'STC Fit');

define('JS_PATH', '/assets/js/');
define('CSS_PATH', '/assets/css/');

define('EXERCISE_IMG_PATH', '/images/exercises/'); // Where all the images showing how an exercise is performed are stored.

define('PHASES_PATH', '/phases/'); // This is the path to where the text files containing the user's phase exercises are kept.
define('FOOD_PLAN_DOC_PATH', '/documents/food_plans/');

define('PROGRESS_IMG_PATH', '/images/user/progress/'); // Path where user progress images are stored.

define('NO_REPLY_EMAIL', 'no-reply@stcfit.com');
define('FOOTER_EMAIL', 'support@stcfit.com'); // This email is at the bottom of most emails.
define('SUPPORT_EMAIL', 'support@stcfit.com'); // This email is at the bottom of most emails.
define('ADMIN_EMAIL', 'support@stcfit.com');

define('SEND_EMAILS', true); // This flag determines whether to send emails or dump them in the browser.

define('REGISTRATION_STEPS', 4); // This does not include the step where a user gives their name and email.

define('MAX_CHILDREN', 6); // Change this value for the registration and edit profile as needed. 6 Was a start point

define('MIN_AGE', 18);
define('MAX_AGE', 100);

define('ENTRIES_PER_SESSION', 4);

define('TRAINER_REMINDERS_ENTRIES', 3);
define('DASH_DATA_PERIOD', 7257600); // 12 weeks
define('MINIMUM_SUBSCRIPTION', 7257600); // 12 weeks
define('TRAINER_DASH_PERIOD', 6); // 6 months
define('TRAINER_PREFIX', 'STC0');
define('PHASES_PER_PAGE', 10); // This indicates how many results to show per page, on the trainers/programs page

define('REVENUE_PERIOD', 12); // 12 months of revenue data in admin
define('TRAINER_DATA_PERIOD', 3); // 3 months of trainer data in admin

/* Payment Constants */
define('EWAY_FORCE_SUCCESS', false);
// define('EWAY_CUSTOMERID', '91230558');
// define('EWAY_USERNAME', 'support@eyedea.com.au.sand');
// define('EWAY_PASSWORD', 'Pan12cakes3');

define('EWAY_CUSTOMERID', '14341009');
define('EWAY_USERNAME', 'michael.scott@stcfit.com');
define('EWAY_PASSWORD', 'Ew$yf1tneSS');

define("EWAY_TEST_GATEWAY","https://www.eway.com.au/gateway/ManagedPaymentService/test/managedCreditCardPayment.asmx");
define("EWAY_LIVE_GATEWAY","https://www.eway.com.au/gateway/ManagedPaymentService/managedCreditCardPayment.asmx");

define('EWAY_namespace','https://www.eway.com.au/gateway/managedpayment');
define('EWAY_endpoint','https://www.eway.com.au/gateway/managedpayment/');
define('EWAY_action_create_user','CreateCustomer');
define('EWAY_action_update_user','UpdateCustomer');
define('EWAY_action_bill_user','ProcessPayment');
define('EWAY_action_get_user','QueryCustomer');
define('EWAY_TEST_AMOUNT',99.00); // change the cents portion to get the test return code from eway
define('EWAY_TEST_REF','STC Fit');
/* End Payment Constants */

define('CRON_URL', 'http://www.stcfit.com');

/* End of file constants.php */
/* Location: ./application/config/constants.php */
