<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "index";
$route['404_override'] = 'index/show_404';
$route['4oh4'] = 'index/show_404';

$route['registration/pre-exercise'] = 'registration/pre_exercise';
$route['registration/your-details'] = 'registration/your_details';
$route['registration/tailor-program'] = 'registration/tailor_program';

$route['medical-failed'] = 'index/index'; // This is to track failed medicals for analytics

$route['login'] = 'auth/login';
$route['logout'] = 'auth/logout';
$route['trial'] = 'auth/trial';
$route['trial-success'] = 'static_pages/trial_success';
$route['forgot-password'] = 'auth/forgot_password';

$route['settings/my-profile'] = 'settings/my_profile';
$route['settings/my-plan'] = 'settings/my_plan';
$route['settings/view-invoice/(.*)'] = 'settings/view_invoice/$1';
$route['settings/delete-account'] = 'settings/delete_account';
$route['settings/reactivate-account'] = 'settings/reactivate_account';
$route['dashboard/(.*)'] = 'dashboard/index/$1';

$route['images/exercises/(.*)'] = 'index/image_loader/'.urlencode(EXERCISE_IMG_PATH).'/$1';

$route['images/user/progress/(.*)'] = 'index/image_loader/'.urlencode(PROGRESS_IMG_PATH).'/$1';

$route['settings/billing/(.*)'] = 'settings/billing/$1';

$route['food/change-plan'] = 'food/change_plan';

$route['food/get-goals/(.*)'] = 'food/get_goals/$1';

$route['food/previous-entries/(.*)/(.*)'] = 'food/previous_entries/$1/$2';

$route['progress/new-entry'] = 'progress/new_entry';

$route['progress/progress-photos'] = 'progress/progress_photos';



$route['cancel-subscription/(.*)/(.*)'] = 'index/cancel_subscription/$1/$2';
$route['continue-registration/(.*)/(.*)'] = 'index/continue_registration/$1/$2';

$route['trainers/client-login/(.*)'] = 'trainers/index/client_login/$1';

$route['trainers/suspend-user/(.*)'] = 'trainers/index/suspend_user/$1';
$route['trainers/reactivate-user/(.*)'] = 'trainers/index/reactivate_user/$1';

$route['trainers/programs/phase-page/(.*)/(.*)'] = 'trainers/programs/phase_page/$1/$2';

$route['trainers/programs/view-phase/(.*)'] = 'trainers/programs/view_phase/$1';

$route['admin/program-management'] = 'admin/program_management';

$route['admin/exercise-library'] = 'admin/exercise_library';
$route['admin/exercise-library/add-edit-exercise'] = 'admin/exercise_library/add_edit_exercise';
$route['admin/exercise-library/add-edit-circuit'] = 'admin/exercise_library/add_edit_circuit';

$route['admin/exercise-library/add-edit-exercise/(.*)'] = 'admin/exercise_library/add_edit_exercise/$1';
$route['admin/exercise-library/add-edit-circuit/(.*)'] = 'admin/exercise_library/add_edit_circuit/$1';
$route['admin/exercise-library/delete-exercise/(.*)'] = 'admin/exercise_library/delete_exercise/$1';

$route['admin/users'] = 'admin/user_management';

$route['admin/test'] = 'admin/index/test';
$route['admin/trainer-login/(.*)'] = 'admin/index/trainer_login/$1';
$route['admin/user-login/(.*)'] = 'admin/index/user_login/$1';

$route['admin/users/change-trainer'] = 'admin/user_management/change_trainer';

$route['admin/suspend-user/(.*)'] = 'admin/index/suspend_user/$1';
$route['admin/reactivate-user/(.*)'] = 'admin/index/reactivate_user/$1';
$route['admin/paid-user/(.*)'] = 'admin/index/paid_user/$1';
$route['admin/override-medical/(.*)'] = 'admin/index/override_medical/$1';

$route['admin/make-user-paid/(.*)'] = 'admin/index/make_user_paid/$1';
$route['admin/make-user-free/(.*)'] = 'admin/index/make_user_free/$1';

$route['admin/program-management/get-data/(.*)'] = 'admin/program_management/get_data/$1';
$route['admin/program-management/add-edit-phase'] = 'admin/program_management/add_edit_phase';
$route['admin/program-management/add-edit-session'] = 'admin/program_management/add_edit_session';

$route['admin/program-management/delete-phase'] = 'admin/program_management/delete_phase';
$route['admin/program-management/delete-session'] = 'admin/program_management/delete_session';

/* Static pages */
$route['terms-and-conditions'] = 'static_pages/terms_and_conditions';
$route['how-it-works'] = 'static_pages/how_it_works';
$route['weight-loss'] = 'static_pages/weight_loss';
$route['pricing'] = 'static_pages/pricing';
$route['results'] = 'static_pages/results';
$route['contact-us'] = 'static_pages/contact_us';
$route['contact-success'] = 'static_pages/contact_success';

/* End of file routes.php */
/* Location: ./application/config/routes.php */
