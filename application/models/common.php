<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Common is where any general functions go that don't align with any of the standard
 */ 
class Common extends CI_Model {
	function __construct() {	
		parent::__construct();
	}

	function display($options=array()) {
		$default_options = array(
			'title' => SITE_NAME,
			'title_prefix' => SITE_NAME,
			'title_suffix' => '',
			'extra_js' => array(), // Array of filenames of extra JS to add to the controller's pages
			'extra_css' => array(), // Any extra CSS files for this controller
			'extra_head' => array(), // Any other misc head tags
			'template_data' => array(),
			'page_header' => '',
			'template' => 'templates/page',
			'view' => 'frontend/home',
			'fields' => array(),
			'content' => array(),
			'return_content' => false
		);

		extract(array_merge($default_options,$options));

		if(!empty($extra_css) && !is_array($extra_css)){
			$extra_css = array($extra_css);
		}

		if(!empty($extra_js) && !is_array($extra_js)){
			$extra_js = array($extra_js);
		}

		if(!empty($extra_head) && !is_array($extra_head)){
			$extra_head = array($extra_head);
		}

		$this->load->helper('path');

		$js = set_realpath('.'.JS_PATH,true);
		if($js) {
			$controller = $this->router->fetch_class();
			if(file_exists($js.$controller.'.js')) $include_js = JS_PATH.$controller.'.js';
		}

		$css = set_realpath('.'.CSS_PATH,true);
		
		if($css) {
			$controller = $this->router->fetch_class();
			if(file_exists($css.$controller.'.css')) $include_css = CSS_PATH.$controller.'.css';
		}
		
		$data = array(
			'title' => SITE_NAME.' :: '.$title,
			'include_js' => (isset($include_js) ? $include_js : ''),
			'include_css' => (isset($include_css) ? $include_css : ''),
			'extra_js' => (count($extra_js) ? $extra_js : ''),
			'extra_css' => (count($extra_css) ? $extra_css : ''),
			'extra_head' => (count($extra_head) ? $extra_head : ''),
			'template_data' => $template_data,
			'page_header' => $page_header,
			'content' => $this->load->view($view, $content, true)
		);

		if($return_content == true){
			return $this->load->view($template,$data, true);
		} else {
			$this->load->view($template,$data);
		}
	}

	function split_name($fullname) {  // Puts fullname into [0], prefixes into [1], firstname in [2], lastname in [3], suffixes in [4]
		$name = array();
		preg_match('#^(\w+\.)?\s*([\'\’\w]+)\s+([\'\’\w]+)\s*(\w+\.?)?$#', $fullname, $name);
		if(empty($name)) $name = array('', '', $fullname, '', '');	// at least return something kind of valid
		return $name;
	}

	public function send_email($options = array()){
		$default_options = array(
			'view' => '',
			'email_to' => '',
			'subject' => '',
			'data' => array(),
			'template' => 'templates/email'
		);

		$options = array_merge($default_options, $options);
		if(SEND_EMAILS == false){
			$email = $this->display(array(
				'template' => $options['template'],
				'title' => 'Email - '.$options['subject'],
				'view' => 'email/'.$options['view'] . '-html',
				'return_content' => true,
				'template_data' => array(
					'email_title' => $options['subject']
				),
				'content' => array(
					'email_data' => $options['data'],
					'text_view' => $this->load->view('email/'.$options['view'].'-txt', $options['data'], TRUE)
				)
			));
			echo $email;
			die();
		} else {
			$this->load->library('email');
			$config['protocol'] = 'sendmail';
			$config['mailpath'] = '/usr/sbin/sendmail';
			$config['charset'] = 'iso-8859-1';
			$config['wordwrap'] = TRUE;
			$config['crlf'] = "\r\n";

			$this->email->initialize($config);
			$this->email->from(NO_REPLY_EMAIL, SITE_NAME);
			$this->email->to($options['email_to']);
			$this->email->subject(SITE_NAME . ' :: ' .$options['subject']);

			$html_data = $this->display(array(
				'template' => $options['template'],
				'title' => 'Email - '.$options['subject'],
				'view' => 'email/'.$options['view'] . '-html',
				'template_data' => array(
					'email_title' => $options['subject']
				),
				'return_content' => true,
				'content' => $options['data']
			));

			$this->email->message($html_data);
			$this->email->set_alt_message($this->load->view('email/'.$options['view'].'-txt', $options['data'], TRUE));
			$this->email->send();
		}
		
		return true;
	}

	function merge_flash($type=null, $with=array()){
		$return = array();
		$flash = $this->session->flashdata($type);
		if(!empty($flash)){
			$return = array_merge($flash, $with);
		} else {
			$return = $with;
		}
		return $return;
	}

	function calculate_body_fat($gender=null, $measurements=null){
		if(empty($gender) || empty($measurements)){
			return false;
		}

		$measurements = array(
			'weight' => (floatval($measurements['weight']) * 2.20462),
			'wrist' => (floatval($measurements['wrist']) / 2.5),
			'waist' => (floatval($measurements['waist']) / 2.5),
			'hips' => (floatval($measurements['hips']) / 2.5),
			'forearm' => (floatval($measurements['forearm']) / 2.5)
		);
		if($gender == 1){ // Female
			$factor_1 = (floatval($measurements['weight'])*0.732+8.987);
			$factor_2 = (floatval($measurements['wrist'])/3.140);
			$factor_3 = (floatval($measurements['waist'])*0.157);
			$factor_4 = (floatval($measurements['hips'])*0.249);
			$factor_5 = (floatval($measurements['forearm'])*0.434);
			$lean_body_mass = ($factor_1+$factor_2-$factor_3-$factor_4+$factor_5);
			$body_fat_weight = ($measurements['weight'] - $lean_body_mass);
			$body_fat = number_format((($body_fat_weight*100) / $measurements['weight']),1);
		} else { // Other
			$factor_1 = (floatval($measurements['weight'])*1.082+94.42);
			$factor_2 = (floatval($measurements['waist'])*4.15);
			$lean_body_mass = ($factor_1-$factor_2);
			$body_fat_weight = (floatval($measurements['weight']) - $lean_body_mass);
			$body_fat = number_format((($body_fat_weight*100)/floatval($measurements['weight'])), 1);
		}

		return floatval($body_fat);
	}

	function date_convert($date=null){
		if(empty($date)){
			return null;
		}
		if(!preg_match('/^((0[1-9])|(1[0-9])|(2[0-9])|(3[0-1]))\/((0[1-9])|(1[0-2]))\/(\d{4})$/', $date)){
			return null;
		}
		
		$date = explode('/', $date);
		$date = mktime(0,0,0,$date[1],$date[0],$date[2]);
		return $date;
	}
}

?>