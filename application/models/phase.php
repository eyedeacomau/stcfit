<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * The training model is not to be confused with the trainers model. This model's responsibility is to handle everything that is training based. This may include things like phase generation for a user.
 * CN 2015-01-12: Commented out the start date in the get_progress method line 390
 */
class Phase extends CI_Model {
	function __construct() {
		parent::__construct();
	}

	function create_phase($user_id=null){
		if(empty($user_id) && $this->tank_auth->is_logged_in() == false){
			return false;
		} elseif($this->tank_auth->is_logged_in() == true){
			$user_id = $this->tank_auth->get_user_id();
		}

		$phase_check = $this->db->query("SELECT
			`user_profiles`.`training_type`,
			COALESCE(MAX(`user_phases`.`phase_number`), 0) AS `current_phase`
		FROM
			`user_profiles`
		LEFT JOIN
			`user_phases` ON `user_profiles`.`training_type`=`user_phases`.`training_type`
			AND
			`user_phases`.`user_id`='".mysql_real_escape_string($user_id)."'
		WHERE
			`user_profiles`.`user_id`='".mysql_real_escape_string($user_id)."'
		LIMIT
			1
		")->row_array();

		if(empty($phase_check['training_type'])){
			return false;
		}

		// The reason for this long MYSQL is to not only find out if there is a phase matching this user's criteria available, but making sure that this new phase actually contains any data.
		$new_phase = $this->db->query("SELECT
				`phases`.`phase_number`
			FROM
				`phases`
			INNER JOIN
				`phase_sessions` ON `phases`.`phase_number`=`phase_sessions`.`phase_number`
			INNER JOIN
				`sessions` ON `phase_sessions`.`session_identifier`=`sessions`.`session_identifier`
			INNER JOIN
				`session_content` ON `sessions`.`session_identifier`=`session_content`.`session_identifier`
			INNER JOIN
				`exercises` ON `session_content`.`exercise_identifier`=`exercises`.`exercise_identifier`
			WHERE
				`phases`.`training_type`='".$phase_check['training_type']."'
			AND
				`phases`.`phase_number` = (
					SELECT
						MIN(`phases`.`phase_number`)
					FROM
						`phases`
					WHERE
						`phases`.`phase_number` > '".$phase_check['current_phase']."'
					AND
						`phases`.`training_type`='".$phase_check['training_type']."'
					LIMIT
						1
				)
			AND
				`phase_sessions`.`training_type` = '".$phase_check['training_type']."'
			AND
				`phases`.`valid_to` IS NULL
			AND
				`phase_sessions`.`valid_to` IS NULL
			AND
				`sessions`.`valid_to` IS NULL
			AND
				`session_content`.`valid_to` IS NULL
			AND
				`exercises`.`valid_to` IS NULL
			GROUP BY
				`sessions`.`session_identifier`
		");

		if($new_phase->num_rows() < 1){ // Unable to find a phase with this user's criteria.
			return false;
		}

		$new_phase = $new_phase->result_array();
		$new_phase = $new_phase[0];

		$this->db->set(array(
			'phase_started' => time(),
			'phase_number' => $new_phase['phase_number'],
			'training_type' => $phase_check['training_type'],
			'user_id' => $user_id
		));

		$this->db->insert('user_phases');
		return true;
	}

	function get_all_phase_data($user_id=null){
		if(empty($user_id) && $this->tank_auth->is_logged_in() == false){
			return false;
		} elseif($this->tank_auth->is_logged_in() == true){
			$user_id = $this->tank_auth->get_user_id();
		}

		$current_phase = $this->db->query("SELECT
				`exercises`.`name` AS `exercise_name`,
				`exercises`.`circuit` AS `is_circuit`,
				`user_entries`.`entry_data`,
				`session_content`.*
			FROM
				`user_phases`
			INNER JOIN
				`phases` ON `user_phases`.`phase_number`=`phases`.`phase_number`
			INNER JOIN
				`phase_sessions` ON `phases`.`phase_number`=`phase_sessions`.`phase_number`
			INNER JOIN
				`sessions` ON `phase_sessions`.`session_identifier`=`sessions`.`session_identifier`
			INNER JOIN
				`session_content` ON `sessions`.`session_identifier`=`session_content`.`session_identifier`
			INNER JOIN
				`exercises` ON `session_content`.`exercise_identifier`=`exercises`.`exercise_identifier`
			LEFT JOIN
				`user_entries` ON `session_content`.`id`=`user_entries`.`entry_id`
				AND
				`user_entries`.`user_id`='".mysql_real_escape_string($user_id)."'
			WHERE
			(
				`phases`.`valid_from` <= `user_phases`.`phase_started`
				AND
				`phases`.`training_type`=`user_phases`.`training_type`
				AND
				(
					`phases`.`valid_to` > `user_phases`.`phase_started`
					OR
					`phases`.`valid_to` IS NULL
				)
			)

			AND
			(
				`phase_sessions`.`valid_from` <= `user_phases`.`phase_started`
				AND
				`phase_sessions`.`training_type`=`phases`.`training_type`
				AND
				(
					`phase_sessions`.`valid_to` > `user_phases`.`phase_started`
					OR
					`phase_sessions`.`valid_to` IS NULL
				)
			)

			AND
			(
				`sessions`.`valid_from` <= `user_phases`.`phase_started`
				AND
				(
					`sessions`.`valid_to` > `user_phases`.`phase_started`
					OR
					`sessions`.`valid_to` IS NULL
				)
			)

			AND
			(
				`session_content`.`valid_from` <= `user_phases`.`phase_started`
				AND
				(
					`session_content`.`valid_to` > `user_phases`.`phase_started`
					OR
					`session_content`.`valid_to` IS NULL
				)
			)

			AND
			(
				`exercises`.`valid_from` <= `user_phases`.`phase_started`
				AND
				(
					`exercises`.`valid_to` > `user_phases`.`phase_started`
					OR
					`exercises`.`valid_to` IS NULL
				)
			)
			AND
				`user_phases`.`completed`=0
			AND
				`user_phases`.`user_id`='".mysql_real_escape_string($user_id)."'
			ORDER BY
				`session_identifier`, `session_content`.`order`
		");

		if($current_phase->num_rows() < 1){
			return false;
		}

		return $current_phase->result_array();
	}

	function get_current_phase($user_id=null,$phase_num=0){
		if(empty($user_id)){
			return false;
		}

		if($phase_num==0){
			$phase_num = '`user_phases`.`phase_number`';
		}

		$current_phase = $this->db->query("SELECT
				`phases`.*,
				`training_types`.`name` AS `type_name`,
				COUNT(`sessions`.`id`)-COUNT(`completed_sessions`.`session_identifier`) AS `incomplete_sessions`
			FROM
				`user_phases`
			INNER JOIN
				`phases` ON ".$phase_num."=`phases`.`phase_number`
			INNER JOIN
				`training_types` ON `phases`.`training_type`=`training_types`.`id`
			LEFT JOIN
				`phase_sessions` ON `phases`.`phase_number`=`phase_sessions`.`phase_number`
			LEFT JOIN
				`sessions` ON `phase_sessions`.`session_identifier`=`sessions`.`session_identifier`
			LEFT JOIN
				`completed_sessions` ON `sessions`.`session_identifier`=`completed_sessions`.`session_identifier`
				AND
				`completed_sessions`.`user_id`=`user_phases`.`user_id`
			WHERE
			(
				`phases`.`valid_from` <= `user_phases`.`phase_started`
				AND
				`phases`.`training_type`=`user_phases`.`training_type`
				AND
				(
					`phases`.`valid_to` > `user_phases`.`phase_started`
					OR
					`phases`.`valid_to` IS NULL
				)
			)
			AND
			(
				`phase_sessions`.`valid_from` <= `user_phases`.`phase_started`
				AND
				`phase_sessions`.`training_type`=`phases`.`training_type`
				AND
				(
					`phase_sessions`.`valid_to` > `user_phases`.`phase_started`
					OR
					`phase_sessions`.`valid_to` IS NULL
				)
			)
			AND
			(
				`sessions`.`valid_from` <= `user_phases`.`phase_started`
				AND
				(SELECT
					COUNT(`session_content`.`id`)
				FROM
					`session_content`
				INNER JOIN
					`exercises` ON `session_content`.`exercise_identifier`=`exercises`.`exercise_identifier`
				WHERE
					`session_content`.`session_identifier`=`sessions`.`session_identifier`
				AND
					(
					`session_content`.`valid_to` > `user_phases`.`phase_started`
					OR
					`session_content`.`valid_to` IS NULL
					)
				AND
					(
					`exercises`.`valid_to` > `user_phases`.`phase_started`
					OR
					`exercises`.`valid_to` IS NULL
					)
				) > 0
				AND
				(
					`sessions`.`valid_to` > `user_phases`.`phase_started`
					OR
					`sessions`.`valid_to` IS NULL
				)
			)
			AND
				`user_phases`.`completed`=0
			AND
				`user_phases`.`user_id`='".mysql_real_escape_string($user_id)."'
			LIMIT
				1
		");

		if($current_phase->num_rows() < 1){
			return false;
		}
		$result = $current_phase->row_array();
		if(isset($result['incomplete_sessions']) && $result['incomplete_sessions'] == 0 && $phase_num == 0){
			$this->db->set(array(
				'completed' => 1
			));

			$this->db->where('user_id', $user_id);
			$this->db->where('phase_number', $result['phase_number']);
			$this->db->where('training_type', $result['training_type']);
			$this->db->update('user_phases');
			return false;
		}
		return $current_phase->row_array();
	}

	function get_end_phase($user_id=null){
		if(empty($user_id)){
			return false;
		}
		$current_phase = $this->db->query("
			SELECT
				`phases`.`phase_number` AS `end_phase`
			FROM
				`phases`
			INNER JOIN
				`user_profiles` ON
				`user_profiles`.`user_id`='".mysql_real_escape_string($user_id)."'
			WHERE
				`phases`.`training_type` = `user_profiles`.`training_type`
			ORDER BY
				`end_phase`
			DESC LIMIT 1
		");

		if($current_phase->num_rows() < 1){
			return false;
		}
		return $current_phase->row_array();
	}

	function get_progress($user=null, $type=null, $full=true){
		if(empty($user['user_id'])){
			return false;
		}

/*		if(empty($user['allocated_trainer'])){ // If this user has not been assinged a trainer, this attempts to do so.
			$get_trainer = $this->db->query("SELECT
					`users`.`id` AS `trainer_id`
				FROM
					`users`
				INNER JOIN
					`trainer_profiles` ON `users`.`id`=`trainer_profiles`.`user_id`
				LEFT JOIN
					`user_profiles` ON `users`.`id`=`user_profiles`.`allocated_trainer`
				WHERE
					`users`.`is_trainer`=1
				AND
					`users`.`banned`=0
				GROUP BY
					`user_profiles`.`allocated_trainer`
				ORDER BY
					COUNT(`user_profiles`.`allocated_trainer`) ASC
				LIMIT
					1
			")->row_array();
			if(!empty($get_trainer['trainer_id'])){
				$this->db->set(array(
					'allocated_trainer' => $get_trainer['trainer_id']
				));
				$this->db->where('user_id', $this->user_id);
				$this->db->update('user_profiles');

				$this->db->set(array(
					'trainer_id' => $get_trainer['trainer_id'],
					'client_id' => $this->user_id,
					'reminder_date' => time(),
					'notes' => 'New user alert! This user has been assigned to you! Please welcome them.',
				));
				$this->db->insert('trainer_reminders');
				$user['allocated_trainer'] = $get_trainer['trainer_id'];
			}
		}*/ // Removed at the client's request 30-06-2014 - Craig Barben

		// Will get all measurement entries for this user in the last 30 days, plus the original entry.
		$data = array();

		$data['measurements'] = $this->db->query("SELECT
				`measurement_entries`.*,
				(`measurement_entries`.`shoulders`+
				`measurement_entries`.`neck`+
				`measurement_entries`.`chest`+
				`measurement_entries`.`waist`+
				`measurement_entries`.`bicep`+
				`measurement_entries`.`hips`+
				`measurement_entries`.`quads`+
				`measurement_entries`.`calf`+
				`measurement_entries`.`wrist`+
				`measurement_entries`.`forearm`) AS `y`,
				`original`.`shoulders` AS `original_shoulders`,
				`original`.`neck` AS `original_neck`,
				`original`.`chest` AS `original_chest`,
				`original`.`waist` AS `original_waist`,
				`original`.`bicep` AS `original_bicep`,
				`original`.`hips` AS `original_hips`,
				`original`.`quads` AS `original_quads`,
				`original`.`calf` AS `original_calf`,
				`original`.`wrist` AS `original_wrist`,
				`original`.`forearm` AS `original_forearm`
			FROM
				`measurement_entries`
			LEFT JOIN
				`measurement_entries` AS `original` ON `measurement_entries`.`user_id`=`original`.`user_id`
				AND
				`original`.`id` = (
					SELECT
						MIN(`id`)
					FROM
						`measurement_entries`
					WHERE
						`user_id`='".mysql_real_escape_string($user['user_id'])."'
				)
			WHERE
				`measurement_entries`.`user_id`='".mysql_real_escape_string($user['user_id'])."'
			AND
				(
				#	`measurement_entries`.`entry_date` >=".(time()-DASH_DATA_PERIOD)."
				#	AND
					`measurement_entries`.`entry_date` <= ".time()."
				)
			ORDER BY
				`measurement_entries`.`id` ASC
		")->result_array();

		foreach($data['measurements'] as &$entry){
			$entry['body_fat'] = $this->common->calculate_body_fat(3, $entry);
			$entry['y'] = floatval($entry['y']);
			if($full == false){
				$entry['y'] = $entry['body_fat'];
			}
			$data['categories'][] = date('d/m',$entry['entry_date']);
		}

		$current_phase_data = $this->get_all_phase_data($user['user_id']);
		if(!empty($current_phase_data)){
			$completed = 0;
			$total_entries = 0;
			// Unfortunately there is no better way to do the following. This calculates how many entries are needed in total for this session and how many have entries against them.
			$data['session_stats']['incomplete_sessions'] = array();
			foreach($current_phase_data as $exercise){
				$sets = (!empty($exercise['is_circuit']) ? 1:$exercise['sets']); // If this is a circuit it will only have 1 set.
				$total_entries += (intval($sets)*ENTRIES_PER_SESSION);
				$entry_data = json_decode($exercise['entry_data'], true);
				if(!empty($entry_data)){
					foreach($entry_data as $entry_number => $sets){
						if(!empty($exercise['is_circuit'])){
							if(!empty($sets['data'])){
								$completed++;
							}
						} else {
							foreach($sets as $set){
								if(!empty($set['reps']) && !empty($set['weight'])){
									$completed++;
								}
							}
						}
					}
				}
			}
			$percent_complete = round((($completed/$total_entries)*100));
			$data['progress']['training']['complete'] = $percent_complete;
			$data['progress']['training']['incomplete'] = (100-$percent_complete);
		}

		// Removed by Craig Barben on 02-07-2014 at thge clients request


		// $food_entries = $this->db->query("SELECT
		// 		COALESCE(`food_plans`.`entries`,0) AS `total_entries`,
		// 		COALESCE(COUNT(`food_entries`.`id`),0) AS `completed_entries`
		// 	FROM
		// 		`user_phases`
		// 	LEFT JOIN
		// 		`food_entries` ON `user_phases`.`training_type`=`food_entries`.`training_type`
		// 		AND
		// 		`food_entries`.`user_id`='".$user['user_id']."'
		// 		AND
		// 		`user_phases`.`phase_number`=`food_entries`.`phase_number`
		// 		AND
		// 		(
		// 			`food_entries`.`calories` != ''
		// 			AND
		// 			`food_entries`.`calories` IS NOT NULL
		// 			AND
		// 			`food_entries`.`protein` != ''
		// 			AND
		// 			`food_entries`.`protein` IS NOT NULL
		// 			AND
		// 			`food_entries`.`carbs` != ''
		// 			AND
		// 			`food_entries`.`carbs` IS NOT NULL
		// 			AND
		// 			`food_entries`.`sugars` != ''
		// 			AND
		// 			`food_entries`.`sugars` IS NOT NULL
		// 			AND
		// 			`food_entries`.`fats` != ''
		// 			AND
		// 			`food_entries`.`fats` IS NOT NULL
		// 		)
		// 	LEFT JOIN
		// 		`user_profiles` ON `user_phases`.`user_id`=`user_profiles`.`user_id`
		// 	LEFT JOIN
		// 		`food_plans` ON `user_profiles`.`food_plan`=`food_plans`.`id`
		// 	WHERE
		// 		`user_phases`.`training_type`='".mysql_real_escape_string($type)."'
		// 	AND
		// 		`user_phases`.`completed`=0
		// 	AND
		// 		`user_phases`.`user_id`='".mysql_real_escape_string($user['user_id'])."'
		// 	LIMIT
		// 		1
		// ")->row_array();

		$data['user_id'] = $user['user_id'];
		// $data['progress']['food_entries']['complete'] = $food_entries['completed_entries'];
		// $data['progress']['food_entries']['incomplete'] = $food_entries['total_entries'];
		//$_SESSION['phase_progress'] = $data;
		return $data;
	}

	function get_current_sessions($user_id=null,$phase_num=0){
		if(empty($user_id)){
			return false;
		}

		if($phase_num==0){
			$phase_num = '`user_phases`.`phase_number`';
		}

		$sessions = $this->db->query("SELECT
				`sessions`.*,
				`phase_sessions`.`order` AS `session_number`,
				`completed_sessions`.`id` AS `session_complete`
			FROM
				`user_phases`
			INNER JOIN
				`phases` ON ".$phase_num."=`phases`.`phase_number`
			INNER JOIN
				`phase_sessions` ON `phases`.`phase_number`=`phase_sessions`.`phase_number`
			INNER JOIN
				`sessions` ON `phase_sessions`.`session_identifier`=`sessions`.`session_identifier`
			LEFT JOIN
				`completed_sessions` ON `sessions`.`session_identifier`=`completed_sessions`.`session_identifier`
				AND
				`completed_sessions`.`user_id`=`user_phases`.`user_id`
			WHERE
			(
				`phases`.`valid_from` <= `user_phases`.`phase_started`
				AND
				`phases`.`training_type`=`user_phases`.`training_type`
				AND
				(
					`phases`.`valid_to` > `user_phases`.`phase_started`
					OR
					`phases`.`valid_to` IS NULL
				)
			)

			AND
			(
				`phase_sessions`.`valid_from` <= `user_phases`.`phase_started`
				AND
				`phase_sessions`.`training_type`=`phases`.`training_type`
				AND
				(
					`phase_sessions`.`valid_to` > `user_phases`.`phase_started`
					OR
					`phase_sessions`.`valid_to` IS NULL
				)
			)

			AND
			(
				`sessions`.`valid_from` <= `user_phases`.`phase_started`
				AND
				(SELECT
					COUNT(`session_content`.`id`)
				FROM
					`session_content`
				INNER JOIN
					`exercises` ON `session_content`.`exercise_identifier`=`exercises`.`exercise_identifier`
				WHERE
					`session_content`.`session_identifier`=`sessions`.`session_identifier`
				AND
					(
					`session_content`.`valid_to` > `user_phases`.`phase_started`
					OR
					`session_content`.`valid_to` IS NULL
					)
				AND
					(
					`exercises`.`valid_to` > `user_phases`.`phase_started`
					OR
					`exercises`.`valid_to` IS NULL
					)
				) > 0
				AND
				(
					`sessions`.`valid_to` > `user_phases`.`phase_started`
					OR
					`sessions`.`valid_to` IS NULL
				)
				AND
				(
					`sessions`.`valid_to` > `user_phases`.`phase_started`
					OR
					`sessions`.`valid_to` IS NULL
				)
			)".
			($phase_num=='`user_phases`.`phase_number`'?' AND `user_phases`.`completed` = 0':'').
			"
			AND
				`user_phases`.`user_id`='".mysql_real_escape_string($user_id)."'
			GROUP BY
				`sessions`.`session_identifier`
			ORDER BY
				`phase_sessions`.`order` ASC
		");

		if($sessions->num_rows() < 1){
			return false;
		}

		return $sessions->result_array();
	}

	function get_session_exercise_data($user_id=null, $session_identifier=null){
		if(empty($user_id) || empty($session_identifier)){
			return false;
		}

		$session_data = $this->db->query("SELECT
				`exercises`.`name` AS `exercise_name`,
				`exercises`.`description`,
				`exercises`.`circuit` AS `is_circuit`,
				`exercises`.`id` AS `exercise_id`,
				`user_entries`.`entry_data`,
				`sessions`.`name` AS `session_name`,
				`sessions`.`description` AS `session_description`,
				`sessions`.`video_link` AS `session_video_link`,
				`phase_sessions`.`order` AS `session_number`,
				`phases`.`name` AS `phase_name`,
				`phases`.`phase_number` AS `phase_number`,
				`training_types`.`name` AS `training_type_name`,
				`session_content`.*
			FROM
				`user_phases`
			INNER JOIN
				`phases` ON `user_phases`.`phase_number`=`phases`.`phase_number`
			INNER JOIN
				`training_types` ON `phases`.`training_type`=`training_types`.`id`
			INNER JOIN
				`phase_sessions` ON `phases`.`phase_number`=`phase_sessions`.`phase_number`
			INNER JOIN
				`sessions` ON `phase_sessions`.`session_identifier`=`sessions`.`session_identifier`
			INNER JOIN
				`session_content` ON `sessions`.`session_identifier`=`session_content`.`session_identifier`
			INNER JOIN
				`exercises` ON `session_content`.`exercise_identifier`=`exercises`.`exercise_identifier`
			LEFT JOIN
				`user_entries` ON `session_content`.`id`=`user_entries`.`entry_id`
				AND
				`user_entries`.`user_id`='".mysql_real_escape_string($user_id)."'
			WHERE
			(
				`phases`.`valid_from` <= `user_phases`.`phase_started`
				AND
				`phases`.`training_type`=`user_phases`.`training_type`
				AND
				(
					`phases`.`valid_to` > `user_phases`.`phase_started`
					OR
					`phases`.`valid_to` IS NULL
				)
			)

			AND
			(
				`phase_sessions`.`valid_from` <= `user_phases`.`phase_started`
				AND
				`phase_sessions`.`training_type`=`phases`.`training_type`
				AND
				(
					`phase_sessions`.`valid_to` > `user_phases`.`phase_started`
					OR
					`phase_sessions`.`valid_to` IS NULL
				)
			)

			AND
			(
				`sessions`.`valid_from` <= `user_phases`.`phase_started`
				AND
				(
					`sessions`.`valid_to` > `user_phases`.`phase_started`
					OR
					`sessions`.`valid_to` IS NULL
				)
			)

			AND
			(
				`session_content`.`valid_from` <= `user_phases`.`phase_started`
				AND
				(
					`session_content`.`valid_to` > `user_phases`.`phase_started`
					OR
					`session_content`.`valid_to` IS NULL
				)
			)

			AND
			(
				`exercises`.`valid_from` <= `user_phases`.`phase_started`
				AND
				(
					`exercises`.`valid_to` > `user_phases`.`phase_started`
					OR
					`exercises`.`valid_to` IS NULL
				)
			)
			AND
				`user_phases`.`user_id`='".mysql_real_escape_string($user_id)."'
			AND
				`phase_sessions`.`session_identifier`='".mysql_real_escape_string($session_identifier)."'
			ORDER BY
				`session_content`.`order`
		");

		if($session_data->num_rows() < 1){
			return false;
		}
		return $session_data->result_array();
	}

	function change_training_type($user_id=null, $to_type=null){
		if(empty($user_id) || empty($to_type)){
			return false;
		}

		$phase = $this->db->query("SELECT
			`user_phases`.*
		FROM
			`user_phases`
		WHERE
			`user_phases`.`completed`=0
		AND
			`user_phases`.`user_id`='".$user_id."'
		LIMIT
			1
		");

		if($phase->num_rows() > 0){
			$phase = $phase->row_array();
			$current_sessions = $this->db->query("SELECT
				`phase_sessions`.`session_identifier`
			FROM
				`phase_sessions`
			WHERE
				`phase_sessions`.`phase_number`='".$phase['phase_number']."'
			AND
				`phase_sessions`.`training_type`='".$phase['training_type']."'
			GROUP BY
				`phase_sessions`.`session_identifier`
			");

			// Although I do not like deleteing from tables, This is necessary.
			if($current_sessions->num_rows() > 0){
				$ids = array();
				foreach($current_sessions->result_array() as $current){
					$ids[] = $current['session_identifier'];
				}

				$this->db->where('session_identifier IN ('.implode(', ', $ids).')', null);
				$this->db->where('user_id', $user_id);
				$this->db->delete('user_entries');

				// ---------------------------------------------------------------
				$this->db->where('session_identifier IN('.implode(', ', $ids).')', null);
				$this->db->where('user_id', $user_id);
				$this->db->delete('completed_sessions');
			}

			$this->db->where('user_id', $user_id);
			$this->db->where('completed', '0');
			$this->db->where('training_type', $phase['training_type']);
			$this->db->where('phase_number', $phase['phase_number']);
			$this->db->delete('user_phases');
		}

		$this->db->set(array(
			'training_type' => $to_type
		));
		$this->db->where('user_id', $user_id);
		$this->db->update('user_profiles');

		return true;
	}
}

?>
