<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Data is where any functions that don't align with other models, that use the databse go.
 */
class Data extends CI_Model {
	function __construct() {
		parent::__construct();
	}

	function get_registration_data($profile_id=null){
		$user_data = $this->db->query("SELECT
			`pre_exercise_questions`.*,
			`pre_exercise_questions`.`id` AS `medical_complete`,
			`user_profiles`.`id` AS `profile_id`,
			`user_profiles`.*
		FROM
			`user_profiles`
		LEFT JOIN
			`pre_exercise_questions` ON `user_profiles`.`id`=`pre_exercise_questions`.`profile_id`
		WHERE
			`user_profiles`.`id`='".mysql_real_escape_string($profile_id)."'
		LIMIT
			1
		");
		return $user_data;
	}

	function registration_step($profile_id=null) {
		$user_data = $this->get_registration_data($profile_id);

		$user_exists = $user_data->num_rows();
		$user_data = $user_data->row_array();

		if(empty($user_exists)){
			$this->session->set_userdata(array()); // Emptying the array
			redirect('/registration');
		} elseif(empty($user_data['eway_token'])){
			redirect('/registration/payment');
		} elseif(empty($user_data['medical_complete'])){
			redirect('/registration/pre-exercise');
		// } elseif(empty($user_data['all_details'])){
		// 	redirect('/registration/your-details');
		// } elseif(empty($user_data['tailoring_complete'])){
		// 	redirect('/registration/tailor-program');
		} else {
			return true;
		}
	}

	function unread_messages() {
		if($this->tank_auth->is_logged_in() == false){
			return 0;
		}
		$user_id = $this->tank_auth->get_user_id();
		$messages = $this->db->query("SELECT * FROM `messages` WHERE `for_user_id`='".$user_id."' AND `unread`=1")->num_rows();
		if($messages > 9){
			$messages = '9+';
		}
		return $messages;
	}

	function get_all_userdata($user_id=null){
		if(!empty($user_id)){
			$user = $this->db->query("SELECT
				`users`.*,
				`user_profiles`.*,
				`users`.`id` AS `user_id`,
				`user_profiles`.`id` AS `profile_id`,
				`food_plans`.`entries` AS `total_food_entries`,
				`account_deleted`.`id` AS `account_deleted`,
				CONCAT(`trainer_profiles`.`first_name`, ' ', `trainer_profiles`.`last_name`) AS `trainer_name`
			FROM
				`users`
			LEFT JOIN
				`user_profiles` ON `users`.`id`=`user_profiles`.`user_id`
			LEFT JOIN
				`account_deleted` ON `users`.`id`=`account_deleted`.`user_id`
				AND
				`account_deleted`.`obsolete` != 1
				AND
				(
					`account_deleted`.`trainer_override` = 0
					OR
					`account_deleted`.`user_confirmed` = 1
				)
			LEFT JOIN
				`food_plans` ON `user_profiles`.`food_plan`=`food_plans`.`id`
			LEFT JOIN
				`trainer_profiles` ON `user_profiles`.`allocated_trainer`=`trainer_profiles`.`user_id`
			WHERE
				`users`.`id`='".mysql_real_escape_string($user_id)."'
			LIMIT
				1
			");

			if($user->num_rows() > 0) {
				return $user->row_array();
			}
			return null;
		}
	}
}

?>
