<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Any commonly used methods to do with payment are in this model.
 */ 
class Payment extends CI_Model {
	function __construct() {	
		parent::__construct();
	}

	/**
	 *  	                                                          
	 *  	88888888888                                               
	 *  	88                                                        
	 *  	88                                                        
	 *  	88aaaaa      8b      db      d8  ,adPPYYba,  8b       d8  
	 *  	88"""""      `8b    d88b    d8'  ""     `Y8  `8b     d8'  
	 *  	88            `8b  d8'`8b  d8'   ,adPPPPP88   `8b   d8'   
	 *  	88             `8bd8'  `8bd8'    88,    ,88    `8b,d8'    
	 *  	88888888888      YP      YP      `"8bbdP"Y8      Y88'     
	 *  	                                                 d8'      
	 *  	                                                d8'     
	 *  	                                                                       
	*/

	private function eway_connect() {
		require_once APPPATH.'libraries/nusoap/nusoap.php'; // All the gear eway requires for SOAP

		$gateway = EWAY_TEST_GATEWAY;	// Once this system is in production, change CI's ENVIRONMENT define to 'production' and the model will use the correct server
		
		if(defined('ENVIRONMENT')) {
			switch (ENVIRONMENT) {
				case 'production':
					$gateway = EWAY_LIVE_GATEWAY;
				break;
				default:
				case 'development':
					$gateway = EWAY_TEST_GATEWAY;
				break;
			}
		}
		

		$client = new nusoap_client($gateway, false);
		$client->namespaces['soap'] = EWAY_namespace;
		$headers = "<eWAYHeader xmlns=\"".EWAY_namespace."\"><eWAYCustomerID>".EWAY_CUSTOMERID."</eWAYCustomerID><Username>".EWAY_USERNAME."</Username><Password>".EWAY_PASSWORD."</Password></eWAYHeader>";
		$client->setHeaders($headers);
		return $client;
	}

	public function eway_user_data($user, $card_details) {
		if(!is_array($user) && is_int($user)){
			$user = $this->data->get_all_userdata($user);
		}

		$user_data = array(
			'Title' => '',
			'FirstName' => $user['first_name'],
			'LastName' => $user['last_name'],
			'Address' => $user['street_address'],
			'Suburb' => $user['suburb'],
			'State' => $user['state'],
			'Company' => '',
			'PostCode' => $user['postcode'],
			'Country' => 'au',
			'Email' => $user['email'],
			'Fax' => '',
			'Phone' => $user['phone'],
			'Mobile' => '',
			'CustomerRef' => $user['user_id'],
			'JobDesc' => 'STC Fit Client',
			'Comments' => '',
			'URL' => '',
			'CCNumber' => $card_details['card_number'],
			'CCNameOnCard' => $card_details['card_name'],
			'CCExpiryMonth' => $card_details['expiry_month'],
			'CCExpiryYear' => $card_details['expiry_year']
		);

		foreach($user_data as $k => $v){
			$user_data['soap:'.$k] = $v;
			unset($user_data[$k]);
		}

		return $user_data; 
	}

	function eway_create_user($user, $card_details){
		$soap = $this->eway_connect();
		return $soap->call('soap:'.EWAY_action_create_user, $this->payment->eway_user_data($user, $card_details), '', EWAY_endpoint.EWAY_action_create_user);
	}

	function eway_update_user($token, $user, $card_details) {
		$soap = $this->payment->eway_connect();
		$user_data = $this->payment->eway_user_data($user, $card_details);
		$user_data['soap:managedCustomerID'] = $token;
		$result = $soap->call('soap:' . EWAY_action_update_user, $user_data, '', EWAY_endpoint.EWAY_action_update_user);
		return $result;
	}

	function eway_get_user($token) {
		$soap = $this->payment->eway_connect();
		return $soap->call('soap:' . EWAY_action_get_user, array('soap:managedCustomerID' => $token), '', EWAY_endpoint.EWAY_action_get_user);
	}

	function eway_bill_user($token, $amount = EWAY_TEST_AMOUNT, $reference = EWAY_TEST_REF) {
		$soap = $this->payment->eway_connect();
		if($amount == 0) return array(
			'ewayTrxnError' => '00, Transaction not processed with eWay due to $0.00 amount',
			'ewayTrxnStatus' => 'True',
			'ewayTrxnNumber' => '',
			'ewayReturnAmount' => '0',
			'ewayAuthCode' => ''
		);

		if(ENVIRONMENT != 'production' && EWAY_FORCE_SUCCESS) $amount = round($amount);	// set cents to zero for eway to return success.

		$user_data = array(
			'soap:managedCustomerID' => $token,
			'soap:amount' => ($amount * 100),
			'soap:invoiceReference' => (time() + rand(1000, 9999)),
			'soap:invoiceDescription' => $reference
		);
		return $soap->call('soap:'.EWAY_action_bill_user, $user_data, '', EWAY_endpoint.EWAY_action_bill_user);
	}
}

?>