<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * The admin model is where any necessary SQL goes for the admin section.
 */
class Admin extends CI_Model {
	function __construct() {
		parent::__construct();
	}

	function new_users(){
		$users = $this->db->query("SELECT
				`users`.`id`
			FROM
				`users`
			INNER JOIN
				`user_profiles` ON `users`.`id`=`user_profiles`.`user_id`
			LEFT JOIN
				`billing_history` ON `users`.`id`=`billing_history`.`user_id`
			WHERE
				`users`.`is_admin`=0
			AND
				`users`.`is_trainer`=0
			AND
				`user_profiles`.`allocated_trainer` IS NULL
			AND
				`billing_history`.`id` IS NOT NULL
			")->num_rows();

		return $users;
	}

	function revenue_data(){
		for($x=(REVENUE_PERIOD-1);$x>=0;$x--){
			$return['revenue_data'][date('M \'y', strtotime(date('Y-m-01').' -'.$x.' months'))] = array(
				'y' => 0
			);
			// CB :: Edited 31/07/2014
			//$return['revenue_months'][] = date('M \'y', strtotime('-'.($x-1).' months'));
		}
		//die(var_dump($return));
		$revenue = $this->db->query("SELECT
			`billing_history`.`amount`,
			`billing_history`.`date`
		FROM
			`billing_history`
		WHERE
			`billing_history`.`date` >='".strtotime('-'.REVENUE_PERIOD.' months')."'
		");


		foreach($revenue->result_array() as $moneh){
			// CB :: Edited 31/07/2014
			$return['revenue_data'][date('M \'y', $moneh['date'])]['y'] += floatval($moneh['amount']);
		}

		// CB :: Edited 31/07/2014
		$return['revenue_months'] = array_keys($return['revenue_data']);
		$return['revenue_data'] = array_values($return['revenue_data']);
		return $return;
	}

	function trainer_data(){
		$sql = "SELECT
			`users`.`id` AS `trainer_id`,
			UNIX_TIMESTAMP(`users`.`created`) AS `trainer_created`,
			`trainer_profiles`.`first_name`,
			`trainer_profiles`.`last_name`,
			(
				SELECT COUNT(`u2`.`id`)
				FROM `user_profiles`
				INNER JOIN `users` u2
					ON `u2`.`id` = `user_profiles`.`user_id`
				WHERE `u2`.`banned` = 1
				AND `user_profiles`.`allocated_trainer` = `users`.`id`
				AND `user_profiles`.`paid_period_ends` > ".time()."

			) AS `suspended`
		FROM
			`users`
		INNER JOIN
			`trainer_profiles` ON `users`.`id`=`trainer_profiles`.`user_id`
		WHERE
			`users`.`is_trainer`=1
		";

		$trainers = $this->db->query($sql);

		if($trainers->num_rows() < 1){
			return false;
		}

		$return = array();
		foreach($trainers->result_array() as $trainer){
			$trainer_id = $trainer['trainer_id'];
			$return[$trainer_id]['first_name'] = $trainer['first_name'];
			$return[$trainer_id]['last_name'] = $trainer['last_name'];
			$return[$trainer_id]['suspended'] = $trainer['suspended'];
			$return[$trainer_id]['trainer_created'] = ceil(((time()-intval($trainer['trainer_created']))/86400)/30.4);

			$return[$trainer_id]['all_time_active'] = ($this->trainer_all_users($trainer_id,time(),true)-$this->trainer_lost_users($trainer_id,0,time(),true)-$this->trainer_suspended_users($trainer_id, true));
			$return[$trainer_id]['new_users'] = $this->trainer_new_users($trainer_id,true);

			$return[$trainer_id]['month_data']['total']['name'] = 'Total';
			$return[$trainer_id]['month_data']['lost']['name'] = 'Lost';
			$return[$trainer_id]['month_data']['active']['name'] = 'Active';

			for($x=TRAINER_DATA_PERIOD;$x>0;$x--){
				$total = $lost = $suspend = 0;

				$month = date('F', strtotime(date('Y-m-01').' -'.($x-1).' months'));

				$return[$trainer_id]['cats'][$month] = $month;

				$start_ts = strtotime(date('Y-m-01 0:00:00', strtotime('-'.($x-1).' months')));
				$end_ts = strtotime(date('Y-m-t 23:59:59', strtotime('-'.($x-1).' months')));
				$end_ts = min($end_ts,time());

				$total = $this->trainer_all_users($trainer_id, $end_ts, true);
				$lost = $this->trainer_lost_users($trainer_id, $start_ts, $end_ts, true);
				$suspend = $this->trainer_suspended_users($trainer_id, true);

				$return[$trainer_id]['month_data']['total']['data'][] = intval($total);
				$return[$trainer_id]['month_data']['lost']['data'][] = intval($lost);
				$return[$trainer_id]['month_data']['active']['data'][] = (intval($total) - intval($lost) - intval($suspend));
			}
		}
		return $return;
	}

	function trainer_all_users($trainer_id, $end_ts, $count = false){
		$sql = "SELECT
					`users`.`id`
				FROM
					`user_profiles`
				INNER JOIN
					`users` ON `user_profiles`.`user_id`=`users`.`id`
				WHERE
					UNIX_TIMESTAMP(`users`.`created`) <= '".$end_ts."'
				AND
					`user_profiles`.`allocated_trainer`= '".$trainer_id."'";
		$results = $this->db->query($sql);

		if($count){
			return $results->num_rows();
		} else {
			return $results->result_array();
		}
	}

	function trainer_new_users($trainer_id, $count = false){
		$sql = "SELECT
					`users`.`id`
				FROM
					`user_profiles`
				INNER JOIN
					`users` ON `user_profiles`.`user_id`=`users`.`id`
				LEFT JOIN
					`account_deleted` ON `user_profiles`.`user_id`=`account_deleted`.`user_id`
				WHERE
					UNIX_TIMESTAMP(`users`.`created`) >= '".(mktime(0,0,0)-604800)."'
				AND
					`user_profiles`.`allocated_trainer`= '".$trainer_id."'
				AND
					`users`.`banned` = 0
				AND
					`user_profiles`.`paid_period_ends` > '".time()."'
				AND
				(
					`account_deleted`.`id` IS NULL
					OR
					(
						`account_deleted`.`obsolete` = 1
						AND
						(
							`account_deleted`.`trainer_override` = 1
							OR
							`account_deleted`.`user_confirmed` = 0
						)
					)
				)";
		$results = $this->db->query($sql);

		if($count){
			return $results->num_rows();
		} else {
			return $results->result_array();
		}
	}

	function trainer_suspended_users($trainer_id, $count = false){
		$sql = "SELECT
					`users`.`id`
				FROM
					`user_profiles`
				INNER JOIN
					`users` ON `user_profiles`.`user_id`=`users`.`id`
				WHERE
					`user_profiles`.`allocated_trainer`= '".$trainer_id."'
				AND
					`users`.`banned` = 1";
		$results = $this->db->query($sql);

		if($count){
			return $results->num_rows();
		} else {
			return $results->result_array();
		}
	}

	function trainer_lost_users($trainer_id, $start_ts, $end_ts, $count = false, $previous = array()){
		$insert = "";
		$already_counted = array();
		if(!empty($previous)){
			$already_counted = implode(',',$previous);
			$insert = "AND `users`.`id` NOT IN (".$already_counted.")";
		}
		
		$sql = "SELECT
					DISTINCT(`users`.`id`)
				FROM
					`user_profiles`
				INNER JOIN
					`users` ON `user_profiles`.`user_id`=`users`.`id`
				LEFT JOIN
					`account_deleted` ON `user_profiles`.`user_id`=`account_deleted`.`user_id`
				WHERE
					`user_profiles`.`allocated_trainer` = '".$trainer_id."'
					AND
					`users`.`banned` = 0
					AND
					(
						(
							`account_deleted`.`deletion_date` >= '".$start_ts."'
							AND
							`account_deleted`.`deletion_date` < '".$end_ts."'
							AND
							`account_deleted`.`obsolete` = 0
							AND
							(
								`account_deleted`.`trainer_override` = 0
								OR
								`account_deleted`.`user_confirmed` = 1
							)
						) OR (
							`user_profiles`.`paid_period_ends` >= '".$start_ts."'
							AND
							`user_profiles`.`paid_period_ends` < '".$end_ts."'
						)
					)
					".$insert."
				GROUP BY `users`.`id`";

		$results = $this->db->query($sql);
		if($count){
			return $results->num_rows();
		} else {
			return $results->result_array();
		}
	}

	function get_users($search_params='', $arr_by_trainers=false) {
		$users = array();
		$user_params = '';
		$trainer_params = '';
		if(!empty($search_params)){
			$user_params .= "
				AND
					(
					`user_profiles`.`first_name` LIKE '%".$search_params."%'
					OR
					`user_profiles`.`last_name` LIKE '%".$search_params."%'
					OR
					CONCAT(`user_profiles`.`first_name`, ' ', `user_profiles`.`last_name`) LIKE '%".$search_params."%'
					OR
					`user_profiles`.`phone` LIKE '%".$search_params."%'
					OR
					`users`.`email` LIKE '%".$search_params."%'
					)
			";

			$trainer_params .= "
				AND
					(
					`trainer_profiles`.`first_name` LIKE '%".$search_params."%'
					OR
					`trainer_profiles`.`last_name` LIKE '%".$search_params."%'
					OR
					CONCAT(`trainer_profiles`.`first_name`, ' ', `trainer_profiles`.`last_name`) LIKE '%".$search_params."%'
					OR
					`trainer_profiles`.`contact` LIKE '%".$search_params."%'
					OR
					`users`.`email` LIKE '%".$search_params."%'
					)
			";
		}
		$users['trainers'] = $this->db->query("SELECT
			`users`.`email`,
			`users`.`banned` AS `is_suspended`,
			`users`.`id` AS `user_id`,
			UNIX_TIMESTAMP(`users`.`created`) AS `signup_date`,
			`trainer_profiles`.*
		FROM
			`users`
		INNER JOIN
			`trainer_profiles` ON `users`.`id`=`trainer_profiles`.`user_id`
		WHERE
			`users`.`is_trainer`=1
		".$trainer_params."
		ORDER BY UNIX_TIMESTAMP(`users`.`created`) DESC
		");

		if($users['trainers']->num_rows() < 1){
			unset($users['trainers']);
		} else {
			$users['trainers'] = $users['trainers']->result_array();
			foreach ($users['trainers'] as &$ar){

				$end_ts = time();
				$start_ts = 0;

				$total = $this->trainer_all_users($ar['user_id'], $end_ts, true);
				$lost = $this->trainer_lost_users($ar['user_id'], $start_ts, $end_ts, true);
				$suspend = $this->trainer_suspended_users($ar['user_id'],true);

				$ar['lost_clients'] = intval($lost);
				$ar['total_clients'] = intval($total);
				$ar['recent_clients'] = (intval($total) - intval($lost) - intval($suspend)); // Owen Hardman :: 27/10/14 :: Really active not recent!!
			}
		}

		$users['user_trainers'] = $this->db->query("SELECT
			`users`.`email`,
			`users`.`banned` AS `is_suspended`,
			`users`.`id` AS `user_id`,
			UNIX_TIMESTAMP(`users`.`created`) AS `signup_date`,
			`trainer_profiles`.*
		FROM
			`users`
		INNER JOIN
			`trainer_profiles` ON `users`.`id`=`trainer_profiles`.`user_id`
		WHERE
			`users`.`is_trainer`=1
		");

		if($users['user_trainers']->num_rows() < 1){
			unset($users['user_trainers']);
		} else {
			$users['user_trainers'] = $users['user_trainers']->result_array();
		}

		$users['clients'] = $this->db->query("SELECT
			`users`.`email`,
			`users`.`id` AS `user_id`,
			`users`.`banned` AS `is_suspended`,
			UNIX_TIMESTAMP(`users`.`created`) AS `signup_date`,
			`user_profiles`.`first_name`,
			`user_profiles`.`last_name`,
			`user_profiles`.`free`,
			`user_profiles`.`allocated_trainer`,
			`user_profiles`.`paid_period_ends`,
			`user_profiles`.`refer`,
			`training_types`.`name` AS `training_type`
		FROM
			`users`
		INNER JOIN
			`user_profiles` ON `users`.`id`=`user_profiles`.`user_id`
		LEFT JOIN
			`training_types` ON `user_profiles`.`training_type`=`training_types`.`id`
		WHERE
			`users`.`is_trainer`=0
		AND
			`users`.`is_admin`=0
		/* AND
			`user_profiles`.`paid_period_ends` >= '".time()."' */
		".$user_params."
		".(!empty($arr_by_trainers) ? ' ORDER BY `user_profiles`.`allocated_trainer` ASC':' ORDER BY UNIX_TIMESTAMP(`users`.`created`) DESC')."
		");

		$users['failed_medical'] = $this->db->query("SELECT
			`user_profiles`.`registration_email` AS `email`,
			`user_profiles`.`first_name`,
			`user_profiles`.`last_name`,
			`user_profiles`.`free`,
			`user_profiles`.`signup_started`,
			`user_profiles`.`id` AS `profile_id`
		FROM
			`user_profiles`
		INNER JOIN
			`pre_exercise_questions` ON `user_profiles`.`id`=`pre_exercise_questions`.`profile_id`
		WHERE
			`pre_exercise_questions`.`failed` = 1
		AND
			`pre_exercise_questions`.`admin_override` = 0
		");

		if($users['failed_medical']->num_rows() < 1){
			unset($users['failed_medical']);
		} else {
			$users['failed_medical'] = $users['failed_medical']->result_array();
		}

		if($users['clients']->num_rows() < 1){
			unset($users['clients']);
		} else {
			$users['clients'] = $users['clients']->result_array();
		}

		return $users;
	}

	function export_users(){
		$marital = $this->config->item('marital_statuses');
		$marital_statuses = array();
		foreach($marital as $id => $status){
			$marital_statuses[] = "
				WHEN `user_profiles`.`marital_status` = '".$id."' THEN '".$status."'
			";
		}

		$genders = $this->config->item('genders');
		$gender_types = array();
		foreach($genders as $id => $gender){
			$gender_types[] = "
				WHEN `user_profiles`.`gender` = '".$id."' THEN '".$gender."'
			";
		}

		$all_states = $this->config->item('states');
		$states = array();
		foreach($all_states as $id => $state){
			$states[] = "
				WHEN `user_profiles`.`state` = '".$id."' THEN '".$state['label']."'
			";
		}

		$users = $this->db->query("SELECT
			`users`.`email`,
			`users`.`created` AS `signup_date`,
			CONCAT(`trainer_profiles`.`first_name`, ' ', `trainer_profiles`.`last_name`) AS `trainer`,
			`training_types`.`name` AS `training_type`,
			`user_profiles`.`first_name`,
			`user_profiles`.`last_name`,
			`user_profiles`.`free`,
			CONCAT('\"',`user_profiles`.`phone`, '\"') AS `contact`,
			CASE WHEN
					`user_profiles`.`marketing` = 1 THEN 'Yes'
				ELSE
					'No'
			END AS `receive_marketing`,
			CASE
				".implode("\n", $marital_statuses)."
				ELSE
				'Undefined'
			END AS `marital_status`,
			CASE
				".implode("\n", $gender_types)."
				ELSE
				'Undefined'
			END AS `gender`,
			`user_profiles`.`age`,
			`user_profiles`.`street_address`,
			`user_profiles`.`suburb`,
			`user_profiles`.`postcode`,
			CASE
				".implode("\n", $states)."
				ELSE
				'Undefined'
			END AS `state`,
			CASE WHEN
				`user_profiles`.`paid_period_ends` < '".time()."' THEN 'Yes'
				ELSE
				'No'
			END AS 'account_expired'
		FROM
			`users`
		INNER JOIN
			`user_profiles` ON `users`.`id`=`user_profiles`.`user_id`
		LEFT JOIN
			`training_types` ON `user_profiles`.`training_type`=`training_types`.`id`
		LEFT JOIN
			`trainer_profiles` ON `user_profiles`.`allocated_trainer`=`trainer_profiles`.`user_id`
		WHERE
			`users`.`is_trainer`=0
		AND
			`users`.`is_admin`=0
		GROUP BY
			`users`.`id`
		");

		$this->load->dbutil();
		$this->load->helper('download');
		force_download('STC Users - '.date('d-m-Y H:i:s').'.csv', $this->dbutil->csv_from_result($users));
	}

	function get_exercises(){
		$exercises = $this->db->query("SELECT
			`exercises`.`id` AS `table_key`,
			`exercises`.`name` AS `exercise_name`,
			`exercises`.`exercise_identifier`
		FROM
			`exercises`
		WHERE
			`circuit` = 0
		AND
			`exercises`.`valid_to` IS NULL
		ORDER BY
			`exercises`.`exercise_identifier` ASC
		");
		if($exercises->num_rows() < 1){
			return false;
		}

		return $exercises->result_array();
	}

	function get_circuits(){
		$circuits = $this->db->query("SELECT
			`exercises`.`id` AS `table_key`,
			`exercises`.`name` AS `exercise_name`,
			`exercises`.`exercise_identifier`
		FROM
			`exercises`
		WHERE
			`circuit` = 1
		AND
			`exercises`.`valid_to` IS NULL
		ORDER BY
			`exercises`.`exercise_identifier` ASC
		");
		if($circuits->num_rows() < 1){
			return false;
		}

		return $circuits->result_array();
	}

	function get_training_types(){
		$types = $this->db->query("SELECT * FROM `training_types`");
		if($types->num_rows() < 1){
			return false;
		}
		return $types->result_array();
	}

	function get_training_phases($training_type=null){
		if(empty($training_type)){
			return false;
		}

		$phases = $this->db->query("SELECT
			`phases`.`id` AS `phase_literal_id`,
			`phases`.`phase_number`,
			`phases`.`name`
		FROM
			`phases`
		WHERE
			`phases`.`training_type`='".$training_type."'
		AND
			`phases`.`valid_to` IS NULL
		ORDER BY
			`phases`.`phase_number` ASC
		");

		if($phases->num_rows() < 1){
			return false;
		}

		return $phases->result_array();
	}

	function get_phase_sessions($training_type=null, $phase_number=null){
		if(empty($training_type) || empty($phase_number)){
			return false;
		}

		$sessions = $this->db->query("SELECT
			`sessions`.`session_identifier`,
			`sessions`.`name`,
			`phase_sessions`.`order` AS `session_number`
		FROM
			`phase_sessions`
		INNER JOIN
			`sessions` ON `phase_sessions`.`session_identifier`=`sessions`.`session_identifier`
		WHERE
			`phase_sessions`.`valid_to` IS NULL
		AND
			`phase_sessions`.`training_type`='".mysql_real_escape_string($training_type)."'
		AND
			`phase_sessions`.`phase_number`='".mysql_real_escape_string($phase_number)."'
		AND
			`sessions`.`valid_to` IS NULL
		ORDER BY
			`phase_sessions`.`order` ASC
		");

		if($sessions->num_rows() < 1){
			return false;
		}

		return $sessions->result_array();
	}

	function get_session_exercises($session_identifier){
		if(empty($session_identifier)){
			return false;
		}
		$exercises = $this->db->query("SELECT
			`exercises`.*,
			`session_content`.`reps`,
			`session_content`.`tempo`,
			`session_content`.`sets`,
			`session_content`.`order`
		FROM
			`sessions`
		INNER JOIN
			`session_content` ON `sessions`.`session_identifier`=`session_content`.`session_identifier`
		INNER JOIN
			`exercises` ON `session_content`.`exercise_identifier`=`exercises`.`exercise_identifier`
		WHERE
			`sessions`.`valid_to` IS NULL
		AND
			`session_content`.`valid_to` IS NULL
		AND
			`exercises`.`valid_to` IS NULL
		AND
			`sessions`.`session_identifier`='".mysql_real_escape_string($session_identifier)."'
		ORDER BY
			`session_content`.`order` ASC
		");

		if($exercises->num_rows() < 1){
			return false;
		}
		$return = array();
		foreach($exercises->result_array() as $exercise){
			if(empty($exercise['circuit'])){
				$return['exercises'][] = $exercise;
			} else {
				$return['circuits'][] = $exercise;
			}
		}
		return $return;
	}

	function add_trainer() {

	}

	function get_phase($phase_number=null, $training_type=null){
		if(empty($phase_number) || empty($training_type)){
			return false;
		}

		$phase = $this->db->query("SELECT
			`phases`.*
		FROM
			`phases`
		WHERE
			`phases`.`valid_to` IS NULL
		AND
			`phases`.`training_type`='".mysql_real_escape_string($training_type)."'
		AND
			`phases`.`phase_number`='".mysql_real_escape_string($phase_number)."'
		LIMIT
			1
		");

		if($phase->num_rows() < 1){
			return false;
		}

		return $phase->row_array();
	}

	function get_session($session_identifier=null){
		if(empty($session_identifier)){
			return false;
		}

		$session = $this->db->query("SELECT
			`sessions`.*,
			`phase_sessions`.`order`
		FROM
			`sessions`
		INNER JOIN
			`phase_sessions` ON `sessions`.`session_identifier`=`phase_sessions`.`session_identifier`
			AND
			`phase_sessions`.`valid_to` IS NULL
		WHERE
			`sessions`.`valid_to` IS NULL
		AND
			`sessions`.`session_identifier`='".mysql_real_escape_string($session_identifier)."'
		LIMIT
			1
		");
		if($session->num_rows() < 1){
			return false;
		}

		return $session->row_array();
	}

	function food_plans(){
		$food = $this->db->query("SELECT * FROM `food_plans`");
		if($food->num_rows() < 1){
			return false;
		}

		return $food->result_array();
	}

	function reports($options=array()){
		$default_options = array(
			'type' => 'revenue',
			'trainer' => null,
			'date_from' => date('d/m/Y', strtotime(date('Y-m-01').' -12 months')),
			'date_to' => date('d/m/Y'),
			'program_type' => null,
			'age_from' => null,
			'age_to' => null,
			'gender' => null,
			'marital_status' => null,
			'children' => null,
			'display' => 'months',
			'export' => false
		);

		$options = array_merge($default_options, $options);
		$options['date_from'] = $this->common->date_convert($options['date_from']);
		$options['date_to'] = $this->common->date_convert($options['date_to']);
		$graph_data = array();

		switch($options['display']){
			case 'days' :
				for($x=$options['date_from'];$x<=$options['date_to']; $x += 86400){
					$graph_data['data'][date('d/m/Y', $x)] = array(
						'y' => 0
					);
				}
				break;
			case 'months' :
				for($x=$options['date_from'];$x<$options['date_to']; $x = mktime(0,0,1, date('n', $x) + 1, 1, date('Y', $x))){
					$graph_data['data'][date('m/Y', $x)] = array(
						'y' => 0
					);
				}
				$graph_data['data'][date('m/Y', $options['date_to'])] = array(
					'y' => 0
				);
				break;
		}

		$joins = array();
		$and = array();
		if(!empty($options['trainer'])){
			$and[] = "
				AND
					`user_profiles`.`allocated_trainer`='".mysql_real_escape_string($options['trainer'])."'
			";
		}
		if(!is_null($options['program_type'])){
			$and[] = "
					AND
					`user_profiles`.`training_type`='".mysql_real_escape_string($options['program_type'])."'
			";
		}
		if(!is_null($options['gender'])){
			$and[] = "
				AND
					`user_profiles`.`gender`='".mysql_real_escape_string($options['gender'])."'
			";
		}
		if(!is_null($options['age_from'])){
			$and[] = "
				AND
					`user_profiles`.`age` >= '".mysql_real_escape_string($options['age_from'])."'
			";
		}
		if(!is_null($options['age_to'])){
			$and[] = "
				AND
					`user_profiles`.`age` <= '".mysql_real_escape_string($options['age_to'])."'
			";
		}
		if(!is_null($options['marital_status'])){
			$and[] = "
				AND
					`user_profiles`.`marital_status` = '".mysql_real_escape_string($options['marital_status'])."'
			";
		}
		if(!is_null($options['children'])){
			$and[] = "
				AND
					`user_profiles`.`number_of_children` = '".mysql_real_escape_string($options['children'])."'
			";
		}

		$graph_data['prefix'] = '$';
		$graph_data['graph_title'] = 'Total Revenue';
		$graph_data['decimal_places'] = 2;
		$graph_data['line_colour'] = "#f15e52";


		switch($options['type']){
			case 'revenue':
				$query = $this->db->query("SELECT
					`billing_history`.*,
					FROM_UNIXTIME(`billing_history`.`date`) AS `actual_date`,
					`user_profiles`.`first_name`,
					`user_profiles`.`last_name`,
					CONCAT('\"',`user_profiles`.`phone`,'\"') AS `phone`
				FROM
					`billing_history`
				INNER JOIN
					`user_profiles` ON `billing_history`.`user_id`=`user_profiles`.`user_id`
					".(!empty($joins) ? implode("\n", $joins):'')."
				WHERE
					`billing_history`.`date` >= '".mysql_real_escape_string($options['date_from'])."'
				AND
					`billing_history`.`date` < '".mysql_real_escape_string($options['date_to'])."'
					".(!empty($and) ? implode("\n", $and):'')."
				");
				if($query->num_rows() > 0){
					switch($options['display']){
						case 'days':
							foreach($query->result_array() as $history){
								if(isset($graph_data['data'][date('d/m/Y', $history['date'])])){
									$graph_data['data'][date('d/m/Y', $history['date'])]['y'] += floatval($history['amount']);
								}
							}
							break;
						case 'months':
							foreach($query->result_array() as $history){
								if(isset($graph_data['data'][date('m/Y', $history['date'])])){
									$graph_data['data'][date('m/Y', $history['date'])]['y'] += floatval($history['amount']);
								}
							}
							break;
					}

				}
				break;
			case 'signups':
				$graph_data['graph_title'] = 'Total Signups';
				$graph_data['line_colour'] = "#f9a340";
				$graph_data['prefix'] = '';
				$graph_data['decimal_places'] = 0;
				$query = $this->db->query("SELECT
					`user_profiles`.*,
					FROM_UNIXTIME(`user_profiles`.`signup_started`) AS `actual_signup_start_date`
				FROM
					`user_profiles`
					".(!empty($joins) ? implode("\n", $joins):'')."
				WHERE
					`user_profiles`.`signup_started` >= '".mysql_real_escape_string($options['date_from'])."'
				AND
					`user_profiles`.`signup_started` < '".mysql_real_escape_string($options['date_to'])."'
					".(!empty($and) ? implode("\n", $and):'')."
				");
				if($query->num_rows() > 0){
					switch($options['display']){
						case 'days':
							foreach($query->result_array() as $signups){
								if(isset($graph_data['data'][date('d/m/Y', $signups['signup_started'])])){
									$graph_data['data'][date('d/m/Y', $signups['signup_started'])]['y'] += 1;
								}
							}
							break;
						case 'months':
							foreach($query->result_array() as $signups){
								if(isset($graph_data['data'][date('m/Y', $signups['signup_started'])])){
									$graph_data['data'][date('m/Y', $signups['signup_started'])]['y'] += 1;
								}
							}
							break;
					}

				}
				break;
			case 'users':
				$graph_data['graph_title'] = 'Total Users';
				$graph_data['line_colour'] = "#20a4f7";
				$graph_data['prefix'] = '';
				$graph_data['decimal_places'] = 0;
				$query = $this->db->query("SELECT
					UNIX_TIMESTAMP(`users`.`created`) AS `signed_up`,
					`users`.`created` AS `acutal_signup_date`,
					FROM_UNIXTIME(`user_profiles`.`paid_period_ends`) AS `paid_period_ends_actual`,
					`user_profiles`.*
				FROM
					`user_profiles`
				INNER JOIN
					`users` ON `user_profiles`.`user_id`=`users`.`id`
					".(!empty($joins) ? implode("\n", $joins):'')."
				WHERE
					`user_profiles`.`signup_complete`=1
				AND
					UNIX_TIMESTAMP(`users`.`created`) >= '".mysql_real_escape_string($options['date_from'])."'
				AND
					UNIX_TIMESTAMP(`users`.`created`) < '".mysql_real_escape_string($options['date_to'])."'
					".(!empty($and) ? implode("\n", $and):'')."
				");
				if($query->num_rows() > 0){
					switch($options['display']){
						case 'days':
							foreach($query->result_array() as $users){
								if(isset($graph_data['data'][date('d/m/Y', $users['signed_up'])])){
									$graph_data['data'][date('d/m/Y', $users['signed_up'])]['y'] += 1;
								}
							}
							break;
						case 'months':
							foreach($query->result_array() as $users){
								if(isset($graph_data['data'][date('m/Y', $users['signed_up'])])){
									$graph_data['data'][date('m/Y', $users['signed_up'])]['y'] += 1;
								}
							}
							break;
					}

				}
				break;
		}
		$graph_data['show_labels'] = true;
		if(!empty($graph_data['data']) && count(array_keys($graph_data['data'])) > 13){
			$graph_data['show_labels'] = false;
		}

		return $graph_data;

	}

	function get_trainers(){
		$trainers = $this->db->query("SELECT
			`users`.`email`,
			`users`.`id` AS `user_id`,
			`trainer_profiles`.*
		FROM
			`users`
		INNER JOIN
			`trainer_profiles` ON `users`.`id`=`trainer_profiles`.`user_id`
		WHERE
			`users`.`is_trainer`=1
		");

		if($trainers->num_rows() < 1){
			return false;
		}

		return $trainers->result_array();
	}
}

?>
