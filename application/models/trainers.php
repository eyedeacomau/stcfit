<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * The trainer model is where any necessary SQL goes for the trainer section.
 */
class Trainers extends CI_Model {
	function __construct() {
		parent::__construct();
	}

	function get_trainer_data(){
		if($this->tank_auth->is_logged_in() == false){
			return false;
		}
		$user_id = $this->tank_auth->get_user_id();
		$trainer = $this->db->query("SELECT
			`users`.*,
			`users`.`id` AS `trainer_id`,
			`trainer_profiles`.*,
			COALESCE((SELECT
				COUNT(`messages`.`id`)
			FROM
				`messages`
			WHERE
				`messages`.`for_user_id`=`users`.`id`
			AND
				`messages`.`unread`=1
			),0) `unread_messages`,
			COALESCE((SELECT
				COUNT(`trainer_reminders`.`id`)
			FROM
				`trainer_reminders`
			WHERE
				`trainer_reminders`.`obsolete`=0
			AND
				`trainer_reminders`.`trainer_id`=`users`.`id`
			AND
				`trainer_reminders`.`reminder_date` <= '".time()."'
			), 0) `trainer_reminders`
		FROM
			`users`
		LEFT JOIN
			`trainer_profiles` ON `users`.`id`=`trainer_profiles`.`user_id`
		WHERE
			`users`.`id`='".$user_id."'
		LIMIT
			1");

		if($trainer->num_rows() < 1){
			return false;
		}

		return $trainer->row_array();
	}

	function lost_clients(){

		$this->load->model('admin');

		if($this->tank_auth->is_logged_in() == false){
			return false;
		}
		$user_id = $this->tank_auth->get_user_id();

		$return = array();
		$return['month_data'] = array();
		$return['reasons']['total'] = 0;
		$return['reasons']['info'] = $this->config->item('leave_reasons');
		$return['current_clients'] = intval($this->admin->trainer_all_users($user_id, time(), true)) - intval($this->admin->trainer_suspended_users($user_id, true));
		
		$user_ids = array();
		
		for($x=TRAINER_DASH_PERIOD;$x>0;$x--){
			$total = $lost = $suspend = 0;

			$month = date('F', strtotime(date('Y-m-01').' -'.($x-1).' months'));

			$start_ts = strtotime(date('Y-m-01 0:00:00', strtotime('-'.($x-1).' months')));
			$end_ts = strtotime(date('Y-m-t 23:59:59', strtotime('-'.($x-1).' months')));
			$end_ts = min($end_ts,time());

			$total = $this->admin->trainer_all_users($user_id, $end_ts, true);
			$lost = $this->admin->trainer_lost_users($user_id, $start_ts, $end_ts, false, $user_ids);

			$return['reasons']['total']+= count($lost);

			foreach ($lost as $lost_user){
				if(!in_array($lost_user['id'], $user_ids)){
					$user_ids[] = $lost_user['id'];
					
					$user_detail = $this->db->query("
						SELECT
							*,
							`account_deleted`.`id` AS `deletion_id`
						FROM
							`users`
						INNER JOIN
							`user_profiles`
							ON
							`users`.`id` = `user_profiles`.`user_id`
						LEFT JOIN
							`account_deleted`
							ON
							`users`.`id` = `account_deleted`.`user_id`
						WHERE
							`users`.`id` = '".$lost_user['id']."'
						LIMIT
							1
					");
					$user_detail = $user_detail->row_array();
	
					if(is_null($user_detail['reason'])){
						$user_detail['reason'] = 6; // force mark them as unpaid
						$user_detail['deletion_date'] = $user_detail['paid_period_ends'];
					}
	
					$return['reasons']['info'][$user_detail['reason']]['total'] += 1;
					$return['lost_clients'][] = $user_detail;
				}
			}

			$return['month_data'][$month] = array('monthly_total'=>intval($total),'monthly_lost'=>count($lost));
			
		}

		return $return;
	}

	function trainer_reminders($trainer_id=null){
		if(empty($trainer_id)){
			return false;
		}

		$reminders = $this->db->query("SELECT
			`trainer_reminders`.*,
			`user_profiles`.*,
			COALESCE((SELECT
				`progress_photos`.`image_name`
			FROM
				`progress_photos`
			WHERE
				`progress_photos`.`user_id`=`user_profiles`.`user_id`
			ORDER BY
				`progress_photos`.`id` DESC
			LIMIT
				1
			), NULL) `progress_photo`
		FROM
			`trainer_reminders`
		INNER JOIN
			`user_profiles` ON `trainer_reminders`.`client_id`=`user_profiles`.`user_id`
		WHERE
			`trainer_reminders`.`obsolete` = 0
		AND
			`trainer_reminders`.`trainer_id`='".mysql_real_escape_string($trainer_id)."'
		AND
			`trainer_reminders`.`reminder_date` <= '".time()."'
		");

		if($reminders->num_rows() < 1){
			return false;
		}

		$reminders = $reminders->result_array();
		// fold in recent measurement entries.
		foreach($reminders as &$user){
			$first_entry = $this->db->query("SELECT
				`measurement_entries`.*
			FROM
				`measurement_entries`
			WHERE
				`measurement_entries`.`user_id`='".$user['user_id']."'
			ORDER BY
				`measurement_entries`.`id` ASC
			LIMIT
				1
			");

			$and = '';
			if($first_entry->num_rows() < 1){
				$first_entry = array();
			} else {
				$first_entry = $first_entry->result_array();
				$and .= '
				AND
					`measurement_entries`.`id` NOT IN('.$first_entry[0]['id'].')
				';
			}

			$entries = $this->db->query("SELECT
				`measurement_entries`.*
			FROM
				`measurement_entries`
			WHERE
				`measurement_entries`.`user_id`='".$user['user_id']."'
			".$and."
			ORDER BY
				`measurement_entries`.`id` DESC
			LIMIT
				2
			");

			if($entries->num_rows() < 1){
				$entries = array();
			} else {
				$entries = array_reverse($entries->result_array());
			}

			$user['entries'] = array();
			if(!empty($first_entry) || !empty($entries)){
				$user['entries'] = array_merge($first_entry, $entries);
				$last_entry = $user['entries'][(count($user['entries'])-1)];
				$user['body_fat'] = $this->common->calculate_body_fat($user['gender'], $last_entry);
			}
		}

		return $reminders;
	}

	function get_clients($trainer_id=null, $search_params=''){
		if(empty($trainer_id)){
			return false;
		}

		$extra = '';
		if(!empty($search_params)){
			$search_params = mysql_real_escape_string($search_params);
			$extra .= "
			AND
				(
				`user_profiles`.`first_name` LIKE '%".$search_params."%'
				OR
				`user_profiles`.`last_name` LIKE '%".$search_params."%'
				OR
				CONCAT(`user_profiles`.`first_name`, ' ', `user_profiles`.`last_name`) LIKE '%".$search_params."%'
				OR
				`users`.`email` LIKE '%".$search_params."%'
				OR
				`user_profiles`.`phone` LIKE '%".$search_params."%'
				)
			";
		}

		$clients = $this->db->query("SELECT
			`user_profiles`.*,
			UNIX_TIMESTAMP(`users`.`created`) AS `signup_date`,
			`users`.`email` AS `user_email`,
			`users`.`banned` AS `is_suspended`
		FROM
			`users`
		INNER JOIN
			`user_profiles` ON `users`.`id`=`user_profiles`.`user_id`
		LEFT JOIN
			`account_deleted` ON `user_profiles`.`user_id`=`account_deleted`.`user_id`
			AND
			`account_deleted`.`obsolete` = 0
			AND
			(
				`account_deleted`.`defer_til` = 0
				AND
				`account_deleted`.`trainer_override`= 0
				OR
				`account_deleted`.`user_confirmed` = 1
			)
		WHERE
			`user_profiles`.`allocated_trainer`='".mysql_real_escape_string($trainer_id)."'
		AND
			(
				`user_profiles`.`paid_period_ends` > ".time()."
				OR
				`user_profiles`.`free` = 1
			)
		AND
			`account_deleted`.`id` IS NULL
		".$extra."
		");

		if($clients->num_rows() < 1){
			return false;
		}
		return $clients->result_array();
	}

	function get_programs(){
		$types = $this->db->query("SELECT
			`training_types`.`name` AS `type_name`,
			`training_types`.`id` AS `type_id`,
			COALESCE((SELECT
				COUNT(`id`)
			FROM
				`phases`
			WHERE
				`phases`.`valid_to` IS NULL
			AND
				`phases`.`training_type`=`training_types`.`id`
			), 0) `total_phases`
		FROM
			`training_types`
		ORDER BY
			`training_types`.`id` ASC
		");

		if($types->num_rows() < 1){
			return false;
		}

		$types = $types->result_array();
		foreach($types as &$type){
			$type['phases'] = $this->get_program_phases($type['type_id']);
		}

		return $types;
	}

	function get_program_phases($type=null, $page=0){
		if(empty($type)){
			return false;
		}

		$page = intval($page);

		$phases = $this->db->query("SELECT
			`phases`.*
		FROM
			`phases`
		WHERE
			`phases`.`valid_to` IS NULL
		AND
			`phases`.`training_type` = '".mysql_real_escape_string($type['type_id'])."'
		ORDER BY
			`phases`.`phase_number` ASC
		LIMIT
			".(!empty($page) && $page != 1 ? (($page-1)*PHASES_PER_PAGE):'0').",".PHASES_PER_PAGE
		);

		if($phases->num_rows() < 1){
			return false;
		}
		$return = '';
		foreach($phases->result_array() as $phase){
			$return .= '
				<div class="phase-row">
					<span class="phase-name">Phase '.$phase['phase_number'].' - '.$phase['name'].'</span>
					<a href="#" data-phaseid="'.$phase['id'].'" class="view-phase btn btn-primary">Enter Program</a>
					<p class="phase-description">'.$phase['description'].'</p>
				</div>
			';
		}

		return $return;
	}

	function get_messages($trainer_id=null){
		if(empty($trainer_id)){
			return false;
		}

		$messages = $this->db->query("SELECT
			`messages`.*,
			CONCAT(`user_profiles`.`first_name`, ' ', `user_profiles`.`last_name`) AS `conversation_name`,
			`user_profiles`.`user_id`
		FROM
			`messages`
		LEFT JOIN
			`user_profiles` ON `messages`.`from_user_id`=`user_profiles`.`user_id`
			OR
			`messages`.`for_user_id`=`user_profiles`.`user_id`
		WHERE
			`messages`.`for_user_id`='".mysql_real_escape_string($trainer_id)."'
		OR
			`messages`.`from_user_id`='".mysql_real_escape_string($trainer_id)."'
		ORDER BY
			`messages`.`id` ASC
		");

		if($messages->num_rows() < 1){
			return false;
		}

		$messages = $messages->result_array();
		$conversations = array();
		// user_id will be null if the message is from the trainer.
		foreach($messages as $message){
			if(empty($conversations[$message['user_id']])){
				$conversations[$message['user_id']] = array(
					'name' => $message['conversation_name'],
					'messages' => array(),
					'unread' => 0,
					'trainer_id' => $trainer_id
				);
			}
			if(!empty($message['unread']) && $message['for_user_id'] == $trainer_id){
				$conversations[$message['user_id']]['unread'] += 1;
			}

			$conversations[$message['user_id']]['messages'][] = $message;
		}

		return $conversations;
	}
}

?>
