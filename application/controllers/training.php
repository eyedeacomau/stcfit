<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Training extends Users_Controller {
	function __construct(){
		parent::__construct();
		if(!$this->tank_auth->is_logged_in()){ // Checking if the user is logged in.
			$this->session->set_flashdata('errors', array('You must be logged in to access the dashboard area.'));
			redirect('/');
		}
		$this->user_id = $this->tank_auth->get_user_id();
	}

	function index($phase=0) {
		$errors = array();
		$sessions = array();
		$user_sessions = array();
		$current_phase = array();

		$this->load->model('data');
		$this->load->model('phase');
		$user = $this->data->get_all_userdata($this->user_id);

		// Gets current phase. If no current phase. Attempts to make one.
		$current_phase = null;
		$phase_sessions = null;

		if(!$current_phase = $this->phase->get_current_phase($this->user_id,$phase)){
			if($phase==0){
				if($this->phase->create_phase()){
					$current_phase = $this->phase->get_current_phase($this->user_id);
					$session_progress = $this->phase->get_progress($user, $user['training_type'], $this->user_id);
				}
			}
		}

		if(!empty($current_phase)){
			$phase_sessions = $this->phase->get_current_sessions($this->user_id,$phase);
		} else {
			$errors[] = "Unable to generate a phase based on this user's preferences. If this is not rectified shortly. Please contact an STC Fit administrator.";
		}

		$persist_current_phase = $this->phase->get_current_phase($this->user_id);
		$end_phase = $this->phase->get_end_phase($this->user_id);

		$this->common->display(array(
			'view' => 'dashboard/training/index',
			'template' => 'templates/dashboard',
			'title' => 'Training',
			'extra_css' => array(
				CSS_PATH.'dashboard.css'
			),
			'template_data' => array(
				'name' => $user['first_name'],
				'full_width' => true,
				'unread' => $this->data->unread_messages()
			),
			'content' => array(
				'errors' => implode('<br />', $this->common->merge_flash('errors', $errors)),
				'phase_sessions' => $phase_sessions,
				'current_phase' => $current_phase,
				'user' => $user,
				'end_phase' => $end_phase,
				'persist_current_phase' => $persist_current_phase
			)
		));
	}

	function session($session_identifier=null){
		$this->load->model('data');
		$this->load->model('phase');
		$errors = array();
		$success_messages = array();
		$user = $this->data->get_all_userdata($this->user_id);

		if($this->input->post('update_session')){
			$session_complete = true;
			$batch = array();
			// First deleting the entry in completed sessions if it exists
			$this->db->where('user_id', $this->user_id);
			$this->db->where('session_identifier', $session_identifier);
			$this->db->delete('completed_sessions');
			$entries = $this->input->post('entry');
			if(!empty($entries)){
				foreach($entries as $entry_id => $exercise_data){
					$initial = array(
						'user_id' => $this->user_id,
						'entry_id' => $entry_id,
						'session_identifier' => $session_identifier
					);
					$json_data = array();
					foreach($exercise_data['data'] as $entry_number => $sets){
						$json_data[$entry_number] = $sets;
						foreach($sets as $set_number => $set_data){
							if(empty($set_data['weight']) || empty($set_data['reps'])){
								$session_complete = false;
							}
						}
					}
					$initial['entry_data'] = json_encode($json_data);
					$batch[] = $initial;
				}
			}

			$cicuit_entries = $this->input->post('circuit_entry');
			if(!empty($cicuit_entries)){
				foreach($cicuit_entries as $entry_id => $circuit_data){
					$initial = array(
						'user_id' => $this->user_id,
						'entry_id' => $entry_id,
						'session_identifier' => $session_identifier
					);
					$json_data = array();
					foreach($circuit_data as $entry_number => $data){
						$json_data[$entry_number] = $data;
						if(empty($data['data'])){
							$session_complete = false;
						}
					}
					$initial['entry_data'] = json_encode($json_data);
					$batch[] = $initial;
				}
			}


			$this->db->where('session_identifier', $session_identifier);
			$this->db->where('user_id', $this->user_id);
			$this->db->delete('user_entries');

			$this->db->insert_batch('user_entries', $batch);
			if($session_complete == true){
				$this->db->set(array(
					'user_id' => $this->user_id,
					'session_identifier' => $session_identifier,
					'completed_on' => time()
				));
				$this->db->insert('completed_sessions');
			}

			$success_messages[] = 'Successfully updated session data.';
		}
		$session_data = $this->phase->get_session_exercise_data($this->user_id, $session_identifier);
		$exercises = array();
		$circuits = array();

		if(!empty($session_data)){
			foreach($session_data as $exercise_data){
				$user_entries = array();
				if(!empty($exercise_data['entry_data'])){
					$user_entries = json_decode($exercise_data['entry_data'], true);
				}

				if(empty($exercise_data['is_circuit'])){
					$exercises[] = array(
						'entry_id' => $exercise_data['id'],
						'exercise_id' => $exercise_data['exercise_id'],
						'exercise_identifier' => $exercise_data['exercise_identifier'],
						'exercise_name' => (!empty($exercise_data['exercise_name']) ? $exercise_data['exercise_name']:'Undefined.'),
						'exercise_sets' => $exercise_data['sets'],
						'exercise_reps' => $exercise_data['reps'],
						'exercise_tempo' => $exercise_data['tempo'],
						'exercise_data' => $user_entries
					);
				} else {
					$circuits[] = array(
						'entry_id' => $exercise_data['id'],
						'exercise_id' => $exercise_data['exercise_id'],
						'exercise_identifier' => $exercise_data['exercise_identifier'],
						'exercise_name' => (!empty($exercise_data['exercise_name']) ? $exercise_data['exercise_name']:'Undefined.'),
						'exercise_description' => (!empty($exercise_data['description']) ? $exercise_data['description']:'Undefined.'),
						'exercise_data' => $user_entries
					);
				}

			}
		}

		$this_phase = array(
			'name' => $session_data[0]['phase_name'],
			'phase_number' => $session_data[0]['phase_number'],
			'type_name' => $session_data[0]['training_type_name']
		);

		$this_session = array(
			'name' => $session_data[0]['session_name'],
			'description' => $session_data[0]['session_description'],
			'video_link' => $session_data[0]['session_video_link'],
			'session_number' => $session_data[0]['session_number']
		);

		$this->common->display(array(
			'view' => 'dashboard/training/session',
			'template' => 'templates/dashboard',
			'title' => 'Session Entry',
			'extra_css' => array(
				CSS_PATH.'dashboard.css'
			),
			'template_data' => array(
				'name' => $user['first_name'],
				'full_width' => true,
				'unread' => $this->data->unread_messages()
			),
			'content' => array(
				'errors' => implode('<br />', $this->common->merge_flash('errors', $errors)),
				'success_messages' => implode('<br />', $this->common->merge_flash('success_messages', $success_messages)),
				'phase' => $this_phase,
				'session' => $this_session,
				'exercises' => $exercises,
				'circuits' => $circuits,
				'session_identifier' => $session_identifier,
				'user' => $user
			)
		));
	}

	function exercise($exercise_id=null){
		if(!empty($exercise_id) && $this->input->is_ajax_request()){
			$exercise = $this->db->query("SELECT * FROM `exercises` WHERE `exercises`.`id`='".mysql_real_escape_string($exercise_id)."' LIMIT 1");
			if($exercise->num_rows() < 1){
				$this->common->display(array(
					'view' => '4oh4',
					'title' => '404 - Page Not Found!',
					'template' => 'templates/page',
					'content' => array()
				));
			} else {
				$exercise = $exercise->row_array();
				$this->load->view('includes/view_exercise', array('exercise' => $exercise));
			}
		} else {
			$this->common->display(array(
				'view' => '4oh4',
				'title' => '404 - Page Not Found!',
				'template' => 'templates/page',
				'content' => array()
			));
		}
	}
}
