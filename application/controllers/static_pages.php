<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Static_Pages extends CI_Controller {
	function __construct(){
		parent::__construct();
	}

	function terms_and_conditions(){
		$this->common->display(array(
			'view' => 'static/terms_and_conditions',
			'title' => 'Terms and Conditions',
			'content' => array()
		));
	}

	function how_it_works(){
		$this->common->display(array(
			'view' => 'static/how_it_works',
			'title' => 'How It Works',
			'content' => array()
		));
	}

	function weight_loss(){
		$this->common->display(array(
			'view' => 'static/weight_loss',
			'title' => 'Weight Loss and Fat Reduction',
			'content' => array()
		));
	}

	function pricing(){
		$this->common->display(array(
			'view' => 'static/pricing',
			'title' => 'Pricing',
			'content' => array()
		));
	}

	function results(){
		$this->common->display(array(
			'view' => 'static/results',
			'title' => 'Results',
			'content' => array()
		));
	}

	function trial_success(){
		$this->common->display(array(
			'view' => 'static/trial_success',
			'title' => '7 Day Free Trial Signup Complete',
			'content' => array()
		));
	}

	function contact_us(){
		$errors = array();
		$success_messages = array();

		// If a contact form has been posted
		if($this->input->post('send_enquiry')){
			$this->load->library('form_validation');
			$this->form_validation->set_rules(array(
				array('field' => 'name', 'label' => 'Name', 'rules' => 'required|trim'),
				array('field' => 'email', 'label' => 'Email', 'rules' => 'required|trim|valid_email'),
				array('field' => 'enquiry', 'label' => 'Enquiry', 'rules' => 'required|trim')
			));

			$response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6LcC8VIUAAAAAP_06MC01Xah0FH3uWNHFvOTiUnD&response=".$this->input->post('g-recaptcha-response'));
			$obj = json_decode($response);

			if(		stristr($this->input->post('enquiry'),'essay') !== FALSE
				||	stristr($this->input->post('enquiry'),'http') !== FALSE
				||	stristr($this->input->post('enquiry'),'www') !== FALSE
				){
				$obj->success = false;
			}

			if($this->form_validation->run() && $obj->success == true){
				$this->common->send_email(array(
					'email_to' => FOOTER_EMAIL,
					'subject' => 'New Enquiry',
					'view' => 'new_enquiry',
					'data' => array(
						'admin' => TRUE,
						'name' => $this->input->post('name'),
						'email' => $this->input->post('email'),
						'enquiry' => $this->input->post('enquiry')
					)
				));

				$this->common->send_email(array(
					'email_to' => $this->input->post('email'),
					'subject' => 'Your Enquiry',
					'view' => 'new_enquiry',
					'data' => array(
						'admin' => FALSE,
						'name' => $this->input->post('name'),
						'email' => $this->input->post('email'),
						'enquiry' => $this->input->post('enquiry')
					)
				));
				redirect('/contact-success');
			} else {
				$errors = $this->form_validation->error_array();
			}
		}

		$this->common->display(array(
			'view' => 'static/contact_us',
			'title' => 'Contact Us',
			'content' => array(
				'name' => $this->input->post('name'),
				'email' => $this->input->post('email'),
				'enquiry' => $this->input->post('enquiry'),
				'errors' => implode('<br />', $this->common->merge_flash('errors', $errors)),
				'success_messages' => implode('<br />', $this->common->merge_flash('success_messages', $success_messages))
			)
		));
	}

	function contact_success(){
		$this->common->display(array(
			'view' => 'static/contact_success',
			'title' => 'Contact Success',
			'content' => array()
		));
	}
}
