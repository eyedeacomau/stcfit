<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Settings extends Users_Controller {
	function __construct(){
		parent::__construct();
		if($this->tank_auth->is_logged_in() == false){
			$this->session->set_flashdata('errors', array('You must be logged in to access the dashboard area.'));
			redirect('/login');
		}

		$this->user_id = $this->tank_auth->get_user_id();
		$this->load->model('data');
	}

	function index() {
		redirect('/settings/my-profile');
	}

	function my_profile() {
		$errors = array();
		$success_messages = array();
		$this->load->library('form_validation');
		$user = $this->data->get_all_userdata($this->user_id);

		if($this->input->post('profile_save')){
			$this->form_validation->set_rules(array(
				array('field' => 'first_name', 'label' => 'First Name', 'rules' => 'required|trim'),
				array('field' => 'last_name', 'label' => 'Last Name', 'rules' => 'required|trim'),
				array('field' => 'password', 'label' => 'Password', 'rules' => 'xss_clean|min_length[8]'),
				array('field' => 'confirm_password', 'label' => 'Confirm Password', 'rules' => 'xss_clean|min_length[8]|matches[password]'),
				array('field' => 'phone', 'label' => 'Phone', 'rules' => 'required|trim'),
				array('field' => 'gender', 'label' => 'Gender', 'rules' => 'required|trim'),
				array('field' => 'age', 'label' => 'Age', 'rules' => 'required|trim'),
				array('field' => 'marital_status', 'label' => 'Marital Status', 'rules' => 'required|trim'),
				array('field' => 'number_of_children', 'label' => 'Number of Children', 'rules' => 'required|trim'),
				array('field' => 'street_address', 'label' => 'Street Address', 'rules' => 'required|trim'),
				array('field' => 'suburb', 'label' => 'Suburb / City', 'rules' => 'required|xss_clean|trim'),
				array('field' => 'state', 'label' => 'State', 'rules' => 'required|trim'),
				array('field' => 'country', 'label' => 'Country', 'rules' => 'required|trim'),
				array('field' => 'postcode', 'label' => 'Postcode', 'rules' => 'trim|numeric'),
				array('field' => 'training_type', 'label' => 'Training Type', 'rules' => 'required|trim')
			));

			if($this->form_validation->run() == true){
				$data = array(
					'first_name' => $this->input->post('first_name'),
					'last_name' => $this->input->post('last_name'),
					'phone' => $this->input->post('phone'),
					'gender' => $this->input->post('gender'),
					'age' => $this->input->post('age'),
					'marital_status' => $this->input->post('marital_status'),
					'number_of_children' => $this->input->post('number_of_children'),
					'street_address' => $this->input->post('street_address'),
					'suburb' => $this->input->post('suburb'),
					'state' => $this->input->post('state'),
					'country' => $this->input->post('country'),
					'postcode' => $this->input->post('postcode')
				);
				$this->db->set($data);
				$this->db->where('user_id', $this->user_id);
				$this->db->update('user_profiles');

				$password = $this->input->post('password');
				if(!empty($password)){
					if($this->tank_auth->change_password('', $password, true)){
						$success_messages[] = 'Your password has now been changed!';
					} else {
						$errors[] = 'Could not save new password.';
					}
				}

				if($this->input->post('training_type') != $user['training_type']){
					$this->load->model('phase');
					// param_1 = user_id, param_2 = to_type.
					$new_phase = $this->phase->change_training_type($this->user_id, $this->input->post('training_type'));
					if($new_phase == true){
						$success_messages[] = 'Your training type has now been changed.';
						$data['training_type'] = $this->input->post('training_type');
					} else {
						$errors[] = 'Unable to change your training type at this time.';
					}
				}

				if($this->input->post('email') != $user['email']){
					$email = mysql_real_escape_string($this->input->post('email'));
					if(filter_var($email, FILTER_VALIDATE_EMAIL)){
						$check = $this->db->query("SELECT `email` FROM `users` WHERE `email`='".$email."' LIMIT 1");
						if($check->num_rows() < 1){
							$this->db->set(array(
								'email' => $email
							));
							$this->db->where('id', $this->user_id);
							$this->db->update('users');
							$data['email'] = $email;
						} else {
							$errors[] = 'The email that you entered is currently in use by another user.';
						}
					} else {
						$errors[] = 'The email that you entered is invalid.';
					}
				}

				$user = array_merge($user, $data);
				$success_messages[] = 'Your details have been saved';
			} else {
				$errors = array_merge($errors, $this->form_validation->error_array());
			}
		}


		$training_types = $this->db->query("SELECT * FROM `training_types`")->result_array();

		$this->common->display(array(
			'view' => 'dashboard/settings/my_profile',
			'template' => 'templates/dashboard',
			'extra_css' => array(
				CSS_PATH.'dashboard.css'
			),
			'title' => 'My Profile',
			'template_data' => array(
				'name' => $user['first_name'],
				'unread' => $this->data->unread_messages()
			),
			'content' => array(
				'errors' => implode('<br />', $this->common->merge_flash('errors', $errors)),
				'success_messages' => implode('<br />', $this->common->merge_flash('success_messages', $success_messages)),
				'user' => $user,
				'training_types' => $training_types
			)
		));
	}

	function my_plan() {
		$errors = array();
		$success_messages = array();
		$user = $this->data->get_all_userdata($this->user_id);
		$payment_methods = $this->config->item('payment_methods');

		if($this->input->post('update_method')){
			$method = 4; // Setting payment method to yearly initially.
			if($this->input->post('payment_frequency') == 1 && !empty($payment_methods[$this->input->post('payment_method')])){
				$method = intval($this->input->post('payment_method')); // If the user has selected another payment method.
			}

			$chosen_method = $payment_methods[$method];
			$this->db->set(array(
				'renewal_type' => $method
			));
			$this->db->where('user_id', $user['user_id']);
			$this->db->update('user_profiles');
			$user['renewal_type'] = $method;

			$success_messages[] = 'You have successfully updated your account renewal type.';
		}

		$this->common->display(array(
			'view' => 'dashboard/settings/my_plan',
			'template' => 'templates/dashboard',
			'title' => 'My Plan',
			'extra_css' => array(
				CSS_PATH.'dashboard.css'
			),
			'template_data' => array(
				'name' => $user['first_name'],
				'unread' => $this->data->unread_messages()
			),
			'content' => array(
				'errors' => implode('<br />', $this->common->merge_flash('errors', $errors)),
				'success_messages' => implode('<br />', $this->common->merge_flash('success_messages', $success_messages)),
				'payment_methods' => $payment_methods,
				'user' => $user
			)
		));
	}

	function billing($tab=null) {
		$this->load->model('payment');
		$this->load->library('form_validation');
		$errors = array();
		$success_messages = array();

		$cc_details = array();
		$user = $this->data->get_all_userdata($this->user_id);

		if($this->input->post('update_card') && !empty($user['eway_token'])){
			$this->form_validation->set_rules(array(
				array('field' => 'card_name', 'label' => 'Name On Card', 'rules' => 'trim|required'),
				array('field' => 'card_number', 'label' => 'Credit Card Number', 'rules' => 'trim|luhn_check|required'),
				array('field' => 'card_ccv', 'label' => 'CCV', 'rules' => 'trim|required|numeric|min_length[3]|max_length[4]'),
				array('field' => 'expiry_month', 'label' => 'Card Expiry Month', 'rules' => 'trim|required'),
				array('field' => 'expiry_year', 'label' => 'Card Expiry Year', 'rules' => 'trim|required')
			));
			if($this->form_validation->run() == true){
				$card_details = array(
					'card_number' => $this->input->post('card_number'),
					'card_name' => $this->input->post('card_name'),
					'expiry_month' => $this->input->post('expiry_month'),
					'expiry_year' => $this->input->post('expiry_year')
				);
				$update = $this->payment->eway_update_user($user['eway_token'], $user, $card_details);
				if($update == 'true'){
					$success_messages[] = 'You have successfully updated your credit card details.';
				}
			} else {
				$errors = array_merge($errors, $this->form_validation->error_array());
			}
		}

		if($this->input->post('add_card') && empty($user['eway_token'])){
			$this->form_validation->set_rules(array(
				array('field' => 'card_name', 'label' => 'Name On Card', 'rules' => 'trim|required'),
				array('field' => 'card_number', 'label' => 'Credit Card Number', 'rules' => 'trim|luhn_check|required'),
				array('field' => 'card_ccv', 'label' => 'CCV', 'rules' => 'trim|required|numeric|min_length[3]|max_length[4]'),
				array('field' => 'expiry_month', 'label' => 'Card Expiry Month', 'rules' => 'trim|required'),
				array('field' => 'expiry_year', 'label' => 'Card Expiry Year', 'rules' => 'trim|required')
			));
			if($this->form_validation->run() == true){
				$card_expiry = explode('/', $this->input->post('card_expiry')); // [0] = Month, [1] = Year
				$card_details = array(
					'card_number' => $this->input->post('card_number'),
					'card_name' => $this->input->post('card_name'),
					'expiry_month' => $this->input->post('expiry_month'),
					'expiry_year' => $this->input->post('expiry_year')
				);

				$token = $this->payment->eway_create_user(intval($user['user_id']), $card_details);
				if(!is_array($token)){
					$this->db->set(array(
						'eway_token' => $token
					));
					$this->db->where('user_id', $user['user_id']);
					$this->db->update('user_profiles');
					$user['eway_token'] = $token;
					$success_messages[] = 'You have successfully added this card to your account.';
				} else {
					$errors[] = $token['faultstring'].'<br />';
				}
			} else {
				$errors = array_merge($errors, $this->form_validation->error_array());
			}
		}

		if($this->input->post('delete_card')){
			$first = $this->db->query("SELECT `date` FROM `billing_history` WHERE `user_id`='".mysql_real_escape_string($this->user_id)."' ORDER BY `id` ASC LIMIT 1");
			if($first->num_rows() > 0){
				$first = $first->row_array();
				if((intval($user['paid_period_ends'])-intval($first['date'])) >= MINIMUM_SUBSCRIPTION){
					$this->db->set(array(
						'eway_token' => ''
					));
					$this->db->where('user_id', $user['user_id']);
					$this->db->update('user_profiles');
					unset($user['eway_token']);
				} else {
					$errors[] = 'At this stage you have not met the minimum 12 week commitment. You cannot remove your credit card at this time.';
				}
			}
		}

		$billing_history = $this->db->query("SELECT * FROM `billing_history` WHERE `user_id`='".mysql_real_escape_string($this->user_id)."'")->result_array();

		if(!empty($user['eway_token'])){
			$cc_details = $this->payment->eway_get_user($user['eway_token']);
		} else {
			if($user['free'] != 1) {
			$errors[] = 'You currently do not have a credit card linked up with your account.<br />Your paid period runs out on <strong>'.date('d F Y', $user['paid_period_ends']).'</strong>.<br />Please add a credit card now to prevent the cancellation of your account.';
			}
		}

		$this->common->display(array(
			'view' => 'dashboard/settings/billing',
			'template' => 'templates/dashboard',
			'title' => 'Billing',
			'extra_css' => array(
				CSS_PATH.'dashboard.css'
			),
			'template_data' => array(
				'name' => $user['first_name'],
				'unread' => $this->data->unread_messages()
			),
			'content' => array(
				'errors' => implode('<br />', $this->common->merge_flash('errors', $errors)),
				'success_messages' => implode('<br />', $this->common->merge_flash('success_messages', $success_messages)),
				'billing_history' => $billing_history,
				'cc_details' => $cc_details,
				'tab' => $tab,
				'user' => $user
			)
		));
	}

	function view_invoice($invoice_id=null){
		$user = $this->data->get_all_userdata($this->user_id);

		$errors = array();
		$invoice = '';

		if(!empty($invoice_id)){
			$check = $this->db->query("SELECT * FROM `billing_history` WHERE `id`='".mysql_real_escape_string($invoice_id)."' AND `user_id`='".mysql_real_escape_string($this->user_id)."' LIMIT 1");
			if($check->num_rows() > 0){
				$invoice = $this->load->view('includes/invoice_table', array('user' => $user, 'invoice_data' => $check->row_array()) , true);
			} else {
				$errors[] = 'Sorry but that is invalid invoice ID.';
			}
		} else {
			$errors[] = 'No Invoice ID Provided.';
		}

		$this->common->display(array(
			'view' => 'dashboard/settings/view_invoice',
			'template' => 'templates/dashboard',
			'title' => 'View Invoice - '.$invoice_id,
			'extra_css' => array(
				CSS_PATH.'dashboard.css'
			),
			'template_data' => array(
				'name' => $user['first_name'],
				'unread' => $this->data->unread_messages()
			),
			'content' => array(
				'errors' => implode('<br />', $this->common->merge_flash('errors', $errors)),
				'invoice' => $invoice
			)
		));
	}

	function delete_account(){
		$user = $this->data->get_all_userdata($this->user_id);
		$this->load->library('form_validation');
		if($this->input->post('delete_account')){
			$this->form_validation->set_rules(array(
				array('field' => 'leave_reason', 'label' => 'Reason for leaving', 'rules' => 'trim|required')
			));
			if($this->form_validation->run()){
				$first = $this->db->query("SELECT `date` FROM `billing_history` WHERE `user_id`='".mysql_real_escape_string($this->user_id)."' ORDER BY `id` ASC LIMIT 1");
				if($first->num_rows() > 0){
					$first = $first->row_array();
					if((intval($user['paid_period_ends'])-intval($first['date'])) >= MINIMUM_SUBSCRIPTION){
						// Set any existing attempts to delete account to obsolete.
						$this->db->set(array(
							'obsolete' => 1
						));
						$this->db->where('user_id', $this->user_id);
						$this->db->update('account_deleted');

						// Insert new request for account deletion
						$this->db->set(array(
							'user_id' => $this->user_id,
							'reason' => $this->input->post('leave_reason'),
							'deletion_date' => time(),
							'confirmation_key' => md5(time().rand(1,10000).$this->user_id)
						));

						$this->db->insert('account_deleted');
						$this->session->set_flashdata('success_messages', array('You have successfully opted out of the STC Fit program. You will retain access for the period already purchased. An STC Fit staff member may be in contact with you shortly.'));
					} else {
						$this->session->set_flashdata('errors', array('At this stage you have not met the minimum 12 week commitment. You cannot remove your credit card at this time.'));
					}
				}
			} else {
				$this->session->set_flashdata('errors', $this->form_validation->error_array());
			}
		}
		redirect('/settings/billing/credit-card-details');
	}

	function reactivate_account(){
		$user = $this->data->get_all_userdata($this->user_id);
		$this->load->library('form_validation');
		if($this->input->post('reactivate_account')){
			$this->db->set(array(
				'obsolete' => 1
			));
			$this->db->where('user_id', $this->user_id);
			$this->db->update('account_deleted');
			$this->session->set_flashdata('success_messages', array('You have successfully reactivated your account!'));
		}
		redirect('/settings/billing/credit-card-details');
	}
}
