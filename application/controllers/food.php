<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Food extends Users_Controller {
	function __construct(){
		parent::__construct();
		if(!$this->tank_auth->is_logged_in()){ // Checking if the user is logged in.
			$this->session->set_flashdata('errors', array('You must be logged in to access the dashboard area.'));
			redirect('/');
		}
		$this->user_id = $this->tank_auth->get_user_id();
	}

	function index() {
		$errors = array();
		$this->load->model('data');
		$this->load->model('phase');

		$user = $this->data->get_all_userdata($this->user_id);
		$phase = $this->phase->get_current_phase($this->user_id);

		if($this->input->post('submit_goals')){
			$this->load->library('form_validation');
			$this->form_validation->set_rules(array(
				array('field' => 'entry[calories][]', 'label' => 'Calories', 'rules' => 'required|numeric'),
				array('field' => 'entry[protein][]', 'label' => 'Protein', 'rules' => 'required|numeric'),
				array('field' => 'entry[carbohydrates][]', 'label' => 'Carbohydrates', 'rules' => 'required|numeric'),
				array('field' => 'entry[sugars][]', 'label' => 'Sugars', 'rules' => 'required|numeric'),
				array('field' => 'entry[fats][]', 'label' => 'Fats', 'rules' => 'required|numeric'),
				array('field' => 'selected_plan', 'label' => 'Plan', 'rules' => 'required|numeric')
			));
			if($this->form_validation->run()){
				$this->db->set(array(
					'obsolete' => 1
				));
				$this->db->where('plan_id', $this->input->post('selected_plan'));
				$this->db->where('user_id', $this->user_id);
				$this->db->update('food_goals');

				$batch = array();
				$count = $this->input->post('entry_count');
				$entries = $this->input->post('entry');

				for($x=0; $x<intval($count); $x++){
					$batch[] = array(
						'user_id' => $this->user_id,
						'plan_id' => $this->input->post('selected_plan'),
						'goal_number' => ($x+1),
						'date' => time(),
						'calories' => $entries['calories'][$x],
						'protein' => $entries['protein'][$x],
						'carbs' => $entries['carbohydrates'][$x],
						'sugars' => $entries['sugars'][$x],
						'fats' => $entries['fats'][$x]
					);
				}

				$this->db->insert_batch('food_goals', $batch);
				$this->db->set(array(
					'food_plan' => $this->input->post('selected_plan')
				));
				$this->db->where('user_id', $this->user_id);
				$this->db->update('user_profiles');
				$user['food_plan'] = $this->input->post('selected_plan');
			} else {
				$errors = array('All fields are required for setting your food goals, including:<br />
					<ul>
						<li>Calories</li>
						<li>Protein</li>
						<li>Carbohydrates</li>
						<li>Sugars</li>
						<li>Fats</li>
					</ul>
				');
			}
		}

		if(!empty($user['food_plan']) && $this->session->flashdata('change_plan') == false && empty($errors)){
			redirect('/food/entry');
		}

		$food_plans = $this->db->query("SELECT `food_plans`.* FROM `food_plans`")->result_array();
		$this->common->display(array(
			'view' => 'dashboard/food/index',
			'template' => 'templates/dashboard',
			'title' => 'Food',
			'extra_css' => array(
				CSS_PATH.'dashboard.css'
			),
			'template_data' => array(
				'name' => $user['first_name'],
				'full_width' => true,
				'unread' => $this->data->unread_messages()
			),
			'content' => array(
				'errors' => implode('<br />', $this->common->merge_flash('errors', $errors)),
				'food_plans' => $food_plans,
				'user' => $user,
				'phase' => $phase,
				'goals' => $this->get_goals(null, true)
			)
		));
	}

	function entry(){
		$this->load->model('data');
		$this->load->model('phase');

		$errors = array();
		$success_messages = array();
		$user = $this->data->get_all_userdata($this->user_id);
		$phase = $this->phase->get_current_phase($this->user_id);


		if(empty($user['food_plan'])){
			$this->session->set_flashdata('errors', array('You can not make an entry for your food until you have selected a food plan.'));
			redirect('/dashboard');
		}

		$goal_number = null;

		if($this->input->post('input_submit')){
			$this->load->library('form_validation');
			$this->form_validation->set_rules(array(
				array('field' => 'calories', 'label' => 'Calories', 'rules' => 'required|numeric|trim'),
				array('field' => 'protein', 'label' => 'Protein', 'rules' => 'required|numeric|trim'),
				array('field' => 'carbohydrates', 'label' => 'Carbohydrates', 'rules' => 'required|numeric|trim'),
				array('field' => 'sugars', 'label' => 'Sugars', 'rules' => 'required|numeric|trim'),
				array('field' => 'fats', 'label' => 'Fats', 'rules' => 'required|numeric|trim'),
				array('field' => 'plan_id', 'label' => 'Plan ID', 'rules' => 'required|integer'),
				array('field' => 'goal_number', 'label' => 'Goal Number', 'rules' => 'required|integer'),
			));

			if($this->form_validation->run() == true){
				if($this->input->post('plan_id') == $user['food_plan']){
					$goal_number = $this->input->post('goal_number');
					$this->db->set(array(
						'user_id' => $this->user_id,
						'plan_id' => $user['food_plan'],
						'goal_number' => $goal_number,
						'calories' => $this->input->post('calories'),
						'protein' => $this->input->post('protein'),
						'carbs' => $this->input->post('carbohydrates'),
						'sugars' => $this->input->post('sugars'),
						'fats' => $this->input->post('fats'),
						'entry_date' => time()
					));
					$this->db->insert('food_entries');
					$success_messages[] = 'Food entry made successfully';
				} else {
					$errors[] = 'The food plan you are attempting to make an entry against is not the food plan you have currently got selected. Please change your plan if you would like to make an entry against it.';
				}
			} else {
				$errors = $this->form_validation->error_array();
			}
		}

		$food_plan = $this->db->query("SELECT * FROM `food_plans` WHERE `food_plans`.`id`='".$user['food_plan']."'")->row_array();

		$goals = $this->db->query("SELECT
			`food_goals`.*
		FROM
			`food_goals`
		WHERE
			`food_goals`.`obsolete`=0
		AND
			`food_goals`.`user_id`='".$this->user_id."'
		AND
			`food_goals`.`plan_id`='".$user['food_plan']."'
		ORDER BY
			`food_goals`.`goal_number` ASC
		LIMIT
			4
		")->result_array();

		$this->common->display(array(
			'view' => 'dashboard/food/entry',
			'template' => 'templates/dashboard',
			'title' => 'Food',
			'extra_css' => array(
				CSS_PATH.'dashboard.css'
			),
			'template_data' => array(
				'name' => $user['first_name'],
				'full_width' => true,
				'unread' => $this->data->unread_messages()
			),
			'content' => array(
				'errors' => implode('<br />', $this->common->merge_flash('errors', $errors)),
				'success_messages' => implode('<br />', $this->common->merge_flash('success_messages', $success_messages)),
				'user' => $user,
				'phase' => $phase,
				'goals' => $goals,
				'goal_number' => $goal_number
			)
		));
	}

	function previous_entries($plan_id=null, $goal_number=null){
		if(empty($plan_id) || empty($goal_number)){
			exit;
		}

		$entries = $this->db->query("SELECT
			`food_entries`.*
		FROM
			`food_entries`
		WHERE
			`food_entries`.`user_id`='".$this->user_id."'
		AND
			`food_entries`.`plan_id`='".$plan_id."'
		AND
			`food_entries`.`goal_number`='".$goal_number."'
		ORDER BY
			`food_entries`.`entry_date` DESC
		");

		if($entries->num_rows() > 0){
			$entries = $entries->result_array();
		} else {
			$entries = array();
		}

		$this->load->view('includes/previous_food_entries', array('entries' => $entries));
	}

	function change_plan(){ // Simply sets flash data so that the index method does not redirect the user straight to entry.
		$this->session->set_flashdata('change_plan', true);
		redirect('/food');
	}

	function get_goals($plan_id=null, $return=false){
		$errors = array();
		$success_messages = array();
		$this->load->model('data');
		$user = $this->data->get_all_userdata($this->user_id);

		if(empty($plan_id)){
			$plan_id = $user['food_plan'];
		}

		$food = $this->db->query("SELECT
			`food_plans`.*
		FROM
			`food_plans`
			".(!empty($plan_id) ? " WHERE `food_plans`.`id`='".mysql_real_escape_string($plan_id)."' ":"")."
		ORDER BY
			`food_plans`.`id` ASC
		LIMIT
		 	1
		")->row_array();

		$goals = $this->db->query("SELECT
			`food_goals`.*
		FROM
			`food_goals`
		WHERE
			`food_goals`.`user_id`='".$this->user_id."'
		AND
			`food_goals`.`plan_id`='".$food['id']."'
		AND
			`food_goals`.`obsolete`=0
		LIMIT
			4
		")->result_array();

		$html = $this->load->view('includes/food_goals', array('goal_count' => $food['entries'], 'goal_entries' => $goals), true);
		if($return == false){
			echo $html;
			exit;
		}

		return $html;
	}
}