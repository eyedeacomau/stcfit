<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Renew extends CI_Controller {
	function __construct(){
		parent::__construct();
		if(!$this->tank_auth->is_logged_in()){ // Checking if the user is logged in.
			$this->session->set_flashdata('errors', array('You must be logged in to access the dashboard area.'));
			redirect('/');
		} elseif($this->session->userdata('is_admin')){
			redirect('/admin');
		} elseif($this->session->userdata('is_trainer')){
			redirect('/trainers');
		}

		$this->user_id = $this->tank_auth->get_user_id();
	}

	function index() {
		$this->load->model('data');
		$this->load->model('payment');
		$this->load->library('form_validation');
		$payment_methods = $this->config->item('payment_methods');
		$errors = array();
		$user = $this->data->get_all_userdata($this->user_id);
		if(intval($user['paid_period_ends']) > time()){
			redirect('/dashboard');
		}
		$cc_details = array();
		if(!empty($user['eway_token'])){
			$cc_details = $this->payment->eway_get_user($user['eway_token']);
		}

		if($this->input->post('submit_payment_details')){
			$this->form_validation->set_rules(array(
				array('field' => 'payment_method', 'label' => 'Payment Method', 'rules' => 'trim'),
				array('field' => 'payment_frequency', 'label' => 'Payment Frequency', 'rules' => 'trim|required'),
				array('field' => 'card_name', 'label' => 'Name On Card', 'rules' => 'trim|required'),
				array('field' => 'card_number', 'label' => 'Credit Card Number', 'rules' => 'trim|luhn_check|required'),
				array('field' => 'card_ccv', 'label' => 'CCV', 'rules' => 'trim|required|numeric|min_length[3]|max_length[4]'),
				array('field' => 'expiry_month', 'label' => 'Card Expiry', 'rules' => 'trim|required'),
				array('field' => 'expiry_year', 'label' => 'Card Expiry', 'rules' => 'trim|required')
			));

			if($this->form_validation->run()){
				$this->load->model('payment');
				$method = 4; // Setting payment method to yearly initially.
				if($this->input->post('payment_frequency') == 1 && !empty($payment_methods[$this->input->post('payment_method')])){
					$method = intval($this->input->post('payment_method')); // If the user has selected another payment method.
				}

				$chosen_method = $payment_methods[$method];
				$card_details = array(
					'card_number' => $this->input->post('card_number'),
					'card_name' => $this->input->post('card_name'),
					'expiry_month' => $this->input->post('expiry_month'),
					'expiry_year' => $this->input->post('expiry_year')
				);
				if(!empty($user['user_id'])){
					$token = $this->payment->eway_create_user(intval($user['user_id']), $card_details);
					if(!is_array($token)){

						$bill_user = $this->payment->eway_bill_user($token, number_format($chosen_method['amount'],2), 'STC Fit '.ucwords($chosen_method['title']).' Memebership Fee.');
						if(isset($bill_user['ewayTrxnStatus']) && $bill_user['ewayTrxnStatus'] == 'True'){
							// Updating the user profiles table
							$trainer_code = $this->input->post('trainer_code');
							$this->db->set(array(
								'eway_token' => $token,
								'renewal_type' => $method,
								'paid_period_ends' => (time()+($chosen_method['days']*86400)),
								'signup_complete' => 1
							));
							$this->db->where('user_id', $user['user_id']);
							$this->db->update('user_profiles');

							// Adding this record into billing history

							$data = array(
								'user_id' => $user['user_id'],
								'amount' => number_format($chosen_method['amount'],2),
								'description' => 'Get Ripped (' .$chosen_method['title']. ')',
								'date' => time(),
								'reference' => $bill_user['ewayTrxnNumber']
							);

							$this->db->set($data);
							$this->db->insert('billing_history');
							$data['id'] = $this->db->insert_id();

							// ------ Send the user an invoice email --------
							$user_information = $this->data->get_all_userdata($data['user_id']);
							$invoice = $this->load->view('includes/invoice_table', array('user' => $user_information, 'invoice_data' => $data) , true);

							$this->common->send_email(array(
								'email_to' => $user_information['email'],
								'subject' => 'Thanks for renewing your account!',
								'view' => 'renewal', 
								'data' => array(
									'user' => $user_information,
									'invoice' => $invoice
								)
							));								
							// ------ End send invoice ----------------------
							$this->db->set(array(
								'obsolete' => 1
							));
							
							$this->db->where('user_id', $this->user_id);
							$this->db->update('account_deleted');

							// ------- Makes sure the billing resumes for this user. --------
							$this->db->set(array(
								'obsolete' => 1
							));
							$this->db->where('user_id', $this->user_id);
							$this->db->update('billing_failed');
							// ---------------------------------------------------------------

							if($user == true){
								$this->session->set_flashdata('success_messages', array('You have successfully renewed your subscription! Thanks for remaining a part of the STC Fit team.'));
								redirect('/dashboard');
							} else {
								$this->session->session_destroy();
								$this->session->set_flashdata('errors', array('Something went wrong.'));
								redirect('/registration');
							}
						} else {
							$errors[] = $bill_user['ewayTrxnError'];
						}
					} else {
						$errors[] = $token['faultstring'];
					}
				} else {
					$errors[] = 'No user account. An error occured.';
				}
			} else {
				$errors = array_merge($errors, $this->form_validation->error_array());
			}
		}

		$this->common->display(array(
			'view' => 'dashboard/settings/renew',
			'template' => 'templates/dashboard',
			'title' => 'Renew Subscription',
			'extra_css' => array(
				CSS_PATH.'dashboard.css'
			),
			'template_data' => array(
				'name' => $user['first_name'],
				'unread' => $this->data->unread_messages()
			),
			'content' => array(
				'errors' => implode('<br />', $this->common->merge_flash('errors', $errors)),
				'cc_details' => $cc_details,
				'payment_methods' => $payment_methods
			)
		));
	}	
}