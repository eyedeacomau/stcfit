<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends Users_Controller {
	function __construct(){
		parent::__construct();
		if(!$this->tank_auth->is_logged_in()){ // Checking if the user is logged in.
			$this->session->set_flashdata('errors', array('You must be logged in to access the dashboard area.'));
			redirect('/');
		}

		$this->user_id = $this->tank_auth->get_user_id();
	}

	function index($payment_method=null) {
		if($payment_method == 'update_message'){
			return $this->update_message();
		}
		$errors = array();
		$messages = array();
		$this->load->model('data');
		$this->load->model('phase');

		$user = $this->data->get_all_userdata($this->user_id);
		$phase = $this->phase->get_progress($user, $user['training_type'], false);

		if(empty($phase['measurements'])){
			$messages[] = 'There is currently no available dashboard data for this user.';
		}
		$this->common->display(array(
			'view' => 'dashboard/index',
			'template' => 'templates/dashboard',
			'title' => 'Dashboard',
			'extra_js' => array(
				JS_PATH.'plugins/highcharts.js'
			),
			'template_data' => array(
				'name' => $user['first_name'],
				'unread' => $this->data->unread_messages()
			),
			'content' => array(
				'errors' => implode('<br />', $this->common->merge_flash('errors', $errors)),
				'messages' => implode('<br />', $this->common->merge_flash('messages', $messages)),
				'phase' => $phase,
				'show_message' => $user['show_message'],
				'trainer_name' => $user['trainer_name']
			)
		));
	}

	function update_message() {
		$query = $this->db->simple_query("
			UPDATE
				`user_profiles`
			SET
				`show_message` = '".($_POST['dont_show'] ? '0' : '1')."'
			WHERE
				`user_id` = '".$this->user_id."'
		");
		die(json_encode(array(
			'user_id' => $this->user_id,
			'show_message' => ($_POST['dont_show'] ? '0' : '1'),
			'query_success' => $query
		)));
	}
}