<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Programs extends Trainers_Controller {
	function __construct(){
		parent::__construct();
	}
	
	function index() {
		$errors = array();
		$this->load->model('trainers');
		$trainer = $this->trainers->get_trainer_data();
		$programs = $this->trainers->get_programs();

		$this->common->display(array(
			'view' => 'trainers/programs',
			'template' => 'templates/trainers',
			'template_data' => array(
				'trainer' => $trainer,
				'page' => $this->uri->segment(2)
			),
			'title' => 'Trainer Dashboard - Programs',
			'content' => array(
				'errors' => implode('<br />', $this->common->merge_flash('errors', $errors)),
				'trainer' => $trainer,
				'programs' => $programs
			)
		));
	}

	function phase_page($page=null, $type=null){
		if($this->input->is_ajax_request() == false || empty($page) || empty($type)){
			$this->session->set_flashdata('errors', array('You cannot access this page.'));
			redirect('/trainers');
		}
		$this->load->model('trainers');
		echo $this->trainers->get_program_phases($type, $page);
		//echo "foo";
		exit;
	}

	function view_phase($phase_id=null){
		if($this->input->is_ajax_request() == false){
			$this->session->set_flashdata('errors', array('You cannot access this page.'));
			redirect('/trainers');
		}

		$phase = $this->db->query("SELECT
				`exercises`.`name` AS `exercise_name`,
				`exercises`.`circuit` AS `is_circuit`,
				`exercises`.`description` AS `exercise_description`,
				`session_content`.*,
				`phases`.`name` AS `phase_name`,
				`phases`.`phase_number`
			FROM
				`phases`
			INNER JOIN
				`phase_sessions` ON `phases`.`phase_number`=`phase_sessions`.`phase_number`
			INNER JOIN
				`sessions` ON `phase_sessions`.`session_identifier`=`sessions`.`session_identifier`
			INNER JOIN
				`session_content` ON `sessions`.`session_identifier`=`session_content`.`session_identifier`
			INNER JOIN
				`exercises` ON `session_content`.`exercise_identifier`=`exercises`.`exercise_identifier`
			WHERE
				`phases`.`valid_to` IS NULL
			AND
				`phase_sessions`.`valid_to` IS NULL
			AND
				`phase_sessions`.`training_type`=`phases`.`training_type`
			AND
				`sessions`.`valid_to` IS NULL
			AND
				`session_content`.`valid_to` IS NULL
			AND
				`exercises`.`valid_to` IS NULL
			AND
				`phases`.`id`='".mysql_real_escape_string($phase_id)."'
			ORDER BY
				`session_identifier`, `exercises`.`circuit` ASC, `session_content`.`order`
		");
		
		if($phase->num_rows() > 0){
			$phase = $phase->result_array();
			$data['phase_name'] = $phase[0]['phase_name'];
			$data['phase_number'] = $phase[0]['phase_number'];
			foreach($phase as &$exercises){
				$data['sessions'][$exercises['session_identifier']][] = $exercises;
			}
			echo $this->load->view('includes/view_phase', array('data' => $data), true);
		}
		exit;
	}
}