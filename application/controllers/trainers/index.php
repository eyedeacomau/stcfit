<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Index extends Trainers_Controller {
	function __construct(){
		parent::__construct();
	}
	
	function index() {
		$errors = array();
		$success_messages = array();
		$this->load->model('trainers');
		$trainer = $this->trainers->get_trainer_data();
		$deferral_periods = $this->config->item('deferral_periods');
		$this->load->library('form_validation');
		if($this->input->post('re_activate')){
			$this->form_validation->set_rules(array(
				array('field' => 'user_id', 'label' => 'User ID', 'rules' => 'required|trim'),
				array('field' => 'deletion_id', 'label' => 'User ID', 'rules' => 'required|trim')
			));
			if($this->form_validation->run()){
				$check = $this->db->query("SELECT
					`users`.`email`,
					`user_profiles`.*,
					`account_deleted`.`confirmation_key`
				FROM
					`users`
				INNER JOIN
					`user_profiles` ON `users`.`id`=`user_profiles`.`user_id`
				LEFT JOIN
					`account_deleted` ON `user_profiles`.`user_id`=`account_deleted`.`user_id`
				WHERE
					`user_profiles`.`user_id` = '".mysql_real_escape_string($this->input->post('user_id'))."'
				AND
					`user_profiles`.`allocated_trainer`='".mysql_real_escape_string($trainer['trainer_id'])."'
				AND
					`account_deleted`.`id`='".mysql_real_escape_string($this->input->post('deletion_id'))."'
				LIMIT
					1
				");
				if($check->num_rows() > 0){
					$check = $check->row_array();

					$this->db->set(array(
						'trainer_override' => 1
					));
					$this->db->where('user_id', $this->input->post('user_id'));
					$this->db->update('account_deleted');
					$this->common->send_email(array(
						'email_to' => $check['email'],
						'subject' => 'Confirmation of resubscription!',
						'view' => 'resubscribe', 
						'data' => array(
							'user' => $check
						)
					));
					$success_messages[] = 'You have successfully resubscribed this user!';
				} else {
					$errors[] = 'You do not have permission to alter the subscription status of this user.';
				}
			} else {
				$errors = $this->form_validation->error_array();
			}
		} elseif($this->input->post('defer_subscription')){
			$this->form_validation->set_rules(array(
				array('field' => 'user_id', 'label' => 'User ID', 'rules' => 'required|trim'),
				array('field' => 'deletion_id', 'label' => 'Deletion ID', 'rules' => 'required|trim'),
				array('field' => 'defer_months', 'label' => 'User ID', 'rules' => 'required|trim')
			));
			if($this->form_validation->run()){
				$check = $this->db->query("SELECT
					`users`.`email`,
					`user_profiles`.*,
					`account_deleted`.`confirmation_key`
				FROM
					`users`
				INNER JOIN
					`user_profiles` ON `users`.`id`=`user_profiles`.`user_id`
				LEFT JOIN
					`account_deleted` ON `user_profiles`.`user_id`=`account_deleted`.`user_id`
				WHERE
					`user_profiles`.`user_id` = '".mysql_real_escape_string($this->input->post('user_id'))."'
				AND
					`user_profiles`.`allocated_trainer`='".mysql_real_escape_string($trainer['trainer_id'])."'
				AND
					`account_deleted`.`id`='".mysql_real_escape_string($this->input->post('deletion_id'))."'
				LIMIT
					1
				");
				if(!array_key_exists(intval($this->input->post('defer_months')), $deferral_periods)){
					$errors[] = 'You have attempted to defer this user for an invalid period of time.';
				} elseif($check->num_rows() > 0){
					$check = $check->row_array();
					// Deferring the user's subscription for the alotted period.
					$period = (strtotime('+'.$deferral_periods[intval($this->input->post('defer_months'))].' months')-time());
					$this->db->set(array(
						'defer_til' => (time()+$period),
						'defer_period_id' => intval($this->input->post('defer_months')),
						'trainer_override' => 1
					));
					$this->db->where('user_id', $check['user_id']);
					$this->db->where('obsolete', 0);
					$this->db->update('account_deleted');
					// Setting the paid_period_ends to add the deferral period.
					$this->db->set(array(
						'paid_period_ends' => (intval($check['paid_period_ends'])+$period)
					));
					$this->db->where('user_id', $check['user_id']);
					$this->db->update('user_profiles');
					// Sending a confirmation email to the user

					$check['defer_til'] = (time()+$period);
					$this->common->send_email(array(
						'email_to' => $check['email'],
						'subject' => 'Confirmation of account deferral!',
						'view' => 'defer_subscription',
						'data' => array(
							'user' => $check
						)
					));
					$success_messages[] = 'This user\'s subscription has been successfully deferred til '.date('d/m/Y h:i:sa', (time()+$period));
				} else {
					$errors[] = 'You do not have permission to alter the subscription status of this user.';
				}
			} else {
				$errors = $this->form_validation->error_array();
			}
		}

		$clients = $this->trainers->lost_clients();

		foreach($clients['month_data'] as $month){
			$clients['linear_lost'][] = $month['monthly_lost'];
			$clients['linear_total'][] = $month['monthly_total'];
		}

		$this->common->display(array(
			'view' => 'trainers/dashboard',
			'template' => 'templates/trainers',
			'extra_js' => array(
				JS_PATH.'plugins/highcharts.js',
				JS_PATH.'trainers/dashboard.js'
			),
			'template_data' => array(
				'trainer' => $trainer,
				'page' => $this->uri->segment(2)
			),
			'title' => 'Trainer Dashboard',
			'content' => array(
				'errors' => implode('<br />', $this->common->merge_flash('errors', $errors)),
				'success_messages' => implode('<br />', $this->common->merge_flash('success_messages', $success_messages)),
				'clients' => $clients,
				'deferral_periods' => $deferral_periods
			)
		));
	}

	function client_login($user_id=null){
		$login = $this->tank_auth->login_as_user($user_id);
		if($login == false){
			$this->session->set_flashdata('errors', array('Cannot login as this user.'));
			redirect('/trainers');
		}
	}

	function suspend_user($user_id=null){
		if(empty($user_id)){
			$this->session->set_flashdata('errors', array('Cannot complete designated action.'));
			redirect('/trainers');
		}
		$this->load->model('trainers');
		$trainer = $this->trainers->get_trainer_data();
		$check = $this->db->query("SELECT
			`user_profiles`.`id`
		FROM
			`user_profiles`
		WHERE
			`user_profiles`.`user_id`='".mysql_real_escape_string($user_id)."'
		AND
			`user_profiles`.`allocated_trainer`='".mysql_real_escape_string($trainer['trainer_id'])."'
		LIMIT
			1");
		if($check->num_rows() < 1){
			$this->session->set_flashdata('errors', array('You do not have the necessary permission to suspend this user.'));
			redirect('/trainers/clients');
		}
		$this->db->set(array(
			'banned' => 1,
			'ban_reason' => 'Trainer has suspended user.'
		));
		$this->db->where('id', $user_id);
		$this->db->update('users');
		$this->session->set_flashdata('success_messages', array('You have successfully suspended this user. This may also be undone in this same panel.'));
		redirect('/trainers/clients');
	}

	function reactivate_user($user_id=null){
		if(empty($user_id)){
			$this->session->set_flashdata('errors', array('Cannot complete designated action.'));
			redirect('/trainers');
		}
		$this->load->model('trainers');
		$trainer = $this->trainers->get_trainer_data();
		$check = $this->db->query("SELECT
			`user_profiles`.`id`
		FROM
			`user_profiles`
		WHERE
			`user_profiles`.`user_id`='".mysql_real_escape_string($user_id)."'
		AND
			`user_profiles`.`allocated_trainer`='".mysql_real_escape_string($trainer['trainer_id'])."'
		LIMIT
			1");
		if($check->num_rows() < 1){
			$this->session->set_flashdata('errors', array('You do not have the necessary permission to re-activate this user.'));
			redirect('/trainers/clients');
		}
		$this->db->set(array(
			'banned' => 0,
			'ban_reason' => null
		));
		$this->db->where('id', $user_id);
		$this->db->update('users');
		$this->session->set_flashdata('success_messages', array('You have successfully re-activated this user. This may also be undone in this same panel.'));
		redirect('/trainers/clients');
	}
}