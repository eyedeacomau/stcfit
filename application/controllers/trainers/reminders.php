<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reminders extends Trainers_Controller {
	function __construct(){
		parent::__construct();
	}
	
	function index() {
		$errors = array();
		$success_messages = array();
		$messages = array();

		$this->load->model('trainers');
		$trainer = $this->trainers->get_trainer_data();
		if($this->input->post('submit_reminder')){
			$this->load->library('form_validation');
			$this->form_validation->set_rules(array(
				array('field' => 'notes', 'label' => 'Notes', 'rules' => 'trim'),
				array('field' => 'next_reminder', 'label' => 'Next Reminder', 'rules' => 'trim|required'),
				array('field' => 'user_id', 'label' => 'User ID', 'rules' => 'trim|required')
			));
			if($this->form_validation->run()){
				$next_reminder = $this->common->date_convert($this->input->post('next_reminder'));
				if((intval($next_reminder)-time()) <= (86400*7*3)){ // 3 weeks
					$this->db->set(array(
						'obsolete' => 1
					));
					$this->db->where('client_id', $this->input->post('user_id'));
					$this->db->where('trainer_id', $trainer['trainer_id']);
					$this->db->update('trainer_reminders');
					// Adding the new entry
					$this->db->set(array(
						'trainer_id' => $trainer['trainer_id'],
						'client_id' => $this->input->post('user_id'),
						'reminder_date' => $next_reminder,
						'notes' => $this->input->post('notes')
					));

					$this->db->insert('trainer_reminders');
					$success_messages[] = 'You have successfully added a new reminder for this user.';
				} else {
					$errors[] = 'Your next reminder must be no greater than 3 weeks from now.';
				}
			} else {
				$errors = $this->form_validation->error_array();
			}
		}
		$reminders = $this->trainers->trainer_reminders($trainer['trainer_id']);
		if(empty($reminders)){
			$messages[] = 'You do not currently have any outstanding reminders.';
		}
		$allowed_sizes = $this->config->item('allowed_image_sizes');

		$this->common->display(array(
			'view' => 'trainers/reminders',
			'template' => 'templates/trainers',
			'extra_css' => array(
				CSS_PATH.'plugins/datepicker.css'
			),
			'extra_js' => array(
				JS_PATH.'plugins/bootstrap-datepicker.js'
			),
			'template_data' => array(
				'trainer' => $trainer,
				'page' => $this->uri->segment(2)
			),
			'title' => 'Trainer Dashboard - Reminders',
			'content' => array(
				'errors' => implode('<br />', $this->common->merge_flash('errors', $errors)),
				'success_messages' => implode('<br />', $this->common->merge_flash('success_messages', $success_messages)),
				'messages' => implode('<br />', $this->common->merge_flash('messages', $messages)),
				'trainer' => $trainer,
				'reminders' => $reminders,
				'allowed_sizes' => $allowed_sizes,
				'states' => $this->config->item('states')
			)
		));
	}
}