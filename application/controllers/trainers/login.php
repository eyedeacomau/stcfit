<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {
	function __construct(){
		parent::__construct();
	}

	function index() {
		$errors = array();
		$this->common->display(array(
			'view' => 'trainers/login',
			'template' => 'templates/empty',
			'title' => 'Trainers Login',
			'content' => array(
				'errors' => implode('<br />', $this->common->merge_flash('errors', $errors))
			)
		));
	}
}