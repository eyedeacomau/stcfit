<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Clients extends Trainers_Controller {
	function __construct(){
		parent::__construct();
	}
	
	function index() {
		$errors = array();
		$messages = array();
		$this->load->model('trainers');
		$trainer = $this->trainers->get_trainer_data();
		$search_params = '';
		if($this->input->get_post('client_search')){
			$search_params = mysql_real_escape_string($this->input->get_post('search_params'));
		}

		$clients = $this->trainers->get_clients($trainer['trainer_id'], $search_params);
		if(empty($clients)){
			$messages[] = 'No results were found'.(!empty($search_params) ? ' matching "'.$search_params.'". <a href="/trainers/clients">Click here to return</a>.':'.');
		}
		$this->common->display(array(
			'view' => 'trainers/clients',
			'template' => 'templates/trainers',
			'template_data' => array(
				'trainer' => $trainer,
				'page' => $this->uri->segment(2)
			),
			'title' => 'Trainer Dashboard - Clients',
			'content' => array(
				'errors' => implode('<br />', $this->common->merge_flash('errors', $errors)),
				'messages' => implode('<br />', $this->common->merge_flash('messages', $messages)),
				'trainer' => $trainer,
				'clients' => $clients,
				'search_params' => $search_params
			)
		));
	}
}