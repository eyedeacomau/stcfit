<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Messages extends Trainers_Controller {
	function __construct(){
		parent::__construct();
	}

	function index() {
		$errors = array();
		$messages = array();
		$success_messages = array();
		$this->load->model(array('data','common','trainers'));
		$trainer = $this->trainers->get_trainer_data();
		if($this->input->post('send_message')){
			$this->load->library('form_validation');
			$this->form_validation->set_rules(array(
				array('field' => 'message_to', 'label' => 'Message to', 'rules' => 'required|trim'),
				array('field' => 'message_body', 'label' => 'Message body', 'rules' => 'required|trim')
			));
			if($this->form_validation->run()){
				$this->db->set(array(
					'for_user_id' => $this->input->post('message_to'),
					'from_user_id' => $trainer['trainer_id'],
					'message_body' => strip_tags($this->input->post('message_body')),
					'date' => time()
				));
				$this->db->insert('messages');
				$user = $this->data->get_all_userdata($this->input->post('message_to'));
				$this->common->send_email(array(
					'view' => 'new_message',
					'email_to' => $user['email'], // to user email address
					'subject' => 'New Message',
					'data' => array(
						'message' => array(
							'message' => strip_tags($this->input->post('message_body')),
							'created' => time(),
							'user' => $trainer // from user
						)
					)
				));
				$success_messages[] = 'Sent message successfully!';
			} else {
				$errors = $this->form_validation->error_array();
			}
		}
		$trainer_messages = $this->trainers->get_messages($trainer['trainer_id']);
		if(empty($trainer_messages)){
			$messages[] = 'You currently have no messages';
		}
		$clients = $this->trainers->get_clients($trainer['trainer_id']);

		$this->common->display(array(
			'view' => 'trainers/messages',
			'template' => 'templates/trainers',
			'extra_css' => array(
				CSS_PATH.'plugins/chosen.min.css'
			),
			'extra_js' => array(
				JS_PATH.'plugins/chosen.jquery.min.js'
			),
			'template_data' => array(
				'trainer' => $trainer,
				'page' => $this->uri->segment(2)
			),
			'title' => 'Trainer Dashboard - Messages',
			'content' => array(
				'errors' => implode('<br />', $this->common->merge_flash('errors', $errors)),
				'messages' => implode('<br />', $this->common->merge_flash('messages', $messages)),
				'success_messages' => implode('<br />', $this->common->merge_flash('success_messages', $success_messages)),
				'trainer' => $trainer,
				'trainer_messages' => $trainer_messages,
				'clients' => $clients
			)
		));
	}

	function mark_as_read(){
		if($this->input->is_ajax_request() == false){
			$this->session->set_flashdata('errors', array('You cannot access this area.'));
			redirect('/trainers');
		}
		$this->db->set(array(
			'unread' => 0
		));
		
		$this->db->where('for_user_id', $this->input->post('trainer_id'));
		$this->db->where('from_user_id', $this->input->post('user_id'));
		
		$this->db->update('messages');
		echo json_encode(array('completed' => true));
		exit;
	}
}