<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cron extends CI_Controller {
	function __construct(){
		parent::__construct();
		// if($this->input->is_cli_request() == false){
		// 	die("Cannot access the cron area directly!");
		// }
		$this->config->set_item('base_url', CRON_URL);
	}

	// Every 6 Hours.
	function billing(){
		$renewal_types = $this->config->item('payment_methods');

		$period = (time()+43200); // Will attempt billing half a day in advance.
		$users = $this->db->query("SELECT
			`user_profiles`.`eway_token`,
			`users`.`id` AS `user_id`,
			`users`.`email`,
			`user_profiles`.`paid_period_ends`,
			`user_profiles`.`renewal_type`,
			`user_profiles`.`free`,
			`user_profiles`.`street_address`,
			`user_profiles`.`suburb`,
			`user_profiles`.`postcode`,
			`user_profiles`.`state`
		FROM
			`user_profiles`
		INNER JOIN
			`users` ON `user_profiles`.`user_id`=`users`.`id`
		LEFT JOIN
			`account_deleted` ON  `users`.`id`=`account_deleted`.`user_id`
			AND
			`account_deleted`.`obsolete`=0
			AND
				(
				`account_deleted`.`trainer_override`=0
				OR
				`account_deleted`.`user_confirmed`=1
				)
		LEFT JOIN
			`billing_failed` ON `users`.`id`=`billing_failed`.`user_id`
			AND
			`billing_failed`.`obsolete` = 0
		WHERE
			`account_deleted`.`id` IS NULL
		AND
			`billing_failed`.`id` IS NULL
		AND
			`users`.`banned` = 0
		AND
			`user_profiles`.`paid_period_ends` <= '".$period."'
		");

		if($users->num_rows() < 1){
			exit;
		}
		$errors = array();
		foreach($users->result_array() as $user){

			$this->load->model('payment');

			if(empty($user['renewal_type'])) $user['renewal_type'] = 1;
			if($user['free'] == 1) $user['renewal_type'] = 4;

			$payment_method = $renewal_types[intval($user['renewal_type'])];
			if($user['free'] != 1) $bill_user = $this->payment->eway_bill_user($user['eway_token'], number_format($payment_method['amount'],2), 'STC Fit '.ucwords($payment_method['title']).' Memebership Fee.');
			if($user['free'] == 1 || (isset($bill_user['ewayTrxnStatus']) && $bill_user['ewayTrxnStatus'] == 'True')){
				// ------- Insert into billing history. --------
				$data = array(
					'user_id' => $user['user_id'],
					'amount' => ($user['free'] == 1 ? '0.00' : number_format($payment_method['amount'],2)),
					'description' => ($user['free'] == 1 ? 'Get Ripped (Free)' : 'Get Ripped (' .$payment_method['title']. ')'),
					'date' => time(),
					'reference' => ($user['free'] == 1 ? 'Free Account' : $bill_user['ewayTrxnNumber'])
				);

				$this->db->set($data);
				$this->db->insert('billing_history');
				// ----------------------------------------------

				// -------- Update this user's expiry period. ---

				$this->db->set(array(
					'paid_period_ends' => intval($user['paid_period_ends'])+($payment_method['days']*86400)
				));
				$this->db->where('user_id', $user['user_id']);
				$this->db->update('user_profiles');

				// ----------------------------------------------
				$invoice = $this->load->view('includes/invoice_table', array('user' => $user, 'invoice_data' => $data) , true);
				if(BILLING_NOTIFICATIONS && $user['free'] == 0) $this->common->send_email(array(
					'email_to' => $user['email'],
					'subject' => 'Your renewal invoice!',
					'view' => 'billing_success', 
					'data' => array(
						'user' => $user,
						'invoice' => $invoice
					)
				));
			} else {
				$errors[] = $bill_user['ewayTrxnError'];
				$this->db->set(array(
					'user_id' => $user['user_id'],
					'date' => time(),
					'reason' => implode("\n", $errors)
				));
				$this->db->insert('billing_failed');

				if(BILLING_NOTIFICATIONS && $user['free'] == 0) $this->common->send_email(array(
					'email_to' => $user['email'],
					'subject' => 'Billing Failed!',
					'view' => 'billing_failed',
					'data' => array(
						'user' => $user,
						'errors' => $errors
					)
				));
			}
		}
	}

	// Every hour
	function registration_reminders(){ // Reminds users after 2 hours if they have not completed their registration.
		$profiles = $this->db->query("SELECT
			`user_profiles`.*
		FROM
			`user_profiles`
		WHERE
			`user_profiles`.`signup_complete` = 0
		AND
			`user_profiles`.`signup_started` <= '".(time()-7200)."'
		AND
			`user_profiles`.`registration_reminder` = 0
		AND
			`user_profiles`.`id` NOT IN(SELECT
				`pre_exercise_questions`.`profile_id`
			FROM
				`pre_exercise_questions`
			WHERE
				`pre_exercise_questions`.`failed`=1
			AND
				`pre_exercise_questions`.`admin_override` = 0
			)
		");

		if($profiles->num_rows() < 1){
			exit;
		}

		foreach($profiles->result_array() as $profile){
			$this->common->send_email(array(
				'email_to' => $profile['registration_email'],
				'subject' => 'You didn\'t complete your registration!',
				'view' => 'complete_signup',
				'data' => array(
					'user' => $profile
				)
			));

			$this->db->set(array(
				'registration_reminder' => 1
			));
			$this->db->where('id', $profile['id']);
			$this->db->update('user_profiles');
		}
	}
}