<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Messages extends Users_Controller {
	function __construct(){
		parent::__construct();
		if(!$this->tank_auth->is_logged_in()){ // Checking if the user is logged in.
			$this->session->set_flashdata('errors', array('You must be logged in to access the dashboard area.'));
			redirect('/');
		}
		$this->user_id = $this->tank_auth->get_user_id();
	}

	function index() {
		$errors = array();
		$messages = array();
		$this->load->model(array('data','common'));
		$user = $this->data->get_all_userdata($this->user_id);

		if(empty($user['allocated_trainer'])){
			$this->session->set_flashdata('messages', array('You cannot access the messages section until such time that you are allocated a trainer.'));
			redirect('/dashboard');
		}
		// If a message has been posted
		if($this->input->post('send_message')){
			$this->load->library('form_validation');
			$this->form_validation->set_rules(array(
				array('field' => 'message_body', 'label' => 'Message Body', 'rules' => 'required|trim')
			));
			if($this->form_validation->run()){
				// Save the message, stripping tags
				$this->db->set(array(
					'for_user_id' => $user['allocated_trainer'],
					'from_user_id' => $this->user_id,
					'message_body' => strip_tags($this->input->post('message_body')),
					'date' => time()
				));
				$this->db->insert('messages');
				$trainer = $this->data->get_all_userdata($user['allocated_trainer']);
				$this->common->send_email(array(
					'view' => 'new_message',
					'email_to' => $trainer['email'], // to trainer email address
					'subject' => 'New Message',
					'data' => array(
						'message' => array(
							'message' => strip_tags($this->input->post('message_body')),
							'created' => time(),
							'user' => $user // from user
						)
					)
				));
			} else {
				$errors = $this->form_validation->error_array();
			}
		}
		// Setting all unread messages to read.
		$this->db->set(array(
			'unread' => 0
		));
		$this->db->where('for_user_id', $this->user_id);
		$this->db->update('messages');

		// Get all inbox messages
		$inbox = $this->db->query("SELECT
			`messages`.*,
			CONCAT(`trainer_profiles`.`first_name`, ' ', `trainer_profiles`.`last_name`) AS `trainer`
		FROM
			`messages`
		LEFT JOIN
			`trainer_profiles` ON `messages`.`from_user_id`=`trainer_profiles`.`user_id`
		WHERE
			`messages`.`for_user_id` = '".$this->user_id."'
		OR
			`messages`.`from_user_id` = '".$this->user_id."'
		ORDER BY
			`messages`.`id` ASC
		")->result_array();

		if(empty($inbox)){
			$messages[] = 'Your inbox is currently empty.';
		}
		$this->common->display(array(
			'view' => 'dashboard/messages/index',
			'template' => 'templates/dashboard',
			'title' => 'Messages',
			'extra_css' => array(
				CSS_PATH.'dashboard.css'
			),
			'template_data' => array(
				'name' => $user['first_name'],
				'unread' => $this->data->unread_messages()
			),
			'content' => array(
				'errors' => implode('<br />', $this->common->merge_flash('errors', $errors)),
				'messages' => implode('<br />', $this->common->merge_flash('messages', $messages)),
				'inbox' => $inbox,
				'user' => $user
			)
		));
	}
}