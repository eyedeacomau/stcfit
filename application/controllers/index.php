<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Index extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->is_trainer = $this->session->userdata('is_trainer');
		$this->is_admin = $this->session->userdata('is_admin');
		
		/*
		 *
		 *	Added by Andrew 12/09/2014
		 *	If a referral cookie exists, save int othe session. Continues at end of registration.
		 *
		 */

		if($this->input->get('ref') && $this->input->cookie('refer') == false){
			
			$cookie = array(
			    'name'   => 'refer',
			    'value'  => $this->input->get('ref'),
			    'expire' => '7776000',
			    'domain' => '.stcfit.com',
			    'secure' => TRUE
			);
			
			$this->input->set_cookie($cookie);
		}
		
		
		/*
		 *
		 *	end Added by Andrew 12/09/2014
		 *
		 */
		
	}

	function index() {
		
		
		$errors = array();
		$success_messages = array();
		$messages = array();
		$this->common->display(array(
			'view' => 'frontend/home',
			'title' => 'Home',
			'content' => array(
				'errors' => implode('<br />', $this->common->merge_flash('errors', $errors)),
				'messages' => implode('<br />', $this->common->merge_flash('messages', $messages)),
				'success_messages' => implode('<br />', $this->common->merge_flash('success_messages', $success_messages))
			)
		));
	}

	function show_404(){
		$this->common->display(array(
			'view' => '4oh4',
			'title' => '404 - Page Not Found!',
			'template' => 'templates/page',
			'content' => array()
		));
	}

	function cancel_subscription($user_id=null, $key=null){
		if(!empty($user_id) && !empty($key)){
			$check = $this->db->query("SELECT
				`account_deleted`.*,
				`user_profiles`.`paid_period_ends`
			FROM
				`account_deleted`
			INNER JOIN
				`user_profiles` ON `account_deleted`.`user_id`=`user_profiles`.`user_id`
			WHERE
				`account_deleted`.`user_id`='".mysql_real_escape_string($user_id)."'
			AND
				`account_deleted`.`obsolete` = 0
			AND
				`account_deleted`.`confirmation_key`='".mysql_real_escape_string($key)."'
			LIMIT
				1");
			if($check->num_rows() > 0){
				$deferral_periods = $this->config->item('deferral_periods');

				$check = $check->row_array();
				$this->db->set(array(
					'user_confirmed' => 1
				));
				$this->db->where('id', $check['id']);
				$this->db->update('account_deleted');
				if(!empty($check['defer_til'])){
					$expires = ($check['paid_period_ends']-(strtotime('+'.$deferral_periods[intval($check['defer_period_id'])].' months')-time()));
					$this->db->set(array(
						'paid_period_ends' => $expires
					));
					$this->db->where('user_id', $user_id);
					$this->db->update('user_profiles');
				}
				$this->session->set_flashdata('success_messages', array('You have successfully cancelled your STC Fit subscription. You may retain access for your already purchased period.'));
				redirect('/');
			} else {
				$this->session->set_flashdata('errors', array('The cancellation key provided was invalid or has expired.'));
				redirect('/');
			}
		}
	}

	function continue_registration($profile_id=null, $key=null){
		if(empty($profile_id) || empty($key)){
			$this->session->set_flashdata('errors', array('Something went wrong. Please restart your registration.'));
			redirect('/');
		}

		if($this->tank_auth->is_logged_in()){
			$this->tank_auth->logout();
		}

		$this->session->set_userdata(array());
		$profile = $this->db->query("SELECT
			`user_profiles`.`id` AS `profile_id`
		FROM
			`user_profiles`
		WHERE
			`user_profiles`.`signup_complete`=0
		AND
			`user_profiles`.`id`='".mysql_real_escape_string($profile_id)."'
		AND
			`user_profiles`.`continue_key`='".mysql_real_escape_string($key)."'
		LIMIT
			1
		");

		if($profile->num_rows() < 1){
			$this->session->set_flashdata('errors', array('Something went wrong. Please restart your registration.'));
			redirect('/');
		}

		$profile = $profile->row_array();
		$this->session->set_userdata(array(
			'profile_id' => $profile['profile_id']
		));
		$this->load->model('data');
		$this->data->registration_step($profile['profile_id']);
	}

	
	public function image_loader($path=null, $image_requested=null){
		$path = urldecode($path);
		if(!empty($image_requested)){
			$size_config = $this->config->item('allowed_image_sizes');
			$img_details = explode("-", $image_requested);
			$img_size = $img_details[0];
			$img_name = $img_details[1];
			if(!file_exists(FCPATH.$path.$image_requested)){
				foreach($size_config as $img){
					if($img['size'] == $img_size){
						system('/usr/bin/convert '.FCPATH.$path.$img_name.$img['params'].FCPATH.$path.$image_requested);
						$fp = fopen(FCPATH.$path.$image_requested, 'rb');
						header("Content-Type: image/".pathinfo($img_name, PATHINFO_EXTENSION));
						header("Content-Length: " . filesize(FCPATH.$path.$image_requested));
						fpassthru($fp);
						exit;
					}
				}
			}
		}
	}
}