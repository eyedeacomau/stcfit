<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Exercise_Library extends Admin_Controller {
	function __construct(){
		parent::__construct();
	}

	function index() {
		$errors = array();
		$success_messages = array();
		$this->load->model('admin');
		
		if($this->input->post('add_edit_submit')){
			$this->load->library('form_validation');
			$this->form_validation->set_rules(array(
				array('field' => 'exercise_identifier', 'label' => 'Exercise Identifier', 'rules' => 'trim'),
				array('field' => 'title', 'label' => 'Title', 'rules' => 'required|trim'),
				array('field' => 'description', 'label' => 'Description', 'rules' => 'required|trim'),
				array('field' => 'video_link', 'label' => 'Video Link', 'rules' => 'trim'),
				array('field' => 'benefits', 'label' => 'Benefits', 'rules' => 'trim'),
				array('field' => 'low_modifiers', 'label' => 'Low Modifiers', 'rules' => 'trim'),
				array('field' => 'medium_modifiers', 'label' => 'Medium Modifiers', 'rules' => 'trim'),
				array('field' => 'high_modifiers', 'label' => 'High Modifiers', 'rules' => 'trim'),
				array('field' => 'image_1', 'label' => 'Image 1', 'rules' => 'trim'),
				array('field' => 'image_2', 'label' => 'Image 2', 'rules' => 'trim'),
				array('field' => 'image_3', 'label' => 'Image 3', 'rules' => 'trim')
			));

			if($this->form_validation->run()){
				$existing = $this->input->post('old_images');
				$supported = array(
					'jpg',
					'jpeg',
					'png',
					'gif'
				);
				$images = array();
				if(!empty($_FILES['images'])){
					for($x=0; $x<(count($_FILES['images']['name']));$x++){
						if(!empty($_FILES['images']['name'][$x])){
							$name = md5(microtime().time().rand(1,10000));
							$tmp = $_FILES['images']['tmp_name'][$x];
							$ext = pathinfo($_FILES['images']['name'][$x], PATHINFO_EXTENSION);
							if(!in_array($ext, $supported)){
								$errors[] = 'Image type not supported!';
							} elseif($_FILES['images']['size'][$x] > 3145728){ // 3 MB
								$errors[] = 'The file you are attempting to upload exceeds 3MB, please make the image smaller before uploading.';
							} else {
								move_uploaded_file($tmp,FCPATH.EXERCISE_IMG_PATH.$name.'.'.$ext);
								$images[$x] = $name.'.'.$ext;
							}
						} else {
							if(!empty($existing[$x])){
								$images[$x] = $existing[$x];
							}
						}
					}
				}
				if(empty($errors)){
					preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+(?=\?)|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $this->input->post('video_link'), $youtube_video);
					$exercise_identifier = $this->input->post('exercise_identifier');
					if(empty($exercise_identifier)){
						$new = $this->db->query("SELECT MAX(`exercise_identifier`) AS `max_identifier` FROM `exercises`")->row_array();
						if(empty($new['max_identifier'])){
							$new['max_identifier'] = 0;
						}
						$exercise_identifier = (intval($new['max_identifier'])+1);
					}
					$data = array(
						'name' => $this->input->post('title'),
						'description' => $this->input->post('description'),
						'benefits' => $this->input->post('benefits'),
						'image_1' => (!empty($images[0]) ? $images[0]:null),
						'image_2' => (!empty($images[1]) ? $images[1]:null),
						'image_3' => (!empty($images[2]) ? $images[2]:null),
						'low_modifiers' => $this->input->post('low_modifiers'),
						'medium_modifiers' => $this->input->post('medium_modifiers'),
						'high_modifiers' => $this->input->post('high_modifiers'),
						'video_link' => (!empty($youtube_video[0]) ? $youtube_video[0]:null),
						'circuit' => $this->input->post('is_circuit'),
						'exercise_identifier' => $exercise_identifier,
						'valid_from' => time()
					);

					if($this->input->post('exercise_identifier')){
						// Making all other versions of this exercise obsolete.
						$this->db->set(array(
							'valid_to' => time()
						));
						$this->db->where('exercise_identifier', $this->input->post('exercise_identifier'));
						$this->db->update('exercises');
					}
					$this->db->set($data);
					$this->db->insert('exercises');
					$success_messages[] = 'Exercise action successfully completed!';
				}
			} else {
				$errors = $this->form_validation->error_array();
			}
		}
		$exercises = $this->admin->get_exercises();
		$circuits = $this->admin->get_circuits();

		$this->common->display(array(
			'view' => 'admin/exercise_library',
			'template' => 'templates/admin',
			'title' => 'Admin Dashboard - Exercise Library',
			'template_data' => array(
				'page' => $this->uri->segment(2),
				'new_users' => $this->admin->new_users()
			),
			'extra_css' => array(
				CSS_PATH.'/plugins/chosen.min.css'
			),
			'extra_js' => array(
				JS_PATH.'plugins/chosen.jquery.min.js'
			),
			'content' => array(
				'errors' => implode('<br />', $this->common->merge_flash('errors', $errors)),
				'success_messages' => implode('<br />', $this->common->merge_flash('success_messages', $success_messages)),
				'exercises' => $exercises,
				'circuits' => $circuits
			)
		));
	}

	function add_edit_exercise($exercise_table_id=null){
		$exercise = array();

		if(!empty($exercise_table_id)){
			$exercise = $this->db->query("SELECT * FROM `exercises` WHERE `id`='".mysql_real_escape_string($exercise_table_id)."' AND `circuit`=0 AND `valid_to` IS NULL LIMIT 1")->row_array();
		}

		$this->load->view('includes/exercise_library/exercise', array('exercise' => $exercise, 'image_sizes' => $this->config->item('allowed_image_sizes')));
	}

	function add_edit_circuit($exercise_table_id=null){
		$circuit = array();

		if(!empty($exercise_table_id)){
			$circuit = $this->db->query("SELECT * FROM `exercises` WHERE `id`='".mysql_real_escape_string($exercise_table_id)."' AND `circuit`=1 AND `valid_to` IS NULL LIMIT 1")->row_array();
		}

		$this->load->view('includes/exercise_library/circuit', array('circuit' => $circuit, 'image_sizes' => $this->config->item('allowed_image_sizes')));
	}

	function delete_exercise($table_key=null){
		$this->db->set(array(
			'valid_to' => time()
		));
		$this->db->where('id', $table_key);
		$this->db->update('exercises');

		die(json_encode(array('success' => true)));
	}
}