<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Program_Management extends Admin_Controller {
	function __construct(){
		parent::__construct();
	}

	function index() {
		$errors = array();
		$success_messages = array();
		$this->load->model('admin');
		$this->load->library('ckeditor');
		$this->ckeditor->basePath = '/assets/ckeditor/';
		$this->ckeditor->config['toolbar'] = array(
			array('Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord',	'-', 'Undo', 'Redo'),
			array('Bold', 'Italic', 'Underline', 'NumberedList', 'BulletedList', 'Source')
	    );
		$this->ckeditor->config['language'] = 'en';
		$this->ckeditor->config['width'] = '260px';
		$this->ckeditor->config['height'] = '350px';

		if($this->input->post('submit_food_plans')){
			$this->load->library('form_validation');
			$this->form_validation->set_rules(array(
				array('field' => 'id[]', 'label' => 'Plan ID', 'rules' => 'required|trim'),
				array('field' => 'name[]', 'label' => 'Plan Name', 'rules' => 'required|trim'),
				array('field' => 'description[]', 'label' => 'Plan Description', 'rules' => 'required|trim'),
				array('field' => 'entries[]', 'label' => 'Plan entry amount', 'rules' => 'required|trim')
			));

			if($this->form_validation->run() == true){
				$id = $this->input->post('id');
				$plan_names = $this->input->post('name');
				$description = $this->input->post('description');
				$entries = $this->input->post('entries');
				$old_file = $this->input->post('old_file');

				$plans = $this->admin->food_plans();

				$batch = array();
				$supported = array(
					'pdf'
				);
				$files = array();
				foreach($id as $entry){
					$file = null;
					if(!empty($_FILES['files']['name'][intval($entry['id'])])){
						$name = strtolower(str_replace(array('-', '_', '\'', '"', ' '), array('', '', '', '', '_'), $_FILES['files']['name'][intval($entry['id'])]));
						$tmp = $_FILES['files']['tmp_name'][intval($entry['id'])];
						$ext = pathinfo($_FILES['files']['name'][intval($entry['id'])], PATHINFO_EXTENSION);

						if(in_array($ext, $supported)){
							if(file_exists(FCPATH.FOOD_PLAN_DOC_PATH.$old_file[intval($entry['id'])])){
								unlink(FCPATH.FOOD_PLAN_DOC_PATH.$old_file[intval($entry['id'])]);
							}
							move_uploaded_file($tmp,FCPATH.FOOD_PLAN_DOC_PATH.$name);
							$file = $name;
						} else {
							$errors[] = 'file type not supported! PDF Only.';
						}
					}

					if(empty($file) && !empty($old_file[intval($entry['id'])])){
						$file = $old_file[intval($entry['id'])];
					}

					$batch[] = array(
						'id' => $entry['id'],
						'title' => $plan_names[$entry['id']],
						'description' => $description[$entry['id']],
						'entries' => $entries[$entry['id']],
						'document_path' => $file
					);
				}
				
				if(!empty($batch) && empty($errors)){
					$this->db->update_batch('food_plans', $batch, 'id');
					$success_messages[] = 'Successfully updated food plans.';
				}
			} else {
				$errors = $this->form_validation->error_array();
			}
		}

		if($this->input->post('submit_program')){
			$this->load->library('form_validation');
			$this->form_validation->set_rules(array(
				array('field' => 'training_type', 'label' => 'Training type', 'rules' => 'required|trim'),
				array('field' => 'training_phase', 'label' => 'Training phase', 'rules' => 'required|trim'),
				array('field' => 'training_session', 'label' => 'Training session', 'rules' => 'required|trim'),
				array('field' => 'exercise[]', 'label' => 'Exercise', 'rules' => 'trim'),
				array('field' => 'circuit[]', 'label' => 'Circuit', 'rules' => 'trim'),
				array('field' => 'sets[]', 'label' => 'Sets', 'rules' => 'trim|integer'),
				array('field' => 'reps[]', 'label' => 'Reps', 'rules' => 'trim'),
				array('field' => 'tempo[]', 'label' => 'Tempo', 'rules' => 'trim')
			));

			if($this->form_validation->run() == true){
				$session_identifier = $this->input->post('training_session');

				$this->db->set(array(
					'valid_to' => time()
				));
				$this->db->where('session_identifier', $session_identifier);
				$this->db->where('valid_to IS NULL',null);
				$this->db->update('session_content');

				$batch = array();
				$order = 0;

				$exercises = $this->input->post('exercise');
				$circuits = $this->input->post('circuit');

				$sets = $this->input->post('sets');
				$reps = $this->input->post('reps');
				$tempo = $this->input->post('tempo');
				
				if(!empty($exercises)){
					foreach($exercises as $key => $value){
						$order++;
						$batch[] = array(
							'exercise_identifier' => $value,
							'reps' => $reps[$key],
							'sets' => $sets[$key],
							'tempo' => $tempo[$key],
							'session_identifier' => $session_identifier,
							'valid_from' => time(),
							'order' => $order
						);
					}
				}
				
				if(!empty($circuits)){
					foreach($circuits as $circuit){
						$order++;
						$batch[] = array(
							'exercise_identifier' => $circuit,
							'session_identifier' => $session_identifier,
							'reps' => null,
							'sets' => null,
							'tempo' => null,
							'valid_from' => time(),
							'order' => $order
						);
					}
				}
				if(!empty($batch)){
					$this->db->insert_batch('session_content', $batch);
				}
				
				$success_messages[] = 'Successfully updated session. Please note that this will not affect any users that are currently in this phase, however will apply to any future users.';
			} else {
				$errors = $this->form_validation->error_array();
			}
		}


		$training_types = $this->admin->get_training_types();

		$this->common->display(array(
			'view' => 'admin/program_management',
			'template' => 'templates/admin',
			'template_data' => array(
				'page' => $this->uri->segment(2),
				'new_users' => $this->admin->new_users()
			),
			'extra_css' => array(
				CSS_PATH.'plugins/chosen.min.css'
			),
			'extra_js' => array(
				JS_PATH.'plugins/chosen.jquery.min.js',
				JS_PATH.'plugins/jquery.ui.sortable.min.js',
				'/assets/ckeditor/ckeditor.js'
			),
			'title' => 'Admin Dashboard - Program Management',
			'content' => array(
				'errors' => implode('<br />', $this->common->merge_flash('errors', $errors)),
				'success_messages' => implode('<br />', $this->common->merge_flash('success_messages', $success_messages)),
				'training_types' => $training_types,
				'food_plans' => $this->admin->food_plans()
			)
		));
	}

	function get_data($type=null){
		if($this->input->is_ajax_request() == false){
			$this->session->set_flashdata('errors', array('You cannot access this area directly'));
			redirect('/admin/program-management');
		}


		$this->load->model('admin');
		$training_type = $this->input->post('training_type');
		$phase_number = $this->input->post('phase_number');
		$session_identifier = $this->input->post('session_identifier');

		switch($type){
			case 'phases':
				echo json_encode($this->admin->get_training_phases($training_type));
				break;
			case 'sessions':
				echo json_encode($this->admin->get_phase_sessions($training_type, $phase_number));
				break;
			case 'exercises':
				$data = array(
					'all_exercises' => $this->admin->get_exercises(),
					'all_circuits' => $this->admin->get_circuits(),
					'session_exercises' => $this->admin->get_session_exercises($session_identifier)
				);

				$this->load->view('includes/program_management/main_include', $data);
				break;
		}
	}

	function add_edit_phase(){
		if($this->input->is_ajax_request() == false){
			$this->session->set_flashdata('errors', array('You cannot access this area directly'));
			redirect('/admin/program-management');
		}
		if($this->input->post('add_update_phase')){
			$this->load->library('form_validation');
			$this->form_validation->set_rules(array(
				array('field' => 'training_type', 'label' => 'Training type', 'rules' => 'required|trim'),
				array('field' => 'phase_name', 'label' => 'Phase Name', 'rules' => 'required|trim'),
				array('field' => 'phase_description', 'label' => 'Phase Description', 'rules' => 'required|trim')
			));

			if($this->form_validation->run() == true){
				$youtube_video = null;
				preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+(?=\?)|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $this->input->post('video_link'), $youtube_video);
				$data = array(
					'name' => $this->input->post('phase_name'),
					'description' => $this->input->post('phase_description'),
					'training_type' => $this->input->post('training_type'),
					'video_link' => (!empty($youtube_video[0]) ? $youtube_video[0]:null),
					'valid_from' => time(),
					'phase_number' => 1
				);
				if($this->input->post('phase_number')){
					$data['phase_number'] = $this->input->post('phase_number');
				} else {
					$new_phase = $this->db->query("SELECT
						MAX(`phases`.`phase_number`) AS `current_max`
					FROM
						`phases`
					WHERE
						`phases`.`valid_to` IS NULL
					AND
						`phases`.`training_type`='".mysql_real_escape_string($this->input->post('training_type'))."'
					")->row_array();
					if(!empty($new_phase['current_max'])){
						$data['phase_number'] = (intval($new_phase['current_max'])+1);
					}
				}

				$this->db->set(array(
					'valid_to' => time()
				));
				$this->db->where('training_type', $data['training_type']);
				$this->db->where('phase_number', $data['phase_number']);
				$this->db->where('valid_to IS NULL', null);
				$this->db->update('phases');

				$this->db->set($data);
				$this->db->insert('phases');

				echo json_encode(array(
					'success' => true,
					'training_type' => $data['training_type']
				));
				exit;
			} else {
				$errors = $this->form_validation->error_array();
				echo json_encode(array(
					'success' => false,
					'errors' => implode('<br />', $errors)
				));
				exit;
			}
		} else {
			$phase = array();
			$phase_number = $this->input->post('phase_number');
			$training_type = $this->input->post('training_type');
			$action = $this->input->post('action');

			if(!empty($phase_number) && !empty($training_type) && $action == 'edit'){
				$this->load->model('admin');
				$phase = $this->admin->get_phase($phase_number, $training_type);
			}
			$this->load->view('includes/program_management/add_edit_phase', array('phase' => $phase, 'training_type' => $training_type));
		}
	}

	function add_edit_session(){
		if($this->input->is_ajax_request() == false){
			$this->session->set_flashdata('errors', array('You cannot access this area directly'));
			redirect('/admin/program-management');
		}

		if($this->input->post('add_update_session')){
			$this->load->library('form_validation');
			$this->form_validation->set_rules(array(
				array('field' => 'training_type', 'label' => 'Training type', 'rules' => 'required|trim'),
				array('field' => 'phase_number', 'label' => 'Training type', 'rules' => 'required|trim'),
				array('field' => 'session_name', 'label' => 'Session Name', 'rules' => 'required|trim'),
				array('field' => 'session_description', 'label' => 'Session Description', 'rules' => 'required|trim')
			));

			if($this->form_validation->run() == true){
				$youtube_video = null;
				preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+(?=\?)|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $this->input->post('video_link'), $youtube_video);
				$data = array(
					'name' => $this->input->post('session_name'),
					'description' => $this->input->post('session_description'),
					'video_link' => (!empty($youtube_video[0]) ? $youtube_video[0]:null),
					'valid_from' => time(),
					'session_identifier' => 1
				);
				if($this->input->post('session_identifier')){
					$data['session_identifier'] = $this->input->post('session_identifier');
				} else {
					$new_session = $this->db->query("SELECT
						MAX(`sessions`.`session_identifier`) AS `current_max`
					FROM
						`sessions`
					WHERE
						`sessions`.`valid_to` IS NULL
					")->row_array();
					if(!empty($new_session['current_max'])){
						$data['session_identifier'] = (intval($new_session['current_max'])+1);
					}
				}

				// Setting all current sessions with this identifier to obsolete.
				$this->db->set(array(
					'valid_to' => time()
				));
				$this->db->where('session_identifier', $data['session_identifier']);
				$this->db->where('valid_to IS NULL',null);
				$this->db->update('sessions');
				// ---------------------------------------------------------------

				//  Inserting the new session
				$this->db->set($data);
				$this->db->insert('sessions');
				// --------------------------------

				// Setting sessions to obsolete in the phase sessions table
				$this->db->set(array(
					'valid_to' => time()
				));

				$this->db->where('session_identifier', $data['session_identifier']);
				$this->db->where('training_type', $this->input->post('training_type'));
				$this->db->where('phase_number', $this->input->post('phase_number'));
				$this->db->where('valid_to IS NULL', null);

				$this->db->update('phase_sessions');
				//----------------------------------------------------------

				// Adding the new entry to the phase sessions table.
				$order = 1;
				if($this->input->post('session_order')){
					$order = $this->input->post('session_order');
				} else {
					$new_order = $this->db->query("SELECT
						MAX(`order`) AS `order`
					FROM
						`phase_sessions`
					WHERE
						`valid_to` IS NULL
					AND
						`phase_sessions`.`training_type`='".mysql_real_escape_string($this->input->post('training_type'))."'
					AND
						`phase_sessions`.`phase_number`='".mysql_real_escape_string($this->input->post('phase_number'))."'
					")->row_array();
					if(!empty($new_order['order'])){
						$order = (intval($new_order['order'])+1);
					}
				}

				$this->db->set(array(
					'session_identifier' => $data['session_identifier'],
					'training_type' => $this->input->post('training_type'),
					'valid_from' => time(),
					'phase_number' => $this->input->post('phase_number'),
					'order' => $order
				));

				$this->db->insert('phase_sessions');
				// ------------------------------------------------------------

				echo json_encode(array(
					'success' => true,
					'phase_number' => $this->input->post('phase_number'),
					'training_type' => $this->input->post('training_type')
				));
				exit;
			} else {
				$errors = $this->form_validation->error_array();
				echo json_encode(array(
					'success' => false,
					'errors' => implode('<br />', $errors)
				));
				exit;
			}
		} else {
			$session = array();
			$session_identifier = $this->input->post('session_identifier');
			$phase_number = $this->input->post('phase_number');
			$training_type = $this->input->post('training_type');
			$action = $this->input->post('action');

			if(!empty($session_identifier) && $action == 'edit'){
				$this->load->model('admin');
				$session = $this->admin->get_session($session_identifier);
			}
			$this->load->view('includes/program_management/add_edit_session', array('session' => $session, 'training_type' => $training_type, 'phase_number' => $phase_number));
		}
	}

	function delete_phase(){
		if($this->input->is_ajax_request() == false){
			$this->session->set_flashdata('errors', array('You cannot access this area directly'));
			redirect('/admin/program-management');
		}

		$phase_number = $this->input->post('phase_number');
		$training_type = $this->input->post('training_type');

		if(!empty($phase_number) && !empty($training_type)){

			$this->db->set(array(
				'valid_to' => time()
			));

			$this->db->where('phase_number', $phase_number);
			$this->db->where('training_type', $training_type);
			$this->db->where('valid_to IS NULL', null);
			$this->db->update('phases');

			// ---------------------------------------------------------------------

			$this->db->set(array(
				'valid_to' => time()
			));

			$this->db->where('phase_number', $phase_number);
			$this->db->where('training_type', $training_type);
			$this->db->where('valid_to IS NULL', null);
			$this->db->update('phase_sessions');



			echo json_encode(array(
				'deleted' => true,
				'remove_id' => $phase_number,
				'change' => 'phase'
			));
			exit;
		}
	}

	function delete_session(){
		if($this->input->is_ajax_request() == false){
			$this->session->set_flashdata('errors', array('You cannot access this area directly'));
			redirect('/admin/program-management');
		}

		$session_identifier = $this->input->post('session_identifier');
		if(!empty($session_identifier)){
			$this->db->set(array(
				'valid_to' => time()
			));

			$this->db->where('session_identifier', $session_identifier);
			$this->db->where('valid_to IS NULL', null);
			$this->db->update('sessions');
			// --------------------------------------------------------
			$this->db->set(array(
				'valid_to' => time()
			));

			$this->db->where('session_identifier', $session_identifier);
			$this->db->where('valid_to IS NULL', null);
			$this->db->update('phase_sessions');
			echo json_encode(array(
				'deleted' => true,
				'remove_id' => $session_identifier,
				'change' => 'session'
			));
			exit;
		}
	}
}