<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reports extends Admin_Controller {
	function __construct(){
		parent::__construct();
	}

	function index() {
		$errors = array();
		$this->load->model('admin');
		
		$trainers = $this->admin->get_trainers();
		$training_types = $this->admin->get_training_types();
		$genders = $this->config->item('genders');
		$marital = $this->config->item('marital_statuses');
		$filters = array();

		if($this->input->post('filter')){
			$filters = $this->input->post('filter');
			foreach($filters as $key => $value){
				if(empty($value)){
					unset($filters[$key]);
				}
			}
		}

		$reports = $this->admin->reports($filters);
		if($this->input->post('export') && !empty($reports)){
			$this->load->dbutil();
			$this->load->helper('download');
			$csv = $this->dbutil->csv_from_result($this->db->query($this->db->last_query()));
			force_download('Data Export - '.date('d-m-Y H:i:s').'.csv', $csv);
		}

		$this->common->display(array(
			'view' => 'admin/reports',
			'template' => 'templates/admin',
			'template_data' => array(
				'page' => $this->uri->segment(2),
				'new_users' => $this->admin->new_users()
			),
			'extra_css' => array(
				CSS_PATH.'plugins/chosen.min.css',
				CSS_PATH.'plugins/datepicker.css'
			),
			'extra_js' => array(
				JS_PATH.'plugins/highcharts.js',
				JS_PATH.'plugins/chosen.jquery.min.js',
				JS_PATH.'admin/common.js',
				JS_PATH.'plugins/bootstrap-datepicker.js'
			),
			'title' => 'Admin Dashboard - Reports',
			'content' => array(
				'errors' => implode('<br />', $this->common->merge_flash('errors', $errors)),
				'reports' => $reports,
				'trainers' => $trainers,
				'training_types' => $training_types,
				'genders' => $genders,
				'marital' => $marital,
				'filters' => $filters
			)
		));
	}
}