<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_Management extends Admin_Controller {
	function __construct(){
		parent::__construct();
	}

	function index() {
		$errors = array();
		$messages = array();
		$success_messages = array();
		$this->load->model('admin');
		$search_params = '';
		$tab = '';

		$arr_by_trainers = false;
		if($this->input->get_post('tab')){
			$tab = $this->input->get_post('tab');
		}

		if($this->input->get_post('arr_by_trainer')){
			$arr_by_trainers = true;
		}
		if($this->input->post('user_search')){
			$search_params = mysql_real_escape_string($this->input->get_post('search_params'));
		}

		if($this->input->post('export_users')){
			$this->admin->export_users();
		}

		if($this->input->post('trainer_create')) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules(array(
				array('field' => 'trainer_first_name', 'label' => 'First Name', 'rules' => 'required|trim'),
				array('field' => 'trainer_last_name', 'label' => 'Last Name', 'rules' => 'required|trim'),
				array('field' => 'trainer_email', 'label' => 'Email', 'rules' => 'required|xss_clean|valid_email|allowed_email'),
				array('field' => 'trainer_contact', 'label' => 'Phone', 'rules' => 'required|xss_clean'),
				array('field' => 'trainer_password', 'label' => 'Password', 'rules' => 'required|trim'),
				array('field' => 'trainer_confirm_password', 'label' => 'Confirm Password', 'rules' => 'required|trim|matches[trainer_password]')
			));

			if($this->form_validation->run() == true) {
				$user = $this->tank_auth->create_user('', $this->input->post('trainer_email'), $this->input->post('trainer_password'), false, false);
				if(!is_null($user)){ // User Created successfully.
					$this->db->set(array(
						'user_id' => $user['user_id'],
						'first_name' => $this->input->post('trainer_first_name'),
						'last_name' => $this->input->post('trainer_last_name'),
						'contact' => $this->input->post('trainer_contact')
					));
					$this->db->insert('trainer_profiles');

					$this->db->set(array(
						'is_trainer' => 1
					));
					$this->db->where('id', $user['user_id']);
					$this->db->update('users');

					$success_messages[] = 'Trainer created successfully';
				}
			} else {
				$errors = $this->form_validation->error_array();
			}
		}

		$users = $this->admin->get_users($search_params, $arr_by_trainers);
		$new_users = $this->admin->new_users();
		$this->common->display(array(
			'view' => 'admin/users',
			'template' => 'templates/admin',
			'template_data' => array(
				'page' => $this->uri->segment(2),
				'new_users' => $new_users
			),
			'title' => 'Admin Dashboard - Users',
			'content' => array(
				'errors' => implode('<br />', $this->common->merge_flash('errors', $errors)),
				'messages' => implode('<br />', $this->common->merge_flash('messages', $messages)),
				'success_messages' => implode('<br />', $this->common->merge_flash('success_messages', $success_messages)),
				'users' => $users,
				'tab' => $tab,
				'search_params' => $search_params,
				'new_users' => $new_users
			)
		));
	}

	function change_trainer(){
		if($this->input->is_ajax_request() == false){
			$this->session->set_flashdata('errors', array('You do not have permission to view this area.'));
		}

		$check = $this->db->query("SELECT
			`trainer_profiles`.`user_id`,
			`trainer_profiles`.`first_name`,
			`trainer_profiles`.`last_name`
		FROM
			`users`
		INNER JOIN
			`trainer_profiles` ON `users`.`id`=`trainer_profiles`.`user_id`
		WHERE
			`users`.`is_trainer`=1
		AND
			`users`.`banned`=0
		AND
			`users`.`id`='".mysql_real_escape_string($this->input->post('trainer'))."'
		LIMIT
			1
		");

		if($check->num_rows() < 1){
			echo json_encode(array('changed' => false));
			exit;
		}

		$trainer = $check->row_array();
		$this->db->set(array(
			'allocated_trainer' => $this->input->post('trainer')
		));
		$this->db->where('user_id', $this->input->post('user_id'));
		$this->db->update('user_profiles');

		$this->db->set(array(
			'obsolete' => 1
		));
		$this->db->where('client_id', $this->input->post('user_id'));
		$this->db->update('trainer_reminders');

		$this->db->set(array(
			'trainer_id' => $this->input->post('trainer'),
			'client_id' => $this->input->post('user_id'),
			'reminder_date' => time(),
			'notes' => 'New user alert! This user has been assigned to you by the administration. Please welcome them.'
		));
		$this->db->insert('trainer_reminders');
		echo json_encode(array('changed' => true, 'trainer' => $trainer['first_name'].' '.$trainer['last_name']));
		exit;
	}
}
