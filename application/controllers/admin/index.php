<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Index extends Admin_Controller {
	function __construct(){
		parent::__construct();
	}

	function test() {
		$renewal_types = $this->config->item('payment_methods');
		print_r($renewal_types);
	}
	function index() {
		$errors = array();
		$this->load->model('admin');
		$revenue = $this->admin->revenue_data();
		$trainer_data = $this->admin->trainer_data();
		$this->common->display(array(
			'view' => 'admin/dashboard',
			'template' => 'templates/admin',
			'template_data' => array(
				'page' => $this->uri->segment(2),
				'new_users' => $this->admin->new_users()
			),
			'extra_js' => array(
				JS_PATH.'plugins/highcharts.js',
				JS_PATH.'admin/dashboard.js'
			),
			'title' => 'Admin Dashboard',
			'content' => array(
				'errors' => implode('<br />', $this->common->merge_flash('errors', $errors)),
				'revenue' => $revenue,
				'trainer_data' => $trainer_data
			)
		));
	}

	function trainer_login($user_id=null){
		if(empty($user_id)){
			return false;
		}
		$login = $this->tank_auth->login_as_user($user_id);
		if($login == false){
			$this->session->set_flashdata('errors', array('Cannot login as this trainer.'));
			redirect('/admin/users');
		}
	}

	function user_login($user_id=null){
		if(empty($user_id)){
			return false;
		}

		$login = $this->tank_auth->login_as_user($user_id);
		if($login == false){
			$this->session->set_flashdata('errors', array('Cannot login as this user.'));
			redirect('/admin/users');
		}
	}

	function suspend_user($user_id=null){
		if(empty($user_id)){
			$this->session->set_flashdata('errors', array('Cannot complete designated action.'));
			redirect('/admin');
		}

		$check = $this->db->query("SELECT
			`users`.`is_trainer`,
			`users`.`id` AS `user_id`
		FROM
			`users`
		LEFT JOIN
			`user_profiles` ON `users`.`id`=`user_profiles`.`user_id`
		WHERE
			`users`.`id`='".mysql_real_escape_string($user_id)."'
		LIMIT
			1
		");

		if($check->num_rows() < 1){
			$this->session->set_flashdata('errors', array('You do not have the necessary permission to suspend this user.'));
			redirect('/admin/users');
		}

		$check = $check->row_array();

		if(!empty($check['is_trainer'])){
			$this->db->set(array(
				'allocated_trainer' => null
			));
			$this->db->where('allocated_trainer', $check['user_id']);
			$this->db->update('user_profiles');
		}

		$this->db->set(array(
			'banned' => 1,
			'ban_reason' => 'Suspended by admin'
		));
		$this->db->where('id', $user_id);
		$this->db->update('users');
		$this->session->set_flashdata('success_messages', array('You have successfully suspended this user. This may also be undone in this same panel.'));
		redirect('/admin/users');
	}

	function reactivate_user($user_id=null){
		if(empty($user_id)){
			$this->session->set_flashdata('errors', array('Cannot complete designated action.'));
			redirect('/admin');
		}

		$check = $this->db->query("SELECT
			`user_profiles`.`id`
		FROM
			`users`
		LEFT JOIN
			`user_profiles` ON `users`.`id`=`user_profiles`.`user_id`
		WHERE
			`users`.`id`='".mysql_real_escape_string($user_id)."'
		LIMIT
			1");
		if($check->num_rows() < 1){
			$this->session->set_flashdata('errors', array('You do not have the necessary permission to re-activate this user.'));
			redirect('/admin/users');
		}
		$this->db->set(array(
			'banned' => 0,
			'ban_reason' => null
		));
		$this->db->where('id', $user_id);
		$this->db->update('users');
		$this->session->set_flashdata('success_messages', array('You have successfully re-activated this user. This may also be undone in this same panel.'));
		redirect('/admin/users');
	}

	function make_user_paid($user_id = null) {
		if(empty($user_id)){
			$this->session->set_flashdata('errors', array('Cannot complete designated action.'));
			redirect('/admin');
		}
		$this->db->update('user_profiles', array('free' => 0, 'paid_period_ends' => time()), array('user_id' => $user_id));
		$this->session->set_flashdata('success_messages', array('You have successfully made this user paid. This may also be undone in this same panel.'));
		redirect('/admin/users');
	}

	function make_user_free($user_id = null) {
		if(empty($user_id)){
			$this->session->set_flashdata('errors', array('Cannot complete designated action.'));
			redirect('/admin');
		}
		$this->db->update('user_profiles', array('free' => 1, 'paid_period_ends' => strtotime('+1 days', time())), array('user_id' => $user_id));
		$this->session->set_flashdata('success_messages', array('You have successfully made this user free. This may also be undone in this same panel.'));
		redirect('/admin/users');
	}

	public function sort_payment($a, $b){
	    if ($a['order'] == $b['order']) {
	        return 0;
	    }
	    return ($a['order'] < $b['order']) ? -1 : 1;
	}

	function paid_user($user_id=null){
		if(empty($user_id)){
			$this->session->set_flashdata('errors', array('Cannot complete designated action.'));
			redirect('/admin');
		}

		$methods = $this->config->item('payment_methods');

		$result = $this->db->select('renewal_type')->where('user_id',$user_id)->limit(1)->get('user_profiles');
		$result = $result->row_array();
		$method = $result['renewal_type'];

		if(is_null($method)){
			die('This user cannot be paid. They do not have a valid renewal type. Contact MoveDigital.');
		}

		$this->db->set(array(
			'obsolete' => 1
		));
		$this->db->where('user_id', $user_id);
		$this->db->update('billing_failed');

		$this->db->set(array(
			'paid_period_ends' => time()+(86400*$methods[$method]['days'])
		));
		$this->db->where('user_id', $user_id);
		$this->db->update('user_profiles');
		$this->session->set_flashdata('success_messages', array('You have successfully marked this user as paid.'));
		redirect('/admin/users');
	}

	function override_medical($profile_id=null){
		if(empty($profile_id)){
			$this->session->set_flashdata('errors', array('An error occured whilst trying to override the medical for this user.'));
			redirect('/admin/users');
		}
		$this->db->set(array(
			'admin_override' => 1
		));
		$this->db->where('profile_id', $profile_id);
		$this->db->update('pre_exercise_questions');
		$user = $this->db->query("SELECT * FROM `user_profiles` WHERE `user_profiles`.`id`='".mysql_real_escape_string($profile_id)."' LIMIT 1");
		if($user->num_rows() > 0){
			$this->common->send_email(array(
				'email_to' => $user_information['email'],
				'subject' => 'Continue registration!',
				'view' => 'override_medical',
				'data' => array(
					'user' => $user->row_array()
				)
			));
		}

		$this->session->set_flashdata('success_messages', array('You have successfully overridden the failed medical for this user'));
		redirect('/admin/users');
	}
}
