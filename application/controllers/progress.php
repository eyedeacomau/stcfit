<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Progress extends Users_Controller {
	function __construct(){
		parent::__construct();
		if(!$this->tank_auth->is_logged_in()){ // Checking if the user is logged in.
			$this->session->set_flashdata('errors', array('You must be logged in to access the dashboard area.'));
			redirect('/');
		}
		$this->user_id = $this->tank_auth->get_user_id();
		$this->load->driver('cache');
    	$this->cache->clean(); // For some reason that cannot be determined, this page is getting cached in google chrome. This prevents it.
	}

	function index() {
		$errors = array();
		$success_messages = array();
		$this->load->model('data');
		$this->load->model('phase');
		$user = $this->data->get_all_userdata($this->user_id);
		$progress_data = $this->phase->get_progress($user, $user['training_type']);

		$this->common->display(array(
			'view' => 'dashboard/progress/index',
			'template' => 'templates/dashboard',
			'title' => 'Progress',
			'extra_js' => array(
				JS_PATH.'plugins/highcharts.js'
			),
			'extra_css' => array(
				CSS_PATH.'dashboard.css'
			),
			'template_data' => array(
				'name' => $user['first_name'],
				'unread' => $this->data->unread_messages()
			),
			'content' => array(
				'errors' => implode('<br />', $this->common->merge_flash('errors', $errors)),
				'success_messages' => implode('<br />', $this->common->merge_flash('success_messages', $success_messages)),
				'progress_data' => $progress_data
			)
		));
	}

	function new_entry() {
		$errors = array();
		$this->load->model('data');
		$user = $this->data->get_all_userdata($this->user_id);
		if($this->input->post('add_entry')){
			$this->load->library('form_validation');
			$this->form_validation->set_rules(array(
				array('field' => 'weight', 'label' => 'Weight', 'rules' => 'required|trim|numeric'),
				array('field' => 'shoulders', 'label' => 'Shoulders', 'rules' => 'required|trim|numeric'),
				array('field' => 'neck', 'label' => 'Neck', 'rules' => 'required|trim|numeric'),
				array('field' => 'chest', 'label' => 'Chest', 'rules' => 'required|trim|numeric'),
				array('field' => 'waist', 'label' => 'Waist', 'rules' => 'required|trim|numeric'),
				array('field' => 'bicep', 'label' => 'Bicep', 'rules' => 'required|trim|numeric'),
				array('field' => 'hips', 'label' => 'Hips', 'rules' => 'required|trim|numeric'),
				array('field' => 'quads', 'label' => 'Quads', 'rules' => 'required|trim|numeric'),
				array('field' => 'calfs', 'label' => 'Calfs', 'rules' => 'required|trim|numeric'),
				array('field' => 'wrist', 'label' => 'Wrist', 'rules' => 'required|trim|numeric'),
				array('field' => 'forearm', 'label' => 'Forearm', 'rules' => 'required|trim|numeric')
			));
			if($this->form_validation->run()){
				$photo = null;
				if(isset($_FILES['progress_photo']) && $_FILES['progress_photo']['size'] > 0){
					$ext = pathinfo($_FILES['progress_photo']['name'], PATHINFO_EXTENSION);
					$name = md5(time()+rand(1,10000)) . '.'.$ext;
					$allowed = array(
						'jpeg',
						'jpg',
						'png',
						'gif'
					);

					if(in_array($ext, $allowed)){
						$config['upload_path'] = FCPATH.PROGRESS_IMG_PATH;
						$config['allowed_types'] = '*';
						$config['file_name'] = $name;
						$config['max_size']	= '3000';
						$config['max_width']  = '0';
						$config['max_height']  = '0';
						$this->load->library('upload', $config);
						if($this->upload->do_upload('progress_photo')){
							$this->db->set(array(
								'user_id' => $this->user_id,
								'image_name' => $name,
								'date' => time()
							));
							$this->db->insert('progress_photos');
							$photo = $this->db->insert_id();
						} else {
							$errors[] = $this->upload->display_errors();
						}
					} else{
						$errors[] = 'File type not allowed!';
					}
				}
				
				if(empty($errors)){
					$this->db->set(array(
						'user_id' => $this->user_id,
						'weight' => floatval($this->input->post('weight')),
						'shoulders' => floatval($this->input->post('shoulders')),
						'neck' => floatval($this->input->post('neck')),
						'chest' => floatval($this->input->post('chest')),
						'waist' => floatval($this->input->post('waist')),
						'bicep' => floatval($this->input->post('bicep')),
						'hips' => floatval($this->input->post('hips')),
						'quads' => floatval($this->input->post('quads')),
						'calf' => floatval($this->input->post('calfs')),
						'wrist' => floatval($this->input->post('wrist')),
						'forearm' => floatval($this->input->post('forearm')),
						'progress_photo' => $photo,
						'entry_date' => time()
					));
					$this->db->insert('measurement_entries');
					$this->session->set_flashdata('success_messages', array('Successfully added measurement entry for '.date('d/m/Y', time())));
					redirect('/progress');
				}	
			} else {
				$errors = $this->form_validation->error_array();
			}
		}
		$check = $this->db->query("SELECT * FROM `measurement_entries` WHERE `user_id`='".mysql_real_escape_string($this->user_id)."' ORDER BY `id` DESC LIMIT 1");
		$count = $check->num_rows();
		$last_entry = $check->row_array();

		if($count != 0 && (!empty($last_entry) && (time() - intval($last_entry['entry_date'])) < 86400)){ // Checks that the last entry made was at least 24 hours ago.
			$this->session->set_flashdata('errors', array('You have already made an entry today. You must wait a minimum of 24 hours before making another entry'));
			redirect('/progress');
		}

		$this->common->display(array(
			'view' => 'dashboard/progress/new_entry',
			'template' => 'templates/dashboard',
			'title' => 'Progress - New Entry',
			'extra_css' => array(
				CSS_PATH.'dashboard.css'
			),
			'template_data' => array(
				'name' => $user['first_name'],
				'unread' => $this->data->unread_messages()
			),
			'content' => array(
				'errors' => implode('<br />', $this->common->merge_flash('errors', $errors))
			)
		));
	}

	function progress_photos(){
		$errors = array();
		$this->load->model('data');
		$user = $this->data->get_all_userdata($this->user_id);
		$image_sizes = $this->config->item('allowed_image_sizes');

		$photos = $this->db->query("SELECT * FROM `progress_photos` WHERE `user_id`='".$this->user_id."' ORDER BY `id` DESC");
		if($photos->num_rows() < 1){
			$photos = array();
		} else {
			$photos = $photos->result_array();
		}
		$this->common->display(array(
			'view' => 'dashboard/progress/progress_photos',
			'template' => 'templates/dashboard',
			'title' => 'Progress - View Progress Photos',
			'extra_css' => array(
				CSS_PATH.'dashboard.css'
			),
			'template_data' => array(
				'name' => $user['first_name'],
				'unread' => $this->data->unread_messages()
			),
			'content' => array(
				'errors' => implode('<br />', $this->common->merge_flash('errors', $errors)),
				'image_sizes' => $image_sizes,
				'photos' => $photos
			)
		));
	}
}