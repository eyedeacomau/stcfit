<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Registration extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('data');
		if($this->tank_auth->is_logged_in()){
			$user = $this->data->get_all_userdata($this->tank_auth->get_user_id());
			if(empty($user['signup_complete'])){
				$profile_id = $user['profile_id'];
				$this->tank_auth->logout();
				$this->session->set_userdata(array('profile_id' => $profile_id));

				$this->session->set_flashdata('errors', array('You must complete the signup process before continuing to your account.'));
				$this->data->registration_step($profile_id);
			} else {
				redirect('/dashboard');
			}
		}
	}

	function index() {
		$this->load->library('form_validation');
		$data = array();
		if($this->input->post('company') != ''){
			die('');
		}

		if($this->input->post('register') && $this->session->userdata('profile_id') == false){
			$this->form_validation->set_rules(array(
				array('field' => 'name', 'label' => 'Name', 'rules' => 'required|xss_clean'),
				array('field' => 'email', 'label' => 'Email', 'rules' => 'required|xss_clean|valid_email|allowed_email'),
				array('field' => 'terms', 'label' => 'Terms and conditions', 'rules' => 'required'),
				array('field' => 'marketing', 'label' => 'Email', 'rules' => 'trim')
			));

			if ($this->form_validation->run() == true) {
				$this->session->set_userdata(array()); // Emptying the array if the user has restarted the signup process.

				$name = $this->common->split_name($this->input->post('name'));
				if(!empty($name[3])){
					$this->db->set(array(
						'email' => $this->input->post('email'),
						'last_login' => date('Y-m-d'),
						'created' => date('Y-m-d'),
						'modified' => date('Y-m-d')
					));
					$this->db->insert('users');
					$user_id = $this->db->insert_id();

					$this->db->set(array(
						'user_id' => $user_id,
						'first_name' => $name[2],
						'last_name' => $name[3],
						'registration_email' => $this->input->post('email'),
						'marketing' => $this->input->post('marketing'),
						'signup_started' => time(),
						'continue_key' => md5(microtime())
					));
					$this->db->insert('user_profiles');
					$profile_id = $this->db->insert_id();
					$this->session->set_userdata(array(
						'profile_id' => $profile_id
					));

					$this->data->registration_step($profile_id);

					 if($this->input->cookie('refer') !== false){
						 $refer = $this->input->cookie('refer');
						 $this->load->helper('cookie');
						 delete_cookie("refer");

						 $data = array(
						 	'refer' => trim($refer)
						 );
						 $this->db->where('id', $profile_id);
						 $this->db->update('user_profiles', $data);
					 }
				} else {
					$data['errors'] = 'You must provide a first and last name!';
				}
			} else {
				$data['errors'] = implode('<br />', $this->common->merge_flash('errors', $this->form_validation->error_array()));
			}
		} elseif($this->session->userdata('profile_id') != false) {
			$this->data->registration_step($this->session->userdata('profile_id')); // This will redirect to the necessary registration step.
		}
		$this->common->display(array(
			'view' => 'registration/index',
			'title' => 'Registration',
			'page_header' => 'STC Registration',
			'content' => $data
		));
	}

	function pre_exercise(){
		$this->load->library('form_validation');
		$profile_id = $this->session->userdata('profile_id');
		$errors = array();

		if($profile_id == false){
			$this->session->set_userdata(array());
			redirect('/registration');
		} else {
			$reg_data = $this->data->get_registration_data($profile_id);
			$reg_data = $reg_data->row_array();
			if(!empty($reg_data['medical_complete'])){ // Don't want users to be able to complete the medical twice.
				$registration = $this->data->registration_step($profile_id);
				if($registration == true){
					redirect('/');
				}
			}
			if($this->input->post('submit_pre_questions')){
				$this->form_validation->set_rules(array(
					array('field' => 'suffered_stroke', 'label' => 'Suffered Stroke', 'rules' => 'required'),
					array('field' => 'unexplained_pains', 'label' => 'Unexplained Pains', 'rules' => 'required'),
					array('field' => 'has_dizziness', 'label' => 'Has Dizziness', 'rules' => 'required'),
					array('field' => 'had_asthma', 'label' => 'Had Athsma', 'rules' => 'required'),
					array('field' => 'blood_glucose', 'label' => 'Blood Glucose', 'rules' => 'required'),
					array('field' => 'muscle_problems', 'label' => 'Muscle Problems', 'rules' => 'required'),
					array('field' => 'any_other_conditions', 'label' => 'Any Other Conditions', 'rules' => 'required')
				));

				if($this->form_validation->run() == true){
					$data = array(
						'suffered_stroke' => ($this->input->post('suffered_stroke') == 1 ? 0:1),
						'unexplained_pains' => ($this->input->post('unexplained_pains') == 1 ? 0:1),
						'has_dizziness' => ($this->input->post('has_dizziness') == 1 ? 0:1),
						'had_asthma' => ($this->input->post('had_asthma') == 1 ? 0:1),
						'blood_glucose' => ($this->input->post('blood_glucose') == 1 ? 0:1),
						'muscle_problems' => ($this->input->post('muscle_problems') == 1 ? 0:1),
						'any_other_conditions' => ($this->input->post('any_other_conditions') == 1 ? 0:1)
					);

			#		if(array_sum($data) > 0) { // Because a No is worth 1 and a yes is worth 2, if the user answers yes to any of the questions this will fail them.
			#			$data['failed'] = 1;
			#		}
					$data['profile_id'] = $profile_id;
					$this->db->set($data);
					$this->db->insert('pre_exercise_questions');
					// if(!empty($data['failed'])){ // User failed the medical
					// 	// Email to the administration letting them know that there was a failed medical.
					// 	$this->common->send_email(array(
					// 		'email_to' => ADMIN_EMAIL,
					// 		'subject' => 'Failed Medical!',
					// 		'view' => 'failed_medical',
					// 		'data' => array(
					// 			'user' => $reg_data,
					// 			'medical' => $data
					// 		)
					// 	));
					//
					// 	// Email to the user, informing them of the next steps.
					// 	$this->common->send_email(array(
					// 		'email_to' => $reg_data['registration_email'],
					// 		'subject' => 'Did not pass medical',
					// 		'view' => 'failed_medical_user',
					// 		'data' => array(
					// 			'user' => $reg_data
					// 		)
					// 	));
					// 	$this->session->unset_userdata('profile_id'); // Removes this from the session.
					// 	$this->session->set_flashdata('errors', array('You have failed your medical. This unfortunately means that you will not be able to use this service until such time that you are able to provide STC Fit with a medical certificate deeming you fit to do so. A staff member may be in touch with you shortly to discuss your options.'));
					// 	redirect('/medical-failed');
					// } else { // User passed the medical

						$user = $this->db->query("SELECT
							`user_profiles`.*,
							`users`.`id` AS `account_id`
						FROM
							`user_profiles`
						INNER JOIN
							`users` ON `user_profiles`.`user_id`=`users`.`id`
						WHERE
							`user_profiles`.`id`='".mysql_real_escape_string($profile_id)."'
						LIMIT
							1
						")->row_array();

						$data = array(
							'all_details' => 1,
							'tailoring_complete' => 1,
							'signup_complete' => 1
						);
						$this->db->set($data);
						$this->db->where('id', $profile_id);
						$this->db->update('user_profiles');

						$this->load->model('phase');
						$plan = $this->phase->create_phase($user['account_id']);

						$this->session->userdata('profile_id',false);

						$this->tank_auth->login_by_id($user['account_id']);

						redirect('/dashboard');
					// }
				} else {
					$errors = $this->form_validation->error_array();
				}
			}
		}

		$this->common->display(array(
			'view' => 'registration/pre_exercise',
			'template' => 'templates/registration',
			'title' => 'Registration',
			'template_data' => array(
				'first_name' => $reg_data['first_name']
			),
			'content' => array(
				'reg_data' => $reg_data,
				'errors' => implode('<br />', $this->common->merge_flash('errors', $errors))
			)
		));
	}

	// function your_details(){
	// 	$this->load->library('form_validation');
	// 	$profile_id = $this->session->userdata('profile_id');
	// 	$errors = array();
	// 	if($profile_id == false){
	// 		$this->session->set_userdata(array());
	// 		redirect('/registration');
	// 	} else {
	// 		$reg_data = $this->data->get_registration_data($profile_id)->row_array();
	// 		if(!empty($reg_data['all_details'])){
	// 			$registration = $this->data->registration_step($profile_id);
	// 			if($registration == true){
	// 				redirect('/');
	// 			}
	// 		}
	// 		if($this->input->post('submit_your_details')){
	// 			$this->form_validation->set_rules(array(
	// 				array('field' => 'gender', 'label' => 'Gender', 'rules' => 'required|trim'),
	// 				array('field' => 'age', 'label' => 'Age', 'rules' => 'required|trim'),
	// 				array('field' => 'marital_status', 'label' => 'Marital Status', 'rules' => 'trim'),
	// 				array('field' => 'number_of_children', 'label' => 'Number of Children', 'rules' => 'trim'),
	// 				array('field' => 'street_number', 'label' => 'Street Number', 'rules' => 'required|trim'),
	// 				array('field' => 'street_name', 'label' => 'Street Name', 'rules' => 'required|trim'),
	// 				array('field' => 'suburb', 'label' => 'Suburb / City', 'rules' => 'required|xss_clean|trim'),
	// 				array('field' => 'state', 'label' => 'State', 'rules' => 'required|trim'),
	// 				array('field' => 'country', 'label' => 'Country', 'rules' => 'required|trim'),
	// 				array('field' => 'postcode', 'label' => 'Postcode', 'rules' => 'required|trim|numeric')
	// 			));
	//
	// 			if($this->form_validation->run() == true){
	// 				$marital = $this->input->post('marital_status');
	// 				$children = $this->input->post('number_of_children');
	//
	// 				$user = $this->db->query("SELECT
	// 					`user_profiles`.*,
	// 					`users`.`id` AS `account_id`
	// 				FROM
	// 					`user_profiles`
	// 				INNER JOIN
	// 					`users` ON `user_profiles`.`user_id`=`users`.`id`
	// 				WHERE
	// 					`user_profiles`.`id`='".mysql_real_escape_string($profile_id)."'
	// 				LIMIT
	// 					1
	// 				")->row_array();
	//
	// 				if(!is_null($user)){ // User Created successfully.
	// 					$this->db->set(array(
	// 						'user_id' => $user['account_id'],
	// 						'gender' => $this->input->post('gender'),
	// 						'age' => $this->input->post('age'),
	// 						'marital_status' => (!empty($marital) ? $marital:null),
	// 						'number_of_children' => (!empty($children) ? $children:null),
	// 						'street_address' => $this->input->post('street_number') . " " . $this->input->post('street_name'),
	// 						'suburb' => $this->input->post('suburb'),
	// 						'state' => $this->input->post('state'),
	// 						'country' => $this->input->post('country'),
	// 						'postcode' => $this->input->post('postcode'),
	// 						'all_details' => 1,
	// 						'tailoring_complete' => 1,
	// 						'signup_complete' => 1
	// 					));
	// 					$this->db->where('id', $profile_id);
	// 					$this->db->update('user_profiles');
	//
	// 					$this->load->model('phase');
	// 					$plan = $this->phase->create_phase($user['account_id']);
	//
	// 					redirect('/dashboard');
	// 				}
	// 			} else {
	// 				$errors = $this->form_validation->error_array();
	// 			}
	// 		}
	// 	}
	//
	// 	$this->common->display(array(
	// 		'view' => 'registration/your_details',
	// 		'template' => 'templates/registration',
	// 		'title' => 'Registration',
	// 		'template_data' => array(
	// 			'first_name' => $reg_data['first_name']
	// 		),
	// 		'content' => array(
	// 			'reg_data' => $reg_data,
	// 			'errors' => implode('<br />', $this->common->merge_flash('errors', $errors))
	// 		)
	// 	));
	// }

	// function tailor_program(){
	// 	$this->load->library('form_validation');
	// 	$profile_id = $this->session->userdata('profile_id');
	// 	$errors = array();
	// 	$priorities = $this->db->query("SELECT * FROM `training_types`")->result_array();
	// 	$user = $this->db->query("SELECT
	// 		`user_profiles`.*,
	// 		`users`.`id` AS `account_id`
	// 	FROM
	// 		`user_profiles`
	// 	INNER JOIN
	// 		`users` ON `user_profiles`.`user_id`=`users`.`id`
	// 	WHERE
	// 		`user_profiles`.`id`='".mysql_real_escape_string($profile_id)."'
	// 	LIMIT
	// 		1
	// 	")->row_array();
	// 	if($profile_id == false){
	// 		$this->session->set_userdata(array());
	// 		redirect('/registration');
	// 	} else {
	// 		$reg_data = $this->data->get_registration_data($profile_id)->row_array();
	// 		if(!empty($reg_data['tailoring_complete'])){
	// 			$registration = $this->data->registration_step($profile_id);
	// 			if($registration == true){
	// 				redirect('/');
	// 			}
	// 		}
	// 		if($this->input->post('submit_program')){
	// 			$user_priorities = array();
	// 			$rules = array(
	// 				array('field' => 'previous_program', 'label' => 'Previous Program', 'rules' => 'required|trim'),
	// 				array('field' => 'program_ended', 'label' => 'How Long Ago Did You Start This Program', 'rules' => 'trim'),
	// 				array('field' => 'current_condition', 'label' => 'Current Condition', 'rules' => 'required|trim|xss_clean'),
	// 				array('field' => 'results_by', 'label' => 'Achieve Results By', 'rules' => 'required|trim'),
	// 				array('field' => 'days_to_commit' , 'label' => 'Days To Commit', 'rules' => 'required|trim'),
	// 				array('field' => 'length_of_consideration', 'label' => 'Length Of Consideration', 'rules' => 'required|trim'),
	// 				array('field' => 'seriousness_scale', 'label' => 'How Serious Are You', 'rules' => 'required|trim'),
	// 				array('field' => 'start_prevention', 'label' => 'What Kept You From Starting Earlier', 'rules' => 'required|trim|xss_clean'),
	// 				array('field' => 'still_a_problem', 'label' => 'Is This Still A Problem', 'rules' => 'required|trim'),
	// 				array('field' => 'training_type', 'label' => 'Training type', 'rules' => 'required|trim')
	// 			);
	//
	// 			$this->form_validation->set_rules($rules);
	// 			if($this->form_validation->run() == true) {
	// 				$data = array(
	// 					'previous_program' => ($this->input->post('previous_program') == 1 ? 0:1),
	// 					'program_ended' => $this->input->post('program_ended'),
	// 					'current_condition' => $this->input->post('current_condition'),
	// 					'results_by' => $this->common->date_convert($this->input->post('results_by')),
	// 					'days_to_commit' => $this->input->post('days_to_commit'),
	// 					'length_of_consideration' => $this->input->post('length_of_consideration'),
	// 					'seriousness_scale' => $this->input->post('seriousness_scale'),
	// 					'start_prevention' => $this->input->post('start_prevention'),
	// 					'still_a_problem' => ($this->input->post('still_a_problem') == 1 ? 0:1),
	// 					'training_type' => $this->input->post('training_type'),
	// 					'tailoring_complete' => 1
	// 				);
	//
	// 				$this->db->set($data);
	// 				$this->db->where('id', $profile_id);
	// 				$this->db->update('user_profiles');
	// 				// -------- Inserting the user's priorities ---------
	// 				$this->load->model('phase');
	// 				$plan = $this->phase->create_phase($user['account_id']);
	// 				redirect('/registration/payment');
	//
	// 			} else {
	// 				$errors = $this->form_validation->error_array();
	// 			}
	// 		}
	// 	}
	//
	// 	$this->common->display(array(
	// 		'view' => 'registration/tailor_program',
	// 		'template' => 'templates/registration',
	// 		'title' => 'Registration',
	// 		'template_data' => array(
	// 			'first_name' => $reg_data['first_name']
	// 		),
	// 		'extra_css' => array(
	// 			CSS_PATH.'plugins/datepicker.css'
	// 		),
	// 		'extra_js' => array(
	// 			JS_PATH.'plugins/bootstrap-datepicker.js'
	// 		),
	// 		'content' => array(
	// 			'reg_data' => $reg_data,
	// 			'errors' => implode('<br />', $this->common->merge_flash('errors', $errors)),
	// 			'priorities' => $priorities
	// 		)
	// 	));
	// }

	public function sort_payment($a, $b){
	    if ($a['order'] == $b['order']) {
	        return 0;
	    }
	    return ($a['order'] < $b['order']) ? -1 : 1;
	}

	function payment(){
		$this->load->library('form_validation');
		$profile_id = $this->session->userdata('profile_id');
		$payment_methods = $this->config->item('payment_methods');
		$errors = array();

		if($profile_id == false){
			$this->session->set_userdata(array());
			redirect('/registration');
		} else {
			$reg_data = $this->data->get_registration_data($profile_id)->row_array();
			if(!empty($reg_data['eway_token'])){ // Don't want users to be able to complete the medical twice.
				$registration = $this->data->registration_step($profile_id);
				if($registration == true){
					redirect('/');
				}
			}
			if($this->input->post('submit_payment_details')){
				$this->form_validation->set_rules(array(
					array('field' => 'password', 'label' => 'Password', 'rules' => 'required|xss_clean|min_length[8]'),
					array('field' => 'confirm_password', 'label' => 'Confirm Password', 'rules' => 'required|xss_clean|min_length[8]|matches[password]'),
					array('field' => 'phone', 'label' => 'Phone', 'rules' => 'required|trim'),
					array('field' => 'payment_method', 'label' => 'Payment Method', 'rules' => 'trim'),
					array('field' => 'payment_frequency', 'label' => 'Payment Frequency', 'rules' => 'trim|required'),
					array('field' => 'trainer_code', 'label' => 'Trainer Code', 'rules' => 'trim'),
					array('field' => 'card_name', 'label' => 'Name On Card', 'rules' => 'trim|required'),
					array('field' => 'card_number', 'label' => 'Credit Card Number', 'rules' => 'trim|luhn_check|required'),
					array('field' => 'card_ccv', 'label' => 'CCV', 'rules' => 'trim|required|numeric|min_length[3]|max_length[4]'),
					array('field' => 'expiry_month', 'label' => 'Card Expiry', 'rules' => 'trim|required'),
					array('field' => 'expiry_year', 'label' => 'Card Expiry', 'rules' => 'trim|required')
				));

				if($this->form_validation->run()){
					$req_trainer = $this->input->post('trainer_code');
					$trainer = null;
					if(!empty($req_trainer)){
						$req_trainer = str_replace(TRAINER_PREFIX, '', strtoupper($req_trainer));
						$trainer_check = $this->db->query("SELECT
							*
						FROM
							`users`
						WHERE
							`users`.`is_trainer`=1
						AND
							`users`.`id`='".$req_trainer."'
						LIMIT
							1
						")->row_array();

						if(!empty($trainer_check['id'])){
							$trainer = $trainer_check['id'];
						} else {
							$errors[] = 'This trainer code does not exist. If you are unsure of this, just leave the trainer field blank and you will be automatically assigned a trainer.';
						}
					}
					if(empty($errors)){
						$this->load->model('payment');
						$method = 5;

						$chosen_method = $payment_methods[$method];
						$card_details = array(
							'card_number' => $this->input->post('card_number'),
							'card_name' => $this->input->post('card_name'),
							'expiry_month' => $this->input->post('expiry_month'),
							'expiry_year' => $this->input->post('expiry_year')
						);
						$user = $this->db->query("SELECT
							`user_profiles`.*,
							`users`.`id` AS `account_id`
						FROM
							`user_profiles`
						INNER JOIN
							`users` ON `user_profiles`.`user_id`=`users`.`id`
						WHERE
							`user_profiles`.`id`='".mysql_real_escape_string($profile_id)."'
						LIMIT
							1
						")->row_array();

						if(!empty($user['account_id'])){
							$token = $this->payment->eway_create_user(intval($user['account_id']), $card_details);
							if(!is_array($token)){
							#	$bill_user = $this->payment->eway_bill_user($token, number_format($chosen_method['amount'],2), 'STC Fit '.ucwords($chosen_method['title']).' Memebership Fee.');
							#	if(isset($bill_user['ewayTrxnStatus']) && $bill_user['ewayTrxnStatus'] == 'True'){

									$hasher = new PasswordHash(
											$this->config->item('phpass_hash_strength', 'tank_auth'),
											$this->config->item('phpass_hash_portable', 'tank_auth'));
									$hashed_password = $hasher->HashPassword($this->input->post('password'));

									$this->users->change_password($user['account_id'],$hashed_password);

									// Updating the user profiles table
									$this->db->set(array(
										'allocated_trainer' => (!empty($trainer) ? $trainer:null),
										'phone' => $this->input->post('phone'),
										'eway_token' => $token,
										'renewal_type' => $method,
										'training_type' => $this->input->post('program_type'),
										'paid_period_ends' => time()+($chosen_method['days']*86400),
										'user_id' => $user['account_id'],
										'all_details' => 1
									));
									$this->db->where('id', $profile_id);
									$this->db->update('user_profiles');

									if(!empty($trainer)){
										$this->db->set(array(
											'trainer_id' => $trainer,
											'client_id' => $user['account_id'],
											'reminder_date' => time(),
											'notes' => 'New user alert! This user has been assigned to you! Please welcome them.',
										));
										$this->db->insert('trainer_reminders');
									}
									// Adding this record into billing history

							#		$data = array(
							#			'user_id' => $user['account_id'],
							#			'amount' => number_format($chosen_method['amount'],2),
							#			'description' => 'Get Ripped (' .$chosen_method['title']. ')',
							#			'date' => time(),
							#			'reference' => $bill_user['ewayTrxnNumber']
							#		);

							#		$this->db->set($data);
							#		$this->db->insert('billing_history');
							#		$data['id'] = $this->db->insert_id();

							#		$user_information = $this->data->get_all_userdata($data['user_id']);
							#		$invoice = $this->load->view('includes/invoice_table', array('user' => $user_information, 'invoice_data' => $data) , true);

									// $this->common->send_email(array(
									// 	'email_to' => $user_information['email'],
									// 	'subject' => 'Welcome to STC Fit!',
									// 	'view' => 'welcome',
									// 	'data' => array(
									// 		'user' => $user_information,
									// 		'invoice' => $invoice
									// 	)
									// ));
									// ------ End send invoice ----------------------

									redirect('/registration/pre-exercise');
							#	} else {
							#		$errors[] = $bill_user['ewayTrxnError'];
							#	}
							} else {
								$errors[] = $token['faultstring'];
							}
						} else {
							$errors[] = 'No user account. An error occured.';
						}
					}
				} else {
					$errors = array_merge($errors, $this->form_validation->error_array());
				}
			}
		}

		$this->common->display(array(
			'view' => 'registration/payment',
			'template' => 'templates/registration',
			'title' => 'Registration',
			'template_data' => array(
				'first_name' => $reg_data['first_name']
			),
			'content' => array(
				'reg_data' => $reg_data,
				'errors' => implode('<br />', $this->common->merge_flash('errors', $errors)),
				'payment_methods' => $payment_methods
			)
		));
	}

	function check_trainer(){
		if(!$this->input->is_ajax_request()){
			redirect('/registration');
		}

		$req_trainer = str_replace(TRAINER_PREFIX, '', strtoupper($this->input->post('trainer_code')));
		$trainer = $this->db->query("SELECT
			`trainer_profiles`.*
		FROM
			`users`
		INNER JOIN
			`trainer_profiles` ON `users`.`id`=`trainer_profiles`.`user_id`
		WHERE
			`users`.`is_trainer`=1
		AND
			`users`.`banned`=0
		AND
			`users`.`id`='".$req_trainer."'
		LIMIT
			1
		");

		$return = array(
			'found' => false,
			'data' => array()
		);

		if($trainer->num_rows() > 0){
			$return['found'] = true;
			$return['data'] = $trainer->row_array();
		}

		die(json_encode($return));
	}
}
