<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	function __construct() {
		parent::__construct();
		// Globally available variables that define a user's access level.
		
		$this->is_trainer = $this->session->userdata('is_trainer');
		$this->is_admin = $this->session->userdata('is_admin');
		
		
		
	}
}

class Users_Controller extends MY_Controller {
	function __construct() {
		parent::__construct();
		
		
		
		
		if($this->tank_auth->is_logged_in() == false){
			redirect('/login');
		} elseif($this->is_admin == true){
			redirect('/admin');
		} elseif($this->is_trainer == true){
			redirect('/trainers');
		}
		$check = $this->db->query("SELECT
			`user_profiles`.`free`,
			`user_profiles`.`paid_period_ends`,
			`account_deleted`.*
		FROM
			`user_profiles`
		LEFT JOIN
			`account_deleted` ON `user_profiles`.`user_id`=`account_deleted`.`user_id`
			AND
			`account_deleted`.`obsolete` = 0
			AND
			`account_deleted`.`user_confirmed` = 0
		WHERE
			`user_profiles`.`user_id`='".$this->tank_auth->get_user_id()."'
		LIMIT
			1
		")->row_array();

		if($check['free'] != 1){
			if(empty($check['paid_period_ends']) || intval($check['paid_period_ends']) < time()){
				$this->session->set_flashdata('errors', array('Your paid subscription has ended. Please renew it now to use your STC Fit account.'));
				redirect('/renew');
			} elseif(!empty($check['defer_til']) && intval($check['defer_til']) > time()){
				$this->tank_auth->logout();
				$this->session->set_flashdata('errors', array('The use of this account has been deferred at the user\'s request. This account will be accessible again on date: '.date('d/m/Y h:i:sa',intval($check['defer_til']))));
				redirect('/');
			}
		}
	}
}

class Trainers_Controller extends MY_Controller {
	function __construct() {
		parent::__construct();
		if($this->tank_auth->is_logged_in() == false){
			redirect('/trainers/login');
		}

		$session_data = $this->session->all_userdata();
		if(!empty($session_data['trainer_id'])){
			$trainer = $this->tank_auth->login_by_id($session_data['trainer_id']);
			if($trainer == true){
				$this->is_trainer = true;
			}
		}

		if($this->is_trainer == false){
			$this->session->set_flashdata('errors', array('You do not have the necessary permission to view this area.'));
			redirect('/');
		}
	}
}

class Admin_Controller extends MY_Controller {
	function __construct() {
		parent::__construct();
		if($this->tank_auth->is_logged_in() == false){
			redirect('/admin/login');
		}

		$session_data = $this->session->all_userdata();
		if(!empty($session_data['admin_id'])){
			$admin = $this->tank_auth->login_by_id($session_data['admin_id']);
			if($admin == true){
				$this->is_admin = true;
			}
		}

		if($this->is_admin == false){
			$this->session->set_flashdata('errors', array('You do not have the necessary permission to view this area.'));
			redirect('/');
		}
	}
}