<?php
class MY_Form_validation extends CI_Form_validation {
    function error_array(){
        return $this->_error_array;
    }

	function allowed_email($input){
        $check = $this->CI->db->query("SELECT * FROM `users` WHERE `email`='".mysql_real_escape_string($input)."' LIMIT 1");
        $medical = $this->CI->db->query("SELECT
            `user_profiles`.*,
            `pre_exercise_questions`.*
        FROM
            `user_profiles`
        LEFT JOIN
            `pre_exercise_questions` ON `user_profiles`.`id`=`pre_exercise_questions`.`profile_id`
            AND
            `pre_exercise_questions`.`admin_override`=0
        WHERE
            `user_profiles`.`registration_email`='".mysql_real_escape_string($input)."'
        LIMIT
            1
        ");
        $medical = $medical->row_array();
        if(!isset($medical['failed'])){
            $medical['failed'] = 0; // Giving this a value incase there was no database entry.
        }
        if($check->num_rows() < 1 && $medical['failed'] != 1){
            return true;
        } elseif($medical['failed'] == 1 && empty($medical['admin_override'])) {
        	$this->set_message('allowed_email', 'Sorry! You have failed the medical requirements to proceed. You must obtain a doctors certificate acknowledging that you are able to participate in this program to continue.');
        	return false;
        } else {
        	$user = $check->row_array();
        	if($user['banned'] == 1){
                $this->set_message('allowed_email', 'Your account has been banned, if you think this is a mistake, contact STC administration throught the contact page.');
        	} else {
        		$this->set_message('allowed_email', 'A user with this email address already exists!');
        	}
            return false;
        }
    }

    function unique_to($input, $compare=array()){
        if(empty($compare)){
            return true;
        } else {
            $compare = explode(',', str_replace(' ', '', $compare));
            foreach($compare as $field){
                if($this->CI->input->get_post($field) == $input){
                    $this->set_message('unique_to', '%s Must be unique!');
                    return false;
                }
            }
        }
        return true;
    }

    function valid_date($input){
        $date = date_parse($input);
        if ($date["error_count"] == 0 && checkdate($date["month"], $date["day"], $date["year"])){
            return true;
        }
        else{
            $this->set_message('valid_date', '%s Must be a date with format dd/mm/yyyy');
            return false;
        }
    }

    function luhn_check($number) {
        // Strip any non-digits (useful for credit card numbers with spaces and hyphens)
        $number=preg_replace('/\D/', '', $number);
        // Set the string length and parity
        $number_length=strlen($number);
        $parity=$number_length % 2;

        // Loop through each digit and do the maths
        $total=0;
        for ($i=0; $i<$number_length; $i++) {
            $digit=$number[$i];
            // Multiply alternate digits by two
            if ($i % 2 == $parity) {
                $digit*=2;
                // If the sum is two digits, add them together (in effect)
                if ($digit > 9) {
                    $digit-=9;
                }
            }
            // Total up the digits
            $total+=$digit;
        }
        // If the total mod 10 equals 0, the number is valid
        if($total % 10 == 0){
            return true;
        }

        $this->set_message('luhn_check', 'You must enter a valid credit card number');
        return false;
    }
}
?>