<hr />
<div class="exercise-editing">
	<?php echo form_open_multipart('/admin/exercise-library', array('role' => 'form', 'class' => 'validate')); ?>
		<input type="hidden" name="is_circuit" value="1" />
		<?php if(!empty($circuit)): ?>
			<input type="hidden" name="exercise_identifier" value="<?php echo $circuit['exercise_identifier']; ?>" />
			<div class="exercise-name">Editing: <?php echo $circuit['name']; ?></div>
		<?php else: ?>
			<div class="exercise-name">New Circuit</div>
		<?php endif; ?>
		<div class="row">
			<div class="col-xs-5">
				<div class="form-group">
					<label for="title">Title</label>
					<input type="text" class="form-control" name="title" id="title" value="<?php echo (!empty($circuit['name']) ? $circuit['name']:'') ?>" required />
				</div>
				<div class="form-group">
					<label for="video_link">Youtube URL</label>
					<input type="text" class="form-control" name="video_link" id="video_link" value="<?php echo (!empty($circuit['video_link']) ? 'http://www.youtube.com/watch?v='.$circuit['video_link']:'') ?>" />
				</div>
				<div class="form-group">
					<label for="description">Description</label>
					<textarea class="form-control" name="description" id="description" required><?php echo (!empty($circuit['description']) ? $circuit['description']:'') ?></textarea>
				</div>
				<div class="form-group">
					<label for="benefits">Benefits</label>
					<textarea class="form-control" name="benefits" id="benefits"><?php echo (!empty($circuit['benefits']) ? $circuit['benefits']:'') ?></textarea>
				</div>
			</div>
			<div class="col-xs-7">
				<div class="form-group photo-upload">
					<label for="exercise_image_1">Photo Upload</label>
					<div class="nice-file" data-filename="No file chosen" data-show-preview="yes" data-imageno="1">
						<input type="hidden" name="old_images[]" value="<?php echo (!empty($circuit['image_1']) ? $circuit['image_1']:''); ?>" />
						<input type="file" name="images[]" id="exercise_image_1" />
					</div>
					<div class="nice-file" data-filename="No file chosen" data-show-preview="yes" data-imageno="2">
						<input type="hidden" name="old_images[]" value="<?php echo (!empty($circuit['image_2']) ? $circuit['image_2']:''); ?>" />
						<input type="file" name="images[]" id="exercise_image_2" />
					</div>
					<div class="nice-file" data-filename="No file chosen" data-show-preview="yes" data-imageno="3">
						<input type="hidden" name="old_images[]" value="<?php echo (!empty($circuit['image_3']) ? $circuit['image_3']:''); ?>" />
						<input type="file" name="images[]" id="exercise_image_3" />
					</div>
				</div>
				<div class="preview-area">
					<div class="image-container">
						<img src="<?php echo (!empty($circuit['image_1']) ? EXERCISE_IMG_PATH.$image_sizes['admin_thumb']['size'].'-'.$circuit['image_1']:'/assets/images/no-image.jpg') ?>" id="image_1" />
					</div>
					<div class="image-container">
						<img src="<?php echo (!empty($circuit['image_2']) ? EXERCISE_IMG_PATH.$image_sizes['admin_thumb']['size'].'-'.$circuit['image_2']:'/assets/images/no-image.jpg') ?>" id="image_2" />
					</div>
					<div class="image-container">
						<img src="<?php echo (!empty($circuit['image_3']) ? EXERCISE_IMG_PATH.$image_sizes['admin_thumb']['size'].'-'.$circuit['image_3']:'/assets/images/no-image.jpg') ?>" id="image_3" />
					</div>
				</div>
			</div>
		</div>
		<div class="submit">
			<input type="submit" name="add_edit_submit" class="btn btn-primary" value="Finish and save" />
		</div>
	<?php echo form_close(); ?>
</div>