<hr />
<div class="exercise-editing">
	<?php echo form_open_multipart('/admin/exercise-library', array('role' => 'form', 'class' => 'validate')); ?>
		<input type="hidden" name="is_circuit" value="0" />
		<?php if(!empty($exercise)): ?>
			<input type="hidden" name="exercise_identifier" value="<?php echo $exercise['exercise_identifier']; ?>" />
			<div class="exercise-name">Editing: <?php echo $exercise['name']; ?></div>
		<?php else: ?>
			<div class="exercise-name">New Exercise</div>
		<?php endif; ?>
		<div class="row">
			<div class="col-xs-5">
				<div class="form-group">
					<label for="title">Title</label>
					<input type="text" class="form-control" name="title" id="title" value="<?php echo (!empty($exercise['name']) ? $exercise['name']:'') ?>" required />
				</div>
				<div class="form-group">
					<label for="video_link">Youtube URL</label>
					<input type="text" class="form-control" name="video_link" id="video_link" value="<?php echo (!empty($exercise['video_link']) ? 'http://www.youtube.com/watch?v='.$exercise['video_link']:'') ?>" />
				</div>
				<div class="form-group">
					<label for="description">Description</label>
					<textarea class="form-control" name="description" id="description" required><?php echo (!empty($exercise['description']) ? $exercise['description']:'') ?></textarea>
				</div>
				<div class="form-group">
					<label for="benefits">Benefits</label>
					<textarea class="form-control" name="benefits" id="benefits"><?php echo (!empty($exercise['benefits']) ? $exercise['benefits']:'') ?></textarea>
				</div>
			</div>
			<div class="col-xs-7">
				<div class="form-group">
					<label for="low_modifiers">Low Modifiers</label>
					<input type="text" name="low_modifiers" id="low_modifiers" class="form-control" value="<?php echo (!empty($exercise['low_modifiers']) ? $exercise['low_modifiers']:'') ?>" required />
				</div>
				<div class="form-group">
					<label for="medium_modifiers">Medium Modifiers</label>
					<input type="text" name="medium_modifiers" id="medium_modifiers" class="form-control" value="<?php echo (!empty($exercise['medium_modifiers']) ? $exercise['medium_modifiers']:'') ?>" required />
				</div>
				<div class="form-group">
					<label for="high_modifiers">High Modifiers</label>
					<input type="text" name="high_modifiers" id="high_modifiers" class="form-control" value="<?php echo (!empty($exercise['high_modifiers']) ? $exercise['high_modifiers']:'') ?>" required />
				</div>
				<div class="form-group photo-upload">
					<label for="exercise_image_1">Photo Upload</label>
					<div class="nice-file" data-filename="No file chosen" data-show-preview="yes" data-imageno="1">
						<input type="hidden" name="old_images[]" value="<?php echo (!empty($exercise['image_1']) ? $exercise['image_1']:''); ?>" />
						<input type="file" name="images[]" id="exercise_image_1" />
					</div>
					<div class="nice-file" data-filename="No file chosen" data-show-preview="yes" data-imageno="2">
						<input type="hidden" name="old_images[]" value="<?php echo (!empty($exercise['image_2']) ? $exercise['image_2']:''); ?>" />
						<input type="file" name="images[]" id="exercise_image_2" />
					</div>
					<div class="nice-file" data-filename="No file chosen" data-show-preview="yes" data-imageno="3">
						<input type="hidden" name="old_images[]" value="<?php echo (!empty($exercise['image_3']) ? $exercise['image_3']:''); ?>" />
						<input type="file" name="images[]" id="exercise_image_3" />
					</div>
				</div>
				<div class="preview-area">
					<div class="image-container">
						<img src="<?php echo (!empty($exercise['image_1']) ? EXERCISE_IMG_PATH.$image_sizes['admin_thumb']['size'].'-'.$exercise['image_1']:'/assets/images/no-image.jpg') ?>" id="image_1" />
					</div>
					<div class="image-container">
						<img src="<?php echo (!empty($exercise['image_2']) ? EXERCISE_IMG_PATH.$image_sizes['admin_thumb']['size'].'-'.$exercise['image_2']:'/assets/images/no-image.jpg') ?>" id="image_2" />
					</div>
					<div class="image-container">
						<img src="<?php echo (!empty($exercise['image_3']) ? EXERCISE_IMG_PATH.$image_sizes['admin_thumb']['size'].'-'.$exercise['image_3']:'/assets/images/no-image.jpg') ?>" id="image_3" />
					</div>
				</div>
			</div>
		</div>
		<div class="submit">
			<input type="submit" name="add_edit_submit" class="btn btn-primary" value="Finish and save" />
		</div>
	<?php echo form_close(); ?>
</div>