<?php if(empty($entries)): ?>
	<div class="message-area area-small alert alert-info alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		You currently have no previous entries for this matching the selected criteria.
	</div>
<?php else: ?>
	<div class="slider-controls">
		<a href="#" id="move-left" style="<?php echo (!empty($entries) && count($entries) < 2 ? 'display:none;':''); ?>">Older <span>&raquo;</span></a>
		<a href="#" id="move-right" style="display:none;"><span>&laquo;</span> Newer</a>
	</div>
	<div class="slider_container">
		<ul id="previous_entry_list">
			<?php foreach($entries as $entry): ?>
				<li>
					<table border="1" cellspacing="0" cellpadding="0">
						<tr>
							<td colspan="2" class="center-cell"><?php echo date('d/m/Y h:i:sa', $entry['entry_date']); ?></td>
						</tr>
						<tr>
							<td>Calories</td>
							<td><?php echo $entry['calories'] ?></td>
						</tr>
						<tr>
							<td>Protein</td>
							<td><?php echo $entry['protein'] ?></td>
						</tr>
						<tr>
							<td>Carbohydrates</td>
							<td><?php echo $entry['carbs'] ?></td>
						</tr>
						<tr>
							<td>Sugars</td>
							<td><?php echo $entry['sugars'] ?></td>
						</tr>
						<tr>
							<td>Fats</td>
							<td><?php echo $entry['fats'] ?></td>
						</tr>
					</table>
				</li>
			<?php endforeach; ?>
		</ul>
	</div>
<?php endif; ?>
<script>
	head.ready(function () {
    	(function ($) {
    		var moving = false;
    		$('#move-left').on('click', function(e){
    			e.preventDefault();
    			if(moving == false){
    				moving = true;
    				$('ul#previous_entry_list').animate({
    					left: ($('ul#previous_entry_list').position().left-710)+'px'
    				}, 200, function(){
    					if((Math.abs($('ul#previous_entry_list').position().left)+710) >= ($('ul#previous_entry_list').width())){
    						$('#move-left').hide();
    					}
    					$('#move-right').show();
    					moving = false;
    				});
    			}
    		});

    		$('#move-right').on('click', function(e){
    			e.preventDefault();
    			if(moving == false){
    				moving = true;
    				$('ul#previous_entry_list').animate({
    					left: ($('ul#previous_entry_list').position().left+710)+'px'
    				}, 200, function(){
    					if(($('ul#previous_entry_list').position().left) >= 0){
    						$('#move-right').hide();
    					}
    					$('#move-left').show();
    					moving = false;
    				});
    			}
    		});
        })(jQuery);
	});
</script>