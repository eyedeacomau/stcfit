<input type="hidden" name="entry_count" value="<?php echo $goal_count; ?>" />
<?php for($x=0;$x<$goal_count; $x++): ?>
	<div class="food-goal-entry">
		<?php if($goal_count > 1): ?>
			<h4>Week <?php echo ($x+1); ?> Goals</h4>
		<?php endif; ?>
		<div class="form-group">
			<label for="calories_<?php echo $x; ?>">Calories</label>
			<div class="input-group-container">
				<div class="input-group">
					<input type="text" class="form-control" name="entry[calories][]" id="calories_<?php echo $x; ?>" value="<?php echo (!empty($goal_entries[$x]['calories']) ? $goal_entries[$x]['calories']:''); ?>" required data-placement="right" data-rule-number />
					<span class="input-group-addon cals">cals</span>
				</div>
			</div>
		</div>
		<div class="form-group">
			<label for="protein_<?php echo $x; ?>">Protein</label>
			<div class="input-group-container">
				<div class="input-group">
					<input type="text" class="form-control" name="entry[protein][]" id="protein_<?php echo $x; ?>" value="<?php echo (!empty($goal_entries[$x]['protein']) ? $goal_entries[$x]['protein']:''); ?>" required data-placement="right" data-rule-number />
					<span class="input-group-addon grams">grams</span>
				</div>
			</div>
		</div>
		<div class="form-group">
			<label for="carbohydrates_<?php echo $x; ?>">Carbohydrates</label>
			<div class="input-group-container">
				<div class="input-group">
					<input type="text" class="form-control" name="entry[carbohydrates][]" id="carbohydrates_<?php echo $x; ?>" value="<?php echo (!empty($goal_entries[$x]['carbs']) ? $goal_entries[$x]['carbs']:''); ?>" required data-placement="right" data-rule-number />
					<span class="input-group-addon grams">grams</span>
				</div>
			</div>
		</div>
		<div class="form-group">
			<label for="sugars_<?php echo $x; ?>">Sugars</label>
			<div class="input-group-container">
				<div class="input-group">
					<input type="text" class="form-control" name="entry[sugars][]" id="sugars_<?php echo $x; ?>" value="<?php echo (!empty($goal_entries[$x]['sugars']) ? $goal_entries[$x]['sugars']:''); ?>" required data-placement="right" data-rule-number />
					<span class="input-group-addon grams">grams</span>
				</div>
			</div>
		</div>
		<div class="form-group">
			<label for="fats_<?php echo $x; ?>">Fats</label>
			<div class="input-group-container">
				<div class="input-group">
					<input type="text" class="form-control" name="entry[fats][]" id="fats_<?php echo $x; ?>" value="<?php echo (!empty($goal_entries[$x]['fats']) ? $goal_entries[$x]['fats']:''); ?>" required data-placement="right" data-rule-number />
					<span class="input-group-addon grams">grams</span>
				</div>
			</div>
		</div>
	</div>
<?php endfor; ?>