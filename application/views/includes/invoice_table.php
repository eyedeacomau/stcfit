<?php
	$states = $this->config->item('states');
?>
<?php if(!empty($invoice_data)): ?>
<table width="100%" cellpadding="10" border="0" style="border-width:1px; border-style:solid; border-color:#616161; text-align:left;">
	<tr style="border-bottom-width:1px; border-bottom-style:solid; border-bottom-color:#616161;">
		<td width="100%" style="padding-top:10px; padding-bottom:10px; background-color:#eaeaea; color:#000000; font-size:20px; font-weight:700; text-align:center;">
			TAX INVOICE
		</td>
	</tr>
	<tr>
		<td width="100%" align="center" style="padding:10px; background-color:#ffffff; color:#6d6d6d; font-size:16px; font-weight:300;">
			<table width="90%" border="0" cellpadding="0">
				<tr>
					<td width="50%" valign="top">
						<table width="100%" border="0">
							<tr>
								<td colspan="2" align="left">
									<?php echo $user['first_name'] .' '.$user['last_name']; ?><br />
									<?php echo $user['street_address']; ?><br />
									<?php echo $user['suburb'] . ' ' . (!empty($user['postcode']) ? $user['postcode']:''); ?><br />
									<?php echo (!empty($states[$user['state']]['label']) ? $states[$user['state']]['label'].', Australia':'International'); ?>
								</td>
							</tr>
							<tr>
								<td align="left" style="padding-top:50px;">
									<b>Invoice Date:</b>
								</td>
								<td align="left" style="padding-top:50px;">
									<?php echo date('d F Y',$invoice_data['date']); ?>
								</td>
							</tr>
							<tr>
								<td align="left">
									<b>Invoice Number:</b>
								</td>
								<td align="left">
									<?php echo 'INV' . $invoice_data['id']; ?>
								</td>
							</tr>
						</table>
					</td>
					<td width="50%" valign="top">
						<table width="100%" border="0">
							<tr>
								<td colspan="2" align="left">
									STCfitness<br />
									ABN: 98 411 770 051<br />
									71A Wyndham Street<br />
									Shepparton VIC 3630, Australia
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td align="center" style="padding-bottom:100px">
			<table width="90%" cellpadding="0" cellspacing="10">
				<tr>
					<th align="left" style="padding-top:50px; padding-bottom:10px;">Description</th>
					<th align="right" style="padding-top:50px; padding-bottom:10px; text-align:right">GST</th>
					<th align="right" style="padding-top:50px; padding-bottom:10px; text-align:right">Amount AUD</th>
				</tr>
				<tr style="border-top-color:#000000; border-top-style:solid; border-top-width:1px;">
					<td style="padding-bottom:10px; padding-top:10px;">
						<?php echo $invoice_data['description']; ?>
					</td>
					<td align="right" style="padding-bottom:10px; padding-top:10px;">
						10%
					</td>
					<td align="right" style="padding-bottom:10px; padding-top:10px;">
						<?php echo number_format($invoice_data['amount']-($invoice_data['amount']/11), 2); ?>
					</td>
				</tr>
				<tr style="border-top-color:#cccccc; border-top-style:solid; border-top-width:1px;">
					<td colspan="2" align="right" style="padding-bottom:10px; padding-top:10px;">Subtotal</td>
					<td style="padding-bottom:10px; padding-top:10px;" align="right"><?php echo number_format($invoice_data['amount']-($invoice_data['amount']/11), 2); ?></td>
				</tr>
				<tr>
					<td colspan="2" align="right" style="padding-bottom:10px; padding-top:10px;">TOTAL GST 10%</td>
					<td align="right"><?php echo number_format(($invoice_data['amount']/11), 2); ?></td>
				</tr>
				<tr>
					<td style="padding-bottom:10px; padding-top:10px;"></td>
					<td align="right" style="padding-bottom:10px; padding-top:10px; border-top-color:#000000; border-top-style:solid; border-top-width:1px;"><b>TOTAL AUD</b></td>
					<td align="right" style="padding-bottom:10px; padding-top:10px; border-top-color:#000000; border-top-style:solid; border-top-width:1px;"><?php echo number_format($invoice_data['amount'], 2); ?></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<?php else: ?>
	<p>No invoice data supplied!</p>
<?php endif; ?>