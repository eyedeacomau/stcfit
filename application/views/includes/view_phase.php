<div class="modal-header gradient-background">
	<strong>Phase <?php echo $data['phase_number']; ?></strong> - <?php echo $data['phase_name']; ?>
	<span class="sprite sprite-32 sprite-close"><button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button></span>
</div>
<div class="modal-body row">
	<?php $count=1; foreach($data['sessions'] as $session_number => $session): ?>
		<div class="session">
			<div class="session-name">Session <?php echo $count; ?></div>
			<table class="session-exercises" width="100%" cellpadding="0" cellspacing="0">
				<tr>
					<th>Exercise Name</th>
					<th>Sets</th>
					<th>Reps</th>
					<th>Tempo</th>
				</tr>
				<?php foreach($session as $exercise): ?>
					<?php if(empty($exercise['is_circuit'])): ?>
						<tr>
							<td valign="top"><?php echo $exercise['exercise_name']; ?></td>
							<td valign="top"><?php echo $exercise['sets']; ?></td>
							<td valign="top"><?php echo $exercise['reps']; ?></td>
							<td valign="top"><?php echo $exercise['tempo']; ?></td>
						</tr>
					<?php else: ?>
						<tr class="circuit">
							<td valign="top"><?php echo $exercise['exercise_name']; ?></td>
							<td colspan="3" valign="top"><?php echo nl2br($exercise['exercise_description']); ?></td>
						</tr>
					<?php endif; ?>
				<?php endforeach; ?>
			</table>
		</div>
	<?php $count++; endforeach; ?>
</div>