<div class="modal-header gradient-background">
	<strong>Exercise Information: </strong><?php echo $exercise['name']; ?>
	<span class="sprite sprite-32 sprite-close"><button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button></span>
</div>
<div class="modal-body row">
	<div class="col-xs-6">
	<div class="canvas-area">
		<ul class="large-images">
				<li id="loading-animation">
					<div class="bubblingG">
                        <span id="bubblingG_1"></span>
                        <span id="bubblingG_2"></span>
                        <span id="bubblingG_3"></span>
                    </div>
				</li>
			<?php if(!empty($exercise['video_link'])): ?>
				<li id="video_1"><iframe width="330" height="205" src="//www.youtube.com/embed/<?php echo $exercise['video_link']; ?>" frameborder="0" allowfullscreen></iframe></li>
			<?php elseif($exercise['image_1']): ?>
				<li id="image_1"><img src="<?php echo EXERCISE_IMG_PATH.'330x205-'.$exercise['image_1']; ?>" /></li>
			<?php elseif($exercise['image_2']): ?>
				<li id="image_2"><img src="<?php echo EXERCISE_IMG_PATH.'330x205-'.$exercise['image_1']; ?>" /></li>
			<?php elseif($exercise['image_3']): ?>
				<li id="image_3"><img src="<?php echo EXERCISE_IMG_PATH.'330x205-'.$exercise['image_1']; ?>" /></li>
			<?php endif; ?>
		</ul>
	</div>
	<ul class="exercise-images">
		<?php if(!empty($exercise['video_link'])): ?>
			<li class="youtube"><a href="#" class="shift" data-type="youtube" data-number="1" data-actual="//www.youtube.com/embed/<?php echo $exercise['video_link']; ?>" data-width="330" data-height="205"><img src="http://img.youtube.com/vi/<?php echo $exercise['video_link']; ?>/0.jpg" /></a></li>
		<?php endif; ?>
		<?php if(!empty($exercise['image_1'])): ?>
			<li><a href="#" class="shift" data-type="image" data-number="1" data-actual="<?php echo EXERCISE_IMG_PATH.'330x205-'.$exercise['image_1']; ?>"><img src="<?php echo EXERCISE_IMG_PATH.'77x53-'.$exercise['image_1']; ?>" /></a></li>
		<?php endif; ?>
		<?php if(!empty($exercise['image_2'])): ?>
			<li><a href="#" class="shift" data-type="image" data-number="2" data-actual="<?php echo EXERCISE_IMG_PATH.'330x205-'.$exercise['image_2']; ?>"><img src="<?php echo EXERCISE_IMG_PATH.'77x53-'.$exercise['image_2']; ?>" /></a></li>
		<?php endif; ?>
		<?php if(!empty($exercise['image_1'])): ?>
			<li><a href="#" class="shift" data-type="image" data-number="3" data-actual="<?php echo EXERCISE_IMG_PATH.'330x205-'.$exercise['image_3']; ?>"><img src="<?php echo EXERCISE_IMG_PATH.'77x53-'.$exercise['image_3']; ?>" /></a></li>
		<?php endif; ?>
	</ul>
	</div>
	<div class="col-xs-6 exercise-info">
		<?php if(!empty($exercise['description'])): ?>
			<h3>Description</h3>
			<p><?php echo nl2br($exercise['description']); ?></p>
		<?php endif; ?>
		<h4>Modifiers</h4>
		<p class="modifier"><span>Low Modifier:</span> <?php echo (!empty($exercise['low_modifiers']) ? $exercise['low_modifiers']:'Undefined'); ?></p>
		<p class="modifier"><span>Medium Modifier:</span> <?php echo (!empty($exercise['medium_modifiers']) ? $exercise['medium_modifiers']:'Undefined'); ?></p>
		<p class="modifier"><span>High Modifier:</span> <?php echo (!empty($exercise['high_modifiers']) ? $exercise['high_modifiers']:'Undefined'); ?></p>
		<?php if(!empty($exercise['benefits'])): ?>
			<h4>Benefits</h4>
			<p><?php echo nl2br($exercise['benefits']); ?></p>
		<?php endif; ?>
	</div>
</div>