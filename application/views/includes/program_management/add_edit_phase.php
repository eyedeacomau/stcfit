<div class="modal-header gradient-background">
	Add/Edit Phase
	<span class="sprite sprite-32 sprite-close"><button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button></span>
</div>
<div class="modal-body row">
<div class="col-xs-12" id="phase_errors" style="display:none;">
	<div class="message-area area-small alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<div class="error_area">
			
		</div>
	</div>
</div>	
	<?php echo form_open('/admin/program-management/add-edit-phase', array('role' => 'form', 'id' => 'phase_form')); ?>
		<input type="hidden" name="training_type" value="<?php echo $training_type; ?>" />
		<?php if(!empty($phase)): ?>
			<input type="hidden" name="phase_number" value="<?php echo $phase['phase_number']; ?>" />
		<?php endif; ?>
		<div class="col-xs-6">
			<div class="form-group">
				<label for="phase_name">Phase Name</label>
				<input type="text" name="phase_name" id="phase_name" class="form-control" value="<?php echo (!empty($phase['name']) ? $phase['name']:''); ?>" />
			</div>
		</div>
		<div class="col-xs-6">
			<div class="form-group">
				<label for="video_link">Youtube URL</label>
				<input type="text" name="video_link" id="video_link" class="form-control" value="<?php echo (!empty($phase['video_link']) ? 'http://www.youtube.com/watch?v='.$phase['video_link']:''); ?>" />
			</div>
		</div>
		<div class="col-xs-12">
			<div class="form-group">
				<label for="phase_description">Phase Description</label>
				<textarea name="phase_description" id="phase_description" class="form-control"><?php echo (!empty($phase['description']) ? $phase['description']:''); ?></textarea>
			</div>
		</div>
		<div class="right-align">
			<input type="submit" name="add_update_phase" class="btn btn-primary" value="Submit!" />
		</div>
	<?php echo form_close(); ?>
</div>
<script>
	head.ready(function () {
		(function ($) {
			$('#phase_form').on('submit', function(e){
				e.preventDefault();
				var submit_button = $(this).find('input[type=submit]');
				submit_button.attr('disabled', 'disabled');
				$.ajax({
					url:$(this).attr('action'),
					method: "POST",
					data: $(this).serialize()+'&add_update_phase=Submit',
					dataType: 'json',
					success: function(response){
						if(response.success == false){
							$('#phase_errors div.error_area').html(response.errors);
							$('#phase_errors').show();
							submit_button.removeAttr('disabled');
						} else{
							reload_phases(response.training_type);
							$('#add_edit').modal('hide');
						}
					}
				});
			});
		})(jQuery);
	});
</script>