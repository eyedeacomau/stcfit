<a href="#" id="add_exercise" class="add-exercise"><span>+</span>Add New Exercise</a>
<div class="sortable-area">
	<div class="sortable-headings">
		<span class="small-cell">Number</span>
		<span class="keep-left">Exercise Name</span>
		<span class="small-cell">Sets</span>
		<span class="small-cell">Reps</span>
		<span class="small-cell">Tempo</span>
		<span class="medium-cell">Re-order</span>
		<span class="small-cell"></span>
	</div>
	<ul id="sortable_exercises" class="sortable-list">
		<?php if(!empty($session_exercises['exercises'])): ?>
			<?php for($x=0;$x<count($session_exercises['exercises']); $x++): ?>
				<li class="ui-state-default">
					<table width="100%" cellspacing="0" cellspacing="0">
						<tbody>
							<tr>
								<td class="small-cell list-number">
									<?php echo ($x+1); ?>
								</td>
								<td class="keep-left">
									<input type="hidden" name="exercise[]" value="<?php echo $session_exercises['exercises'][$x]['exercise_identifier']; ?>" />
									<?php echo $session_exercises['exercises'][$x]['name']; ?>
								</td>
								<td class="small-cell"><input type="text" class="table-input" name="sets[]" value="<?php echo $session_exercises['exercises'][$x]['sets']; ?>" data-required="true" data-int-only="true" /></td>
								<td class="small-cell"><input type="text" class="table-input" name="reps[]" value="<?php echo $session_exercises['exercises'][$x]['reps']; ?>" data-required="true" /></td>
								<td class="small-cell"><input type="text" class="table-input" name="tempo[]" value="<?php echo $session_exercises['exercises'][$x]['tempo']; ?>" data-required="true" /></td>
								<td class="medium-cell"><a href="#move" class="move-me">Hold &amp; Move</a></td>
								<td class="small-cell"><a href="#delete" class="delete-me">Delete</a></td>
							</tr>
						</tbody>
					</table>
				</li>
			<?php endfor; ?>
		<?php endif; ?>
	</ul>
</div>
<a href="#" id="add_circuit" class="add-exercise"><span>+</span>Add New Circuit</a>
<div class="sortable-area">
	<div class="sortable-headings">
		<span class="small-cell">Number</span>
		<span class="keep-left very-large">Circuit Name</span>
		<span class="medium-cell">Re-order</span>
		<span class="small-cell"></span>
	</div>
	<ul id="sortable_circuits" class="sortable-list">
		<?php if(!empty($session_exercises['circuits'])): ?>
			<?php for($x=0;$x<count($session_exercises['circuits']); $x++): ?>
				<li class="ui-state-default">
					<table width="100%" cellspacing="0" cellspacing="0">
						<tbody>
							<tr>
								<td class="small-cell list-number">
									<?php echo ($x+1); ?>
								</td>
								<td class="keep-left very-large">
									<input type="hidden" name="circuit[]" value="<?php echo $session_exercises['circuits'][$x]['exercise_identifier']; ?>" />
									<?php echo $session_exercises['circuits'][$x]['name']; ?>
								</td>
								<td class="medium-cell"><a href="#move" class="move-me">Hold &amp; Move</a></td>
								<td class="small-cell"><a href="#delete" class="delete-me">Delete</a></td>
							</tr>
						</tbody>
					</table>
				</li>
			<?php endfor; ?>
		<?php endif; ?>
	</ul>
</div>
<div class="submit">
	<input type="submit" name="submit_program" class="btn btn-primary" value="Save session" />
</div>
<script type="text/javascript">
	var exercises = <?php echo json_encode($all_exercises); ?>;
	var circuits = <?php echo json_encode($all_circuits); ?>;
	head.ready(function () {
    	(function ($) {
    		go_sortable();
    		$('.delete-me').on("click", function(){
    			ul = $(this).closest('ul');
    			$(this).closest('li').remove();
    			recount(ul);
    		});
			var exercise_select = '<select name="exercise[]" class="chzn">';
			for(x=0;x<exercises.length;x++){
				exercise_select += '<option value="'+exercises[x].exercise_identifier+'">'+exercises[x].exercise_name+'</option>';
			}
			exercise_select += '</select>';

			var circuit_select = '<select name="circuit[]" class="chzn">';
			for(x=0;x<circuits.length;x++){
				circuit_select += '<option value="'+circuits[x].exercise_identifier+'">'+circuits[x].exercise_name+'</option>';
			}
			circuit_select += '</select>';

			$('#add_exercise').on('click', function(e){
				e.preventDefault();
				var ul = $('ul#sortable_exercises');
				var num = ($('li', ul).size()+1);
				var html = '\
					<li class="ui-state-default no-padding">\
						<table width="100%" cellspacing="0" cellspacing="0">\
							<tbody>\
								<tr>\
									<td class="small-cell list-number">'+num+'</td>\
									<td class="keep-left">'+exercise_select+'</td>\
									<td class="small-cell"><input type="text" class="table-input" name="sets[]" value="1" data-required="true" data-int-only="true" /></td>\
									<td class="small-cell"><input type="text" class="table-input" name="reps[]" value="1" data-required="true" /></td>\
									<td class="small-cell"><input type="text" class="table-input" name="tempo[]" value="1" data-required="true" /></td>\
									<td class="medium-cell"><a href="#move" class="move-me">Hold &amp; Move</a></td>\
									<td class="small-cell"><a href="#delete" class="delete-me">Delete</a></td>\
								</tr>\
							</tbody>\
						</table>\
					</li>\
				';
				ul.append(html);
				$('.chzn').chosen({
					width: '100%'
				});
				recount(ul);

				$('.delete-me').unbind().on("click", function(){
	    			ul = $(this).closest('ul');
	    			$(this).closest('li').remove();
	    			recount(ul);
	    		});

	    		$('form.validate').each(function(){
	    			$(this).unbind().removeData();
	    			var validator = $(this).validate({debug: true});
					validator.resetForm();
	    		});
			});

			$('#add_circuit').on('click', function(e){
				e.preventDefault();
				var ul = $('ul#sortable_circuits');
				var num = ($('li', ul).size()+1);
				var html = '\
					<li class="ui-state-default no-padding">\
						<table width="100%" cellspacing="0" cellspacing="0">\
							<tbody>\
								<tr>\
									<td class="small-cell list-number">'+num+'</td>\
									<td class="keep-left very-large">'+circuit_select+'</td>\
									<td class="medium-cell"><a href="#move" class="move-me">Hold &amp; Move</a></td>\
									<td class="small-cell"><a href="#delete" class="delete-me">Delete</a></td>\
								</tr>\
							</tbody>\
						</table>\
					</li>\
				';

				ul.append(html);
				$('.chzn').chosen({
					width: '100%'
				});
				recount(ul);
				$('.delete-me').unbind().on("click", function(){
	    			ul = $(this).closest('ul');
	    			$(this).closest('li').remove();
	    			recount(ul);
	    		});

	    		$('form.validate').each(function(){
	    			var validator = $(this).validate();
					validator.resetForm();
	    		});
			});

			function go_sortable(){
				if($('.sortable-list').length){
					$(".sortable-list").sortable({
						placeholder: "highlight",
						handle: ".move-me",
						opacity: 0.7,
						axis: "y",
						cursor: "move",
						update: function(){
							recount($(this));
						}
					});
				}
			}

			function recount(element){
				$('.list-number', element).each(function(count){
					$(this).html(count+1);
				});
			}
	    })(jQuery);
	});
</script>