<div class="modal-header gradient-background">
	Add/Edit Session
	<span class="sprite sprite-32 sprite-close"><button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button></span>
</div>
<div class="modal-body row">
<div id="session_errors" style="display:none;">
	<div class="message-area area-small alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<div class="error_area">
			
		</div>
	</div>
</div>
	<?php echo form_open('/admin/program-management/add-edit-session', array('role' => 'form', 'id' => 'session_form')); ?>
		<input type="hidden" name="training_type" value="<?php echo $training_type; ?>" />
		<input type="hidden" name="phase_number" value="<?php echo $phase_number; ?>" />
		<?php if(!empty($session)): ?>
			<input type="hidden" name="session_identifier" value="<?php echo $session['session_identifier']; ?>" />
			<input type="hidden" name="session_order" value="<?php echo $session['order']; ?>" />
		<?php endif; ?>
		<div class="col-xs-6">
			<div class="form-group">
				<label for="session_name">Session Name</label>
				<input type="text" name="session_name" id="session_name" class="form-control" value="<?php echo (!empty($session['name']) ? $session['name']:''); ?>" />
			</div>
		</div>
		<div class="col-xs-6">
			<div class="form-group">
				<label for="video_link">Youtube URL</label>
				<input type="text" name="video_link" id="video_link" class="form-control" value="<?php echo (!empty($session['video_link']) ? 'http://www.youtube.com/watch?v='.$session['video_link']:''); ?>" />
			</div>
		</div>
		<div class="col-xs-12">
			<div class="form-group">
				<label for="session_description">Session Description</label>
				<textarea name="session_description" id="session_description" class="form-control"><?php echo (!empty($session['description']) ? $session['description']:''); ?></textarea>
			</div>
		</div>
		<div class="right-align">
			<input type="submit" name="add_update_session" class="btn btn-primary" value="Submit!" />
		</div>
	<?php echo form_close(); ?>
</div>
<script>
	head.ready(function () {
		(function ($) {
			$('#session_form').on('submit', function(e){
				e.preventDefault();
				var submit_button = $(this).find('input[type=submit]');
				submit_button.attr('disabled', 'disabled');
				$.ajax({
					url:$(this).attr('action'),
					method: "POST",
					data: $(this).serialize()+'&add_update_session=Submit',
					dataType: 'json',
					success: function(response){
						if(response.success == false){
							$('#session_errors div.error_area').html(response.errors);
							$('#session_errors').show();
							submit_button.removeAttr('disabled');
						} else{
							reload_sessions(response.phase_number, response.training_type);
							$('#add_edit').modal('hide');
						}
					}
				});
			});
		})(jQuery);
	});
</script>