<?php if(!empty($success_messages)): ?>
	<div class="message-area area-small alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $success_messages; ?>
	</div>
<?php endif; ?>
<?php if(!empty($messages)): ?>
	<div class="message-area area-small alert alert-info alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $messages; ?>
	</div>
<?php endif; ?>
<?php if(!empty($errors)): ?>
	<div class="message-area area-small alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $errors; ?>
	</div>
<?php endif; ?>
<div class="section row" id="graph_section">
	<div class="graph-container">
		<div id="client_graph">

		</div>
	</div>
	<div class="total-clients">
		<span><?php echo (!empty($clients['current_clients']) ? $clients['current_clients']:0); ?></span>
		Current Client<?php echo (!empty($clients['current_clients']) && $clients['current_clients'] == 1 ? '':'s'); ?>
	</div>
	<div class="loss-reasons">
		<h3>Recently Lost Clients</h3>
		<?php foreach($clients['reasons']['info'] as $reason_id => $reason_info): ?>
			<div class="reason">
				<span class="color" style="background-color:<?php echo $reason_info['color']; ?>"></span>
				<span class="total"><?php echo (!empty($reason_info['total']) ? round((($reason_info['total']/$clients['reasons']['total'])*100)).'%':'0%');?></span> <?php echo $reason_info['title']; ?>
			</div>
		<?php endforeach; ?>
	</div>
</div>
<h4 class="section-subheader">Recently Lost clients</h4>
<div class="section row" id="lost_clients">
	<table border="0" width="100%">
		<tr>
			<th>Name</th>
			<th>Phone</th>
			<th>Reason</th>
			<th>Date Joined</th>
			<th>Date Left</th>
			<th></th>
		</tr>

	<?php if(empty($clients['lost_clients'])): ?>
		<tr>
			<td colspan="5" class="no-clients">
				No lost clients for this period!
			</td>
		</tr>
	<?php else: ?>
		<?php $count=0; foreach($clients['lost_clients'] as $client): ?>
			<?php if($count < 10): ?>
				<tr>
					<td width="20%"><?php echo $client['first_name'].' '.$client['last_name']; ?></td>
					<td width="15%"><?php echo $client['phone']; ?></td>
					<td width="25%"><?php echo $clients['reasons']['info'][intval($client['reason'])]['title']; ?></td>
					<td width="15%"><?php echo date('d/m/Y', intval($client['signup_started'])); ?></td>
					<td width="15%"><?php echo date('d/m/Y', intval($client['deletion_date'])); ?></td>
					<td width="10%">
						<?php if(empty($client['user_confirmed'])): ?>
							<div class="btn-group">
								<button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown">
								Options <span class="caret"></span>
								</button>
								<ul class="dropdown-menu" role="menu">
									<li><a href="/trainers/client-login/<?php echo $client['user_id']; ?>">Login as user</a></li>
									<li class="divider"></li>
									<li><a data-deletionid="<?php echo $client['deletion_id']; ?>" data-userid="<?php echo $client['user_id']; ?>" data-name="<?php echo $client['first_name'].' '.$client['last_name']; ?>" class="resubscribe" href="#re-subscribe">Re-subscribe user</a></li>
									<li><a data-deletionid="<?php echo $client['deletion_id']; ?>" data-userid="<?php echo $client['user_id']; ?>" data-name="<?php echo $client['first_name'].' '.$client['last_name']; ?>" class="resubscribe" href="#defer-user-subscription">Defer user subscription</a></li>
								</ul>
							</div>
						<?php else: ?>
							<span class="error-span">Cancelled</span>
						<?php endif; ?>
					</td>
				</tr>
			<?php endif; ?>
		<?php $count++; endforeach; ?>
	<?php endif; ?>
	</table>
</div>
<div class="modal fade" id="change_subscription" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header gradient-background">
				Change subscription for user: <span id="user_name"></span>
				<span class="sprite sprite-32 sprite-close"><button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button></span>
			</div>
			<div class="modal-body row">
				<?php echo form_open('/trainers', array('role' => 'form')); ?>
					<input type="hidden" id="user_id" name="user_id" value="" />
					<input type="hidden" id="deletion_id" name="deletion_id" value="" />
					<input type="submit" name="re_activate" value="Re-activate subscription" class="btn btn-primary btn-lg" />
					<h1>OR</h1>
					<div class="select-box">
						<select name="defer_months" class="form-control">
							<option>Select deferral period.</option>
							<?php foreach($deferral_periods as $key => $value): ?>
								<option value="<?php echo $key; ?>"><?php echo $value; ?> Month<?php echo (intval($value) != 1 ? 's':''); ?></option>
							<?php endforeach; ?>
						</select>
					</div>
					<input type="submit" name="defer_subscription" value="Defer subscription" class="btn btn-primary btn-lg" />
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>
</div>
<script>
	var cats = ["<?php echo implode('", "', array_keys($clients['month_data'])); ?>"];
	var points = [{
		name: 'Lost clients ',
		color: '#f88f21',
		data: <?php echo json_encode($clients['linear_lost']) ."\n"; ?>
	},{
		name: 'Total clients',
		color: '#ee4234',
		data: <?php echo json_encode($clients['linear_total']) ."\n"; ?>
	}];
</script>
