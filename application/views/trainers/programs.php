<?php if(!empty($success_messages)): ?>
	<div class="message-area area-small alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $success_messages; ?>
	</div>
<?php endif; ?>
<?php if(!empty($messages)): ?>
	<div class="message-area area-small alert alert-info alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $messages; ?>
	</div>
<?php endif; ?>
<?php if(!empty($errors)): ?>
	<div class="message-area area-small alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $errors; ?>
	</div>
<?php endif; ?>
<?php if(!empty($programs)): ?>
	<div class="panel-group accordian-area" id="programs">
		<?php $count=0; foreach($programs as $program): ?>
			<div class="panel panel-default">
				<div class="panel-heading">
					<a data-toggle="collapse" data-parent="#accordion" href="#program_<?php echo $count; ?>">
						<?php echo $program['type_name']; ?>
					</a>
				</div>
				<div id="program_<?php echo $count; ?>" class="panel-collapse collapse <?php echo ($count == 0 ? 'in':''); ?>">
					<div class="panel-body row">
						<?php if(!empty($program['phases'])): ?>
							<div class="phase_container">
								<?php echo $program['phases']; ?>
							</div>
							<div class="pagination-container">
								<ul class="pagination" data-training-type="<?php echo $program['type_id']; ?>">
									<?php for($pages=0; $pages<ceil((intval($program['total_phases'])/PHASES_PER_PAGE));$pages++): ?>
										<li class="<?php echo ($pages == 0 ? 'active':''); ?>"><a href="#" data-pagenumber="<?php echo ($pages+1); ?>"><?php echo ($pages+1); ?></a></li>
									<?php endfor; ?>
								</ul>
							</div>
						<?php else:?>
							<p>No results found for this training type.</p>
						<?php endif; ?>
					</div>
				</div>
			</div>
		<?php $count++; endforeach; ?>
	</div>
		<div class="modal fade" id="phase_popup" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="loading-animation">
						<div class="bubblingG">
							<span id="bubblingG_1"></span>
							<span id="bubblingG_2"></span>
							<span id="bubblingG_3"></span>
						</div>
					</div>
					<div class="phase-content">
						<!-- This is where the content will be loaded into -->
					</div>
				</div>
			</div>
		</div>
<?php endif; ?>