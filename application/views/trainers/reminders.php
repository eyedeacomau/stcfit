<?php if(!empty($success_messages)): ?>
	<div class="message-area area-small alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $success_messages; ?>
	</div>
<?php endif; ?>
<?php if(!empty($messages)): ?>
	<div class="message-area area-small alert alert-info alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $messages; ?>
	</div>
<?php endif; ?>
<?php if(!empty($errors)): ?>
	<div class="message-area area-small alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $errors; ?>
	</div>
<?php endif; ?>
<?php if(!empty($reminders)): ?>
	<div class="panel-group accordian-area" id="reminders">
		<?php $count=0; foreach($reminders as $reminder): ?>
			<div class="panel panel-default">
				<div class="panel-heading">
					<a data-toggle="collapse" data-parent="#accordion" href="#reminder_<?php echo $count; ?>">
						<?php echo $reminder['first_name'].' '.$reminder['last_name']; ?>
					</a>
				</div>
				<div id="reminder_<?php echo $count; ?>" class="panel-collapse collapse <?php echo ($count == 0 ? 'in':''); ?>">
					<div class="panel-body row">
						<div class="progress-photo">
							<?php if(!empty($reminder['progress_photo'])): ?>
								<img src="<?php echo PROGRESS_IMG_PATH.$allowed_sizes['progress_photo_thumb']['size'].'-'.$reminder['progress_photo'] ?>" />
							<?php else: ?>
								<img src="/assets/images/no-user.jpg" />
							<?php endif;?>
						</div>
						<div class="user-info">
							<table border="0" cellpadding="0" cellspacing="0" width="100%">
								<tr>
									<td class="left">Name:</td>
									<td colspan="2"><?php echo $reminder['first_name'].' '.$reminder['last_name']; ?></td>
								</tr>
								<tr>
									<td class="left">Phone:</td>
									<td><?php echo $reminder['phone']; ?></td>
								</tr>
								<tr>
									<td class="left">Time:</td>
									<td>
										<?php if(!empty($reminder['state']) && $reminder['state'] != 'N/A'): ?>
											<?php
												date_default_timezone_set($states[$reminder['state']]['timezone']);
												echo date('h:i:sa') . ' <strong>('.$states[$reminder['state']]['label'].')</strong>';
											?>
										<?php elseif(!empty($reminder['state']) && $reminder['state'] == 'N/A'): ?>
											International
										<?php endif; ?>
									</td>
								</tr>
								<tr>
									<td colspan="3" class="large-margin">Last 3 entries of weight</td>
								</tr>
								<tr>
									<td colspan="3">
										<?php for($x=0;$x<3;$x++): ?>
											<div class="weight-entry">
												<span>Entry <?php echo ($x+1); ?>:</span> <?php echo (!empty($reminder['entries'][$x]['weight']) ? $reminder['entries'][$x]['weight'].'kg':'N/A') ?>
											</div>
										<?php endfor; ?>
									</td>
								</tr>
								<tr>
									<td colspan="3" class="small-margin">
										Body fat percentage: <strong><?php echo (!empty($reminder['body_fat']) ? $reminder['body_fat'].'%':'N/A'); ?></strong>
									</td>
								</tr>
								<tr>
									<td colspan="3">
										<strong>Measurements</strong>
									</td>
								</tr>
								<tr>
									<td colspan="3">
										<table class="measurements">
											<tr>
												<th></th>
												<th>First</th>
												<th>2nd Last</th>
												<th>Last</th>
											</tr>
											<tr>
												<td class="m-type">Shoulders</td>
												<td><?php echo (!empty($reminder['entries'][0]['shoulders']) ? $reminder['entries'][0]['shoulders'].'cm':'N/A'); ?></td>
												<td><?php echo (!empty($reminder['entries'][1]['shoulders']) ? $reminder['entries'][1]['shoulders'].'cm':'N/A'); ?></td>
												<td><?php echo (!empty($reminder['entries'][2]['shoulders']) ? $reminder['entries'][2]['shoulders'].'cm':'N/A'); ?></td>
											</tr>
											<tr>
												<td class="m-type">Neck</td>
												<td><?php echo (!empty($reminder['entries'][0]['neck']) ? $reminder['entries'][0]['neck'].'cm':'N/A'); ?></td>
												<td><?php echo (!empty($reminder['entries'][1]['neck']) ? $reminder['entries'][1]['neck'].'cm':'N/A'); ?></td>
												<td><?php echo (!empty($reminder['entries'][2]['neck']) ? $reminder['entries'][2]['neck'].'cm':'N/A'); ?></td>
											</tr>
											<tr>
												<td class="m-type">Chest</td>
												<td><?php echo (!empty($reminder['entries'][0]['chest']) ? $reminder['entries'][0]['chest'].'cm':'N/A'); ?></td>
												<td><?php echo (!empty($reminder['entries'][1]['chest']) ? $reminder['entries'][1]['chest'].'cm':'N/A'); ?></td>
												<td><?php echo (!empty($reminder['entries'][2]['chest']) ? $reminder['entries'][2]['chest'].'cm':'N/A'); ?></td>
											</tr>
											<tr>
												<td class="m-type">Waist</td>
												<td><?php echo (!empty($reminder['entries'][0]['waist']) ? $reminder['entries'][0]['waist'].'cm':'N/A'); ?></td>
												<td><?php echo (!empty($reminder['entries'][1]['waist']) ? $reminder['entries'][1]['waist'].'cm':'N/A'); ?></td>
												<td><?php echo (!empty($reminder['entries'][2]['waist']) ? $reminder['entries'][2]['waist'].'cm':'N/A'); ?></td>
											</tr>
											<tr>
												<td class="m-type">Bicep</td>
												<td><?php echo (!empty($reminder['entries'][0]['bicep']) ? $reminder['entries'][0]['bicep'].'cm':'N/A'); ?></td>
												<td><?php echo (!empty($reminder['entries'][1]['bicep']) ? $reminder['entries'][1]['bicep'].'cm':'N/A'); ?></td>
												<td><?php echo (!empty($reminder['entries'][2]['bicep']) ? $reminder['entries'][2]['bicep'].'cm':'N/A'); ?></td>
											</tr>
											<tr>
												<td class="m-type">Hips</td>
												<td><?php echo (!empty($reminder['entries'][0]['hips']) ? $reminder['entries'][0]['hips'].'cm':'N/A'); ?></td>
												<td><?php echo (!empty($reminder['entries'][1]['hips']) ? $reminder['entries'][1]['hips'].'cm':'N/A'); ?></td>
												<td><?php echo (!empty($reminder['entries'][2]['hips']) ? $reminder['entries'][2]['hips'].'cm':'N/A'); ?></td>
											</tr>
											<tr>
												<td class="m-type">Quads</td>
												<td><?php echo (!empty($reminder['entries'][0]['quads']) ? $reminder['entries'][0]['quads'].'cm':'N/A'); ?></td>
												<td><?php echo (!empty($reminder['entries'][1]['quads']) ? $reminder['entries'][1]['quads'].'cm':'N/A'); ?></td>
												<td><?php echo (!empty($reminder['entries'][2]['quads']) ? $reminder['entries'][2]['quads'].'cm':'N/A'); ?></td>
											</tr>
											<tr>
												<td class="m-type">Calf</td>
												<td><?php echo (!empty($reminder['entries'][0]['calf']) ? $reminder['entries'][0]['calf'].'cm':'N/A'); ?></td>
												<td><?php echo (!empty($reminder['entries'][1]['calf']) ? $reminder['entries'][1]['calf'].'cm':'N/A'); ?></td>
												<td><?php echo (!empty($reminder['entries'][2]['calf']) ? $reminder['entries'][2]['calf'].'cm':'N/A'); ?></td>
											</tr>
											<tr>
												<td class="m-type">Wrist</td>
												<td><?php echo (!empty($reminder['entries'][0]['wrist']) ? $reminder['entries'][0]['wrist'].'cm':'N/A'); ?></td>
												<td><?php echo (!empty($reminder['entries'][1]['wrist']) ? $reminder['entries'][1]['wrist'].'cm':'N/A'); ?></td>
												<td><?php echo (!empty($reminder['entries'][2]['wrist']) ? $reminder['entries'][2]['wrist'].'cm':'N/A'); ?></td>
											</tr>
											<tr>
												<td class="m-type">Forearm</td>
												<td><?php echo (!empty($reminder['entries'][0]['forearm']) ? $reminder['entries'][0]['forearm'].'cm':'N/A'); ?></td>
												<td><?php echo (!empty($reminder['entries'][1]['forearm']) ? $reminder['entries'][1]['forearm'].'cm':'N/A'); ?></td>
												<td><?php echo (!empty($reminder['entries'][2]['forearm']) ? $reminder['entries'][2]['forearm'].'cm':'N/A'); ?></td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</div>
						<div class="next-reminder">
							<?php echo form_open('/trainers/reminders', array('role' => 'form', 'class' => 'validate')); ?>
								<input type="hidden" name="user_id" value="<?php echo $reminder['client_id'] ?>" />
								<div class="form-group">
									<label class="placeholder" for="notes">Notes</label>
									<textarea name="notes" id="notes" class="form-control"></textarea>
								</div>
								<div class="form-group form-inline date">
									<label for="next_reminder">Next Reminder</label>
									<div class="group-container">
										<div class="input-group">
											<input type="text" name="next_reminder" id="next_reminder" class="form-control datepicker" required />
											<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
										</div>
									</div>
								</div>
								<div class="form-inline">
									<a href="/trainers/client-login/<?php echo $reminder['client_id']; ?>" class="btn btn-primary">Login as User</a>
									<input type="submit" name="submit_reminder" class="btn btn-primary" value="Save" />
								</div>
							<?php echo form_close(); ?>
							<div class="last-reminder">
								<span>Previous/system generated notes:</span>
								<p><?php echo $reminder['notes']; ?></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php $count++; endforeach; ?>
	</div>
<?php endif; ?>