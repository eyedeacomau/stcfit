<?php if(!empty($success_messages)): ?>
	<div class="message-area area-small alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $success_messages; ?>
	</div>
<?php endif; ?>
<?php if(!empty($messages)): ?>
	<div class="message-area area-small alert alert-info alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $messages; ?>
	</div>
<?php endif; ?>
<?php if(!empty($errors)): ?>
	<div class="message-area area-small alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $errors; ?>
	</div>
<?php endif; ?>
<?php echo form_open('/trainers/clients', array('role' => 'form', 'method' => 'get')); ?>
	<div class="search-form form-group form-inline">
		<label for="search_params" class="placeholder">Search Here...</label>
		<input type="text" class="form-control" name="search_params" id="search_params" value="<?php echo (!empty($search_params) ? $search_params:''); ?>" />
		<input type="submit" name="client_search" class="btn btn-primary" value="Search" />
	</div>
<?php echo form_close(); ?>
<?php if(!empty($clients)): ?>
	<div id="clients">
		<ul class="nav nav-tabs">
			<li class="active"><a href="#your-clients" data-toggle="tab">Your Clients</a></li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane active" id="your-clients">
				<table border="0" cellpadding="0" cellspacing="0" width="100%">
					<tr>
						<th>Name</th>
						<th>Phone</th>
						<th>Email</th>
						<th>Signup Date</th>
						<th colspan="2"><!-- Options --></th>
					</tr>
					<?php foreach($clients as $client): ?>
						<tr>
							<td><?php echo $client['first_name'].' '.$client['last_name']; ?></td>
							<td><?php echo $client['phone']; ?></td>
							<td><?php echo $client['user_email']; ?></td>
							<td><?php echo date('d/m/Y', $client['signup_date']); ?></td>
							<td align="right">
								<?php if(!empty($client['is_suspended'])): ?>
									<a href="/trainers/reactivate-user/<?php echo $client['user_id']; ?>" class="client-option ask-first" data-message="Are you sure that you would like to reactivate this user?">Re-Activate</a>
								<?php else: ?>
									<a href="/trainers/client-login/<?php echo $client['user_id']; ?>" class="client-option">Login as user</a>
									<a href="/trainers/suspend-user/<?php echo $client['user_id']; ?>" class="client-option ask-first" data-message="Are you sure that you would like to suspend this user?">Suspend</a>
								<?php endif; ?>
							</td>
						</tr>
					<?php endforeach; ?>
				</table>
			</div>
		</div>
	</div>
<?php endif; ?>