<?php if(!empty($success_messages)): ?>
	<div class="message-area area-small alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $success_messages; ?>
	</div>
<?php endif; ?>
<?php if(!empty($messages)): ?>
	<div class="message-area area-small alert alert-info alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $messages; ?>
	</div>
<?php endif; ?>
<?php if(!empty($errors)): ?>
	<div class="message-area area-small alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $errors; ?>
	</div>
<?php endif; ?>
<div class="row" id="messages">
	<?php if(!empty($trainer_messages)): ?>
		<div class="col-xs-6">
			<div class="panel-group" id="message-accordian">
				<?php foreach($trainer_messages as $user_id => $conversation): ?>
					<div class="panel panel-default" data-trainer-id="<?php echo $conversation['trainer_id']; ?>" data-user-id="<?php echo $user_id; ?>" data-unread="<?php echo (!empty($conversation['unread']) ? $conversation['unread']:0); ?>">
						<div class="panel-heading">
							<a data-toggle="collapse" data-parent="#accordion" class="read_messages" href="#conversation_<?php echo $user_id; ?>"><?php echo $conversation['name']; ?> <?php echo (!empty($conversation['unread']) ? '<span class="counter message-counter">'.$conversation['unread'].'</span>':''); ?></a>
						</div>
						<div id="conversation_<?php echo $user_id; ?>" class="panel-collapse collapse">
							<div class="panel-body">
								<div class="messages-section">
									<?php foreach($conversation['messages'] as $message): ?>
										<div class="message row <?php echo ($user_id == intval($message['from_user_id']) ? '':'trainer-message') ?>">
											<div class="message-container">
												<div class="message-from"><strong><?php echo ($user_id == intval($message['from_user_id']) ? $message['conversation_name']:'Me') ?></strong> <?php echo date('d/m/Y - h:i:sa', intval($message['date'])); ?></div>
												<div class="message-content"><?php echo nl2br($message['message_body']); ?></div>
											</div>
										</div>
									<?php endforeach; ?>
								</div>
								<div class="reply-section">
									<?php echo form_open('/trainers/messages', array('role' => 'form', 'class' => 'validate')); ?>
										<input type="hidden" name="message_to" value="<?php echo $user_id; ?>" />
										<div class="form-group">
											<label for="message_body_<?php echo $user_id; ?>">Reply:</label>
											<textarea class="form-control" id="message_body_<?php echo $user_id; ?>" name="message_body" required></textarea>
										</div>
										<div class="submit-message">
											<input type="submit" name="send_message" class="btn btn-primary" value="Reply now" />
										</div>
									<?php echo form_close(); ?>
								</div>
							</div>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	<?php endif; ?>
	<div class="col-xs-6 new-message">
		<?php if(!empty($clients)): ?>
			<?php echo form_open('/trainers/messages', array('role' => 'form', 'class' => 'validate')); ?>
				<div class="message-to form-group">
					<label for="message_to">Message to:</label>
					<select name="message_to" id="message_to" class="chzn" data-placeholder="Select Client">
						<option></option>
						<?php foreach($clients as $client): ?>
							<option value="<?php echo $client['user_id']; ?>"><?php echo $client['first_name'].' '.$client['last_name']; ?></option>
						<?php endforeach; ?>
					</select>
				</div>
				<div class="message-body form-group">
					<label for="message_body">Message:</label>
					<textarea name="message_body" id="message_body" class="form-control" required></textarea>
				</div>
				<div class="submit">
					<input type="submit" name="send_message" class="btn btn-primary" value="Send message" />
				</div>
			<?php echo form_close(); ?>
		<?php endif; ?>
	</div>
</div>