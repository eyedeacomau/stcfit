<table border="0" cellspacing="0" cellpadding="0" width="100%">
	<tr>
		<td>
			<h2 style="font-size:20px; font-weight:bold; color:#000000;">Billing Failed</h2>
		</td>
	</tr>
	<tr>
		<td>
			<p style="font-size:13px; color:#000000;">
				We were unable to charge your STC Fit renewal fees to your account. If you would like to to continue with STC Fit please log in to your account to renew your subscription.
			</p>
			<p style="font-size:13px; color:#000000;">
				Your STF Fit subscription expires on <strong><?php echo date('d/m/Y', $expires);?></strong>.
			</p>
			<p style="font-size:13px; color:#000000;">
				You can contact us through our website by following the below link:<br/>
				<big style="font: 16px/18px Arial, Helvetica, sans-serif;"><b><a href="<?php echo site_url('/contact-us');?>" style="color: #3366cc;">Contact Us</a></b></big>
			</p>
			<p style="font-size:13px; color:#000000;">
				Link doesn't work? Copy the following link to your browser address bar:<br/>
				<?php echo site_url('/contact-us');?>
			</p>
		</td>
	</tr>
	<tr>
		<td style="padding-top:50px; padding-bottom:100px; font-size:13px;">
			<h2 style="font-size:20px;">Regards,</h2>
			<?php echo site_url(); ?><br />
			<?php echo FOOTER_EMAIL; ?>
		</td>
	</tr>
</table>