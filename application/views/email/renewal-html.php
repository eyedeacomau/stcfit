<table border="0" cellspacing="0" cellpadding="0" width="100%">
	<tr>
		<td>
			<h2 style="font-size:20px; font-weight:bold; color:#000000;">Thanks for renewing your STC Fit subscription!</h2>
		</td>
	</tr>
	<tr>
		<td>
			<p style="font-size:13px; color:#000000;">
				Tax invoice:
			</p>
		</td>
	</tr>
	<tr>
		<td align="center" style="padding-top:30px; padding-bottom:25px;">
			<table border="0" cellspacing="0" cellpadding="15" width="100%">
				<tr>
					<td style="font-size:15px; font-weight:700; padding-top:40px;">
						<?php echo $invoice; ?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td style="padding-top:50px; padding-bottom:100px; font-size:13px;">
			<h2 style="font-size:20px;">Regards,</h2>
			<?php echo site_url(); ?><br />
			<?php echo FOOTER_EMAIL; ?>
		</td>
	</tr>
</table>