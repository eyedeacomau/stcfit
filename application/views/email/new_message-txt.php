New Message

<?php echo $message['user']['first_name'];?> has sent you a new message:

_________________________________________________

<?php echo $message['user']['first_name'].' '.$message['user']['last_name'].' ('.date('d/m/y - h:ia', $message['created']).')';?>
<?php echo $message['message'];?>
_________________________________________________

You can view this message in browser by copying the following link to your browser address bar:
<?php echo site_url('/messages'); ?>

Regards,
<?php echo site_url(); ?>

<?php echo FOOTER_EMAIL; ?>