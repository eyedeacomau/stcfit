<table border="0" cellspacing="0" cellpadding="0" width="100%">
	<tr>
		<td>
			<h2 style="font-size:20px; font-weight:bold; color:#000000;">New Trial Signup</h2>
		</td>
	</tr>
	<tr>
		<td align="center" style="padding-top:30px; padding-bottom:25px;">
			<table border="0" cellspacing="0" cellpadding="15" width="100%">
				<tr>
					<td style="font-size:15px; font-weight:700;">
						User Information:
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellspacing="0" cellpadding="5" style="font-size:12px;">
							<tr>
								<td width="50%">Name:</td>
								<td width="50%"><?php echo $name; ?></td>
							</tr>
							<tr>
								<td width="50%">Email:</td>
								<td width="50%"><?php echo $email; ?></td>
							</tr>
							<tr>
								<td width="50%">Phone:</td>
								<td width="50%"><?php echo $phone; ?></td>
							</tr>
							<tr>
								<td width="50%">Type:</td>
								<td width="50%"><?php echo $type; ?></td>
							</tr>
							<tr>
								<td width="50%">Receive Marketing:</td>
								<td width="50%"><?php echo $marketing; ?></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
