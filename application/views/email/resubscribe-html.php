<table border="0" cellspacing="0" cellpadding="0" width="100%">
	<tr>
		<td>
			<h2 style="font-size:20px; font-weight:bold; color:#000000;">Hi <?php echo $user['first_name'];?>!</h2>
		</td>
	</tr>
	<tr>
		<td>
			<p style="font-size:13px; color:#000000;">
				In accordance with the discussion held with your trainer, in which you elected to not cancel your subscription to STC Fit, your subscription will be reactivated.
			</p>
			<p style="font-size:13px; color:#000000;">
				If this is not what was discussed and you would like your subscription to be cancelled please follow the link bellow to confirm your cancellation:<br/>
				<big style="font: 16px/18px Arial, Helvetica, sans-serif;"><b><a href="<?php echo site_url('/cancel-subscription/'.$user['id'].'/'.$confirmation_key); ?>" style="color: #3366cc;">Cancel Subscription</a></b></big>
			</p>
			<p style="font-size:13px; color:#000000;">
				Link doesn't work? Copy the following link to your browser address bar:<br />
				<?php echo site_url('/cancel-subscription/'.$user['id'].'/'.$confirmation_key);?>
			</p>
			<p style="font-size:13px; color:#000000;">
				Please keep in mind that you will retain access to your account for the paid for period.
			</p>
		</td>
	</tr>
	<tr>
		<td style="padding-top:50px; padding-bottom:100px; font-size:13px;">
			<h2 style="font-size:20px;">Regards,</h2>
			<?php echo site_url(); ?><br />
			<?php echo FOOTER_EMAIL; ?>
		</td>
	</tr>
</table>