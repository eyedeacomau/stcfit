Thanks for renewing your STC Fit subscription!

To view the invoice for your payment, simply signin to STC fit with the credentials that you have provided and go to
Dashboard > Settings > Billing

Regards,
<?php echo site_url(); ?>

<?php echo FOOTER_EMAIL; ?>