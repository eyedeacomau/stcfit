It seems as though you did not pass your medical questionaire. This is a prerequisite for training using the STCfit system.
This would have happened if you answered "Yes" to 1 or more questions during your pre-exercise questionaire.


What do I do now?
Not to worry. This does not have to be the end of your STCfit journey. If you think that you accidentily answered the questionaire incorrectly, it is important that you contact STCFfit as soon as possible, so we can get you back on track!


If you answered the medical accurately, here's the general process required to continue using the STCfit system:
 - Generally an STCfit administrator or trainer will contact you within 24-48 hours to discuss your options
 - You may be required to obtain a doctor's certificate, claiming that you are fit to receive training from STCfit
 - You may be asked to prove your well-being through other methods


 We ask that you try not to get disheartened by any of this, in most cases STCfit is able to work through these small hiccups with it's clients, to help you on your way to better health, fitness and stength.