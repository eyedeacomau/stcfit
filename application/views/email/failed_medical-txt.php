Administraive Email - Failed Medical

A user has failed their medical questioinaiere on STC Fit.

User Information:
Name:				<?php echo $user['first_name'] . ' ' . $user['last_name'] . "\n"; ?>
Email:				<?php echo $user['registration_email'] . "\n"; ?>
Receive Marketing:	<?php echo ($user['marketing'] == 1 ? 'Yes':'No') . "\n"; ?>

_________________________________________________

Questionaire Answers:
<?php foreach($medical as $question => $answer): ?>
<?php echo ucwords(str_replace('_', ' ', $question)); ?>:				<?php echo ($answer == 1 ? 'Yes':'No'); ?>	
<?php endforeach; ?>
_________________________________________________

Regards,
<?php echo site_url(); ?>

<?php echo FOOTER_EMAIL; ?>