<table border="0" cellspacing="0" cellpadding="0" width="100%">
	<tr>
		<td>
			<h2 style="font-size:20px; font-weight:bold; color:#000000;">Administraive Email</h2>
		</td>
	</tr>
	<tr>
		<td>
			<p style="font-size:13px; color:#000000;">
				A user has failed their medical questioinaiere on STC Fit.
			</p>
		</td>
	</tr>
	<tr>
		<td align="center" style="padding-top:30px; padding-bottom:25px;">
			<table border="0" cellspacing="0" cellpadding="15" width="100%">
				<tr>
					<td style="font-size:15px; font-weight:700;">
						User Information:
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellspacing="0" cellpadding="5" style="font-size:12px;">
							<tr>
								<td width="50%">Name:</td>
								<td width="50%"><?php echo $user['first_name'] . ' ' . $user['last_name']; ?></td>
							</tr>
							<tr>
								<td width="50%">Email:</td>
								<td width="50%"><?php echo $user['registration_email']; ?></td>
							</tr>
							<tr>
								<td width="50%">Receive Marketing:</td>
								<td width="50%"><?php echo ($user['marketing'] == 1 ? 'Yes':'No'); ?></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td style="font-size:15px; font-weight:700; padding-top:40px;">
						Questionaire Answers:
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellspacing="0" cellpadding="5" style="font-size:12px;">
							<?php foreach($medical as $question => $answer): ?>
								<?php if($question != 'failed' && $question != 'profile_id'): ?>
									<tr>
										<td width="50%"><?php echo ucwords(str_replace('_', ' ', $question)); ?>:</td>
										<td width="50%"><?php echo ($answer == 1 ? 'Yes':'No'); ?></td>
									</tr>
								<?php endif; ?>
							<?php endforeach; ?>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td style="padding-top:50px; padding-bottom:100px; font-size:13px;">
			<h2 style="font-size:20px;">Regards,</h2>
			<?php echo site_url(); ?><br />
			<?php echo FOOTER_EMAIL; ?>
		</td>
	</tr>
</table>