<table border="0" cellspacing="0" cellpadding="0" width="100%">
	<tr>
		<td>
			<h2 style="font-size:20px; font-weight:bold; color:#000000;"><?php echo $admin ? 'New Enquiry' : 'Your Enquiry';?></h2>
		</td>
	</tr>
	<tr>
		<td>
			<p style="font-size:13px; color:#000000;">
				<?php if($admin):?>
					<?php echo $name;?> has submitted a new enquiry:
				<?php else:?>
					You submitted a enquiry to <?php echo ADMIN_EMAIL;?>
				<?php endif;?>
			</p>
		</td>
	</tr>
	<tr>
		<td align="center" style="padding: 30px;">
			<table width="80%" border="0" cellpadding="0" cellspacing="0" style="border: 1px solid #c8352b; background: #f0f0f0; width:430px;">
				<tr>
					<td style="background: #ee4234; padding:5px; font-weight: bold; color: #fff;"><?php echo $name.' ('.date('d/m/y - h:ia').')';?></td>
				</tr>
				<tr>
					<td style="border-bottom: 1px solid #c8352b; background: #ee4234; padding:5px; font-weight: bold; color: #fff;"><?php echo $email;?></td>
				</tr>
				<tr>
					<td style="padding: 10px;"><?php echo nl2br($enquiry);?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td style="padding-top:50px; padding-bottom:100px; font-size:13px;">
			<h2 style="font-size:20px;">Regards,</h2>
			<?php echo site_url(); ?><br />
			<?php echo FOOTER_EMAIL; ?>
		</td>
	</tr>
</table>