Hi <?php echo $user['first_name'];?>!

This email indicates that you have provided sufficient documentation to continue your signup to STC Fit.
Don't worry as we have save your progress and you can continue where you left off by copying the following link to your browser address bar:
<?php echo site_url('/continue-registration/'.$user['id'].'/'.$user['registration_key']); ?>

Regards,
<?php echo site_url();?>

<?php echo FOOTER_EMAIL; ?>