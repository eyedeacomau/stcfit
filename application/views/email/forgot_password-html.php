<table border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<h2 style="font-size:20px; font-weight:bold; color:#000000;">Hi <?php echo $user['first_name']; ?>,</h2>
		</td>
	</tr>
	<tr>
		<td>
			<p style="font-size:13px; color:#000000;">
				So you've forgotten your password huh! No Problems, just click on the link below and you can type in your new password.
			</p>
		</td>
	</tr>
	<tr>
		<td align="center" style="padding-top:30px; padding-bottom:25px;">
			<table border="0" cellspacing="0" cellpadding="15" style="border-width:3px; border-color:#a61010; border-style:solid; background-color:#fcf5f5;">
				<tr>
					<td style="font-size:11px; font-weight:700;">
						Click Here: <a style="color:#000000; font-weight:300; font-size:11px;" href="<?php echo site_url('/auth/reset_password/'.$user_id.'/'.$new_pass_key); ?>" style="color: #3366cc;"><?php echo site_url('/auth/reset_password/'.$user_id.'/'.$new_pass_key); ?></a>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td style="padding-top:50px; padding-bottom:100px; font-size:13px;">
			<h2 style="font-size:20px;">Enjoy!</h2>
			<?php echo site_url(); ?><br />
			<?php echo FOOTER_EMAIL; ?>
		</td>
	</tr>
</table>