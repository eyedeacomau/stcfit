Your account has been successfully billed and your subscription extended to <?php echo date('d/m/Y', $expires);?>.

User Information:
Name:<?php echo $user['first_name'] . ' ' . $user['last_name']; ?>

Email:<?php echo $user['email']; ?>

Receive Marketing:<?php echo ($user['marketing'] == 1 ? 'Yes':'No'); ?>

To view the invoice for your payment, simply sign in to STC fit with the credentials that you have provided and go to
Dashboard > Settings > Billing

Regards,
<?php echo site_url();?>

<?php echo FOOTER_EMAIL; ?>