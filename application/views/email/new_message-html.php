<table border="0" cellspacing="0" cellpadding="0" width="100%">
	<tr>
		<td>
			<h2 style="font-size:20px; font-weight:bold; color:#000000;">New Message</h2>
		</td>
	</tr>
	<tr>
		<td>
			<p style="font-size:13px; color:#000000;">
				<?php echo $message['user']['first_name'];?> has sent you a new message:
			</p>
		</td>
	</tr>
	<tr>
		<td align="center" style="padding: 30px;">
			<table width="80%" border="0" cellpadding="0" cellspacing="0" style="border: 1px solid #c8352b; background: #f0f0f0; width:430px;">
				<tr>
					<td style="border-bottom: 1px solid #c8352b; background: #ee4234; padding:5px; font-weight: bold; color: #fff;"><?php echo $message['user']['first_name'].' '.$message['user']['last_name'].' ('.date('d/m/y - h:ia', $message['created']).')';?></td>
				</tr>
				<tr>
					<td style="padding: 10px;"><?php echo nl2br($message['message']);?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<p style="font-size:13px; color:#000000;">
				You can view this message in browser using the following link:<br/>
				<big style="font: 16px/18px Arial, Helvetica, sans-serif;"><b><a href="<?php echo site_url('/messages'); ?>" style="color: #3366cc;">New message</a></b></big>
			</p>
			<p style="font-size:13px; color:#000000;">
				Link doesn't work? Copy the following link to your browser address bar:<br/>
				<?php echo site_url('/messages');?>
			</p>
		</td>
	</tr>
	<tr>
		<td style="padding-top:50px; padding-bottom:100px; font-size:13px;">
			<h2 style="font-size:20px;">Regards,</h2>
			<?php echo site_url(); ?><br />
			<?php echo FOOTER_EMAIL; ?>
		</td>
	</tr>
</table>