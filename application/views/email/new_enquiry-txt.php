New Enquiry

<?php if($admin):?>
	<?php echo $name;?> has submitted a new enquiry:
<?php else:?>
	You submitted a enquiry to <?php echo ADMIN_EMAIL;?>
<?php endif;?>

_________________________________________________

<?php echo $name.' ('.date('d/m/y - h:ia').')';?>
<?php echo $email;?>
<?php echo $enquiry;?>
_________________________________________________

Regards,
<?php echo site_url(); ?>

<?php echo FOOTER_EMAIL; ?>