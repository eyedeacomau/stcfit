<table border="0" cellspacing="0" cellpadding="0" width="100%">
	<tr>
		<td>
			<h2 style="font-size:20px; font-weight:bold; color:#000000;">Hi <?php echo $user['first_name'];?>!</h2>
		</td>
	</tr>
	<tr>
		<td>
			<p style="font-size:13px; color:#000000;">
				We've noticed that you haven't managed to finish your registration.<br />
				Don't worry as we have saved your progress and you can continue where you left off by following the link below:<br />
				<big style="font: 16px/18px Arial, Helvetica, sans-serif;"><b><a href="<?php echo site_url('/continue-registration/'.$user['id'].'/'.$user['continue_key']); ?>" style="color: #3366cc;">Finish your registration...</a></b></big>
			</p>
			<p style="font-size:13px; color:#000000;">
				Link doesn't work? Copy the following link to your browser address bar:<br />
				<?php echo site_url('/continue-registration/'.$user['id'].'/'.$user['continue_key']);?>
			</p>
		</td>
	</tr>
	<tr>
		<td style="padding-top:50px; padding-bottom:100px; font-size:13px;">
			<h2 style="font-size:20px;">Regards,</h2>
			<?php echo site_url(); ?><br />
			<?php echo FOOTER_EMAIL; ?>
		</td>
	</tr>
</table>