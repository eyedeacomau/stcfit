<table border="0" cellspacing="0" cellpadding="0" width="100%">
	<tr>
		<td>
			<h2 style="font-size:20px; font-weight:bold; color:#000000;">Hi, <?php echo (!empty($user['first_name']) ? $user['first_name']:'User!'); ?></h2>
		</td>
	</tr>
	<tr>
		<td>
			<p style="font-size:13px; color:#000000;">
				It seems as though you did not pass your medical questionaire. This is a prerequisite for training using the STCfit system.<br /><br />
				This would have happened if you answered "Yes" to 1 or more questions during your pre-exercise questionaire.
			</p>
		</td>
	</tr>
	<tr>
		<td align="center" style="padding-top:30px; padding-bottom:25px;">
			<table border="0" cellspacing="0" cellpadding="15" width="100%">
				<tr>
					<td style="font-size:15px; font-weight:700;">
						<b>What do I do now?</b>
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellspacing="0" cellpadding="5" style="font-size:12px;">
							<tr>
								<td>Not to worry. This does not have to be the end of your STCfit journey. If you think that you accidentily answered the questionaire incorrectly, it is important that you contact STCFfit as soon as possible, so we can get you back on track!</td>
							</tr>
							<tr>
								<td><b>If you answered the medical accurately, here's the general process required to continue using the STCfit system:</b></td>
							</tr>
							<tr>
								<ul>
									<li>Generally an STCfit administrator or trainer will contact you within 24-48 hours to discuss your options</li>
									<li>You may be required to obtain a doctor's certificate, claiming that you are fit to receive training from STCfit</li>
									<li>You may be asked to prove your well-being through other methods</li>
								</ul>
							</tr>
							<tr>
								<td>We ask that you try not to get disheartened by any of this, in most cases STCfit is able to work through these small hiccups with it's clients, to help you on your way to better health, fitness and stength.</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td style="padding-top:50px; padding-bottom:100px; font-size:13px;">
			<h2 style="font-size:20px;">Regards,</h2>
			<?php echo site_url(); ?><br />
			<?php echo FOOTER_EMAIL; ?>
		</td>
	</tr>
</table>