Hi <?php echo $user['first_name'];?>!

In accordance with the discussion held with your trainer, your account has been deferred until <?php echo date('d/m/Y', $defer_til);?>.

If this is not what was discussed and you would like your subscription to be cancelled please follow the link below by copying it in to your browser address bar:
<?php echo site_url('/cancel-subscription/'.$user['id'].'/'.$confirmation_key);?>

Please keep in mind that you will retain access to your account for the paid for period.

Regards,
<?php echo site_url();?>

<?php echo FOOTER_EMAIL; ?>