<div class="wrapper registration">
	<div class="inner-content">
		<ul id="registration-nav">
			<li>
				<span class="circle">1</span>
				<span class="label">Payment</span>
			</li>
		    <li>
		    	<span class="circle">2</span>
		    	<span class="label">Pre Exercise</span>
		    </li>
		    <li>
		    	<span class="circle">3</span>
		    	<span class="label">Your Details</span>
		    </li>
		    <li class="active">
		    	<span class="circle">4</span>
		    	<span class="label">Tailor Program</span>
		    </li>
		</ul>
	</div>
</div>
<?php if(!empty($errors)): ?>
	<div class="wrapper registration errors">
		<div class="inner-content">
			<div class="alert alert-danger alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<?php echo $errors; ?>
			</div>
		</div>
	</div>
<?php endif; ?>
<div class="wrapper registration">
	<div class="inner-content header">
		<span class="red-circle">3</span> Tailor Program
	</div>
		<div class="row inner-content body">
		<div class="col-xs-12">
			<?php echo form_open('/registration/tailor-program', array('role' => 'form', 'class' => 'validate')); ?>
				<ol>
					<li>
						<p>Have you followed an exercise program before? <span>*</span></p>
						<label class="radio"><input type="radio" name="previous_program" value="2" <?php echo set_radio('previous_program', '2'); ?> class="form-control" data-placement="left" required data-msg-required="You must select yes or no" /> Yes</label>
						<label class="radio"><input type="radio" name="previous_program" value="1" <?php echo set_radio('previous_program', '1'); ?> class="form-control" data-placement="left" required data-msg-required="You must select yes or no" /> No</label>
					</li>
					<li>
						<p class="clear-bottom">If yes, how long ago?</p>
						<label class="radio radio-left"><input type="radio" name="program_ended" value="3" <?php echo set_radio('program_ended', '3'); ?> class="form-control" /> 3 Months</label>
						<label class="radio radio-left"><input type="radio" name="program_ended" value="12" <?php echo set_radio('program_ended', '12'); ?> class="form-control" /> 12 Months</label>
						<label class="radio radio-left clear-left"><input type="radio" name="program_ended" value="6" <?php echo set_radio('program_ended', '6'); ?> class="form-control" /> 6 Months</label>
						<label class="radio radio-left"><input type="radio" name="program_ended" value="-1" <?php echo set_radio('program_ended', '-1'); ?> class="form-control" /> Longer</label>
					</li>
					<li class="small-margin">
						<p class="clear-bottom">How would you describe your current condition? <span>*</span></p>
						<textarea name="current_condition" class="form-control" required data-placement="right"><?php echo set_value('current_condition', ''); ?></textarea>
					</li>
					<li class="small-margin">
						<p class="clear-bottom">What is your primary goal? Please note that this will determine the type of training you will receive.<span>*</span></p>
						<select name="training_type" class="form-control" id="training_type" required data-placement="left">
							<option></option>
							<?php foreach($priorities as $priority): ?>
								<option value="<?php echo $priority['id']; ?>" <?php echo set_select('training_type', $priority['id']); ?>><?php echo $priority['name']; ?></option>
							<?php endforeach; ?>
						</select>
					</li>
					<li class="date">
						<p>When would you like to achieve your results by? <span>*</span></p>
						<div class="input-group mini-input" data-date-format="dd/mm/yyyy">
							<input type="text" name="results_by" data-min-today="true" class="form-control datepicker" required data-placement="left" value="<?php echo set_value('results_by', ''); ?>" />
							<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
						</div>
						<span class="instructions">Date format dd/mm/yyyy</span>
					</li>
					<li>
						<p class="clear-bottom">How many times per week will you commit to your fitness program?<span>*</span></p>
						<label class="radio radio-left"><input type="radio" name="days_to_commit" value="2" <?php echo set_radio('days_to_commit', '2'); ?> class="form-control" required data-placement="left" /> 2 Days</label>
						<label class="radio radio-left"><input type="radio" name="days_to_commit" value="4" <?php echo set_radio('days_to_commit', '4'); ?> class="form-control" required data-placement="right" /> 4 Days</label>
						<label class="radio radio-left clear-left"><input type="radio" name="days_to_commit" value="3" <?php echo set_radio('days_to_commit', '3'); ?> class="form-control" required data-placement="left" /> 3 Days</label>
						<label class="radio radio-left"><input type="radio" name="days_to_commit" value="5" <?php echo set_radio('days_to_commit', '5'); ?> class="form-control" required data-placement="right" /> 5 Days</label>
					</li>
					<li>
						<p class="clear-bottom">How long have you been thinking about starting an exercise program to achieve your desired result?<span>*</span></p>
						<label class="radio radio-left"><input type="radio" name="length_of_consideration" value="2" <?php echo set_radio('length_of_consideration', '2'); ?> class="form-control" required data-placement="left" /> 2 Months</label>
						<label class="radio radio-left"><input type="radio" name="length_of_consideration" value="4" <?php echo set_radio('length_of_consideration', '4'); ?> class="form-control" required data-placement="right" /> 4 Months</label>
						<label class="radio radio-left clear-left"><input type="radio" name="length_of_consideration" value="3" <?php echo set_radio('length_of_consideration', '3'); ?> class="form-control" required data-placement="left" /> 3 Months</label>
						<label class="radio radio-left"><input type="radio" name="length_of_consideration" value="-1" <?php echo set_radio('length_of_consideration', '-1'); ?> class="form-control" required data-placement="right" /> 5+ Months</label>
					</li>
					<li class="small-margin">
						<p class="clear-bottom">On a scale of 1-10 how serious are you about achieving your results?<span>*</span></p>
						<div class="mini-input">
							<select name="seriousness_scale" class="form-control" id="seriousness_scale" required data-placement="left">
							<option></option>
								<?php for($x=0;$x<10;$x++): ?>
									<option value="<?php echo $x+1; ?>" <?php echo set_select('seriousness_scale', $x); ?>><?php echo $x+1; ?></option>
								<?php endfor; ?>
							</select>
						</div>
					</li>
					<li class="small-margin">
						<p class="clear-bottom">What kept you from starting sooner? <span>*</span></p>
						<textarea name="start_prevention" class="form-control" required data-placement="right"><?php echo set_value('start_prevention'); ?></textarea>
					</li>
					<li>
						<p>Is this still a problem? <span>*</span></p>
						<label class="radio"><input type="radio" name="still_a_problem" value="2" <?php echo set_radio('still_a_problem', '2'); ?> class="form-control" data-placement="left" required data-msg-required="You must select yes or no" /> Yes</label>
						<label class="radio"><input type="radio" name="still_a_problem" value="1" <?php echo set_radio('still_a_problem', '1'); ?> class="form-control" data-placement="left" required data-msg-required="You must select yes or no" /> No</label>
					</li>
				</ol>
				<p class="footer-message">Are you ready to change your life?</p>
				<div class="submit">
					<input type="submit" name="submit_program" value="Yes! Take me to the next step" class="btn btn-lg btn-primary" />
				</div>
			<?php echo form_close(); ?>
		</div>
</div>
