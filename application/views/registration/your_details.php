<div class="wrapper registration">
	<div class="inner-content">
		<ul id="registration-nav">
			<li>
				<span class="circle">1</span>
				<span class="label">Payment</span>
			</li>
		    <li>
		    	<span class="circle">2</span>
		    	<span class="label">Pre Exercise</span>
		    </li>
		    <li class="active">
		    	<span class="circle">3</span>
		    	<span class="label">Your Details</span>
		    </li>
		    <li>
		    	<span class="circle">4</span>
		    	<span class="label">Tailor Program</span>
		    </li>
		</ul>
	</div>
</div>
<?php if(!empty($errors)): ?>
	<div class="wrapper registration errors">
		<div class="inner-content">
			<div class="alert alert-danger alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<?php echo $errors; ?>
			</div>
		</div>
	</div>
<?php endif; ?>
<div class="wrapper registration">
	<div class="inner-content header">
		<span class="red-circle">3</span> Your Details
	</div>
	<div class="row inner-content body">
		<?php echo form_open('/registration/your-details', array('role' => 'form', 'class' => 'validate')); ?>
			<div class="col-xs-5 left-column"><!-- Left column -->
				<h4>Personal Details</h4>
				<div class="form-group">
					<label for="password">Password<span>*</span></label>
					<input type="password" name="password" id="password" class="form-control" required data-placement="left" />
				</div>
				<div class="form-group">
					<label for="confirm_password">Confirm Password<span>*</span></label>
					<input type="password" name="confirm_password" id="confirm_password" class="form-control" required data-rule-equalTo="#password" data-msg-equalTo="Your passwords do not match" data-placement="left" />
				</div>
				<div class="form-group">
					<label for="gender">Gender<span>*</span></label>
					<select name="gender" class="form-control" id="gender" required data-placement="left">
					<option></option>
						<?php foreach($this->config->item('genders') as $key => $gender): ?>
							<option value="<?php echo ($key); ?>"<?php echo set_select('gender', ($key)); ?>><?php echo $gender; ?></option>
						<?php endforeach; ?>
					</select>
				</div>
				<div class="form-group">
					<label for="age">Age<span>*</span></label>
					<select name="age" class="form-control" id="age" required data-placement="left">
					<option></option>
						<?php for($x=MIN_AGE; $x<MAX_AGE; $x++): ?>
							<option value="<?php echo $x; ?>" <?php echo set_select('age', $x); ?>><?php echo $x; ?></option>
						<?php endfor; ?>
						<option value="-1" <?php echo set_select('age', '-1'); ?>><?php echo MAX_AGE ."+"; ?></option>
					</select>
				</div>
				<div class="form-group">
					<label for="marital_status">Marital Status</label>
					<select name="marital_status" class="form-control" id="marital_status">
					<option></option>
						<?php foreach($this->config->item('marital_statuses') as $key => $status): ?>
							<option value="<?php echo $key; ?>" <?php echo set_select('marital_status', $key); ?>><?php echo $status; ?></option>
						<?php endforeach; ?>
					</select>
				</div>
				<div class="form-group">
					<label for="number_of_children">Number of Children</label>
					<select name="number_of_children" class="form-control" id="number_of_children">
					<option></option>
						<?php for($x=0; $x < MAX_CHILDREN; $x++): ?>
							<option value="<?php echo $x; ?>" <?php echo set_select('number_of_children', $x); ?>><?php echo $x; ?></option>
						<?php endfor; ?>
						<option value="-1" <?php echo set_select('number_of_children', '-1'); ?>><?php echo MAX_CHILDREN ."+"; ?></option>
					</select>
				</div>
			</div>
			<div class="col-xs-7"><!-- Right column -->
				<h4>Address Details</h4>
				<div class="form-group form-left street-number">
					<label for="street_number">Number<span>*</span></label>
					<input type="text" name="street_number" value="<?php echo set_value('street_number', ''); ?>" id="street_number" class="form-control" required />
				</div>
				<div class="form-group form-left street-name">
					<label for="street_name">Street Name<span>*</span></label>
					<input type="text" name="street_name" id="street_name" value="<?php echo set_value('street_number', ''); ?>" class="form-control" required data-placement="right" />
				</div>
				<div class="form-group">
					<label for="suburb">Suburb / City<span>*</span></label>
					<input type="text" name="suburb" id="suburb" class="form-control" value="<?php echo set_value('suburb', ''); ?>" required data-placement="right" />
				</div>
				<div class="form-group form-left state">
					<label for="state">State<span>*</span></label>
					<select name="state" class="form-control" id="state" required data-placement="bottom">
						<option></option>
						<?php foreach($this->config->item('states') as $key => $state): ?>
							<?php if($key != 'N/A'): ?>
								<option value="<?php echo $key; ?>" <?php echo set_select('state', $key); ?>><?php echo $state['label']; ?></option>
							<?php endif; ?>
						<?php endforeach; ?>
					</select>
				</div>
				<div class="form-group form-left postcode">
					<label for="street_name">Postcode</label>
					<input type="text" name="postcode" id="postcode" class="form-control" value="<?php echo set_value('postcode', ''); ?>" data-placement="right" data-rule-digits />
				</div>
				<div class="form-group form-left country">
					<label for="country">Country<span>*</span></label>
					<select name="country" class="form-control" id="country" required data-placement="bottom">
						<?php foreach($this->config->item('countries') as $key => $country): ?>
							<option value="<?php echo $key; ?>" <?php echo set_select('country', $key); ?>><?php echo $country; ?></option>
						<?php endforeach; ?>
					</select>
				</div>
			</div>
			<div class="col-xs-12 submit-button">
				<input type="submit" name="submit_your_details" class="btn btn-lg btn-primary" value="Cool, lets go to step 3" />
			</div>
		<?php echo form_close(); ?>
	</div>
</div>
