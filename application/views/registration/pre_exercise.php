<div class="wrapper registration">
	<div class="inner-content">
		<ul id="registration-nav">
			<li>
				<span class="circle">1</span>
				<span class="label">Payment</span>
			</li>
		    <li class="active">
		    	<span class="circle">2</span>
		    	<span class="label">Pre Exercise</span>
		    </li>
		    <!-- <li>
		    	<span class="circle">3</span>
		    	<span class="label">Your Details</span>
		    </li>
		    <li>
		    	<span class="circle">4</span>
		    	<span class="label">Tailor Program</span>
		    </li> -->
		</ul>
	</div>
</div>
<?php if(!empty($errors)): ?>
	<div class="wrapper registration errors">
		<div class="inner-content">
			<div class="alert alert-danger alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				You have not answered all the questions in this step. Please answer all questions honestly before proceeding.
			</div>
		</div>
	</div>
<?php endif; ?>
<div class="wrapper registration">
	<div class="inner-content header">
		<span class="red-circle">2</span> Pre Exercise Questions
	</div>
	<div class="row inner-content body">
		<div class="col-xs-12">
			<?php echo form_open('/registration/pre-exercise', array('role' => 'form', 'class' => 'validate', 'id' => 'preex')); ?>
				<ol>
					<li>
						<p>Has your doctor ever told you that you have a heart condition or have you ever suffered a stroke? <span>*</span></p>
						<label class="radio"><input type="radio" name="suffered_stroke" value="2" <?php echo set_radio('suffered_stroke', '2'); ?> class="form-control" data-placement="left" required data-msg-required="You must select yes or no" /> Yes</label>
						<label class="radio"><input type="radio" name="suffered_stroke" value="1" <?php echo set_radio('suffered_stroke', '1'); ?> class="form-control" data-placement="left" required data-msg-required="You must select yes or no" /> No</label>
					</li>
					<li>
						<p>Do you ever experience unexplained pains in your chest at rest or during physical activity/exercise? <span>*</span></p>
						<label class="radio"><input type="radio" name="unexplained_pains" value="2" <?php echo set_radio('unexplained_pains', '2'); ?> class="form-control" data-placement="left" required data-msg-required="You must select yes or no" /> Yes</label>
						<label class="radio"><input type="radio" name="unexplained_pains" value="1" <?php echo set_radio('unexplained_pains', '1'); ?> class="form-control" data-placement="left" required data-msg-required="You must select yes or no" /> No</label>
					</li>
					<li>
						<p>Do you ever feel faint or have spells of dizziness during physical activity/exercise that causes you to lose balance? <span>*</span></p>
						<label class="radio"><input type="radio" name="has_dizziness" value="2" <?php echo set_radio('has_dizziness', '2'); ?> class="form-control" data-placement="left" required data-msg-required="You must select yes or no" /> Yes</label>
						<label class="radio"><input type="radio" name="has_dizziness" value="1" <?php echo set_radio('has_dizziness', '1'); ?> class="form-control" data-placement="left" required data-msg-required="You must select yes or no" /> No</label>
					</li>
					<li>
						<p>Have you had an athsma attack requiring immediate medical attention at any time over the last 12 months? <span>*</span></p>
						<label class="radio"><input type="radio" name="had_asthma" value="2" <?php echo set_radio('had_athsma', '2'); ?> class="form-control" data-placement="left" required data-msg-required="You must select yes or no" /> Yes</label>
						<label class="radio"><input type="radio" name="had_asthma" value="1" <?php echo set_radio('had_athsma', '1'); ?> class="form-control" data-placement="left" required data-msg-required="You must select yes or no" /> No</label>
					</li>
					<li>
						<p>If you have diabetes (type I or type II) have you had trouble controlling your blood glucose in the last 3 months? <span>*</span></p>
						<label class="radio"><input type="radio" name="blood_glucose" value="2" <?php echo set_radio('blood_glucose', '2'); ?> class="form-control" data-placement="left" required data-msg-required="You must select yes or no" /> Yes</label>
						<label class="radio"><input type="radio" name="blood_glucose" value="1" <?php echo set_radio('blood_glucose', '1'); ?> class="form-control" data-placement="left" required data-msg-required="You must select yes or no" /> No</label>
					</li>
					<li>
						<p>Do you have any diagnosed muscle, bone or joint problems that you have been told could be made worse by participating in physical activity/exercise? <span>*</span></p>
						<label class="radio"><input type="radio" name="muscle_problems" value="2" <?php echo set_radio('muscle_problems', '2'); ?> class="form-control" data-placement="left" required data-msg-required="You must select yes or no" /> Yes</label>
						<label class="radio"><input type="radio" name="muscle_problems" value="1" <?php echo set_radio('muscle_problems', '1'); ?> class="form-control" data-placement="left" required data-msg-required="You must select yes or no" /> No</label>
					</li>
					<li>
						<p>Do you have any other medical condition(s) that may make it dangerous for you to participate in physical activity/exercise? <span>*</span></p>
						<label class="radio"><input type="radio" name="any_other_conditions" value="2" <?php echo set_radio('any_other_conditions', '2'); ?> class="form-control" data-placement="left" required data-msg-required="You must select yes or no" /> Yes</label>
						<label class="radio"><input type="radio" name="any_other_conditions" value="1" <?php echo set_radio('any_other_conditions', '1'); ?> class="form-control" data-placement="left" required data-msg-required="You must select yes or no" /> No</label>
					</li>
				</ol>
				<p class="footer-message">Are you ready to change your life?</p>
				<div class="submit">
					<input type="hidden" name="submit_pre_questions" value="1" />
					<input type="button" id="submit_pre_questions" value="Yes! Take me to my dashboard" class="btn btn-lg btn-primary" />
				</div>
			<?php echo form_close(); ?>
		</div>
	</div>
</div>
<div class="modal fade" id="medical_modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<span class="sprite sprite-32 sprite-close"><button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button></span>
				<div class="row">
					<div class="col-xs-12">
						<h3>Medical Confirmation</h3>
					</div>
				</div>
			</div>
			<div class="modal-body">
				<div class="row forms">
					<div class="col-xs-12">
						<p id="medical_text"></p>
						<label><input type="checkbox" id="medical_check" value="1" /> I understand</label>
						<br />&nbsp;
						<div class="submit">
							<input type="button" id="medical_ok" value="Continue" class="btn btn-primary" />
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
