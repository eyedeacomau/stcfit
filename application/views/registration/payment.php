<div class="wrapper registration">
	<div class="inner-content">
		<ul id="registration-nav">
			<li class="active">
				<span class="circle">1</span>
				<span class="label">Payment</span>
			</li>
		    <li>
		    	<span class="circle">2</span>
		    	<span class="label">Pre Exercise</span>
		    </li>
		    <!-- <li>
		    	<span class="circle">3</span>
		    	<span class="label">Your Details</span>
		    </li>
		    <li>
		    	<span class="circle">4</span>
		    	<span class="label">Tailor Program</span>
		    </li> -->
		</ul>
	</div>
</div>
<?php if(!empty($errors)): ?>
	<div class="wrapper registration errors">
		<div class="inner-content">
			<div class="alert alert-danger alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<?php echo $errors; ?>
			</div>
		</div>
	</div>
<?php endif; ?>
<div class="wrapper registration" id="payment">
	<div class="inner-content header">
		<span class="red-circle">1</span> Payment
	</div>
	<div class="row inner-content body">
		<h4>Select your plan</h4>
		<?php echo form_open('/registration/payment', array('role' => 'form', 'class' => 'validate', 'id' => 'payment_form')); ?>
			<input type="hidden" name="payment_method" value="5" />
			<input type="hidden" name="payment_frequency" value="1" <?php echo set_radio('payment_frequency', '1', TRUE); ?> />
			<div class="row payment-method">
				<div class="col-xs-4">
					<center>
						<h3 class="change-price">Fat Loss</h3>
						<p class="label">Combine Resistance<br />and Circut training<br />for beginners</p>
						<input type="radio" name="program_type" id="program_type_1" value="1" <?php echo set_radio('program_type', '1', TRUE); ?> /><br />
						<label class="btn btn-lg btn-primary" for="program_type_1">Select</label><br />&nbsp;
					</center>
				</div>
				<div class="col-xs-4">
					<center>
						<h3 class="change-price">Strength</h3>
						<p class="label per-what">Get strong using<br />weights for<br />beginners &amp;<br />intermediates</p>
						<input type="radio" name="program_type" id="program_type_2" value="2" <?php echo set_radio('program_type', '2', TRUE); ?> /><br />
						<label class="btn btn-lg btn-primary" for="program_type_2">Select</label><br />&nbsp;
					</center>
				</div>
				<div class="col-xs-4">
					<center>
						<h3 class="change-price">Physique</h3>
						<p class="label per-what">Build muscle and<br />or get shredded<br />for beginners to<br />intermediates</p>
						<input type="radio" name="program_type" id="program_type_3" value="3" <?php echo set_radio('program_type', '3', TRUE); ?> /><br />
						<label class="btn btn-lg btn-primary" for="program_type_3">Select</label><br />&nbsp;
					</center>
				</div>
			</div>
			<div class="form-group form-inline">
				<label for="trainer_code">Trainer's Code</label>
				<input type="text" name="trainer_code" id="trainer_code" class="form-control trainer-code" value="<?php echo set_value('trainer_code', ''); ?>" />
				<span class="trainer-name" id="trainer-name"></span>
			</div>
			<div class="form-group">
				<label for="password">Password<span>*</span></label>
				<input type="password" name="password" id="password" class="form-control" required data-placement="left" />
			</div>
			<div class="form-group">
				<label for="confirm_password">Confirm Password<span>*</span></label>
				<input type="password" name="confirm_password" id="confirm_password" class="form-control" required data-rule-equalTo="#password" data-msg-equalTo="Your passwords do not match" data-placement="left" />
			</div>
			<div class="form-group">
				<label for="phone">Phone<span>*</span></label>
				<input type="text" name="phone" id="phone" class="form-control" value="<?php echo set_value('phone', ''); ?>" required data-placement="left" />
			</div>
			<hr />
			<div class="row payment-details">
				<div class="col-xs-6">
					<div class="form-group">
						<label for="card_name">Name on Card<span>*</span></label>
						<input type="text" name="card_name" id="card_name" class="form-control" required data-placement="left" value="<?php echo set_value('card_name', ''); ?>" />
					</div>
					<div class="form-group">
						<label for="card_number">Card Number<span>*</span></label>
						<input type="text" name="card_number" id="card_number" class="form-control" required data-placement="left" data-rule-creditcard data-msg-creditcard="You must enter a valid credit card number" />
					</div>
					<div class="row">
						<div class="col-xs-3 ccv">
							<div class="form-group">
								<label for="card_ccv">CCV<span>*</span></label>
								<input type="text" name="card_ccv" id="card_ccv" class="form-control" required data-placement="left" />
							</div>
						</div>
						<div class="col-xs-9">
							<div class="form-group expiry">
								<label for="expiry_month">Expiry<span>*</span></label>
								<div class="form-inline">
									<select name="expiry_month" id="expiry_month" class="form-control tiny-select" required data-placement="top">
										<?php for($month=0; $month<12; $month++): ?>
											<option value="<?php echo ($month+1); ?>"><?php echo ($month+1); ?></option>
										<?php endfor; ?>
									</select>
									<select name="expiry_year" id="expiry_year" class="form-control tiny-select" required data-placement="top">
										<?php for($month=date('y'); $month<(intval(date('y'))+10); $month++): ?>
											<option value="<?php echo $month; ?>"><?php echo '20'.$month; ?></option>
										<?php endfor; ?>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<input type="checkbox" name="agree" id="agree" value="1" required data-placement="left" /> I understand $79.80 will be charged 4 weeks from sign up date and every four weeks following unless canceled prior
						</div>
					</div>
				</div>
				<div class="col-xs-6">
					<span class="sprite mastercard"></span>
					<span class="sprite visa"></span>
					<span class="sprite ssl">
						Not quite sure about buying online? STC Fit is with you every step of the way. Your purchase is secured by 256bit SSL Certificate.
					</span>
					<span class="eway-block">
						<a href="http://www.eway.com.au/secure-site-seal?i=10&amp;s=3&amp;pid=e3de70ba-e776-441e-a39e-7c0b9b38b7c6" title="eWAY Payment Gateway" target="_blank" rel="nofollow">
							<img alt="eWAY Payment Gateway" src="https://www.eway.com.au/developer/payment-code/verified-seal.ashx?img=10&amp;size=3&amp;pid=e3de70ba-e776-441e-a39e-7c0b9b38b7c6" />
						</a>
					</span>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 submit-button">
					<input type="submit" name="submit_payment_details" class="btn btn-lg btn-primary" id="payment_submit" value="Complete signup now!" />
				</div>
			</div>
		<?php echo form_close(); ?>
	</div>
</div>
<script>
	var methods = <?php echo json_encode($payment_methods)?>;
</script>
