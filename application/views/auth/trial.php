<?php if(!empty($errors)): ?>
	<div class="wrapper subpage errors">
		<div class="inner-content">
			<div class="alert alert-danger alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<?php echo $errors; ?>
			</div>
		</div>
	</div>
<?php endif; ?>
<div class="wrapper subpage" id="trial-form">
	<div class="inner-content row">
        <div class="col-xs-6 login">
        	<h4>7 Day Free Trial - Signup Now</h4>
			<?php echo form_open("/trial", array('role' => 'form', 'class' => 'validate', 'autocomplete' => 'off')); ?>
				<div class="form-group">
					<label class="placeholder" for="modal_trial_name">Name</label>
					<input type="text" name="name" id="modal_trial_name" class="form-control" required />
				</div>
					<div class="form-group">
						<label class="placeholder" for="modal_trial_phone">Phone</label>
						<input type="tel" name="phone" id="modal_trial_phone" class="form-control" required />
					</div>
				<div class="form-group">
					<label class="placeholder" for="modal_trial_email">Email Address</label>
					<input type="email" name="email" data-placement="top" id="modal_trial_email" class="form-control" required data-rule-email="true" data-msg-email="Please enter a valid email address" />
				</div>
				<div class="checkbox">
					<label>
						<input type="checkbox" value="1" name="marketing" id="marketing"> I agree to receiving marketing.
					</label>
				</div>
				<input type="submit" name="trial" class="btn btn-primary" value="Sign Up" required />
			<?php echo form_close(); ?>
        </div>
	</div>
</div>
