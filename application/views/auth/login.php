<?php if(!empty($errors)): ?>
	<div class="wrapper subpage errors">
		<div class="inner-content">
			<div class="alert alert-danger alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<?php echo $errors; ?>
			</div>
		</div>
	</div>
<?php endif; ?>
<div class="wrapper subpage" id="login-form">
	<div class="inner-content row">
        <div class="col-xs-6 login">
        	<h4>Login Here:</h4>
            <?php echo form_open("/login", array('role' => 'form', 'class' => 'validate', 'autocomplete' => 'off')); ?>
                <div class="form-group">
                    <label for="login_email">Email Address</label>
                    <input type="text" name="email" data-placement="top" id="login_email" class="form-control" required data-rule-email="true" data-msg-email="Please enter a valid email address" />
                </div>
                <div class="form-group">
                    <label for="login_password">Password</label>
                    <input type="password" data-placement="left" name="password" id="login_password" class="form-control" required data-rule-minlength="<?=$this->config->item('password_min_length', 'tank_auth');?>" maxlength="<?=$this->config->item('password_max_length', 'tank_auth');?>" />
                </div>
                <a href="/forgot-password" class="forgot-password">Forgot my password</a>
                <input type="submit" name="login" class="btn btn-lg btn-primary login-submit" value="Login" required />
            <?php echo form_close(); ?>
        </div>
        <div class="col-xs-6 signup">
			<h4>Signup for STC Fit now!</h4>
	        <?php echo form_open("/registration", array('role' => 'form', 'class' => 'validate', 'autocomplete' => 'off')); ?>
	            <div class="form-group">
	                <label for="signup_name">Name</label>
	                <input type="text" name="name" id="signup_name" class="form-control" value="<?php echo set_value('name', ''); ?>" required />
	            </div>
	            <div class="form-group">
	                <label for="signup_email">Email Address</label>
	                <input type="text" data-placement="right" name="email" id="signup_email" value="<?php echo set_value('email', ''); ?>" class="form-control" required data-rule-email="true" data-msg-email="Please enter a valid email address" />
	            </div>
	            <div class="checkbox">
	                <label>
	                    <input type="checkbox" data-placement="bottom" value="1" name="terms" id="terms" <?php echo set_checkbox('terms', '1'); ?> required /> I agree to the terms and conditions.
	                </label>
	            </div>
	            <div class="checkbox">
	                <label>
	                    <input type="checkbox" name="marketing" id="marketing" value="1" <?php echo set_checkbox('marketing', '1', false); ?>> I agree to receiving marketing.
	                </label>
	            </div>
	            <input type="submit" id="register_login" name="register" class="btn btn-lg btn-primary registration-submit" value="Register Now!" />
	        <?php echo form_close(); ?>
	    </div>
	</div>
</div>