<?php if(!empty($errors)): ?>
	<div class="wrapper registration errors">
		<div class="inner-content">
			<div class="alert alert-danger alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<?php echo $errors; ?>
			</div>
		</div>
	</div>
<?php endif; ?>
<div class="wrapper subpage forgot-password">
	<div class="inner-content row">
		<div class="col-xs-6">
			<?php echo form_open($this->uri->uri_string(), array('role' => 'form')); ?>
				<label for="login">Email Address</label>
				<input type="text" name="login" id="login" value="<?php echo set_value('login', ''); ?>" class="form-control" />
				<input type="submit" name="reset" value="Get a new password" class="btn btn-primary" />
			<?php echo form_close(); ?>	
		</div>
		<div class="col-xs-6">
			<p class="registration-message">Don't have an account? No problem! To register with STC Fit now, <br /><a href="/registration">click here</a></p>
		</div>
	</div>
</div>