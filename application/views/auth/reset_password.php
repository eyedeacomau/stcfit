<?php if(!empty($errors)): ?>
	<div class="wrapper registration errors">
		<div class="inner-content">
			<div class="alert alert-danger alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<?php echo $errors; ?>
			</div>
		</div>
	</div>
<?php endif; ?>
<div class="wrapper subpage forgot-password">
	<div class="inner-content row">
		<div class="col-xs-6">
			<?php echo form_open($this->uri->uri_string(), array('role' => 'form')); ?>
				<div class="form-group">
					<label for="new_password">New Password</label>
					<input type="password" name="new_password" id="new_password" value="<?php echo set_value('new_password', ''); ?>" class="form-control" maxlength="<?php echo $this->config->item('password_max_length', 'tank_auth'); ?>" />
				</div>
				<div class="form-group">
					<label for="confirm_new_password">Confirm New Password</label>
					<input type="password" name="confirm_new_password" id="confirm_new_password" value="<?php echo set_value('confirm_new_password', ''); ?>" class="form-control" maxlength="<?php echo $this->config->item('password_max_length', 'tank_auth'); ?>" />
				</div>
				<input type="submit" name="change" value="Change Password" class="btn btn-primary" />
			<?php echo form_close(); ?>	
		</div>
		<div class="col-xs-6">
			<p class="registration-message">Don't have an account? No problem! To register with STC Fit now, <br /><a href="/registration">click here</a></p>
		</div>
	</div>
</div>