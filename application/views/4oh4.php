<div class="wrapper" id="four-oh-four">
	<div class="inner-content">
		<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			404 Error - The page that you have requested does not exist.
		</div>
	</div>
</div>