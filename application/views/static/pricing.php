<div class="wrapper" id="quotes">
	<div class="inner-content">
		<ul>
			<li><a class="btn btn-primary" href="/contact-us">Enquire Now</a></li>
		</ul>
	</div>
</div>
<div id="pricing" class="static-wrapper">
	<div class="inner-content">
	<h1 class="page-heading">Pricing</h1>
	<?php if($this->tank_auth->is_logged_in() == false): ?>
		<a class="btn btn-primary" id="top-right-pricing" href="/contact-us">Enquire Now</a>
	<?php endif; ?>
		<div class="row" id="sell-points">
			<div class="col-xs-4 point">
				<span class="bubble">
					<img src="/assets/images/bubble-1.png" />
				</span>
				<h3>Daily nutrition</h3>
				<span class="point-image">
					<img src="/assets/images/pricing-1.png" />
				</span>
				<ul class="points-list">
					<li>Flexible meal plans</li>
					<li>Familiar, simple tracking</li>
					<li>Suits all commitment levels</li>
					<li>Video instructions</li>
				</ul>
			</div>
			<div class="col-xs-4 point">
				<span class="bubble">
					<img src="/assets/images/bubble-2.png" />
				</span>
				<h3>Training goals</h3>
				<span class="point-image">
					<img src="/assets/images/pricing-2.png" />
				</span>
				<ul class="points-list">
					<li>Weight loss programs</li>
					<li>Physique and strength</li>
					<li>Achieve your goals</li>
					<li>Balanced training</li>
				</ul>
			</div>
			<div class="col-xs-4 point">
				<span class="bubble">
					<img src="/assets/images/bubble-3.png" />
				</span>
				<h3>Track your progress</h3>
				<span class="point-image">
					<img src="/assets/images/pricing-3.png" />
				</span>
				<ul class="points-list">
					<li>See your results</li>
					<li>Monitor your progress</li>
					<li>Professional guidance</li>
					<li>Track your training</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="wrapper gradient-background" id="pricing-expanded">
	<div class="drop-shadow"></div>
	<div class="inner-content row">
		<div class="col-xs-7">
			<ul class="expanded-list">
				<li>Monthly training and program update<span>$?</span></li>
				<li>Video exercise library access<span>$?</span></li>
				<li>Personal contact with your qualified PT<span>$?</span></li>
				<li>Phased nutrition plan to suit your lifestyle<span>$?</span></li>
				<li>Monthly Body Fat calculation and progress tracking<span>$?</span></li>
				<li>Results interpretation from your PT<span>$?</span></li>
				<li>Workout progress tracking<span>$?</span></li>
				<li>Nutrition and food plan tracking<span>$?</span></li>
				<li>Accountability to your PT and much, much more<span>$?</span></li>
				<li><strong>Total Value<span>$?</span></strong></li>
			</ul>
		</div>
		<div class="col-xs-5">
			<div class="pricing-box">
				<?php if($this->tank_auth->is_logged_in() == false): ?>
					<a class="btn btn-primary btn-lg" href="/contact-us">Enquire Now</a>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>
