<div class="wrapper" id="quotes">
	<div class="inner-content">
		<ul>
			<li><a class="btn btn-primary" href="/contact-us">Enquire Now</a></li>
		</ul>
	</div>
</div>
<div id="results" class="static-wrapper">
	<div class="inner-content">
		<h1 class="page-heading">Results</h1>
		<h1 class="text-center">Fat Loss</h1>
		<div class="row">
			<div class="col-xs-6">
				<p>Jackie started with STCfit at 107kg's, since then she’s lost over 25% body fat and now weighs 85kgs. Along the way Jackie has completed the gruelling "Tough Mudder" with her daughter.</p>
				<p><i>"I have battled with my weight for over 30 years. Living off of 800 calories a day and not exercising was not working for me. STCfit has been life changing. I've dropped many clothing sizes and have more energy than ever before. I am a mum of 3 teenagers and I've never been healthier or fitter."</i></p>
			</div>
			<div class="col-xs-6 image">
				<img src="/assets/images/results-1.jpg" class="col-xs-12"/>
				<div class="col-xs-6 text-center">Before</div>
				<div class="col-xs-6 text-center">After</div>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-6 image">
				<img src="/assets/images/results-2.jpg" class="col-xs-12"/>
				<div class="col-xs-6 text-center">Before</div>
				<div class="col-xs-6 text-center">After</div>
			</div>
			<div class="col-xs-6">
				<p>This is Carl, a truck driver who at the age of 15 was already over 100kgs. After years of living an unhealthy lifestyle, Carl came to STCfit ready to change his life. As well as quitting smoking, he lost over 10% body fat in just 12 weeks. Since then he’s managed to gain over 8kg’s of muscle.</p>
				<p><i>"I came to STCfit wanting to change my life, and I certainly have. I've dropped over 10% body fat and built lots of muscle along the way, I eat more than I ever have and really enjoy the training. The biggest part of STCfit is the confidence it has given me outside of the gym. I'm a much stronger and happier person than ever before."</i></p>
			</div>
		</div>

		<h1 class="text-center">Athletic Performance</h1>

		<div class="row">
			<div class="col-xs-6">
				<p>Victoria Came to STCfit not fully knowing what she would like to do with her training. After a few workouts she was hooked! Not only did she drastically improve her body composition, posture, strength and fitness, she is now studying to be a personal trainer herself.</p>
				<p><i>"I feel like I gained a new life after training with STCfit. I realised a lot of things about how the body responds to the stresses you put it under. I am now looking to further the knowledge I started by training to become a PT myself. I love my body and what it is capable of now - I think that says a lot. Thank you STCfit."</i></p>
			</div>
			<div class="col-xs-6 image">
				<img src="/assets/images/results-3.jpg" class="col-xs-12"/>
				<div class="col-xs-6 text-center">Before</div>
				<div class="col-xs-6 text-center">After</div>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-6 image">
				<img src="/assets/images/results-4.jpg" class="col-xs-12"/>
				<div class="col-xs-6 text-center">Before</div>
				<div class="col-xs-6 text-center">After</div>
			</div>
			<div class="col-xs-6">
				<p>Justin, your typical country guy, loves his footy and loves a beer. He came to STCfit as he could not attend his teams pre-season training. He lost 30cms and 6.5kgs, most importantly he improved his fitness test time by 25%!</p>
				<p><i>"I found STCfit's training was always tailored to my fitness and capabilities and I was often pushed beyond what I thought possible. I yielded awesome results in a short space of time. STCfit gave very professional advice, along with great training and nutrition programs. I was able to stay motivated and challenged and was as fit as I'd been in a long time for the football season."</i></p>
			</div>
		</div>

		<h1 class="text-center">Strength/Physique Development</h1>

		<div class="row">
			<div class="col-xs-6">
				<p>Ben start at age 23 weighed just 63kg’s, tired of being the skinny kid, Ben began STCfit’s Physique Development Program. In just 12 months ben gained 12kgs of lean muscle while maintaining the same amount of body fat.</p>
				<p><i>"Training with the STCfit Physique principles I was able to prove to myself that it wasn’t impossible for me to build muscle. I’m confident I’ll continue to grow each month and hopefully eventually achieve my goal of stepping on stage."</i></p>
			</div>
			<div class="col-xs-6 image">
				<img src="/assets/images/results-5.jpg" class="col-xs-12"/>
				<div class="col-xs-6 text-center">Before</div>
				<div class="col-xs-6 text-center">After</div>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-6 image">
				<img src="/assets/images/results-6.jpg" class="col-xs-12"/>
				<div class="col-xs-6 text-center">Before</div>
				<div class="col-xs-6 text-center">After</div>
			</div>
			<div class="col-xs-6">
				<p>Rhys came to us, a young fresh PT looking to put on size. In the first 6 weeks he dropped 6kgs of body fat. Since then he's put on almost 8kgs of lean muscle and trained up to become a STCfit coach himself.</p>
				<p><i>"Education has been the biggest thing for me, gaining the knowledge to not just go through movement but to use my muscles to their fullest. That and the noticeable physique changes along the way has made it something I look forward to doing more of."</i></p>
			</div>
		</div>
	</div>
</div>
