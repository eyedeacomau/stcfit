<div id="quotes" class="wrapper">
	<div class="inner-content">
		<ul>
			<li>7 Day Free Trial &mdash; No Obligatons <a class="btn btn-primary trial" data-toggle="modal" data-target="#trial_modal">Try It Now</a></li>
		</ul>
	</div>
</div>
<div id="weight_loss" class="static-wrapper">
	<div class="inner-content">
		<h1 class="page-heading">Weight Loss and Fat Reduction</h1>

		<div class="row">
			<div class="col-xs-6">



If you're sick of trying 12 week diets or the 6 week weight loss plans or just fed up of being told just drink 3 shakes a day?

We know these approaches don’t work long term. In fact they often make things worse in the long run. The secret to STCfit, is we teach you a system that works with your lifestyle and allows you to enjoy long term success through <strong>Personalised Nutrition & Training along with a very Effective Workout & Exercise Plan.</strong>

              <br> <br> <strong>STCfit is an Australian owned personal training solution. Our goal is to go above and beyond what is expected from an online program. </strong>


<h3>Results Matter</h3>

Jackie started with STCfit at 107kg's, since then she’s lost over 25% body fat and now weighs 85kgs. Along the way Jackie has completed the gruelling "Tough Mudder" with her daughter.

"I have battled with my weight for over 30 years. Living off of 800 calories a day and not exercising was not working for me. STCfit has been life changing. I've dropped many clothing sizes and have more energy than ever before. I am a mum of 3 teenagers and I've never been healthier or fitter."



			</div>
			<div class="col-xs-6 image">
				<img src="/assets/images/results-1.jpg" class="col-xs-12"/>
				<div class="col-xs-6 text-center">Before</div>
				<div class="col-xs-6 text-center">After</div>
			</div>








		</div>



		<div class="row">
			<div class="col-xs-12">
				<span class="all-for-just" style="
width: 100%;
display: block;
text-align: center;
margin: 0px 0px 20px 0px;
font-size: 45px;
">All this for just</span>
			</div>
		</div>

        <div id="payment-box">
			<div class="row payment-method">
				<div class="col-xs-6">
					<h1 class="change-price">$19.95 <span class="aud">AUD</span></h1>
					<p class="label per-what">weekly</p>
					<div class="button-area">
													<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#login_modal">Signup now</button>
											</div>
				</div>
				<div class="col-xs-6">
					<h1>$829.40 <span class="aud">AUD</span></h1>
					<p class="label">Per Year</p>
					<p class="label">($15.95 per week)</p>
					<div class="button-area">
													<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#login_modal">Signup now</button>
											</div>
				</div>
			</div>
		</div>
	</div>
</div>
