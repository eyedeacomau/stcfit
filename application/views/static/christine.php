<div id="quotes" class="wrapper">
	<div class="inner-content">
		<ul>
			<li>7 Day Free Trial &mdash; No Obligatons <a class="btn btn-primary trial" data-toggle="modal" data-target="#trial_modal">Try It Now</a></li>
		</ul>
	</div>
</div>
<div class="static-wrapper" id="how-it-works">
	<div class="inner-content">
		<h1 class="page-heading">How It Works</h1>
		<div class="row hiw-page">
			<div class="col-xs-6 center-content">
				<h2 class="section-header">Training</h2>
				<a href="#expand" data-large-img="/assets/images/training-screen-large.jpg" class="image-area">
					<div class="background-image"><img src="/assets/images/training-screen.jpg" /></div>
					<div class="bubble-image"><img src="/assets/images/bubble-2.png" /></div>
				</a>
				<div class="informational-area">
					<ul class="work-points">
						<li>3 goal dependent training programs (Fat Loss, Performance, Strength/Physique Development)</li>
						<li>Updated programs every 4 weeks</li>
						<li>Coached workout videos</li>
						<li>Exercise library videos</li>
						<li>Descriptions including benefits, execution and modifiers</li>
					</ul>
				</div>
			</div>
			<div class="col-xs-6 center-content">
				<h2 class="section-header">Nutrition</h2>
				<a href="#expand" data-large-img="/assets/images/nutrition-screen-large.jpg" class="image-area">
					<div class="background-image"><img src="/assets/images/nutrition-screen.jpg" /></div>
					<div class="bubble-image"><img src="/assets/images/bubble-1.png" /></div>
				</a>
				<div class="informational-area">
					<ul class="work-points">
						<li>3 Lifestyle dependent nutrition plans</li>
						<li>Video and text explanations of all meal plans</li>
						<li>Simple tracking method</li>
						<li>Reviewable by your trainer</li>
						<li>Recording frequency as decided upon by you and your trainer</li>
					</ul>
				</div>
			</div>
		</div>
		<hr class="hiw-rule" />
		<div class="row hiw-page">
			<div class="col-xs-6 center-content">
				<h2 class="section-header">Messaging</h2>
				<a href="#expand" data-large-img="/assets/images/messaging-screen-large.jpg" class="image-area">
					<div class="background-image"><img src="/assets/images/messaging-screen.jpg" /></div>
					<div class="bubble-image"><img src="/assets/images/bubble-4.png" /></div>
				</a>
				<div class="informational-area">
					<ul class="work-points">
						<li>Easy to use internal messaging system</li>
						<li>Message your trainer any time</li>
						<li>Get a response within one business day</li>
						<li>Build a relationship with your trainer</li>
						<li>Tackle obstacles and celebrate success as a team</li>
					</ul>
				</div>
			</div>
			<div class="col-xs-6 center-content">
				<h2 class="section-header">Progress</h2>
				<a href="#expand" data-large-img="/assets/images/progress-screen-large.jpg" class="image-area">
					<div class="background-image"><img src="/assets/images/progress-screen.jpg" /></div>
					<div class="bubble-image"><img src="/assets/images/bubble-3.png" /></div>
				</a>
				<div class="informational-area">
					<ul class="work-points">
						<li>Track using weight, girth measurements and body fat %</li>
						<li>Professional interpretation of results</li>
						<li>Personalised changes to training and nutrition based on results</li>
						<li>Optional progress photo gallery</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="wrapper gradient-background" id="pricing-expanded">
	<div class="drop-shadow"></div>
	<div class="inner-content row">
		<div class="col-xs-12">
			<div class="pricing-box">
				<h3>All this for just</h3>
				<span class="the-price">$19.95</span>
				<span class="little-text">per week</span>
				<?php if($this->tank_auth->is_logged_in() == false): ?>
					<button type="button" class="btn btn-primary btn-lg" id="bottom-pricing" data-toggle="modal" data-target="#login_modal">Signup</button>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="picture_view" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<div class="loading-animation">
					<div class="bubblingG">
						<span id="bubblingG_1"></span>
						<span id="bubblingG_2"></span>
						<span id="bubblingG_3"></span>
					</div>
				</div>
				<div class="img-content">
					<img />
				</div>
			</div>
		</div>
	</div>
</div>
