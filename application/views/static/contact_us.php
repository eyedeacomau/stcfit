<script src='https://www.google.com/recaptcha/api.js'></script>
<div id="contact_us" class="wrapper">
	<div class="inner-content">
		<h1 class="page-heading">Contact Us</h1>

		<?php if(!empty($messages)): ?>
			<div class="message-area alert alert-info alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<?php echo $messages; ?>
			</div>
		<?php endif; ?>
		<?php if(!empty($errors)): ?>
			<div class="message-area alert alert-danger alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<?php echo $errors; ?>
			</div>
		<?php endif; ?>
		<?php if(!empty($success_messages)): ?>
			<div class="message-area alert alert-success alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<?php echo $success_messages; ?>
			</div>
		<?php endif; ?>
		<div class="contact-form">
			<h4>For any enquiries e-mail us on <a class="quick-phase" href="mailto:<?php echo SUPPORT_EMAIL;?>"><?php echo SUPPORT_EMAIL;?></a>, or fill out form below.</h4>
			<?php echo form_open('/contact-us', array('role' => 'form', 'class' => 'form-horizontal validate')); ?>
				<div class="form-group">
					<label for="name">Name</label>
					<input type="text" name="name" id="name" class="form-control" required data-placement="right" value="<?php echo $name;?>"/>
				</div>
				<div class="form-group">
					<label for="email">Email</label>
					<input type="text" name="email" id="email" class="form-control" required data-placement="right" data-rule-email value="<?php echo $email;?>"/>
				</div>
				<div class="form-group">
					<label for="enquiry">Enquiry</label>
					<textarea name="enquiry" id="enquiry" class="form-control" rows="5" required data-placement="right"><?php echo $enquiry;?></textarea>
				</div>
				<div class="form-group">
					<div class="g-recaptcha" data-sitekey="6LcC8VIUAAAAAD82R1XYhRJm4CnCXlkh2VRI3Cw9"></div>
				</div>
				<div class="form-group submit-button">
					<input type="submit" name="send_enquiry" class="btn btn-primary btn-lg pull-right" value="Send Message" />
				</div>
			<?php echo form_close(); ?>
		</div>
	</div>
</div>
