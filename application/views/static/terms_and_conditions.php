<div id="terms_and_conditions" class="static-wrapper gradient-background">
	<div class="static">
		<h1 class="page-heading">STCfit.com Privacy Policy</h1>

		<h2>Introduction</h2>

		<p>STCfit regards customer privacy as an important part of our relationship with our customers. The following privacy policy applies to all STCfit users, and conforms to Internet privacy standards.</p>

		<p>If you have questions or concerns regarding this statement, you should first contact STCfit:</p>
		<dl class="dl-horizontal">
			<dt>STCfit Pty Ltd ACN</dt>
			<dd> 629 942 034</dd>
			<dt>Trading as STCfit ABN</dt>
			<dd>97 691 210 488</dd>
			<dt>Registered address</dt>
			<dd>71A Wyndham Street</dd>
			<dd>Shepparton Vic. 3630</dd>
			<dd>Australia</dd>
			<dt>Phone</dt>
			<dd>0401092257</dd>
			<dt>Phone</dt>
			<dd>0434542047</dd>
		</dl>

		<h2>Disclaimer</h2>
		<p>The information presented in this website, is by no way intended as medical advice or as a substitute for medical counselling. The information should be used in conjunction with the guidance and care of your physician. Consult your physician before beginning this program, as you would with any exercise and nutrition program. If you choose not to obtain the consent of your physician and/or work with your physician throughout the duration of your time using the recommendations in the program, you are agreeing to accept full responsibility for your actions. By continuing with the program you recognize that despite all precautions on the part of STCfit, there are risks of injury or illness which can occur because of your use of the aforementioned information and you expressly assume such risks and waive, relinquish and release any claim, which you may have against STCfit or its affiliates, as a result of any future physical injury or illness incurred in connection with, or as a result of, the use or misuse of the program.</p>

		<h2>Collection of Information</h2>
		<p>In order to use the STCfit website, we may require information from you in order to provide the best service possible.</p>
		<p>All correspondence may also be collected and stored, particularly in regard to sales, support and accounts, including Email.</p>
		<p>Any information collected by STCfit is collected via correspondence from you or your company. This may be via the telephone, Email, mail, fax or directly through our website.</p>

		<h2>Use of Collection Information</h2>
		<p>Any details collected from STCfit customers are required in order to provide you with our products and/or services, and a high level of customer service.</p>
		<p>Correspondence is recorded in order to provide service references, and to assist in our staff development.</p>


		<h3>Storage of Collected Information</h3>
		<p>The security of your personal information is important to us. When you enter sensitive information (such as credit card numbers) on our website, we encrypt that information using secure socket layer technology (SSL). When Credit Card details are collected, we simply pass them on in order to be processed as required. We never permanently store complete Credit Card details.</p>
		<p>We follow generally accepted industry standards to protect the personal information submitted to us, both during transmission and once we receive it.</p>
		<p>If you have any questions about security on our Website, you can email us at <a href="mailto:support@stcfit.com">support@stcfit.com</a>.</p>

		<h3>Access to Collected Information</h3>
		<p>If your personally identifiable information changes, or if you no longer desire our service, you may correct, update, delete or deactivate it by emailing us at <a href="mailto:support@stcfit.com">support@stcfit.com</a>.</p>

		<h2>Orders</h2>
		<p>If you purchase a product or service from us, we may request certain personally identifiable information from you. You may be required to provide contact information (such as name, Email, and postal address) and financial information (such as credit card number, expiration date).</p>
		<p>We use this information for billing purposes and to fill your orders. If we have trouble processing an order, we will use this information to contact you.</p>

		<h2>Communications</h2>
		<p>STCfit uses personally identifiable information for essential communications, such as Emails, accounts information, and critical service details. We may also use this information for other purposes, including some promotional Emails. If at any time a customer wishes not to receive such correspondence, they can request to be removed from any mailing lists by emailing us at <a href="mailto:support@stcfit.com">support@stcfit.com</a>.</p>
		<p>You will be notified when your personal information is collected by any third party that is not our agent/service provider, so you can make an informed choice as to whether or not to share your information with that party.</p>

		<h2>Third Parties</h2>
		<p>STCfit may at its discretion use other third parties to provide essential services on our site or for our business processes. We may share your details as necessary for the third party to provide that service.</p>
		<p>These third parties are prohibited from using your personally identifiable information for any other purpose.</p>
		<p>STCfit does not share any information with third parties for any unknown or unrelated uses.</p>

		<h2>Legal</h2>
		<p>We reserve the right to disclose your personally identifiable information as required by law and when we believe that disclosure is necessary to protect our rights and/or comply with a judicial proceeding, court order, or legal process served on our Website.</p>

		<h2>Links</h2>
		<p>Links on the STCfit site to external entities are not covered within this policy. The terms and conditions set out in this privacy statement only cover the domain name of STCfit.com.</p>

		<h2>Changes to Privacy Policy</h2>
		<p>If we decide to change our privacy policy, we will post those changes to this privacy statement, the homepage, and other places we deem appropriate so that you are aware of what information we collect, how we use it, and under what circumstances, if any, we disclose it. We reserve the right to modify this privacy statement at any time, so please review it frequently. If we make material changes to this policy, we will notify you here, by Email, or by means of a notice on our homepage.</p>

		<h2>STCfit Security Policy</h2>
		<p>STCfit uses the eWAY Payment Gateway for its online credit card transactions.</p>
		<p>eWAY processes online credit card transactions for thousands of Australian merchants, providing a safe and secure means of collecting payments via the Internet.</p>
		<p>All online credit card transactions performed on this site using the eWAY gateway are secured payments.</p>
		<ul>
			<li>Payments are fully automated with an immediate response.</li>
			<li>Your complete credit card number cannot be viewed by STCfit or any outside party.</li>
			<li>All transactions are performed under 128 Bit SSL Certificate.</li>
			<li>All transaction data is encrypted for storage within eWAY's bank-grade data centre, further protecting your credit card data.</li>
			<li>eWAY is an authorised third party processor for all the major Australian banks.</li>
			<li>eWAY at no time touches your funds; all monies are directly transferred from your credit card to the merchant account held by STCfit.</li>
		</ul>
		<p>For more information about eWAY and online credit card payments, please visit<br/>
		<a href="http://www.eway.com.au/"><strong>www.eWAY.com.au</strong></a></p>

		<h2>Delivery Policy</h2>

		<h3>Physical goods</h3>
		<p>After ordering online, you will receive an email confirmation from eWAY containing your order details (if you have provided your email address). We will normally confirm receipt of your order within a few minutes of ordering. We will attempt to send your goods via Australia Post within 3 working days; however if goods are unavailable delivery will take a little longer.</p>
		<p>If you wish to query a delivery please contact us at <a href="mailto:support@stcfit.com">support@stcfit.com</a></p>

		<h3>Digital Delivery</h3>
		<p>After ordering online, you will receive an email confirmation from eWAY containing your order details (if you have provided your email address). We will normally confirm receipt of your order within a few minutes of ordering. We will attempt to send your software/license/access code via email within 60 minutes.</p>
		<p>If you wish to query a delivery please contact us at <a href="mailto:support@stcfit.com">support@stcfit.com</a>.</p>

		<h2>Refund &amp; Returns Policy</h2>
		<p>If for any reason you are not completely satisfied with your purchase we will give you a 7 day money-back guarantee from the time you receive the goods. Please email us at <a href="mailto:support@stcfit.com">support@stcfit.com</a>, within that time, if you are not satisfied with your purchase, so that we can resolve any problems.</p>
		<p>This refund policy does not apply to goods which have been worn or used, damaged after delivery, or if any attempt has been made to alter the product or if they have been dropped or broken. All products must be returned in their original condition. All postage and insurance costs are to be paid by the buyer. We recommend that you return the product via Registered post and that you pre pay all postage. You assume any risk of lost, theft or damaged goods during transit. STCfit therefore advise you take out shipment registration of insurance with your postal carrier. STCfit will not be responsible for parcels lost or damaged in transit if you choose not to insure.</p>
	</div>
</div>
