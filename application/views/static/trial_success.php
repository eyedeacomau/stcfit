<div id="contact_us" class="static-wrapper">
	<div class="static">
		<div class="message-area alert alert-success alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			Your 7 Day Free Trial signup has successfully been sent to STC Fit. A staff member will respond shortly.
		</div>
	</div>
</div>
