<?php if(!empty($success_messages)): ?>
	<div class="message-area area-small alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $success_messages; ?>
	</div>
<?php endif; ?>
<?php if(!empty($messages)): ?>
	<div class="message-area area-small alert alert-info alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $messages; ?>
	</div>
<?php endif; ?>
<?php if(!empty($errors)): ?>
	<div class="message-area area-small alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $errors; ?>
	</div>
<?php endif; ?>
<div class="section row" id="program_management">
	<?php echo form_open_multipart('/admin/program-management', array('role' => 'form', 'class' => 'custom_validation')); ?>
		<div class="select-group">
			<div class="select-container">
				<select name="management_type" class="chzn" id="management_type">
					<option value="training">Training</option>
					<option value="food">Food</option>
				</select>
			</div>
		</div>
		<div id="training" class="program">
			<div class="selection">
				<?php if(!empty($training_types)): ?>
					<div class="select-group">
						<div class="select-container">
							<select name="training_type" id="training_type" class="chzn" data-placeholder="Select training type">
								<option></option>
								<?php foreach($training_types as $type): ?>
									<option value="<?php echo $type['id']; ?>"><?php echo $type['name']; ?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
				<?php endif; ?>
			</div>
			<div class="session_exercises">
				
			</div>
		</div>
		<div id="food" class="program" style="display:none;">
			<?php if(!empty($food_plans)): ?>
				<?php foreach($food_plans as $food_plan): ?>
					<input type="hidden" name="id[<?php echo $food_plan['id']; ?>]" value="<?php echo $food_plan['id']; ?>" />
					<div class="food-plan">
						<div class="form-group">
							<label>Plan name:</label>
							<input type="text" name="name[<?php echo $food_plan['id']; ?>]" class="form-control" value="<?php echo $food_plan['title']; ?>" />
						</div>
						<div class="form-group">
							<label>Required Entries:</label>
							<input type="text" name="entries[<?php echo $food_plan['id']; ?>]" class="form-control" value="<?php echo $food_plan['entries']; ?>" />
						</div>
						<div class="ck-editor-container">
							<?php echo $this->ckeditor->editor("description[".$food_plan['id']."]", $food_plan['description']); ?>
						</div>
						<div class="nice-file" data-filename="<?php echo (strlen($food_plan['document_path']) > 20 ? '...'.substr($food_plan['document_path'], (strlen($food_plan['document_path'])-20),strlen($food_plan['document_path'])):$food_plan['document_path']); ?>" data-show-preview="yes" data-imageno="1">
							<input type="hidden" name="old_file[<?php echo $food_plan['id']; ?>]" value="<?php echo $food_plan['document_path']; ?>" />
 							<input type="file" name="files[<?php echo $food_plan['id']; ?>]" />
  						</div>
					</div>
				<?php endforeach; ?>
				<div class="food-submit">
					<input type="submit" name="submit_food_plans" value="Save food plans" class="btn btn-primary btn-lg" />
				</div>
			<?php else: ?>
				<div class="message-area area-small alert alert-info alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					No food plans to display.
				</div>
			<?php endif; ?>
		</div>
	<?php echo form_close(); ?>
</div>
<div class="modal fade" id="add_edit" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			
		</div>
	</div>
</div>