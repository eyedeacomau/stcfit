<div class="section row" id="reports">
	<div class="chart-container">
		<div id="report_container"></div>
	</div>
	<div class="filters">
		<?php echo form_open('/admin/reports', array('role' => 'form')); ?>
			<div class="s-container">
				<select name="filter[type]" class="chzn" data-placeholder="Graph Type">
					<option></option>
					<option value="revenue" <?php echo (!empty($filters['type']) && $filters['type'] == 'revenue' ? 'selected="selected"':''); ?>>Revenue</option>
					<option value="signups" <?php echo (!empty($filters['type']) && $filters['type'] == 'signups' ? 'selected="selected"':''); ?>>Signups</option>
					<option value="users" <?php echo (!empty($filters['type']) && $filters['type'] == 'users' ? 'selected="selected"':''); ?>>Users</option>
				</select>
			</div>
			<div class="s-container">
				<select name="filter[trainer]" class="chzn" data-placeholder="All Trainers">
					<option></option>
					<?php if(!empty($trainers)): ?>
						<?php foreach($trainers as $trainer): ?>
							<option value="<?php echo $trainer['user_id']; ?>" <?php echo (!empty($filters['trainer']) && $filters['trainer'] == $trainer['user_id'] ? 'selected="selected"':''); ?>><?php echo $trainer['first_name'].' '.$trainer['last_name']; ?></option>
						<?php endforeach; ?>
					<?php endif; ?>
				</select>
			</div>
			<div class="s-container date">
				<div class="form-group">
					<label for="date_from" class="placeholder">Date From</label>
					<div class="input-group mini-input">
						<input type="text" name="filter[date_from]" id="date_from" class="form-control datepicker" value="<?php echo (!empty($filters['date_from']) ? $filters['date_from']:''); ?>" />
						<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
					</div>
				</div>
			</div>
			<div class="s-container date">
				<div class="form-group">
					<label for="date_to" class="placeholder">Date To</label>
					<div class="input-group mini-input">
						<input type="text" name="filter[date_to]" id="date_to" class="form-control datepicker" value="<?php echo (!empty($filters['date_to']) ? $filters['date_to']:''); ?>" />
						<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
					</div>
				</div>
			</div>
			<div class="s-container">
				<select name="filter[program_type]" class="chzn" data-placeholder="Program Type">
					<option></option>
					<?php if(!empty($training_types)): ?>
						<?php foreach($training_types as $type): ?>
							<option value="<?php echo $type['id']; ?>" <?php echo (!empty($filters['program_type']) && $filters['program_type'] == $type['id'] ? 'selected="selected"':''); ?>><?php echo $type['name']; ?></option>
						<?php endforeach; ?>
					<?php endif; ?>
				</select>
			</div>
			<div class="s-container">
				<select name="filter[gender]" class="chzn" data-placeholder="Gender">
					<option></option>
					<?php if(!empty($training_types)): ?>
						<?php foreach($genders as $id => $gender): ?>
							<option value="<?php echo $id; ?>" <<?php echo (!empty($filters['gender']) && $filters['gender'] == $id ? 'selected="selected"':''); ?>><?php echo $gender; ?></option>
						<?php endforeach; ?>
					<?php endif; ?>
				</select>
			</div>
			<div class="s-container">
				<div class="form-group">
					<label for="age_from" class="placeholder">Age From</label>
					<input type="text" id="age_from" name="filter[age_from]" class="form-control" value="<?php echo (!empty($filters['age_from']) ? $filters['age_from']:''); ?>" />
				</div>
			</div>
			<div class="s-container">
				<div class="form-group">
					<label for="age_to" class="placeholder">Age To</label>
					<input type="text" id="age_to" name="filter[age_to]" class="form-control" value="<?php echo (!empty($filters['age_to']) ? $filters['age_to']:''); ?>" />
				</div>
			</div>
			<div class="s-container">
				<select name="filter[marital_status]" class="chzn" data-placeholder="Marital Status">
					<option></option>
					<?php if(!empty($marital)): ?>
						<?php foreach($marital as $id => $status): ?>
							<option value="<?php echo $id; ?>" <?php echo (!empty($filters['marital_status']) && $filters['marital_status'] == $status ? 'selected="selected"':''); ?>><?php echo $status; ?></option>
						<?php endforeach; ?>
					<?php endif; ?>
				</select>
			</div>
			<div class="s-container">
				<div class="form-group">
					<label for="children" class="placeholder"># of children</label>
					<input type="text" id="children" name="filter[children]" class="form-control" value="<?php echo (!empty($filters['children']) ? $filters['children']:''); ?>" />
				</div>
			</div>
			<div class="s-container">
				<select name="filter[display]" class="chzn" data-placeholder="Display Type">
					<option></option>
					<option value="days" <?php echo (!empty($filters['display']) && $filters['display'] == 'days' ? 'selected="selected"':''); ?>>Day to month</option>
					<option value="months" <?php echo (!empty($filters['display']) && $filters['display'] == 'months' ? 'selected="selected"':''); ?>>Month to year</option>
				</select>
			</div>
			<div class="s-container">
				<div class="form-group check">
					<label>
						<input type="checkbox" name="export" value="1" /> Export Data?
					</label>
				</div>
			</div>
			<div class="submit-filters">
				<input type="submit" name="submit_filters" class="btn btn-primary" value="Filter!" />
			</div>
		<?php echo form_close(); ?>
	</div>
</div>
<script>
	var cats = <?php echo (!empty($reports['data']) ? json_encode(array_keys($reports['data'])):'[]'); ?>;
	var points = <?php echo (!empty($reports['data']) ? json_encode(array_values($reports['data'])):'[]'); ?>;
	var show_labels = <?php echo (!empty($reports['show_labels']) ? 'true':'false')?>;
	var data_prefix = <?php echo (!empty($reports['prefix']) ? '"'.$reports['prefix'].'"':'""'); ?>;
	var graph_title = <?php echo (!empty($reports['graph_title']) ? '"'.$reports['graph_title'].'"':'""')?>;
	var decimal_places = <?php echo (!empty($reports['decimal_places']) ? $reports['decimal_places']:'0')?>;
	var line_colour = <?php echo (!empty($reports['line_colour']) ? '"'.$reports['line_colour'].'"':'"#000000"')?>;
</script>