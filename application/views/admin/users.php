<?php if(!empty($success_messages)): ?>
	<div class="message-area area-small alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $success_messages; ?>
	</div>
<?php endif; ?>
<?php if(!empty($messages)): ?>
	<div class="message-area area-small alert alert-info alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $messages; ?>
	</div>
<?php endif; ?>
<?php if(!empty($errors)): ?>
	<div class="message-area area-small alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $errors; ?>
	</div>
<?php endif; ?>
<?php echo form_open('/admin/users', array('role' => 'form', 'method' => 'post')); ?>
	<div class="form-inline user-search">
		<input type="hidden" name="tab" id="tab" value="trainers" />
		<div class="search-form form-group float-form">
			<label for="search_params" class="placeholder">Search Here...</label>
			<input type="text" class="form-control" name="search_params" id="search_params" value="<?php echo (!empty($search_params) ? $search_params:''); ?>" />
			<input type="submit" name="user_search" class="btn btn-primary" value="Search" />
		</div>
		<a href="#create_trainer" data-toggle="modal" data-target="#create_trainer" class="btn float-submit btn-primary">Create Trainer</a>
		<input type="submit" name="export_users" value="Export Users" class="btn float-submit btn-primary" />
	</div>
<?php echo form_close(); ?>
<div id="clients">
	<ul class="nav nav-tabs">
		<li class="<?php echo (empty($tab) || $tab == 'trainers' ? 'active':'') ?>"><a href="#trainers" data-tab-name="trainers" data-toggle="tab">Trainers</a></li>
		<li class="<?php echo (!empty($tab) && $tab == 'users' ? 'active':'') ?>"><a href="#users" data-tab-name="users" data-toggle="tab">Users<?php echo (!empty($new_users) ? '<span class="counter">'.$new_users.'</span>':''); ?></a></li>
		<li class="<?php echo (!empty($tab) && $tab == 'failed_medical' ? 'active':'') ?>"><a href="#failed_medical" data-tab-name="failed_medical" data-toggle="tab">Failed Medicals</a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane <?php echo (empty($tab) || $tab == 'trainers' ? 'active':'') ?>" id="trainers">
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<?php if(!empty($users['trainers'])): ?>
					<tr>
						<th>Signup Date</th>
						<th>Name</th>
						<th>Phone</th>
						<th>Email</th>
						<th>
							T A L
						</th>
						<th colspan="2"><!-- Options --></th>
					</tr>
					<?php foreach($users['trainers'] as $trainer): ?>
						<tr>
							<td><?php echo date('d/m/Y', $trainer['signup_date']); ?></td>
							<td><?php echo $trainer['first_name'].' '.$trainer['last_name']; ?></td>
							<td><?php echo (!empty($trainer['contact']) ? $trainer['contact']:'N/A'); ?></td>
							<td><?php echo $trainer['email']; ?></td>
							<td>
								<table border="0" cellspacing="0" cellpadding="0" class="inner-table">
									<tr>
										<td><?php echo $trainer['total_clients'] ?></td>
										<td><?php echo $trainer['recent_clients'] ?></td>
										<td><?php echo $trainer['lost_clients'] ?></td>
									</tr>
								</table>								
							</td>
							<td align="right">
								<?php if(!empty($trainer['is_suspended'])): ?>
									<a href="/admin/reactivate-user/<?php echo $trainer['user_id']; ?>" class="user-option ask-first" data-message="Are you sure that you would like to reactivate this trainer?">Re-Activate</a>
								<?php else: ?>
									<a href="/admin/trainer-login/<?php echo $trainer['user_id']; ?>" class="user-option">Login as user</a>
									<a href="/admin/suspend-user/<?php echo $trainer['user_id']; ?>" class="user-option ask-first" data-message="Are you sure that you would like to suspend this trainer? Their clients will be permanently assigned to other trainers.">Suspend</a>
								<?php endif; ?>
							</td>
						</tr>
					<?php endforeach; ?>
				<?php else: ?>
				<div class="message-area inner-message area-small alert alert-info alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					There are no trainers to display.
				</div>
				<?php endif; ?>
			</table>
		</div>
		<div class="tab-pane <?php echo (!empty($tab) && $tab == 'users' ? 'active':'') ?>" id="users">
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<?php if(!empty($users['clients'])): ?>
					<tr>
						<th>Signup Date</th>
						<th>Name</th>
						<th>Plan Type</th>
						<th>Referral</th>
						<th>Trainer</th>
						<th colspan="2"><a href="/admin/users?tab=users&amp;arr_by_trainer=1" class="btn btn-primary float-right">Arrange by trainer</a></th>
					</tr>
					<?php foreach($users['clients'] as $user): ?>
						<tr <?php echo (empty($user['allocated_trainer']) ? 'class="red-row"':''); ?>>
							<td><?php echo date('d/m/Y', $user['signup_date']); ?></td>
							<td><?php echo $user['first_name'].' '.$user['last_name']; ?></td>
							<td><?php echo (!empty($user['training_type']) ? $user['training_type']:'N/A'); ?></td>
							<td><?php echo (!empty($user['refer']) ? $user['refer']:'N/A'); ?></td>
							<td>
								<select class="form-control change_trainer" data-user-id="<?php echo $user['user_id']; ?>">
									<option>Select trainer</option>
									<?php if(!empty($users['user_trainers'])): ?>
										<?php foreach($users['user_trainers'] as $trainer): ?>
											<?php if(empty($trainer['is_suspended'])): ?>
												<option value="<?php echo $trainer['user_id']; ?>" <?php echo(!empty($user['allocated_trainer']) && $user['allocated_trainer'] == $trainer['user_id'] ? 'selected="selected"':''); ?>><?php echo $trainer['first_name'].' '.$trainer['last_name']; ?></option>
											<?php endif; ?>
										<?php endforeach; ?>
									<?php endif; ?>
								</select>
							</td>
							<td align="right">
								<?php if(!empty($user['is_suspended'])): ?>
									<a href="/admin/reactivate-user/<?php echo $user['user_id']; ?>" class="user-option ask-first" data-message="Are you sure that you would like to reactivate this user?">Re-Activate</a>
								<?php elseif($user['paid_period_ends'] < time()): ?>
									<a href="/admin/paid-user/<?php echo $user['user_id']; ?>" class="user-option ask-first" data-message="Are you sure that you would like to manually mark this user paid?">Paid</a>
								<?php else: ?>
									<a href="/admin/user-login/<?php echo $user['user_id']; ?>" class="user-option">Login as user</a>
									<a href="/admin/suspend-user/<?php echo $user['user_id']; ?>" class="user-option ask-first" data-message="Are you sure that you would like to suspend this user?">Suspend</a>
								<?php endif; ?>
								<?php if($user['free'] == 1): ?>
									<a href="/admin/make-user-paid/<?php echo $user['user_id']; ?>" class="user-option ask-first" data-message="Are you sure you want to bill this user?">Make Paid</a>
								<?php else: ?>
									<a href="/admin/make-user-free/<?php echo $user['user_id']; ?>" class="user-option ask-first" data-message="Are you sure you want to make this user free?">Make Free</a>
								<?php endif; ?>
							</td>
						</tr>
					<?php endforeach; ?>
				<?php else: ?>
					<div class="message-area inner-message area-small alert alert-info alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						There are no users to display.
					</div>
				<?php endif; ?>
			</table>
		</div>
		<div class="tab-pane <?php echo (empty($tab) && $tab == 'failed_medical' ? 'active':'') ?>" id="failed_medical">
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<?php if(!empty($users['failed_medical'])): ?>
					<tr>
						<th>Signup Date</th>
						<th>Name</th>
						<th>Email</th>
						<th colspan="2"><!-- Options --></th>
					</tr>
					<?php foreach($users['failed_medical'] as $user): ?>
						<tr>
							<td><?php echo date('d/m/Y', $user['signup_started']); ?></td>
							<td><?php echo $user['first_name'].' '.$user['last_name']; ?></td>
							<td><?php echo $user['email']; ?></td>
							<td align="right">
								<a href="/admin/override-medical/<?php echo $user['profile_id']; ?>" class="user-option ask-first" data-message="Are you sure that you would like to override the failed medical for this user?">Override failure</a>
							</td>
						</tr>
					<?php endforeach; ?>
				<?php else: ?>
				<div class="message-area inner-message area-small alert alert-info alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					There are no failed medicals to display.
				</div>
				<?php endif; ?>
			</table>
		</div>
	</div>
</div>
<div id="create_trainer" class="modal fade">
	<div class="modal-dialog modal-lg">
		<?php echo form_open("/admin/users", array('role' => 'form', 'class' => 'modal-content validate', 'autocomplete' => 'off')); ?>
			<div class="modal-header gradient-background white-text">
				<span class="sprite sprite-32 sprite-close"><button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button></span>
				<h4 class="modal-title">Create Trainer</h4>
			</div>
			<div class="modal-body">
                <div class="form-group">
					<label class="placeholder" for="trainer_first_name">First Name</label>
					<input type="text" data-placement="right" name="trainer_first_name" id="trainer_first_name" class="form-control" value="<?=set_value('trainer_first_name');?>" required />
				</div>
				<div class="form-group">
					<label class="placeholder" for="trainer_last_name">Last Name</label>
					<input type="text" data-placement="right" name="trainer_last_name" id="trainer_last_name" class="form-control" value="<?=set_value('trainer_last_name');?>" required />
				</div>
                <div class="form-group">
                    <label class="placeholder" for="trainer_email">Email Address</label>
                    <input type="text" data-placement="right" name="trainer_email" id="trainer_email" class="form-control"  value="<?=set_value('trainer_email');?>" required data-rule-email="true" data-msg-email="Please enter a valid email address" />
                </div>
                <div class="form-group">
                    <label class="placeholder" for="trainer_contact">Phone Number</label>
                    <input type="text" data-placement="right" name="trainer_contact" id="trainer_contact" class="form-control"  value="<?=set_value('trainer_contact');?>" required />
                </div>
                <div class="form-group">
					<label class="placeholder" for="trainer_password">Password</label>
					<input type="password" data-placement="right" name="trainer_password" id="trainer_password" class="form-control" value="<?=set_value('trainer_password');?>" required data-rule-minlength="<?=$this->config->item('password_min_length', 'tank_auth');?>" maxlength="<?=$this->config->item('password_max_length', 'tank_auth');?>" />
				</div>
				<div class="form-group">
					<label class="placeholder" for="trainer_confirm_password">Confirm Password</label>
					<input type="password" data-placement="right" name="trainer_confirm_password" id="trainer_confirm_password" class="form-control" value="<?=set_value('trainer_confirm_password');?>" required data-rule-equalto="#trainer_password" data-msg-equalto="This must match the password above" data-rule-minlength="<?=$this->config->item('password_min_length', 'tank_auth');?>" maxlength="<?=$this->config->item('password_max_length', 'tank_auth');?>" />
				</div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary" name="trainer_create" id="trainer_create" value="trainer_create">Create Trainer</button>
				<!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
			</div>
		<?php echo form_close(); ?><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->