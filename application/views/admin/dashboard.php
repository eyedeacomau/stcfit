<div class="section row dashboard-graphs">
	<div class="left-graphs col-xs-6">
		<div id="revenue_graph">

		</div>
		<div class="trainer-counts">
		<?php if(!empty($trainer_data)): ?>
			<table border="1" cellpadding="0" cellspacing="0">
				<tr>
					<th>Trainer</th>
					<th>Active Members</th>
					<th>Lost Members</th>
					<th>New Members</th>
				</tr>
				<?php foreach($trainer_data as $trainer): ?>
					<tr>
						<td><?php echo $trainer['first_name'].' '.$trainer['last_name']; ?></td>
						<td><?php echo $trainer['all_time_active']; ?></td>
						<td><?php echo array_sum($trainer['month_data']['lost']['data']); ?></td>
						<td><?php echo $trainer['new_users']; ?></td>
					</tr>
				<?php endforeach; ?>
			</table>
		<?php endif; ?>
	</div>
	</div>
	<div class="right-graphs col-xs-6">
		<div class="legends">
			<span class="legend total">
				<span></span> Total Clients
			</span>
			<span class="legend active">
				<span></span> Lost Clients
			</span>
			<span class="legend lost">
				<span></span> Active Clients
			</span>
		</div>
		<?php if(!empty($trainer_data)): ?>
			<?php foreach($trainer_data as $trainer_id => $trainer): ?>
				<div id="trainer_<?php echo $trainer_id; ?>" class="trainer-graph"></div>
			<?php endforeach; ?>
		<?php else:?>
			<div class="message-area area-small alert alert-info alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				No trainer data to display
			</div>
		<?php endif; ?>
	</div>
</div>
<script>
	var cats = <?php echo json_encode($revenue['revenue_months']); ?>;
	var points = <?php echo json_encode($revenue['revenue_data']); ?>;
	var trainer_graphs = [];
	<?php if(!empty($trainer_data)): ?>
		<?php foreach($trainer_data as $trainer_id => $trainer): ?>
			trainer_<?php echo $trainer_id; ?> = {
				graph_id: 'trainer_<?php echo $trainer_id; ?>',
				cats: <?php echo json_encode(array_values($trainer['cats'])); ?>,
				plots: <?php echo json_encode(array_values($trainer['month_data'])); ?>,
				trainer_name: "<?php echo $trainer['first_name'].' '.$trainer['last_name'] ?>",
				trainer_age: "<?php echo $trainer['trainer_created']; ?>"
			};

			trainer_graphs.push(trainer_<?php echo $trainer_id; ?>);
		<?php endforeach; ?>
	<?php endif; ?>
</script>
