<div class="page-wrapper portal-login">
	<div class="inner-content">
		<div class="portal-logo"><img src="/assets/images/portal-logo.png" /></div>
		<h2>Admin Portal</h2>
		<div class="login-container">
			<?php if(!empty($errors)): ?>
				<div class="message-area area-small alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<?php echo $errors; ?>
				</div>
			<?php endif; ?>
			<div class="login-header">Login</div>
			<div class="login-body">
				<?php echo form_open('/login', array('role' => 'form', 'class' => 'validate')); ?>
					<input type="hidden" name="page" value="admin" />
					<div class="row">
						<div class="input-column form-group">
							<label for="email">Email<span>*</span></label>
							<input type="email" name="email" id="email" class="form-control" required />
						</div>
						<div class="input-column form-group">
							<label for="password">Password<span>*</span></label>
							<input type="password" name="password" id="password" class="form-control" required />
						</div>
						<div class="submit-column">
							<input type="submit" name="login_submit" class="btn btn-primary" value="Login" />
						</div>
					</div>
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>
</div>