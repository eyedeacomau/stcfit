<?php if(!empty($success_messages)): ?>
	<div class="message-area area-small alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $success_messages; ?>
	</div>
<?php endif; ?>
<?php if(!empty($messages)): ?>
	<div class="message-area area-small alert alert-info alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $messages; ?>
	</div>
<?php endif; ?>
<?php if(!empty($errors)): ?>
	<div class="message-area area-small alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $errors; ?>
	</div>
<?php endif; ?>
<div class="section" id="exercise_library">
	<div class="row">
		<div class="select-group">
			<div class="select-container">
				<select name="type" id="ex_type" class="chzn">
					<option value="1">Exercises</option>
					<option value="2">Circuits</option>
				</select>
			</div>
		</div>
		<div class="select-group">
			<input type="hidden" name="exercise_type" value="1" id="exercise_type" />
			<div class="select-container" data-type="1" id="exercises">
				<select name="exercises" class="chzn" data-placeholder="Select exercise">
					<option></option>
					<?php if(!empty($exercises)): ?>
						<?php foreach($exercises as $exercise): ?>
							<option value="<?php echo $exercise['table_key']; ?>"><?php echo $exercise['exercise_name']; ?></option>
						<?php endforeach; ?>
					<?php endif;?>
				</select>
			</div>
			<div class="select-container" style="display:none;" data-type="2" id="circuits">
				<select name="circuits" class="chzn" data-placeholder="Select circuit">
					<option></option>
					<?php if(!empty($circuits)): ?>
						<?php foreach($circuits as $circuits): ?>
							<option value="<?php echo $circuits['table_key']; ?>"><?php echo $circuits['exercise_name']; ?></option>
						<?php endforeach; ?>
					<?php endif;?>
				</select>
			</div>
			<div class="group-options">
				<a href="#EDIT" class="exercise-option" data-option="edit"><span>E</span>Edit</a>
				<a href="#DELETE" class="exercise-option" data-option="delete"><span><strong>-</strong></span>Delete</a>
				<a href="#ADD-NEW" class="exercise-option" data-option="add_new"><span><strong>+</strong></span>Add New</a>
			</div>
		</div>
	</div>
	<div id="edit-area"></div>
</div>