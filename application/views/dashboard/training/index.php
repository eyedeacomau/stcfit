<?php if(!empty($current_phase)): ?>
	<div class="black-label">
		<?php echo $current_phase['type_name']; ?> - Phase <?php echo $current_phase['phase_number'] ?>: Training
	</div>
<?php endif; ?>
<?php if(!empty($end_phase)):
	$end_phase = $end_phase['end_phase']; ?>
	<div class="black-label">
		<div class="btn-group" role="group">
			<?php
			for($i = 1;$i<=$end_phase;$i++){
				if($i<=$persist_current_phase['phase_number']){
					$link = '/training/index/'.$i;
					$class = 'btn-default';
				} else {
					$link = '#';
					$class = 'btn-inverse';
				}
				if($i==$current_phase['phase_number']){
					$class='btn-primary';
				}
				echo '<a class="btn '.$class.'" href="'.$link.'">'.$i.'</a>';
			}
			?>
		</div>
	</div>
<?php endif; ?>
<?php if(!empty($success_messages)): ?>
	<div class="message-area area-small alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $success_messages; ?>
	</div>
<?php endif; ?>
<?php if(!empty($errors)): ?>
	<div class="message-area area-small alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $errors; ?>
	</div>
<?php endif; ?>
<div class="row training-session">
<?php if(!empty($current_phase)): ?>
	<div class="col-xs-12 session is-phase">
		<div class="training-video">
			<?php if(!empty($current_phase['video_link'])): ?>
				<iframe width="330" height="205" src="//www.youtube.com/embed/<?php echo $current_phase['video_link']; ?>" frameborder="0" allowfullscreen></iframe>
			<?php else: ?>
				<img src="/assets/images/video-placeholder.jpg" alt="No video found" title="No video found" />
			<?php endif; ?>
		</div>
		<div class="info-block">
			<h4><?php echo $current_phase['name'] ?></h4>
			<p><?php echo $current_phase['description'] ?></p>
		</div>
	</div>
	<?php $count = 0; foreach($phase_sessions as $session): ?>
		<div class="col-xs-12 session">
			<?php if(isset($session['session_complete'])): ?>
				<span class="status complete">Status: <strong>Complete</strong></span>
			<?php else: ?>
				<span class="status incomplete">Status: <strong>Incomplete</strong></span>
			<?php endif; ?>
			<div class="training-video">
				<?php if(!empty($session['video_link'])): ?>
					<iframe width="330" height="205" src="//www.youtube.com/embed/<?php echo $session['video_link']; ?>" frameborder="0" allowfullscreen></iframe>
				<?php else: ?>
					<img src="/assets/images/video-placeholder.jpg" alt="No video found" title="No video found" />
				<?php endif; ?>
			</div>
			<div class="info-block">
				<h4>Session <?php echo ($count+1); ?></h4>
				<span class="name"><strong>Name: </strong><?php echo $session['name'] ?></span>
				<p><?php echo $session['description'] ?></p>
			</div>
			<a href="/training/session/<?php echo $session['session_identifier']?>" class="btn btn-primary enter-session"><?php echo (!empty($session['completed']) ? 'Re-':'')?>Enter Session</a>
		</div>
	<?php $count++; endforeach; ?>
<?php endif; ?>
</div>
