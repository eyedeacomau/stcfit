<?php if(!empty($phase)): ?>
	<div class="black-label">
		<?php echo $phase['type_name']; ?> - Phase <?php echo $phase['phase_number']; ?>: Training Entry
	</div>
<?php endif; ?>
<div class="row training-session">
<?php if(!empty($session)): ?>
	<div class="col-xs-12 session is-phase">
		<div class="training-video">
			<?php if(!empty($session['video_link'])): ?>
				<iframe width="330" height="205" src="//www.youtube.com/embed/<?php echo $session['video_link']; ?>" frameborder="0" allowfullscreen></iframe>
			<?php else: ?>
				<img src="/assets/images/video-placeholder.jpg" alt="No video found" title="No video found" />
			<?php endif; ?>
		</div>
		<div class="info-block session-entry">
			<h4>Session <?php echo $session['session_number']; ?></h4>
			<span class="name"><strong>Name: </strong><?php echo $session['name'] ?></span>
			<p><?php echo $session['description']; ?></p>
		</div>
	</div>
<?php endif; ?>
</div>
<?php if(!empty($success_messages)): ?>
	<div class="message-area area-small alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $success_messages; ?>
	</div>
<?php endif; ?>
<div id="print_content">
	<div id="training">
		<a href="#print" class="btn btn-primary print-button">Printer friendly version</a> 
		<?php echo form_open('/training/session/'.$session_identifier, array('role' => 'form')); ?>
			<?php if(!empty($exercises)): ?>
				<h3>Exercises</h3>
				<div class="input-grid">
					<table border="0" cellpadding="0" width="100%" class="mother">
						<tr>
							<th>Exercises</th>
							<th class="sets">Sets</th>
							<th class="sets">Reps</th>
							<th class="sets">Tempo</th>
							<?php for($x=0;$x<ENTRIES_PER_SESSION;$x++): ?>
								<th colspan="2">Entry <?php echo $x+1; ?></th>
							<?php endfor; ?>
						</tr>
						<tr class="sub-header">
							<td colspan="4" class="gap"></td>
							<?php for($x=0;$x<ENTRIES_PER_SESSION;$x++): ?>
								<td class="rep-weight">Kg</td>
								<td class="rep-weight">Reps</td>
							<?php endfor; ?>
						</tr>
						<?php foreach($exercises as $exercise): ?>
							<tr>
								<td class="exercise-name"><a href="#open-explanation" class="get_exercise" title="Get Explanation" data-exerciseid="<?php echo $exercise['exercise_id']; ?>"><span class="sprite sprite-questionmark"></span></a><span class="exercise-name"><?php echo $exercise['exercise_name']; ?></span></td>
								<td class="sets"><?php echo $exercise['exercise_sets']; ?></td>
								<td class="sets"><?php echo $exercise['exercise_reps']; ?></td>
								<td class="sets"><?php echo $exercise['exercise_tempo']; ?></td>
								<?php for($entry=0;$entry<ENTRIES_PER_SESSION;$entry++): ?>
									<td colspan="2" class="full">
										<table width="100%">
											<?php for($set=0;$set<$exercise['exercise_sets']; $set++): ?>
												<tr>
													<td><input type="text" name="entry<?php echo "[".$exercise['entry_id']."][data][".($entry+1)."][".($set+1)."]"; ?>[reps]" value="<?php echo (!empty($exercise['exercise_data'][($entry+1)][($set+1)]['reps']) ? $exercise['exercise_data'][($entry+1)][($set+1)]['reps']:''); ?>" /></td>
													<td><input type="text" name="entry<?php echo "[".$exercise['entry_id']."][data][".($entry+1)."][".($set+1)."]"; ?>[weight]" value="<?php echo (!empty($exercise['exercise_data'][($entry+1)][($set+1)]['weight']) ? $exercise['exercise_data'][($entry+1)][($set+1)]['weight']:''); ?>" /></td>
												</tr>
											<?php endfor; ?>
										</table>
									</td>
								<?php endfor; ?>
							</tr>
						<?php endforeach; ?>
					</table>
				</div>
			<?php endif; ?>
			<?php if(!empty($circuits)): ?>
				<h3>Circuits</h3>
				<div class="circuit-grid">
					<?php foreach($circuits as $circuit): ?>
						<table border="0" cellpadding="0" width="100%" class="circuit">
							<tr>
								<th class="name">Name</th>
								<th class="exercises" colspan="3">Exercises</th>
							</tr>
							<tr class="description">
								<td valign="top"><a href="#open-explanation" class="get_exercise" title="Get Explanation" data-exerciseid="<?php echo $circuit['exercise_identifier']; ?>"><span class="sprite sprite-questionmark"></span></a><span class="exercise-name"><?php echo $circuit['exercise_name']; ?></span></td>
								<td valign="top" colspan="3"><?php echo nl2br($circuit['exercise_description']); ?></td>
							</tr>
							<tr>
								<td colspan="4">
								<?php for($entry=0; $entry<ENTRIES_PER_SESSION;$entry++): ?>
									<div class="circuit-entry">
										<div class="entry-header">Entry <?php echo ($entry+1); ?></div>
										<div class="entry-data">
											<textarea name="circuit_entry[<?php echo $circuit['entry_id']; ?>][<?php echo ($entry+1); ?>][data]"><?php echo (!empty($circuit['exercise_data'][($entry+1)]['data']) ? $circuit['exercise_data'][($entry+1)]['data']:''); ?></textarea>
										</div>
									</div>
								<?php endfor; ?>
								</td>
							</tr>
						</table>
					<?php endforeach; ?>
				</div>
			<?php endif; ?>
			<div class="row">
				<div class="col-xs-12">
					<input type="submit" name="update_session" class="btn btn-primary btn-lg btn-right" value="Update" />
				</div>
			</div>
		<?php echo form_close(); ?>
	</div>
</div>