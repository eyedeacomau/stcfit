<div class="black-label">
	<?php echo $phase['training_type'] ?> - Phase <?php echo $phase['phase_number']; ?>: Food &amp; Nutrition Entry
	<a href="/food/change-plan" class="btn btn-primary btn-right">Change Plan</a>
</div>
<?php if(!empty($success_messages)): ?>
	<div class="message-area area-small alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $success_messages; ?>
	</div>
<?php endif; ?>
<?php if(!empty($errors)): ?>
	<div class="message-area area-small alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $errors; ?>
	</div>
<?php endif; ?>
<div class="row food-entry" id="food">
	<?php echo form_open('/food/entry', array('role' => 'form', 'class' => 'validate')); ?>
		<div class="col-xs-6 left-goals">
			<h3>Your Goals</h3>
			<input type="hidden" name="plan_id" id="plan_id" value="<?php echo $user['food_plan']; ?>" />
			<?php if(!empty($goals) && count($goals) > 1): ?>
				<select name="goal_number" id="goal_number" class="form-control">
					<?php foreach($goals as $goal): ?>
						<option value="<?php echo $goal['goal_number']; ?>" <?php echo (!empty($goal_number) && $goal_number == $goal['goal_number'] ? ' selected="selected"':''); ?>>Week <?php echo $goal['goal_number']; ?></option>
					<?php endforeach; ?>
				</select>
			<?php elseif(!empty($goals) && count($goals) < 2): ?>	
				<input type="hidden" name="goal_number" id="goal_number" value="<?php echo $goals[0]['goal_number']; ?>" />
			<?php endif; ?>
			<?php for($x=0; $x<count($goals); $x++): ?>
				<div class="goal_group_container" id="goals_<?php echo $goals[$x]['goal_number'] ?>" style="<?php echo ($x != 0 ? 'display:none;':''); ?>">
					<div class="goal-group">
						<span>Calories</span>
						<span><?php echo (!empty($goals[$x]['calories']) ? $goals[$x]['calories']:''); ?></span>
					</div>
					<div class="goal-group">
						<span>Protein</span>
						<span><?php echo (!empty($goals[$x]['protein']) ? $goals[$x]['protein']:''); ?>g</span>
					</div>
					<div class="goal-group">
						<span>Carbohydrates</span>
						<span><?php echo (!empty($goals[$x]['carbs']) ? $goals[$x]['carbs']:''); ?>g</span>
					</div>
					<div class="goal-group">
						<span>Sugars</span>
						<span><?php echo (!empty($goals[$x]['sugars']) ? $goals[$x]['sugars']:''); ?>g</span>
					</div>
					<div class="goal-group">
						<span>Fats</span>
						<span><?php echo (!empty($goals[$x]['fats']) ? $goals[$x]['fats']:''); ?>g</span>
					</div>
				</div>
			<?php endfor; ?>
		</div>
		<div class="col-xs-6 right-goals labels">
			<h3>Your Input</h3>
			<div class="food-entry-group">
				<div class="form-group">
					<label for="calories">Calories</label>
					<div class="input-group-container">
						<div class="input-group">
							<input type="text" class="form-control" name="calories" id="calories" value="<?php echo set_value('calories',''); ?>" data-placement="right" data-rule-digits />
							<span class="input-group-addon cals">cals</span>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="protein">Protein</label>
					<div class="input-group-container">
						<div class="input-group">
							<input type="text" class="form-control" name="protein" id="protein" value="<?php echo set_value('protein',''); ?>" data-placement="right" data-rule-digits />
							<span class="input-group-addon grams">grams</span>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="carbohydrates">Carbohydrates</label>
					<div class="input-group-container">
						<div class="input-group">
							<input type="text" class="form-control" name="carbohydrates" id="carbohydrates" value="<?php echo set_value('carbohydrates',''); ?>" data-placement="right" data-rule-digits />
							<span class="input-group-addon grams">grams</span>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="sugars">Sugars</label>
					<div class="input-group-container">
						<div class="input-group">
							<input type="text" class="form-control" name="sugars" id="sugars" value="<?php echo set_value('sugars',''); ?>" data-placement="right" data-rule-digits />
							<span class="input-group-addon grams">grams</span>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="fats">Fats</label>
					<div class="input-group-container">
						<div class="input-group">
							<input type="text" class="form-control" name="fats" id="fats" value="<?php echo set_value('fats',''); ?>" data-placement="right" data-rule-digits />
							<span class="input-group-addon grams">grams</span>
						</div>
					</div>
				</div>
			</div>
			<div class="row submit small-right">
				<div class="col-xs-12">
					<input type="submit" name="input_submit" class="btn btn-primary" value="Save input" />
				</div>
			</div>
		</div>
	<?php echo form_close(); ?>
	<div id="previous_food_entries">
		
	</div>
</div>