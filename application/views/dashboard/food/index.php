<?php if(!empty($food_plans)): ?>
	<div class="black-label">
		<?php echo $phase['type_name'] ?> - Phase <?php echo $phase['phase_number']; ?>: Food &amp; Nutrition
	</div>
	<?php echo form_open('/food', array('role' => 'form', 'class' => 'validate')); ?>
		<div id="food">
		<?php if(!empty($errors)): ?>
			<div class="message-area alert alert-danger alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<?php echo $errors; ?>
			</div>
		<?php endif; ?>
			<div class="row food-info">
				<?php $count=0; foreach($food_plans as $plan): ?>
					<div class="col-xs-4">
						<div class="plan-header"><?php echo $plan['title']; ?></div>
						<div class="plan-info">
							<?php echo $plan['description']; ?>
						</div>
						<div class="plan-options">
							<a href="<?php echo FOOD_PLAN_DOC_PATH.$plan['document_path'] ?>" target="_blank" class="btn btn-primary">Download this plan</a>
							<input type="radio" name="selected_plan" id="plan_<?php echo $plan['id']; ?>" class="hidden-radio" value="<?php echo $plan['id']; ?>"<?php echo (!empty($user['food_plan']) && $user['food_plan'] == $plan['id'] || empty($user['food_plan']) && $count == 0 ? ' checked="checked"':''); ?> />
							<label for="plan_<?php echo $plan['id']; ?>" class="btn btn-primary label-radio-button"></label>
						</div>
					</div>
				<?php $count++; endforeach; ?>
			</div>
			<div class="fitness-app">
				<p>To understand what numbers you should put below please download the <span>&lsquo;</span><strong>My Fitness Pal</strong><span>&rsquo;</span> app.</p>
				<a href="https://play.google.com/store/apps/details?id=com.myfitnesspal.android"><img src="/assets/images/google-play.png" alt="Click here to see the app on Google Play" /></a>
				<a href="https://itunes.apple.com/au/app/calorie-counter-diet-tracker/id341232718"><img src="/assets/images/apple-app-store.png" alt="Click here to see the app on Apple" /></a>
			</div>
			<div class="goal-form">
				<h3>Enter your goals here</h3>
				<div id="food_goal_container">
					<?php echo $goals; ?>
				</div>
			</div>
			<div class="row submit">
				<div class="col-xs-12">
					<input type="submit" name="submit_goals" value="Save food plan and goals" class="btn btn-primary btn-lg" />
				</div>
			</div>
		</div>
	<?php echo form_close(); ?>
<?php else: ?>
	<div class="message-area area-small alert alert-info alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		Unable to find any food plans.
	</div>
<?php endif;?>
