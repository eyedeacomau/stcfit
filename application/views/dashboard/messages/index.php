<h2 class="dash-heading">Messages</h2>
<?php if(!empty($messages)): ?>
	<div class="message-area alert alert-info alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $messages; ?>
	</div>
<?php endif; ?>
<?php if(!empty($errors)): ?>
	<div class="message-area alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $errors; ?>
	</div>
<?php endif; ?>
<?php if(!empty($success_messages)): ?>
	<div class="message-area alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $success_messages; ?>
	</div>
<?php endif; ?>
<?php if(!empty($inbox)): ?>
	<div class="message-wrapper">
		<?php foreach($inbox as $message): ?>
			<?php if($message['for_user_id'] == $user['user_id']): ?>
				<div class="message row">
					<div class="message-container">
						<div class="message-from"><?php echo '<strong>'. ucwords($message['trainer']) . '</strong> ' . '('. date('d/m/y \- h:ia', $message['date']) . ')'; ?></div>
						<div class="message-body"><?php echo nl2br($message['message_body']); ?></div>
					</div>
				</div>
			<?php else: ?>
				<div class="message row message-user">
					<div class="message-container">
						<div class="message-from"><strong>Me</strong> (<?php echo date('d/m/y \- h:ia', $message['date']); ?>)</div>
						<div class="message-body"><?php echo nl2br($message['message_body']); ?></div>
					</div>
				</div>
			<?php endif; ?>
		<?php endforeach?>
	</div>
<?php endif; ?>
<?php echo form_open('/messages', array('role' => 'form', 'class' => 'validate')); ?>
	<div class="send-message row">
		<textarea name="message_body" class="form-control"></textarea>
		<input type="submit" name="send_message" class="btn btn-primary btn-lg" value="Send Message" />
	</div>
<?php echo form_close(); ?>