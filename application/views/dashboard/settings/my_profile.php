<h2 class="dash-heading">Settings - My Profile</h2>
<?php if(!empty($success_messages)): ?>
	<div class="message-area alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $success_messages; ?>
	</div>
<?php endif; ?>
<?php if(!empty($errors)): ?>
	<div class="message-area alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $errors; ?>
	</div>
<?php endif; ?>
<?php echo form_open('/settings/my-profile', array('role' => 'form', 'class' => 'validate')); ?>
	<div class="row my-profile">
		<div class="col-xs-6 personal-details">
			<h3>Personal Details</h3>
			<hr />
			<div class="form-group form-inline">
				<label for="first_name">First Name</label>
				<input type="text" class="form-control" name="first_name" id="first_name" required data-placement="right" value="<?php echo $user['first_name']; ?>" />
			</div>
			<div class="form-group form-inline">
				<label for="last_name">Last Name</label>
				<input type="text" class="form-control" name="last_name" id="last_name" required data-placement="right" value="<?php echo $user['last_name']; ?>" />
			</div>
			<div class="form-group form-inline">
				<label for="email">Email</label>
				<input type="email" class="form-control" name="email" id="email" required data-placement="right" value="<?php echo $user['email']; ?>" />
			</div>
			<div class="form-group form-inline">
				<label for="password">Password</label>
				<input type="password" class="form-control" name="password" id="password" data-placement="right" />
			</div>
			<div class="form-group form-inline">
				<label for="confirm_password">Confirm Password</label>
				<input type="password" class="form-control" name="confirm_password" id="confirm_password" data-rule-equalTo="#password" data-msg-equalTo="Your passwords do not match" data-placement="right" />
			</div>
			<div class="form-group form-inline">
				<label for="phone">Phone</label>
				<input type="text" name="phone" id="phone" class="form-control" required data-placement="right" value="<?php echo $user['phone']; ?>" />
			</div>
			<div class="form-group form-inline">
				<label for="gender">Gender</label>
				<select name="gender" class="form-control" id="gender" required data-placement="right">
				<option></option>
					<?php foreach($this->config->item('genders') as $key => $gender): ?>
						<option value="<?php echo $key; ?>"<?php echo ($user['gender'] == $key ? ' selected="selected"':'');?>><?php echo $gender; ?></option>
					<?php endforeach; ?>
				</select>
			</div>
			<div class="form-group form-inline">
				<label for="age">Age</label>
				<select name="age" class="form-control" id="age" required data-placement="right">
				<option></option>
					<?php for($x=MIN_AGE; $x<MAX_AGE; $x++): ?>
						<option value="<?php echo $x; ?>" <?php echo ($user['age'] == $x ? 'selected="selected"':''); ?>><?php echo $x; ?></option>
					<?php endfor; ?>
					<option value="-1" <?php echo ($user['age'] == -1 ? 'selected="selected"':''); ?>><?php echo MAX_AGE ."+"; ?></option>
				</select>
			</div>
			<div class="form-group form-inline">
				<label for="marital_status">Marital Status</label>
				<select name="marital_status" class="form-control" id="marital_status" required data-placement="right">
				<option></option>
					<?php foreach($this->config->item('marital_statuses') as $key => $status): ?>
						<option value="<?php echo $key; ?>" <?php echo ($user['marital_status'] == $key ? 'selected="selected"':''); ?>><?php echo $status; ?></option>
					<?php endforeach; ?>
				</select>
			</div>
			<div class="form-group form-inline">
				<label for="number_of_children">Number of Children</label>
				<select name="number_of_children" class="form-control" id="number_of_children" required data-placement="right">
				<option></option>
					<?php for($x=0; $x < MAX_CHILDREN; $x++): ?>
						<option value="<?php echo $x; ?>" <?php echo ($user['number_of_children'] == $x ? 'selected="selected"':''); ?>><?php echo $x; ?></option>
					<?php endfor; ?>
					<option value="-1" <?php echo ($user['number_of_children'] == -1 ? 'selected="selected"':''); ?>><?php echo MAX_CHILDREN ."+"; ?></option>
				</select>
			</div>
		</div>
		<div class="col-xs-6 address">
			<h3>Address</h3>
			<hr />
			<div class="form-group form-inline">
				<label for="street_address">Street Address</label>
				<input type="text" class="form-control" name="street_address" id="street_address" required data-placement="right" value="<?php echo $user['street_address']; ?>" />
			</div>
			<div class="form-group form-inline">
				<label for="suburb">Suburb/City</label>
				<input type="text" class="form-control" name="suburb" id="suburb" required data-placement="right" value="<?php echo $user['suburb']; ?>" />
			</div>
			<div class="form-group form-inline">
				<label for="state">State</label>
				<select name="state" class="form-control" id="state" required data-placement="right">
					<option></option>
					<?php foreach($this->config->item('states') as $key => $state): ?>
						<?php if($key != 'N/A'): ?>
							<option value="<?php echo $key; ?>" <?php echo ($user['state'] == $key ? 'selected="selected"':''); ?>><?php echo $state['label']; ?></option>
						<?php endif; ?>
					<?php endforeach; ?>
				</select>
			</div>
			<div class="form-group form-inline">
				<label for="postcode">Postcode</label>
				<input type="text" class="form-control" name="postcode" id="postcode" required data-placement="right" value="<?php echo (!empty($user['postcode']) ? $user['postcode']:''); ?>" data-rule-digits />
			</div>
			<div class="form-group form-inline">
				<label for="country">Country</label>
				<select name="country" class="form-control" id="country" required data-placement="bottom">
					<?php foreach($this->config->item('countries') as $key => $country): ?>
						<option value="<?php echo $key; ?>" <?php echo ($user['country'] == $key ? 'selected="selected"':''); ?>><?php echo $country; ?></option>
					<?php endforeach; ?>
				</select>
			</div>
			<div class="form-group form-inline">
				<label for="training_type">Training Type</label>
				<select name="training_type" class="form-control" id="training_type" required data-placement="bottom">
					<?php foreach($training_types as $type): ?>
						<option value="<?php echo $type['id']; ?>" <?php echo ($user['training_type'] == $type['id'] ? 'selected="selected"':''); ?>><?php echo $type['name']; ?></option>
					<?php endforeach; ?>
				</select>
			</div>
		</div>
		<div class="row submit">
			<div class="col-xs-12">
				<input type="submit" class="btn btn-primary btn-lg" name="profile_save" value="Save My Details" />
			</div>
		</div>
	</div>
<?php echo form_close(); ?>