<?php if(!empty($errors)): ?>
	<div class="message-area alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $errors; ?>
	</div>
<?php endif; ?>
<div id="payment">
	<h4>Select your plan</h4>
	<?php echo form_open('/renew', array('role' => 'form', 'class' => 'validate', 'id' => 'payment_form')); ?>
		<div class="row payment-method">
			<div class="col-xs-6">
				<h1 class="change-price"></h1>
				<p class="label per-what"></p>
				<p class="label">Please Select</p>
				<select name="payment_method" class="form-control" id="payment_method">
					<?php foreach($payment_methods as $method_id => $data): ?>
						<?php if($data['title'] != 'yearly'): ?>
							<option value="<?php echo $method_id ?>" <?php echo set_select('payment_method', $method_id); ?>><?php echo '$'.number_format($data['amount'], 2) . " " . ucwords($data['title']); ?></option>
						<?php endif; ?>
					<?php endforeach; ?>
				</select>
				<input type="radio" name="payment_frequency" id="payment_frequency_other" value="1" <?php echo set_radio('payment_frequency', '1', TRUE); ?> required /> 
				<label class="btn btn-lg btn-grey" for="payment_frequency_other"></label>
			</div>
			<div class="col-xs-6">
				<h1>$<?php echo number_format($payment_methods[4]['amount'], 2); ?></h1>
				<p class="label">Per Year</p>
				<p class="label">($<?php echo number_format(($payment_methods[4]['amount']/52), 2); ?> per week)</p>
				<input type="radio" name="payment_frequency" id="payment_frequency_yearly" value="2" <?php echo set_radio('payment_frequency', '2'); ?> required /> 
				<label class="btn btn-lg btn-grey" for="payment_frequency_yearly"></label>
			</div>
		</div>
		<hr />
		<div class="row payment-details">
			<div class="col-xs-6">
				<div class="form-group">
					<label for="card_name">Name on Card<span>*</span></label>
					<input type="text" name="card_name" id="card_name" class="form-control" required data-placement="left" />
				</div>
				<div class="form-group">
					<label for="card_number">Card Number<span>*</span></label>
					<input type="text" name="card_number" id="card_number" class="form-control" required data-placement="left" data-rule-creditcard data-msg-creditcard="You must enter a valid credit card number" />
				</div>
				<div class="row">
					<div class="col-xs-3 ccv">
						<div class="form-group">
							<label for="card_ccv">CCV<span>*</span></label>
							<input type="text" name="card_ccv" id="card_ccv" class="form-control" required data-placement="bottom" />
						</div>
					</div>
					<div class="col-xs-9">
						<div class="form-group expiry">
							<label for="expiry_month">Expiry<span>*</span></label>
							<div class="form-inline">
								<select name="expiry_month" id="expiry_month" class="form-control tiny-select" required data-placement="bottom">
									<?php for($month=0; $month<12; $month++): ?>
										<option value="<?php echo ($month+1); ?>"><?php echo ($month+1); ?></option>
									<?php endfor; ?>
								</select>
								<select name="expiry_year" id="expiry_year" class="form-control tiny-select" required data-placement="bottom">
									<?php for($year=date('y'); $year<100; $year++): ?>
										<option value="<?php echo $year; ?>"><?php echo '20'.$year; ?></option>
									<?php endfor; ?>
								</select>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-6">
				<span class="sprite mastercard"></span>
				<span class="sprite visa"></span>
				<span class="sprite ssl">
					Not quite sure about buying online? STC Fit is with you every step of the way. Your purchase is secured by 256bit SSL Certificate.
				</span>
				<span class="eway-block">
					<a href="http://www.eway.com.au/secure-site-seal?i=10&amp;s=3&amp;pid=e3de70ba-e776-441e-a39e-7c0b9b38b7c6" title="eWAY Payment Gateway" target="_blank" rel="nofollow">
						<img alt="eWAY Payment Gateway" src="https://www.eway.com.au/developer/payment-code/verified-seal.ashx?img=10&amp;size=3&amp;pid=e3de70ba-e776-441e-a39e-7c0b9b38b7c6" />
					</a>
				</span>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 submit-button">
				<input type="submit" name="submit_payment_details" class="btn btn-lg btn-primary" id="payment_submit" value="Renew now!" />
			</div>
		</div>
	<?php echo form_close(); ?>
</div>
<script>
	var methods = <?php echo json_encode($payment_methods)?>;
</script>