<h2 class="dash-heading">Settings - My Plan</h2>
<?php if(!empty($success_messages)): ?>
	<div class="message-area alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $success_messages; ?>
	</div>
<?php endif; ?>
<div id="payment">
	<?php if($user['free'] == 1): ?>
		<p>Your account is set to be free</p>
	<?php else: ?>
		<?php echo form_open('/settings/my-plan', array('role' => 'form')); ?>
			<div class="row payment-method">
				<div class="col-xs-6">
					<h1 class="change-price"></h1>
					<p class="label per-what"></p>
					<p class="label">Please Select</p>
					<select name="payment_method" class="form-control" id="payment_method">
						<?php foreach($payment_methods as $method_id => $data): ?>
							<?php if($data['title'] != 'yearly'): ?>
								<option value="<?php echo $method_id ?>" <?php echo ($user['renewal_type'] == $method_id ? 'selected="selected"':'')?>><?php echo '$'.number_format($data['amount'], 2) . " " . ucwords($data['title']); ?></option>
							<?php endif; ?>
						<?php endforeach; ?>
					</select>
					<input type="radio" name="payment_frequency" id="payment_frequency_other" value="1" <?php echo ($user['renewal_type'] != 4 ? 'checked="checked"':''); ?> required /> 
					<label class="btn btn-lg btn-grey" for="payment_frequency_other"></label>
				</div>
				<div class="col-xs-6">
					<h1>$<?php echo number_format($payment_methods[4]['amount'], 2); ?> <span class="aud">AUD</span></h1>
					<p class="label">Per Year</p>
					<p class="label">($<?php echo number_format(($payment_methods[4]['amount']/52), 2); ?> per week)</p>
					<input type="radio" name="payment_frequency" id="payment_frequency_yearly" value="2" <?php echo ($user['renewal_type'] == 4 ? 'checked="checked"':''); ?> required /> 
					<label class="btn btn-lg btn-grey" for="payment_frequency_yearly"></label>
				</div>
			</div>
			<div class="submit-center">
				<input type="submit" name="update_method" class="btn btn-primary btn-lg" value="Save and Finish" />
			</div>
		<?php echo form_close(); ?>
	<?php endif; ?>
</div>
<script>
	var methods = <?php echo json_encode($payment_methods)?>;
</script>