<h2 class="dash-heading">Settings - Billing History</h2>
<?php if(!empty($success_messages)): ?>
	<div class="message-area alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $success_messages; ?>
	</div>
<?php endif; ?>
<?php if(!empty($errors)): ?>
	<div class="message-area alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $errors; ?>
	</div>
<?php endif; ?>
<div class="billing">
	<ul class="nav nav-tabs">
		<li class="<?php echo (empty($tab) || $tab == 'billing-history' ? 'active':'') ?>"><a href="#billing-history" data-toggle="tab">Billing History</a></li>
		<li class="<?php echo (!empty($tab) && $tab == 'credit-card-details' ? 'active':'') ?>"><a href="#credit-card-details" data-toggle="tab">My Credit Card Details</a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane <?php echo (empty($tab) || $tab == 'billing-history' ? 'active':'') ?>" id="billing-history">
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>Date</th>
						<th>Amount</th>
						<th colspan="2">Plan/Description</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($billing_history as $bill): ?>
						<tr class="top-border">
							<td width="25%">
								<?php echo date('d F Y', $bill['date']); ?>
							</td>
							<td width="25%">
								<?php echo '$'.number_format($bill['amount'], 2); ?>
							</td>
							<td width="25%">
								<?php echo $bill['description']; ?>
							</td>
							<td width="25%" align="right" class="view-invoice">
								<a href="/settings/view-invoice/<?php echo $bill['id']; ?>" class="btn btn-primary">View invoice</a>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>				
			</table>
		</div>
		<div class="tab-pane row <?php echo (!empty($tab) && $tab == 'credit-card-details' ? 'active':'') ?>" id="credit-card-details">
			<?php echo form_open('/settings/billing/credit-card-details', array('role' => 'form', 'class' => 'validate', 'id' => 'credit_card')); ?>
				<div class="col-xs-4 left-column">
					<div class="form-group">
						<label for="card_name">Name on Card<span>*</span></label>
						<input type="text" name="card_name" id="card_name" class="form-control" required data-placement="left" value="<?php echo (!empty($cc_details['CCName']) ? $cc_details['CCName']:''); ?>" />
					</div>
					<div class="form-group">
						<label for="card_number">Card Number<span>*</span></label>
						<input type="text" name="card_number" id="card_number" class="form-control" value="<?php echo (!empty($cc_details['CCNumber']) ? $cc_details['CCNumber']:''); ?>" required data-placement="left" data-rule-creditcard data-msg-creditcard="You must enter a valid credit card number" />
					</div>
					<div class="row">
						<div class="col-xs-3 ccv">
							<div class="form-group">
								<label for="card_ccv">CCV<span>*</span></label>
								<input type="text" name="card_ccv" id="card_ccv" class="form-control" required data-placement="bottom" />
							</div>
						</div>
						<div class="col-xs-9 expiry">
							<div class="form-group expiry">
								<label for="expiry_month">Expiry<span>*</span></label>
								<div class="form-inline">
									<select name="expiry_month" id="expiry_month" class="form-control tiny-select" required data-placement="bottom">
										<?php for($month=0; $month<12; $month++): ?>
											<option value="<?php echo ($month+1); ?>" <?php echo (!empty($cc_details['CCExpiryMonth']) && $cc_details['CCExpiryMonth']==($month+1) ? ' selected="selected"':'') ?>><?php echo ($month+1); ?></option>
										<?php endfor; ?>
									</select>
									<select name="expiry_year" id="expiry_year" class="form-control tiny-select" required data-placement="bottom">
										<?php for($year=date('y'); $year<100; $year++): ?>
											<option value="<?php echo $year; ?>"<?php echo (!empty($cc_details['CCExpiryYear']) && $cc_details['CCExpiryYear']==$year ? ' selected="selected"':'') ?>><?php echo '20'.$year; ?></option>
										<?php endfor; ?>
									</select>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-8 right-column">
					<span class="sprite mastercard"></span>
					<span class="sprite visa"></span>
					<span class="sprite ssl">
						Not quite sure about buying online? STC Fit is with you every step of the way. Your purchase is secured by 256bit SSL Certificate.
					</span>
					<span class="eway-block">
						<a href="http://www.eway.com.au/secure-site-seal?i=10&amp;s=3&amp;pid=e3de70ba-e776-441e-a39e-7c0b9b38b7c6" title="eWAY Payment Gateway" target="_blank" rel="nofollow">
							<img alt="eWAY Payment Gateway" src="https://www.eway.com.au/developer/payment-code/verified-seal.ashx?img=10&amp;size=3&amp;pid=e3de70ba-e776-441e-a39e-7c0b9b38b7c6" />
						</a>
					</span>
				</div>
				<?php if(!empty($user['eway_token'])): ?>
					<div class="submit update">
						<input type="submit" name="update_card" class="btn btn-primary" value="Update Card" />
					</div>
				<?php else:?>
					<div class="submit add">
						<input type="submit" name="add_card" class="btn btn-primary" value="Add Card" />
					</div>
				<?php endif; ?>
			<?php echo form_close(); ?>
			<?php if(!empty($user['eway_token'])): ?>
				<?php echo form_open('/settings/billing/credit-card-details', array('role' => 'form', 'id' => 'delete_card')); ?>
					<div class="submit delete">
						<input type="submit" name="delete_card" id="delete_card" class="btn btn-primary" value="Delete Card" />
					</div>
				<?php echo form_close(); ?>
			<?php endif; ?>
			<?php if(empty($user['account_deleted'])): ?>
				<a href="#delete-account" class="delete-account" id="delete-account" data-toggle="modal" data-target="#delete_account_modal">Delete My Account</a>
			<?php else: ?>
				<a href="#reactivate-account" class="delete-account" id="reactivate-account" data-toggle="modal" data-target="#reactivate_account_modal">Reactivate My Account</a>
			<?php endif; ?>
		</div>
	</div>
</div>
<div class="modal fade" id="delete_account_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header gradient-background white-text">
				<strong>Would you like to delete your account?</strong>
				<span class="sprite sprite-32 sprite-close"><button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button></span>
			</div>
			<div class="modal-body">
				<h2>Please tell us why you have chosen to leave the STC Fit Team:</h2>
				<?php echo form_open('/settings/delete-account', array('role' => 'form', 'class' => 'validate')); ?>
					<?php foreach($this->config->item('leave_reasons') as $key => $reason): ?>
						<div class="form-group">
							<label>
								<input type="radio" name="leave_reason" value="<?php echo $key; ?>" required /> <?php echo $reason['title']; ?>
							</label>
						</div>
					<?php endforeach; ?>
					<p class="footer-note">Please note: If you choose to delete your account at this time, you will retain access for the period that you have paid for.</p>
					<div class="submit">
						<input type="submit" class="btn btn-primary" name="delete_account" value="Delete my account now!" />
					</div>
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="reactivate_account_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header gradient-background white-text">
				<strong>Would you like to reactivate your account?</strong>
				<span class="sprite sprite-32 sprite-close"><button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button></span>
			</div>
			<div class="modal-body">
				<?php echo form_open('/settings/reactivate-account', array('role' => 'form', 'class' => 'validate')); ?>
					<div class="submit">
						<input type="submit" class="btn btn-primary" name="reactivate_account" value="Reactivate my account now!" />
					</div>
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>
</div>