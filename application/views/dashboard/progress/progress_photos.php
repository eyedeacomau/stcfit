<h2 class="dash-heading">Progress Photo Gallery</h2>
<div class="section row progress-photos">
	<div class="col-xs-12">
		<div class="photo_gallery">
			<?php if(!empty($photos)): ?>
				<div id="main_image">
					<ul id="main_images">
						<li id="image_<?php echo $photos[0]['id']; ?>">
							<div class="date-taken"><?php echo date('d/m/Y h:i:sa', $photos[0]['date'])?></div>
							<img src="/images/user/progress/<?php echo $image_sizes['progress_photo']['size'].'-'.$photos[0]['image_name']; ?>" />
						</li>
					</ul>
				</div>
				<ul id="thumbnails">
					<?php foreach($photos as $photo): ?>
						<li>
							<a href="#change-image" class="change-image" data-imageid="<?php echo $photo['id']; ?>" data-date-taken="<?php echo date('d/m/Y h:i:sa', $photo['date'])?>" data-image="/images/user/progress/<?php echo $image_sizes['progress_photo']['size'].'-'.$photo['image_name']; ?>">
								<img src="/images/user/progress/<?php echo $image_sizes['progress_thumb_large']['size'].'-'.$photo['image_name']; ?>" />
							</a>
						</li>
					<?php endforeach; ?>
				</ul>
			<?php else: ?>
			<div class="message-area alert alert-info alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				You do not currently have any progress photos. You may add one when making an entry in the progress section of your dashboard.
			</div>
			<?php endif; ?>
		</div>
	</div>
	<a href="/progress" class="btn btn-primary progress-button"><span>&laquo;</span>Back to progress</a>
</div>