<?php if(!empty($messages)): ?>
	<div class="message-area alert alert-info alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $messages; ?>
	</div>
<?php endif; ?>
<?php if(!empty($errors)): ?>
	<div class="message-area alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $errors; ?>
	</div>
<?php endif; ?>
<h2 class="dash-heading">Progress Tracking</h2>
<div class="new-progress">
	<h3>New Entry</h3>
	<div class="date">Date: <span><?php echo date('d/m/Y', time()); ?></span></div>
	<hr />
	<?php echo form_open_multipart('/progress/new-entry', array('role' => 'form', 'class' => 'validate')); ?>
		<ol>
			<li class="form-group form-inline">
				<label for="weight">Weight</label>
				<div class="input-group-container">
					<div class="input-group">
						<input type="text" class="form-control" name="weight" id="weight" required data-rule-number data-placement="right" />
						<span class="input-group-addon kg">kg</span>
					</div>
				</div>
				<a href="#open-explanation" data-toggle="modal" data-target="#weight-explanation" class="measurement-explanation"><span class="sprite sprite-questionmark"></span></a>
			</li>
			<li class="form-group form-inline">
				<label for="shoulders">Shoulders</label>
				<div class="input-group-container">
					<div class="input-group">
						<input type="text" class="form-control" name="shoulders" id="shoulders" required data-rule-digits data-placement="right" />
						<span class="input-group-addon cm">cm</span>
					</div>
				</div>
				<a href="#open-explanation" data-toggle="modal" data-target="#shoulders-explanation" class="measurement-explanation"><span class="sprite sprite-questionmark"></span></a>
			</li>
			<li class="form-group form-inline">
				<label for="neck">Neck</label>
				<div class="input-group-container">
					<div class="input-group">
						<input type="text" class="form-control" name="neck" id="neck" required data-rule-digits data-placement="right" />
						<span class="input-group-addon cm">cm</span>
					</div>
				</div>
				<a href="#open-explanation" data-toggle="modal" data-target="#neck-explanation" class="measurement-explanation"><span class="sprite sprite-questionmark"></span></a>
			</li>
			<li class="form-group form-inline">
				<label for="chest">Chest</label>
				<div class="input-group-container">
					<div class="input-group">
						<input type="text" class="form-control" name="chest" id="chest" required data-rule-digits data-placement="right" />
						<span class="input-group-addon cm">cm</span>
					</div>
				</div>
				<a href="#open-explanation" data-toggle="modal" data-target="#chest-explanation" class="measurement-explanation"><span class="sprite sprite-questionmark"></span></a>
			</li>
			<li class="form-group form-inline">
				<label for="waist">Waist</label>
				<div class="input-group-container">
					<div class="input-group">
						<input type="text" class="form-control" name="waist" id="waist" required data-rule-digits data-placement="right" />
						<span class="input-group-addon cm">cm</span>
					</div>
				</div>
				<a href="#open-explanation" data-toggle="modal" data-target="#waist-explanation" class="measurement-explanation"><span class="sprite sprite-questionmark"></span></a>
			</li>
			<li class="form-group form-inline">
				<label for="bicep">Bicep</label>
				<div class="input-group-container">
					<div class="input-group">
						<input type="text" class="form-control" name="bicep" id="bicep" required data-rule-digits data-placement="right" />
						<span class="input-group-addon cm">cm</span>
					</div>
				</div>
				<a href="#open-explanation" data-toggle="modal" data-target="#bicep-explanation" class="measurement-explanation"><span class="sprite sprite-questionmark"></span></a>
			</li>
			<li class="form-group form-inline">
				<label for="hips">Hips</label>
				<div class="input-group-container">
					<div class="input-group">
						<input type="text" class="form-control" name="hips" id="hips" required data-rule-digits data-placement="right" />
						<span class="input-group-addon cm">cm</span>
					</div>
				</div>
				<a href="#open-explanation" data-toggle="modal" data-target="#hips-explanation" class="measurement-explanation"><span class="sprite sprite-questionmark"></span></a>
			</li>
			<li class="form-group form-inline">
				<label for="quads">Quads</label>
				<div class="input-group-container">
					<div class="input-group">
						<input type="text" class="form-control" name="quads" id="quads" required data-rule-digits data-placement="right" />
						<span class="input-group-addon cm">cm</span>
					</div>
				</div>
				<a href="#open-explanation" data-toggle="modal" data-target="#quads-explanation" class="measurement-explanation"><span class="sprite sprite-questionmark"></span></a>
			</li>
			<li class="form-group form-inline">
				<label for="calfs">Calfs</label>
				<div class="input-group-container">
					<div class="input-group">
						<input type="text" class="form-control" name="calfs" id="calfs" required data-rule-digits data-placement="right" />
						<span class="input-group-addon cm">cm</span>
					</div>
				</div>
				<a href="#open-explanation" data-toggle="modal" data-target="#calfs-explanation" class="measurement-explanation"><span class="sprite sprite-questionmark"></span></a>
			</li>
			<li class="form-group form-inline">
				<label for="wrist">Wrist</label>
				<div class="input-group-container">
					<div class="input-group">
						<input type="text" class="form-control" name="wrist" id="wrist" required data-rule-digits data-placement="right" />
						<span class="input-group-addon cm">cm</span>
					</div>
				</div>
				<a href="#open-explanation" data-toggle="modal" data-target="#wrist-explanation" class="measurement-explanation"><span class="sprite sprite-questionmark"></span></a>
			</li>
			<li class="form-group form-inline">
				<label for="forearm">Forearm</label>
				<div class="input-group-container">
					<div class="input-group">
						<input type="text" class="form-control" name="forearm" id="forearm" required data-rule-digits data-placement="right" />
						<span class="input-group-addon cm">cm</span>
					</div>
				</div>
				<a href="#open-explanation" data-toggle="modal" data-target="#forearm-explanation" class="measurement-explanation"><span class="sprite sprite-questionmark"></span></a>
			</li>
			<li class="form-group form-inline">
				<label for="progress_photo">Progress Photo</label>
				<div class="nice-file" data-filename="No file chosen">
					<input type="file" name="progress_photo" id="progress_photo" />
				</div>
			</li>
		</ol>
		<div class="submit">
			<input type="submit" name="add_entry" class="btn btn-primary btn-lg" value="Save and Finish" />
		</div>
	<?php echo form_close(); ?>
</div>
<div class="modal fade" id="weight-explanation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header gradient-background white-text">
				<strong>How to measure my weight</strong>
				<span class="sprite sprite-32 sprite-close"><button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button></span>
			</div>
			<div class="modal-body">
				Weight is your total body weight in kilograms(kg). To get as close to an exact measurement make sure you are not carrying any extra items such as clothing or anything that could add to your total weight.
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="shoulders-explanation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header gradient-background white-text">
				<strong>How to measure my shoulders</strong>
				<span class="sprite sprite-32 sprite-close"><button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button></span>
			</div>
			<div class="modal-body">
				Measure widest part of the shoulder, hold firm, don't squeeze.
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="neck-explanation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header gradient-background white-text">
				<strong>How to measure my neck</strong>
				<span class="sprite sprite-32 sprite-close"><button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button></span>
			</div>
			<div class="modal-body">
				At collar, firm, don't squeeze.
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="chest-explanation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header gradient-background white-text">
				<strong>How to measure my chest</strong>
				<span class="sprite sprite-32 sprite-close"><button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button></span>
			</div>
			<div class="modal-body">
				From Under armpits.
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="waist-explanation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header gradient-background white-text">
				<strong>How to measure my waist</strong>
				<span class="sprite sprite-32 sprite-close"><button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button></span>
			</div>
			<div class="modal-body">
				At navel.
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="bicep-explanation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header gradient-background white-text">
				<strong>How to measure my bicep</strong>
				<span class="sprite sprite-32 sprite-close"><button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button></span>
			</div>
			<div class="modal-body">
				At thickest point.
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="hips-explanation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header gradient-background white-text">
				<strong>How to measure my hips</strong>
				<span class="sprite sprite-32 sprite-close"><button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button></span>
			</div>
			<div class="modal-body">
				Thickest part of the bum.
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="quads-explanation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header gradient-background white-text">
				<strong>How to measure my quads</strong>
				<span class="sprite sprite-32 sprite-close"><button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button></span>
			</div>
			<div class="modal-body">
				Under the glute (bum) crease.
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="calfs-explanation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header gradient-background white-text">
				<strong>How to measure my calfs</strong>
				<span class="sprite sprite-32 sprite-close"><button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button></span>
			</div>
			<div class="modal-body">
				At widest point.
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="wrist-explanation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header gradient-background white-text">
				<strong>How to measure my wrist</strong>
				<span class="sprite sprite-32 sprite-close"><button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button></span>
			</div>
			<div class="modal-body">
				Measure below writs bone firm not tight.
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="forearm-explanation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header gradient-background white-text">
				<strong>How to measure my forearm</strong>
				<span class="sprite sprite-32 sprite-close"><button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button></span>
			</div>
			<div class="modal-body">
				 Taken at widest part of the forearm firm and not tight
			</div>
		</div>
	</div>
</div>