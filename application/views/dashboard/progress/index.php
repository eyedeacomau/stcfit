<?php if(!empty($success_messages)): ?>
	<div class="message-area alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $success_messages; ?>
	</div>
<?php endif; ?>
<?php if(!empty($errors)): ?>
	<div class="message-area alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $errors; ?>
	</div>
<?php endif; ?>
<h2 class="dash-heading">Progress Tracking</h2>
<?php if(!empty($progress_data['measurements']) && !empty($progress_data['categories'])): ?>
	<div class="dash_graph_container" id="progress_graph">

	</div>
	<script>
		var cats = <?php echo (!empty($progress_data['categories']) ? json_encode($progress_data['categories']):'[]'); ?>;
		var points = <?php echo (!empty($progress_data['measurements']) ? json_encode($progress_data['measurements']):'[]'); ?>;
	</script>
	<hr />
	<h4>History of Phase Entries:</h4>
	<table width="100%" class="table table-condensed table-bordered">
		<thead>
			<tr>
				<th>Date</th>
				<th>Weight</th>
				<th>Shoulders</th>
				<th>Neck</th>
				<th>Chest</th>
				<th>Waist</th>
				<th>Bicep</th>
				<th>Hips</th>
				<th>Quads</th>
				<th>Calf</th>
				<th>Wrist</th>
				<th>Forearm</th>
				<th>Body Fat %</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($progress_data['measurements'] as $measurement):?>
			<tr>
				<td><?php echo date('d/m/Y',$measurement['entry_date']);?></td>
				<td><?php echo $measurement['weight'];?>kg</td>
				<td><?php echo $measurement['shoulders'];?>cm</td>
				<td><?php echo $measurement['neck'];?>cm</td>
				<td><?php echo $measurement['chest'];?>cm</td>
				<td><?php echo $measurement['waist'];?>cm</td>
				<td><?php echo $measurement['bicep'];?>cm</td>
				<td><?php echo $measurement['hips'];?>cm</td>
				<td><?php echo $measurement['quads'];?>cm</td>
				<td><?php echo $measurement['calf'];?>cm</td>
				<td><?php echo $measurement['wrist'];?>cm</td>
				<td><?php echo $measurement['forearm'];?>cm</td>
				<td><?php echo $measurement['body_fat'];?>%</td>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
<?php else: ?>
	<div class="message-area alert alert-info alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		You currently have no measurement entries. Please click below to create one.
	</div>
<?php endif; ?>
<div class="entry-button">
	<span><?php echo (!empty($progress_data['measurements']) ? count($progress_data['measurements']):'0'); ?> <?php echo (!empty($progress_data['measurements']) && count($progress_data['measurements']) == 1 ? 'Entry':'Entries')?></span>
	<a href="/progress/new-entry" class="btn btn-primary">+ Add New Entry</a>
	<a href="/progress/progress-photos" class="btn btn-primary">View Progress Photos</a>
</div>
