<?php if(!empty($messages)): ?>
	<div class="message-area alert alert-info alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $messages; ?>
	</div>
<?php endif; ?>
<?php if(!empty($errors)): ?>
	<div class="message-area alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $errors; ?>
	</div>
<?php endif; ?>
<?php if(!empty($success_messages)): ?>
	<div class="message-area alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $success_messages; ?>
	</div>
<?php endif; ?>
<div class="dashboard-container">
<?php if(!empty($phase['measurements']) && !empty($phase['categories'])): ?>
	<div class="dash_graph_container" id="dash_graph">

	</div>
<?php endif; ?>
<?php if(!empty($phase)){ ?>
	<hr />
	<h4>Progress of Current Phase:</h4>
	<div class="pie-chart" id="training_progress">
		<div class="graph-label">Training</div>
		<div class="graph"></div>
		<div class="footer-info">Completed <?php echo (!empty($phase['progress']['training']['complete']) ? $phase['progress']['training']['complete']:0); ?>%</div>
	</div>
</div>
<?php } else { ?>
	<hr />
	<h4>Progress of Current Phase:</h4>
	<div class="pie-chart" id="training_progress">
		<div class="graph-label">Training</div>
		<div class="graph"></div>
		<div class="footer-info">Completed 0%</div>
	</div>
</div>
<?php } ?>
</div>
<?php if($show_message): ?>
<div id="welcome_message" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header gradient-background white-text">
				<span class="sprite sprite-32 sprite-close"><button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button></span>
				<h4 class="modal-title">Welcome to STCfit</h4>
			</div>
			<div class="modal-body">

				<p class="large">Congratulations on becoming a member of our team. We are excited to begin working with you to achieve your goals!</p>
				<p>
					<?php if(!empty($trainer_name)): ?>
						Your trainer will be: <strong><?php echo $trainer_name; ?></strong>, should you have any enquiries, you may contact them through the messaging section of the dashboard.
					<?php else: ?>
						You are not currently assigned a trainer, however will be shortly.
					<?php endif; ?>
				</p>
				<br />
				<h4>What to do now:</h4>
				<ol>
					<li>Your first step is selecting your food plan. We recommend starting with Phase 1 for at least your first 8 weeks.</br>
					Click on the food tab and follow the prompts to find out how it works.</li>

					<li>Record where you're starting from. Head to the 'Progress' page to learn how to take your measurements so we can make sure you're getting results.</li>

					<li>Take a "before shot". Grab a current newspaper or magazine to hold beside you in a, front, side and back photo.</br>
					We recommend a bikini or under wear for the ladies, and trunks or small shorts for the men to allow us to see the changes.</li>

					<li>Check out your training plan. Your first program will be available for you in the 'Training' section. Start watching the videos so you are confident with the movements from your first session in the gym. </li>

					<li>Print your program. Have your month's program printed to take with you to the gym, so you can later fill in your completed training info on the 'Training' page.</br>

					<li>If you need to cancel your subscription or update your payment information click "Settings -> My Plan"</li>

					<i><strong>NOTE:</strong> Your program will only update when you have completed all fields of that phase. Programs progress in difficulty so falsifying entries may lead in to the inability to complete future programs correctly.</i></li>

					<li>Meet your trainer. Your trainer will be assigned to you within 1 business day and you will receive a welcome message. If you have any questions your trainer will be able to help.</li>

					<li>Kick some butt! With a clear plan set with your trainer you'll be ready to go!</li>
				</ol>

				<p>Good luck. We believe you have all the tools to achieve great things and we can't wait to be a part of it. </p>

				<strong><i>STCfit</i></strong>.
			</div>
			<div class="modal-footer">
				<label class="checkbox pull-left"><input type="checkbox" class="dont_show" value="1"/> Don't show this message again.</label>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php endif; ?>
<script>
	var cats = <?php echo (!empty($phase['categories']) ? json_encode($phase['categories']):'[]'); ?>;
	var points = <?php echo (!empty($phase['measurements']) ? json_encode($phase['measurements']):'[]'); ?>;
	var training = [{
		name: 'Complete',
		y: <?php echo (!empty($phase['progress']['training']['complete']) ? $phase['progress']['training']['complete']:0); ?>,
		color: '#e84336'
	},
	{
		name: 'Incomplete',
		y: <?php echo (isset($phase['progress']['training']['incomplete']) ? $phase['progress']['training']['incomplete']:1); ?>,
		color: '#f28d21'
	}];
</script>
