<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="viewport" content="width=1200" />
		<meta name="google-site-verification" content="VE6F09uqnRuMLqQd-2QdP0Sy_E5LygdIRKTksA3Zrqs" />
		<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
		<link rel="stylesheet" href="/assets/css/plugins/bootstrap.min.css" />
		<link rel="stylesheet" href="/assets/css/main.css" />
		<link rel="stylesheet" href="/assets/css/trainers/common.css" />
		<link rel="shortcut icon" type="image/x-icon" href="/assets/images/favicon.ico" />
		<?php if(!empty($include_css)) echo '<link rel="stylesheet" href="'.$include_css.'" />';?>
		<?php if(!empty($extra_css)) foreach($extra_css as $css) echo '<link rel="stylesheet" href="'.$css.'" />'; ?>
		<?php if(!empty($extra_head)) foreach($extra_head as $e_head) echo $e_head; ?>
		<!--[if lt IE 9]><script type="text/javascript" src="/assets/js/head.min.js"></script><![endif]-->
		<title><?=$title;?></title>
	</head>
	<body>
	<!-- Google Tag Manager -->
	<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-NXZGSF"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-NXZGSF');</script>
	<!-- End Google Tag Manager -->
		<div class="columns">
			<div class="navigation">
				<a href="/trainers" class="logo"><img src="/assets/images/logo-admin-dashboard.png" /></a>
				<span class="trainer-code"><?php echo (!empty($template_data['trainer']['trainer_id']) ? '('.TRAINER_PREFIX.$template_data['trainer']['trainer_id'].')':''); ?></span>
				<span class="trainer-name"><?php echo (!empty($template_data['trainer']['first_name']) && !empty($template_data['trainer']['last_name']) ? $template_data['trainer']['first_name'].' '.$template_data['trainer']['last_name']:''); ?></span>
				<ul>
					<li class="quick-phase-bg<?php echo (empty($template_data['page']) || $template_data['page'] == 'dashboard' ? ' active':''); ?>"><a href="/trainers">Dashboard</a></li>
					<li class="quick-phase-bg<?php echo (!empty($template_data['page']) && $template_data['page'] == 'reminders' ? ' active':''); ?>"><a href="/trainers/reminders">Reminders<?php echo (!empty($template_data['trainer']['trainer_reminders']) ? '<span class="counter">'.$template_data['trainer']['trainer_reminders'].'</span>':''); ?></a></li>
					<li class="quick-phase-bg<?php echo (!empty($template_data['page']) && $template_data['page'] == 'clients' ? ' active':''); ?>"><a href="/trainers/clients">Clients</a></li>
					<li class="quick-phase-bg<?php echo (!empty($template_data['page']) && $template_data['page'] == 'programs' ? ' active':''); ?>"><a href="/trainers/programs">Programs</a></li>
					<li id="message_count" class="quick-phase-bg<?php echo (!empty($template_data['page']) && $template_data['page'] == 'messages' ? ' active':''); ?>"><a href="/trainers/messages">Messages<?php echo (!empty($template_data['trainer']['unread_messages']) ? '<span class="counter">'.$template_data['trainer']['unread_messages'].'</span>':''); ?></a></li>
					<li class="quick-phase-bg"><a href="/logout">Logout</a></li>
				</ul>
			</div>
			<div class="body">
				<div class="inner">
					<h1 class="section-header"><?php echo (!empty($template_data['page']) ? ucwords($template_data['page']):'Dashboard'); ?></h1>
					<?php echo $content ."\n"; ?>
				</div>
			</div>
		</div>
		<script type="text/javascript" src="<?=JS_PATH;?>plugins/head.min.js"></script>
		<script type="text/javascript">
			head.js(
				{jquery: '<?=JS_PATH;?>plugins/jquery-1.11.0.min.js'},
				{bootstrap: '<?=JS_PATH;?>plugins/bootstrap.min.js'},
				{validation: '<?=JS_PATH;?>plugins/jquery.validate.min.js'},
				<?php if(!empty($extra_js)) for($i=0;$i<count($extra_js);$i++) echo '{extra'.$i.': \''.$extra_js[$i].'\'},'; ?>
				{placeholders: '<?=JS_PATH; ?>plugins/jquery.infieldlabel.js'},
				{main: '<?=JS_PATH;?>main.js'},
				{common: '<?=JS_PATH;?>/trainers/common.js'}<?php if(!empty($include_js)): ?>,
				{extra: '<?=$include_js;?>'} <?php endif; ?>
			);
		</script>
	</body>
</html>
