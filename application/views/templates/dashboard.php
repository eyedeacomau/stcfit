<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=1200" />
        <meta name="google-site-verification" content="VE6F09uqnRuMLqQd-2QdP0Sy_E5LygdIRKTksA3Zrqs" />
        <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />  
        <link rel="stylesheet" href="/assets/css/plugins/bootstrap.min.css" />
        <link rel="stylesheet" href="/assets/css/main.css" />
        <link rel="shortcut icon" type="image/x-icon" href="/assets/images/favicon.ico" />
        <?php if(!empty($include_css)) echo '<link rel="stylesheet" href="'.$include_css.'" />';?>
        <?php if(!empty($extra_css)) foreach($extra_css as $css) echo '<link rel="stylesheet" href="'.$css.'" />'; ?>
        <?php if(!empty($extra_head)) foreach($extra_head as $e_head) echo $e_head; ?>
        <!--[if lt IE 9]><script type="text/javascript" src="/assets/js/head.min.js"></script><![endif]-->
        <title><?=$title;?></title>
    </head>
    <body class="dash">
    <!-- Google Tag Manager -->
    <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-NXZGSF"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-NXZGSF');</script>
    <!-- End Google Tag Manager -->
        <div class="wrapper dash-header">
            <div class="inner-content">
                <a href="/dashboard" class="dash-logo" title="Dashboard Home"><img src="/assets/images/logo-bw.png" alt="Dashboard Home" /></a>
                <span class="user-name">
                    Hi <?php echo $template_data['name']; ?>, welcome to your account
                </span>
                <div class="dash-settings">
                    <div class="btn-group">
                        <button type="button" class="btn btn-default dropdown-toggle settings-button" data-toggle="dropdown">Settings<span class="caret"></span></button>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="/settings/my-profile">My Profile</a></li>
                            <li><a href="/settings/my-plan">My Plan</a></li>
                            <li><a href="/settings/billing">Billing</a></li>
                            <li class="divider"></li>
                            <li><a href="/logout">Logout</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="columns <?php echo (!empty($template_data['full_width']) ? ' full-width':''); ?>">
            <div class="navigation">
                <ul>
                    <li<?php echo ($this->uri->segment(1) == 'dashboard' ? ' class="active"':'')?>>
                        <a class="quick-phase-bg" href="/dashboard"><span class="sprite dashboard"></span>Dashboard</a>
                    </li>
                    <li<?php echo ($this->uri->segment(1) == 'training' ? ' class="active"':'')?>>
                        <a class="quick-phase-bg" href="/training"><span class="sprite training"></span>Training</a>
                    </li>
                    <li<?php echo ($this->uri->segment(1) == 'food' ? ' class="active"':'')?>>
                        <a class="quick-phase-bg" href="/food"><span class="sprite food"></span>Food</a>
                    </li>
                    <li<?php echo ($this->uri->segment(1) == 'messages' ? ' class="active"':'')?>>
                        <a class="quick-phase-bg" href="/messages">
                            <span class="sprite messages"></span>Messages
                            <?php if(!empty($template_data['unread'])): ?>
                                <span class="count-indicator"><?php echo $template_data['unread']; ?></span>
                            <?php endif; ?>
                        </a>
                    </li>
                    <li class="last<?php echo ($this->uri->segment(1) == 'progress' ? ' active':'')?>">
                        <a class="quick-phase-bg" href="/progress"><span class="sprite progress-graph"></span>Progress</a>
                    </li>
                </ul>  
            </div>
            <div class="body">
                <?php echo $content; ?>
            </div>
        </div>
        <footer>
            <div class="wrapper" id="sub_footer">
                <div class="inner-content">
                    <ul class="footer-nav row">
                        <li class="col-xs-3"><a href="#who" data-toggle="tab">Who is STCfit</a></li>
                        <li class="col-xs-3"><a href="#what" data-toggle="tab">What Does STCfit do</a></li>
                        <li class="col-xs-3"><a href="#use" data-toggle="tab">Who should use STCfit</a></li>
                        <li class="col-xs-3"><a href="#contact" data-toggle="tab">Contact us</a></li>
                    </ul>
                    <div class="tab-content row">
                        <div class="tab-pane col-xs-12" id="who">
                            <ul>
                                <li>An Australian based personal training company</li>
                                <li>The new standard of online personal training</li>
                                <li>A community of like-minded people working together</li>
                                <li>Real people who want to help you</li>
                            </ul>
                        </div>
                        <div class="tab-pane col-xs-12" id="what">
                            <ul>
                                <li>Design nutrition and training programs that work</li>
                                <li>Bust fitness industry sales gimmick myths</li>
                                <li>Provide you with everything you need to achieve your goal</li>
                                <li>Offer the best value for money</li>
                                <li>Change people's lives</li>
                            </ul>
                        </div>
                        <div class="tab-pane col-xs-12" id="use">
                            <ul>
                                <li>Anyone who's tried a low calorie diet</li>
                                <li>Anyone who wants achieve physical goals AND be healthy</li>
                                <li>Someone with a gym membership</li>
                                <li>People tired of being sold on magic solution that aren't working long term</li>
                            </ul>
                        </div>
                        <div class="tab-pane col-xs-12" id="contact">
                            <ul>
                                <li>STCfitness ABN: 98 411 770 051</li>
                                <li>71A Wyndham Street</li>
                                <li>Shepparton Vic. 3630</li>
                                <li><a class="quick-phase" href="mailto:<?php echo SUPPORT_EMAIL; ?>"><?php echo SUPPORT_EMAIL; ?></a></li>
                                <li class="social">Facebook: <a href="https://www.facebook.com/stcfit" target="_blank">facebook.com/stcfit</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="wrapper" id="footer">
                <div class="inner-content">
                    &copy; <?php echo date('Y'); ?> STC Fitness. All Rights Reserved.
                </div>
            </div>
        </footer>
        <div class="modal fade" id="exercise_popup" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="loading-animation">
                        <div class="bubblingG">
                            <span id="bubblingG_1"></span>
                            <span id="bubblingG_2"></span>
                            <span id="bubblingG_3"></span>
                        </div>
                    </div>
                    <div class="exercise-content">
                        <!-- This is where the content will be loaded into -->
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="<?=JS_PATH;?>plugins/head.min.js"></script>
        <script type="text/javascript">
            head.js(
                {jquery: '<?=JS_PATH;?>plugins/jquery-1.11.0.min.js'},
                {bootstrap: '<?=JS_PATH;?>plugins/bootstrap.min.js'},
                {validation: '<?=JS_PATH;?>plugins/jquery.validate.min.js'},
                <?php if(!empty($extra_js)) for($i=0;$i<count($extra_js);$i++) echo '{extra'.$i.': \''.$extra_js[$i].'\'},'; ?>
                {placeholders: '<?=JS_PATH; ?>plugins/jquery.infieldlabel.js'},
                {main: '<?=JS_PATH;?>main.js'}<?php if(!empty($include_js)): ?>,
                {extra: '<?=$include_js;?>'} <?php endif; ?>
            );
        </script>
    </body>
</html>
