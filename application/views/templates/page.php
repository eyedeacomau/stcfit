<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=1200" />
        <meta name="google-site-verification" content="VE6F09uqnRuMLqQd-2QdP0Sy_E5LygdIRKTksA3Zrqs" />
        <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
        <link rel="stylesheet" href="/assets/css/plugins/bootstrap.min.css" />
        <link rel="stylesheet" href="/assets/css/main.css" />
        <link rel="shortcut icon" type="image/x-icon" href="/assets/images/favicon.ico" />
        <?php if(!empty($include_css)) echo '<link rel="stylesheet" href="'.$include_css.'" />';?>
        <?php if(!empty($extra_css)) foreach($extra_css as $css) echo '<link rel="stylesheet" href="'.$css.'" />'; ?>
        <?php if(!empty($extra_head)) foreach($extra_head as $e_head) echo $e_head; ?>
        <!--[if lt IE 9]><script type="text/javascript" src="/assets/js/head.min.js"></script><![endif]-->
        <title><?=$title;?></title>
    </head>
    <body>
    <!-- Google Tag Manager -->
    <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-NXZGSF"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-NXZGSF');</script>
    <!-- End Google Tag Manager -->
        <div class="page-wrapper">
            <header>
                <div class="wrapper" id="header">
                    <div class="inner-content">
                        <div class="logo">
                            <a href="/" title="Home"><img src="/assets/images/logo.png" alt="Home" /></a>
                        </div>
                        <?php if(!empty($this->is_admin)): ?>
                            <a class="btn btn-primary login" href="/admin">Admin Dashboard</a>
                        <?php elseif(!empty($this->is_trainer)): ?>
                            <a class="btn btn-primary login" href="/trainers">Trainer Dashboard</a>
                        <?php elseif($this->session->userdata('profile_id') != false): ?>
                            <a class="btn btn-primary login" href="/registration">Continue Registration</a>
                        <?php elseif($this->tank_auth->is_logged_in()): ?>
                            <a class="btn btn-primary login" href="/dashboard">Dashboard</a>
                        <?php else: ?>
                            <button class="btn btn-primary login" data-toggle="modal" data-target="#login_modal">LOGIN/SIGNUP</button>
                        <?php endif; ?>
                        <ul class="nav navbar-nav navbar-right top-nav">
                            <li><a class="phase" href="/">Home</a></li>
                            <li><a class="phase" href="/how-it-works">How it works</a></li>
                            <li><a class="phase" href="/pricing">Pricing</a></li>
                            <li><a class="phase" href="/results">Results</a></li>
                            <li class="last"><a class="phase" href="/contact-us">Contact</a></li>
                        </ul>
                    </div>
                </div>
                <?php if(!empty($page_header)): ?>
                    <div class="wrapper page-name">
                        <div class="inner-content">
                            <h1><?php echo $page_header; ?></h1>
                        </div>
                    </div>
                <?php endif; ?>
            </header>
            <?php echo $content ."\n"; ?>
        </div>
        <footer>
            <div class="wrapper" id="sub_footer">
                <div class="inner-content">
                    <ul class="footer-nav row">
                        <li class="col-xs-3"><a href="#who" data-toggle="tab">Who is STCfit</a></li>
                        <li class="col-xs-3"><a href="#what" data-toggle="tab">What Does STCfit do</a></li>
                        <li class="col-xs-3"><a href="#use" data-toggle="tab">Who should use STCfit</a></li>
                        <li class="col-xs-3"><a href="#contact" data-toggle="tab">Contact us</a></li>
                    </ul>
                    <div class="tab-content row">
                        <div class="tab-pane col-xs-12" id="who">
                            <ul>
                                <li>An Australian based personal training company</li>
                                <li>The new standard of online personal training</li>
                                <li>A community of like-minded people working together</li>
                                <li>Real people who want to help you</li>
                            </ul>
                        </div>
                        <div class="tab-pane col-xs-12" id="what">
                            <ul>
                                <li>Design nutrition and training programs that work</li>
                                <li>Bust fitness industry sales gimmick myths</li>
                                <li>Provide you with everything you need to achieve your goal</li>
                                <li>Offer the best value for money</li>
                                <li>Change people's lives</li>
                            </ul>
                        </div>
                        <div class="tab-pane col-xs-12" id="use">
                            <ul>
                                <li>Anyone who's tried a low calorie diet</li>
                                <li>Anyone who wants achieve physical goals AND be healthy</li>
                                <li>Someone with a gym membership</li>
                                <li>People tired of being sold on magic solution that aren't working long term</li>
                            </ul>
                        </div>
                        <div class="tab-pane col-xs-12" id="contact">
                            <ul>
                                <li>STCfit ABN: 97 691 210 488</li>
                                <li>71A Wyndham Street</li>
                                <li>Shepparton Vic. 3630</li>
                                <li>Australia</li>
                                <li><a class="quick-phase" href="mailto:<?php echo SUPPORT_EMAIL; ?>"><?php echo SUPPORT_EMAIL; ?></a></li>
                                <li class="social">Facebook: <a href="https://www.facebook.com/stcfit" target="_blank">facebook.com/stcfit</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="wrapper" id="footer">
                <div class="inner-content">
                    &copy; <?php echo date('Y'); ?> STCfit. All Rights Reserved.
                </div>
            </div>
        </footer>
        <div class="modal fade" id="login_modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <span class="sprite sprite-32 sprite-close"><button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button></span>
                        <div class="row two-column">
                            <div class="col-xs-6 login">
                                <h2>Login</h2>
                            </div>
                            <div class="col-xs-6 signup">
                                <h2>Signup</h2>
                            </div>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="row forms">
                            <div class="col-xs-6">
                                <?php echo form_open("/login", array('role' => 'form', 'class' => 'validate', 'autocomplete' => 'off')); ?>
                                    <div class="form-group">
                                        <label class="placeholder" for="modal_login_email">Email Address</label>
                                        <input type="text" name="email" data-placement="top" id="modal_login_email" class="form-control" required data-rule-email="true" data-msg-email="Please enter a valid email address" />
                                    </div>
                                    <div class="form-group">
                                        <label class="placeholder" for="modal_login_password">Password</label>
                                        <input type="password" data-placement="left" name="password" id="modal_login_password" class="form-control" required data-rule-minlength="<?=$this->config->item('password_min_length', 'tank_auth');?>" maxlength="<?=$this->config->item('password_max_length', 'tank_auth');?>" />
                                    </div>
                                    <a href="/forgot-password" class="forgot-password">Forgot my password</a>
                                    <input type="submit" name="login" class="btn btn-primary" value="Login" required />
                                <?php echo form_close(); ?>
                            </div>
                            <div class="col-xs-6 signup">
                                <?php echo form_open("/registration", array('role' => 'form', 'class' => 'validate', 'autocomplete' => 'off')); ?>
									<input type="text" name="company" value="" style="position:absolute;top:0;left:0;z-index:-1;" />
                                    <div class="form-group">
                                        <label class="placeholder" for="modal_signup_name">Name</label>
                                        <input type="text" name="name" id="modal_signup_name" class="form-control" required />
                                    </div>
                                    <div class="form-group">
                                        <label class="placeholder" for="modal_signup_email">Email Address</label>
                                        <input type="text" data-placement="right" name="email" id="modal_signup_email" class="form-control" required data-rule-email="true" data-msg-email="Please enter a valid email address" />
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" value="1" data-placement="bottom" name="terms" id="terms" required /> I agree to the
                                        </label>
                                        <a href="/terms-and-conditions">terms and conditions</a>.
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" value="1" name="marketing" id="marketing"> I agree to receiving marketing.
                                        </label>
                                    </div>
                                    <input type="submit" id="register_popup" name="register" class="btn btn-primary" value="Signup" />
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="trial_modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <span class="sprite sprite-32 sprite-close"><button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button></span>
                        <div class="row">
                            <div class="col-xs-12">
                                <h2>7 Day Free Trial</h2>
                            </div>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="row forms">
                            <div class="col-xs-6">
                                <?php echo form_open("/trial", array('role' => 'form', 'class' => 'validate', 'autocomplete' => 'off')); ?>
                                    <div class="form-group">
                                        <label for="modal_trial_name">Name</label>
                                        <input type="text" name="name" id="modal_trial_name" class="form-control" required />
                                    </div>
                                        <div class="form-group">
                                            <label for="modal_trial_phone">Phone</label>
                                            <input type="tel" name="phone" id="modal_trial_phone" class="form-control" required />
                                        </div>
                                    <div class="form-group">
                                        <label for="modal_trial_email">Email Address</label>
                                        <input type="email" name="email" data-placement="top" id="modal_trial_email" class="form-control" required data-rule-email="true" data-msg-email="Please enter a valid email address" />
                                    </div>
                                    <div class="form-group">
                                        <label for="modal_trial_type">Trial Type</label>
                                        <select name="type" data-placement="top" id="modal_trial_type" class="form-control" required>
                                            <option>Fat Loss</option>
                                            <option>Performance</option>
                                            <option>Physique</option>
                                        </select>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" value="1" name="marketing" id="marketing"> I agree to receiving marketing.
                                        </label>
                                    </div>
                                    <input type="submit" name="trial" class="btn btn-primary" value="Sign Up" required />
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="<?=JS_PATH;?>plugins/head.min.js"></script>
        <script type="text/javascript">
            head.js(
                {jquery: '<?=JS_PATH;?>plugins/jquery-1.11.0.min.js'},
                {bootstrap: '<?=JS_PATH;?>plugins/bootstrap.min.js'},
                {validation: '<?=JS_PATH;?>plugins/jquery.validate.min.js'},
                <?php if(!empty($extra_js)) for($i=0;$i<count($extra_js);$i++) echo '{extra'.$i.': \''.$extra_js[$i].'\'},'; ?>
                {placeholders: '<?=JS_PATH; ?>plugins/jquery.infieldlabel.js'},
                {main: '<?=JS_PATH;?>main.js'}<?php if(!empty($include_js)): ?>,
                {extra: '<?=$include_js;?>'} <?php endif; ?>
            );
        </script>
    </body>
</html>
