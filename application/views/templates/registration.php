<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=1200" />
        <meta name="google-site-verification" content="VE6F09uqnRuMLqQd-2QdP0Sy_E5LygdIRKTksA3Zrqs" />
        <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />  
        <link rel="stylesheet" href="/assets/css/plugins/bootstrap.min.css" />
        <link rel="stylesheet" href="/assets/css/main.css" />
        <link rel="shortcut icon" type="image/x-icon" href="/assets/images/favicon.ico" />
        <?php if(!empty($include_css)) echo '<link rel="stylesheet" href="'.$include_css.'" />';?>
        <?php if(!empty($extra_css)) foreach($extra_css as $css) echo '<link rel="stylesheet" href="'.$css.'" />'; ?>
        <?php if(!empty($extra_head)) foreach($extra_head as $e_head) echo $e_head; ?>
        <!--[if lt IE 9]><script type="text/javascript" src="/assets/js/head.min.js"></script><![endif]-->
        <title><?=$title;?></title>
    </head>
    <body>
    <!-- Google Tag Manager -->
    <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-NXZGSF"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-NXZGSF');</script>
    <!-- End Google Tag Manager -->
        <div class="wrapper" id="header">
            <div class="inner-content">
                <div class="logo">
                    <img src="/assets/images/logo.png" alt="STC Fit" />
                </div>
            </div>
        </div>
    	<div class="page-wrapper gradient-background registration">
            <div class="wrapper" id="registration-message">
                <div class="inner-content">
                    <h1>Hi <?php echo ucfirst($template_data['first_name']); ?>, thanks for signing up!</h1>
                    <h2>We just need a few extra details and you will be ready to rock and roll.</h2>
                </div>
            </div>
			<?php echo $content; ?>
		</div>
	    <script type="text/javascript" src="<?=JS_PATH;?>plugins/head.min.js"></script>
        <script type="text/javascript">
            head.js(
                {jquery: '<?=JS_PATH;?>plugins/jquery-1.11.0.min.js'},
                {bootstrap: '<?=JS_PATH;?>plugins/bootstrap.min.js'},
                {validation: '<?=JS_PATH;?>plugins/jquery.validate.min.js'},
                <?php if(!empty($extra_js)) for($i=0;$i<count($extra_js);$i++) echo '{extra'.$i.': \''.$extra_js[$i].'\'},'; ?>
                {placeholders: '<?=JS_PATH; ?>plugins/jquery.infieldlabel.js'},
                {main: '<?=JS_PATH;?>main.js'}<?php if(!empty($include_js)): ?>,
                {extra: '<?=$include_js;?>'} <?php endif; ?>
            );
        </script>
    </body>
</html>
