<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title><?php echo SITE_NAME . ' :: ' . $template_data['email_title']; ?></title></head>
<body>
	<table border="0" cellpadding="50" cellspacing="0" style="width:100%; background-color:#eb4324; text-align:center; font-family:'Arial', 'Verdana', 'Helvetica', 'sans-serif' !important;">
		<tr align="center">
			<td width="600">
				<table border="0" cellpadding="0" cellspacing="0" width="600">
					<tr width="100%">
						<td height="107" style="background-color:#000000">
							<table border="0" cellpadding="0" cellspacing="0" width="600">
								<td width="150">
									<a href="<?php echo site_url(); ?>"><img style="border:none;" src="<?php echo site_url('/assets/images/email-logo.jpg'); ?>"</a>
								</td>
								<td width="400" align="right" style="text-align:right; padding-right:50px; color:#ffffff; font-size:18px;">
									<?php echo $template_data['email_title']; ?>
								</td>
							</table>
						</td>
					</tr>
					<tr width="100%">
						<td style="background-color:#ffffff;">
							<table border="0" cellpadding="25" cellspacing="0" width="600">
								<tr>
									<td width="100%">
										<?php echo $content; ?>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<?php if(!empty($text_view)): ?>
		<div style="margin-top:100px;">
			<?php echo nl2br($text_view); ?>
		</div>
	<?php endif; ?>
</body>