<div class="wrapper" id="home">
	<div class="inner-content banner row">
		<div class="col-xs-6 signup-form">
			<h1>A totally new way of getting fit and healthy in the gym</h1>
			<?php if($this->tank_auth->is_logged_in() == false && $this->session->userdata('profile_id') == false): ?>
				<?php echo form_open('/registration', array('id' => 'signup_form', 'role' => 'form', 'class' => 'validate', 'autocomplete' => 'off')); ?>
					<input type="text" name="company" value="" style="position:absolute;top:0;left:0;z-index:-1;" />
					<div class="form-inline">
						<div class="form-group">
							<label class="placeholder" for="name">Name</label>
							<input type="text" name="name" data-placement="top" id="name" class="form-control" required />
						</div>
						<div class="form-group">
							<label class="placeholder" for="email">Email Address</label>
							<input type="text" name="email" data-placement="top" id="email" class="form-control" required data-rule-email="true" data-msg-email="Please enter a valid email address" />
						</div>
						<div class="checkbox">
							<label>
								<input type="checkbox" value="1" data-placement="left" name="terms" id="terms" required /> I agree to the
							</label>
							<a href="/terms-and-conditions">terms and conditions</a>.
						</div>
						<div class="checkbox">
							<label>
								<input type="checkbox" value="1" name="marketing" id="marketing" /> I agree to receiving marketing.
							</label>
						</div>
						<input type="submit" id="register_home" class="opaque-button" value="Sign up now!" name="register" />
					</div>
				<?php echo form_close(); ?>
			<?php endif; ?>
		</div>
		<div class="col-xs-6 video-area">
			<iframe class="pull-right" width="480" height="360" src="//www.youtube.com/embed/2tlhse5VKG4?rel=0&amp;wmode=transparent" wmode="Opaque" frameborder="0" allowfullscreen></iframe>
		</div>
	</div>
</div>
<div class="wrapper" id="quotes">
	<div class="inner-content">
		<ul>
			<li><a class="btn btn-primary" href="/contact-us">Enquire Now</a></li>
		</ul>
	</div>
</div>
<div class="wrapper" id="home-page-title">
	<div class="inner-content">
		<h2>Why Choose Us</h2>
	</div>
</div>
<div class="wrapper stacks">
	<div class="inner-content row">
		<div class="col-xs-6 image">
			<h4 class="primary">We put the 'personal' back in online training</h4>
				<p>With so many online programs out there, it's hard to know which one to choose.<br />
				STCfit offer:</p>
				<ul>
					<li>- Online training and nutrition programs</li>
					<li>- Communication with your own qualified fitness professional</li>
					<li>- Mentoring to establish the right mindset to achieve your goal</li>
					<li>- Personalised nutrition and training adjustments from your trainer</li>
				</ul>
		</div>
		<div class="col-xs-6">
			<img src="/assets/images/homestack-1.jpg" />
		</div>
	</div>
</div>
<div class="wrapper stacks">
	<div class="inner-content row">
		<div class="col-xs-6 image">
			<img src="/assets/images/homestack-2.jpg" />
		</div>
		<div class="col-xs-6">
			<h4 class="primary">Achieve long term results</h4>
			<p>
				Are you tired of trying 12 week diets? Or 6 week weight loss plans which involve drinking 3 shakes a day? We know these approaches don’t work long term. In fact they often make things worse in the long run. The secret to STCfit is we teach you all the fundamentals which work with your lifestyle so you can enjoy long term success.
			</p>
		</div>
	</div>
</div>
<div class="wrapper stacks last">
	<div class="inner-content row">
		<div class="col-xs-6">
			<h4 class="primary">The three keys to being strong, fit and healthy are:</h4>
			<ol>
				<li>A real food approach</li>
				<li>Challenging workouts</li>
				<li>A great mindset</li>
			</ol>
			<p>We will teach you how conquer all three!</p>
		</div>
		<div class="col-xs-6 image">
			<img src="/assets/images/homestack-3.jpg" />
		</div>
	</div>
</div>
<div class="wrapper signup-button">
	<div class="inner-content">
			<?php if($this->is_admin == true): ?>
                <a class="btn btn-primary btn-lg login" href="/admin">Admin Dashboard</a>
            <?php elseif($this->is_trainer == true): ?>
                <a class="btn btn-primary btn-lg login" href="/trainers">Trainer Dashboard</a>
			<?php elseif($this->session->userdata('profile_id') != false): ?>
				<a href="/registration" class="btn btn-lg btn-primary login">Continue Registration</a>
			<?php elseif($this->tank_auth->is_logged_in()): ?>
				<a href="/dashboard" class="btn btn-lg btn-primary login">Go to Dashboard</a>
			<?php else: ?>
				<a class="btn btn-primary btn-lg" href="/contact-us">Enquire Now</a>
			<?php endif; ?>
	</div>
</div>
<?php if(!empty($errors) || !empty($messages) || !empty($success_messages)): ?>
<div class="modal fade <?php echo (!empty($errors) ? 'errors':(!empty($messages) ? 'messages':(!empty($success_messages) ? 'success':''))); ?>" id="popup_message">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<?php if(!empty($errors)): ?>
					<?php echo $errors; ?>
				<?php elseif(!empty($messages)): ?>
					<?php echo $messages; ?>
				<?php elseif(!empty($success_messages)): ?>
					<?php echo $success_messages; ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>
<div class="modal fade" id="trial_modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<span class="sprite sprite-32 sprite-close"><button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button></span>
				<div class="row">
					<div class="col-xs-12">
						<h2>7 Day Free Trial</h2>
					</div>
				</div>
			</div>
			<div class="modal-body">
				<div class="row forms">
					<div class="col-xs-6">
						<?php echo form_open("/trial", array('role' => 'form', 'class' => 'validate', 'autocomplete' => 'off')); ?>
							<div class="form-group">
								<label for="modal_trial_name">Name</label>
								<input type="text" name="name" id="modal_trial_name" class="form-control" required />
							</div>
							<div class="form-group">
								<label for="modal_trial_phone">Phone</label>
								<input type="tel" name="phone" id="modal_trial_phone" class="form-control" required />
							</div>
							<div class="form-group">
								<label for="modal_trial_email">Email Address</label>
								<input type="email" name="email" data-placement="top" id="modal_trial_email" class="form-control" required data-rule-email="true" data-msg-email="Please enter a valid email address" />
							</div>
							<div class="form-group">
								<label for="modal_trial_type">Trial Type</label>
								<select name="type" data-placement="top" id="modal_trial_type" class="form-control" required>
									<option>Fat Loss</option>
									<option>Performance</option>
									<option>Physique</option>
								</select>
							</div>
							<div class="checkbox">
								<label>
									<input type="checkbox" value="1" name="marketing" id="marketing"> I agree to receiving marketing.
								</label>
							</div>
							<input type="submit" name="trial" class="btn btn-primary" value="Sign Up" required />
						<?php echo form_close(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
